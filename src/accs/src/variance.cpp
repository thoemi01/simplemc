/**
 * @file    variance.cpp
 * @brief   Variance accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/accs/variance.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/eigen.h>
#endif

namespace simpleMC {

namespace accs {

/* Construct a var_acc with certain number of elements */
template <typename T>
var_acc<T>::var_acc(std::size_t size) :
    data_(),
    data2_(),
    count_(0),
    is_result_(false)
{
    assert(size > 0);
    detail::setConstant(data_, size, 0.0);
    detail::setConstant(data2_, size, 0.0);
}

/* Set data, data2, count and state (intended for serialization and MPI) */
template <typename T>
void var_acc<T>::set(const mean_type& data, const variance_type& data2,
                     count_type count, bool is_result) {
    assert(data.size() == data2.size());
    data_ = data;
    data2_ = data2;
    count_ = count;
    is_result_ = is_result;
}

/* Resize data, set everything to zero and change to accumulating state */
template <typename T>
void var_acc<T>::reset(std::size_t size) {
    assert(size > 0);
    detail::setConstant(data_, size, 0.0);
    detail::setConstant(data2_, size, 0.0);
    count_ = 0;
    is_result_ = false;
}

/* Stream operator to incorporate another accumulator */
template <typename T>
var_acc<T>& var_acc<T>::operator<<(const var_acc<T>& other) {
    assert(size() == other.size());
    assert(!is_result_ && !other.is_result_);
    data_ += other.data_;
    data2_ += other.data2_;
    count_ += other.count_;
    return *this;
}

/* Calculate and get mean */
template <typename T>
typename var_acc<T>::mean_type var_acc<T>::mean() const {
    if (is_result_) {
        return data_;
    }
    mean_type res = data_;
    detail::mean(res, count_);
    return res;
}

/* Calculate and get variance */
template <typename T>
typename var_acc<T>::variance_type var_acc<T>::variance() const {
    if (is_result_) {
        return data2_;
    }
    mean_type res_mean = data_;
    variance_type res_var = data2_;
    detail::variance(res_mean, res_var, count_);
    return res_var;
}

/* Convert to the result state */
template <typename T>
void var_acc<T>::convert_to_result() {
    if (!is_result_) {
        detail::variance(data_, data2_, count_);
        is_result_ = true;
    }
}

/* Convert to the accumulating state */
template <typename T>
void var_acc<T>::convert_to_acc() {
    if (is_result_) {
        detail::invvariance(data_, data2_, count_);
        is_result_ = false;
    }
}

template class var_acc<double>;
template class var_acc<std::complex<double>>;

/* Compare two variance accumulators for equality */
template <typename T>
bool operator==(const var_acc<T>& lhs, const var_acc<T>& rhs) {
    if (lhs.size() != rhs.size() || lhs.is_result() != rhs.is_result()) {
        return false;
    }
    return (lhs.data() == rhs.data() &&
            lhs.data2() == rhs.data2() &&
            lhs.count() == rhs.count());
}

/* Compare two variance accumulators for inequality */
template <typename T>
bool operator!=(const var_acc<T>& lhs, const var_acc<T>& rhs) {
    return !(lhs == rhs);
}

template bool operator==(const var_acc<double>&,
const var_acc<double>&);
template bool operator!=(const var_acc<double>&,
const var_acc<double>&);
template bool operator==(const var_acc<std::complex<double>>&,
const var_acc<std::complex<double>>&);
template bool operator!=(const var_acc<std::complex<double>>&,
const var_acc<std::complex<double>>&);

/* Write variance accumulator to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const var_acc<T>& acc) {
    auto tmp = acc;
    tmp.convert_to_result();
    auto mean = tmp.mean();
    auto err = tmp.stderror_of_mean();
    os << "N = " << acc.count() << "\n"
       << "<X> = [";
    if (acc.size() == 0) {
        os << "]\n";
        return os;
    }
    os << mean[0] << " +/- " << err[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << mean[i] << " +/- " << err[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const var_acc<double>&);
template std::ostream& operator<<(std::ostream&,
const var_acc<std::complex<double>>&);

/* Write variance accumulator to file (for plotting purposes) */
template <typename T>
void acc_to_file(const var_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode, bool header) {
    using value_type = typename var_acc<T>::value_type;
    std::ofstream os(filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20(os, 20);
    simpleMC::utils::fixedwidth_ostream osw10(os, 10);
    os.precision(10);
    os << std::left;

    // write file header
    if (header) {
        os << "#Number of samples = " << acc.count() << "\n";
        osw10 << "#i";
        if constexpr (std::is_same<value_type, double>::value) {
            osw20 << "<X>" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        } else {
            osw20 << "<X> real" << "<X> imag" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        }
    }

    // write mean values to file
    auto tmp = acc;
    tmp.convert_to_result();
    auto mean = tmp.mean();
    auto varm = tmp.variance_of_mean();
    auto errm = tmp.stderror_of_mean();
    auto var = tmp.variance();
    auto err = tmp.stderror();
    for (std::size_t i = 0; i < acc.size(); i++) {
        if constexpr (std::is_same<value_type, double>::value) {
            osw10 << i;
            osw20 << mean[i] << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        } else {
            osw10 << i;
            osw20 << mean[i].real() << mean[i].imag() << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        }
    }
}

template void acc_to_file(const var_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
template void acc_to_file(const var_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/* Serialize variance accumulator to Json */
template <typename T>
void to_json(nlohmann::json& j, const var_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    j["size"] = tmp.size();
    j["data"] = tmp.data();
    j["data2"] = tmp.data2();
    j["count"] = tmp.count();
}

template void to_json(nlohmann::json&, const var_acc<double>&);
template void to_json(nlohmann::json&,
const var_acc<std::complex<double>>&);

/* Deserialize variance accumulator from Json */
template <typename T>
void from_json(const nlohmann::json& j, var_acc<T>& acc) {
    using traits = typename var_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using count_type = typename traits::count_type;
    std::size_t size;
    j.at("size").get_to(size);
    mean_type tmp_data;
    variance_type tmp_data2;
    detail::setConstant(tmp_data, size, 0.0);
    detail::setConstant(tmp_data2, size, 0.0);
    j.at("data").get_to(tmp_data);
    j.at("data2").get_to(tmp_data2);
    count_type tmp_count;
    j.at("count").get_to(tmp_count);
    acc.set(tmp_data, tmp_data2, tmp_count, /*is_result=*/ false);
}

template void from_json(const nlohmann::json&, var_acc<double>&);
template void from_json(const nlohmann::json&,
var_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/* Serialize variance accumulator to HDF5 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO& hio, const var_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    hio["size"] << tmp.size();
    hio["data"] << tmp.data();
    hio["data2"] << tmp.data2();
    hio["count"] << tmp.count();
}

template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const var_acc<double>&);
template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const var_acc<std::complex<double>>&);

/* Deserialize variance accumulator from HDF5 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO& hio, var_acc<T>& acc) {
    using traits = typename var_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using count_type = typename traits::count_type;
    std::size_t size;
    hio["size"] >> size;
    mean_type tmp_data;
    variance_type tmp_data2;
    detail::setConstant(tmp_data, size, 0.0);
    detail::setConstant(tmp_data2, size, 0.0);
    hio["data"] >> tmp_data;
    hio["data2"] >> tmp_data2;
    count_type tmp_count;
    hio["count"] >> tmp_count;
    acc.set(tmp_data, tmp_data2, tmp_count, /*is_result=*/ false);
}

template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
var_acc<double>&);
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
var_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/* MPI reduce/Allreduce for variance accumulators */
template <typename T>
void reduce(const var_acc<T>& acc, var_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all, int root) {
    using traits = typename var_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using count_type = typename traits::count_type;

    // necessary to make sure that acc is in accumulating state
    auto tmp_acc = acc;
    tmp_acc.convert_to_acc();
    mean_type tmp_data;
    variance_type tmp_data2;
    detail::setConstant(tmp_data, tmp_acc.size(), 0.0);
    detail::setConstant(tmp_data2, tmp_acc.size(), 0.0);
    count_type tmp_count = 0;
    simpleMC::mpi::reduce(tmp_acc.data(), tmp_data, MPI_SUM, comm, all,
                             root);
    simpleMC::mpi::reduce(tmp_acc.data2(), tmp_data2, MPI_SUM, comm, all,
                             root);
    simpleMC::mpi::reduce(tmp_acc.count(), tmp_count, MPI_SUM, comm, all,
                             root);
    if (comm.rank() == root || all) {
        res.set(tmp_data, tmp_data2, tmp_count, /*is_result=*/ false);
    }
}

template void reduce(const var_acc<double>&, var_acc<double>&,
simpleMC::mpi::communicator, bool, int);
template void reduce(const var_acc<std::complex<double>>&,
var_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
