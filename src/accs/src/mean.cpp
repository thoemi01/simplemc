/**
 * @file    mean.cpp
 * @brief   Mean accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/accs/mean.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/eigen.h>
#endif

namespace simpleMC {

namespace accs {

/* Construct a mean_acc with certain number of elements */
template <typename T>
mean_acc<T>::mean_acc(std::size_t size) :
    data_(),
    count_(0),
    is_result_(false)
{
    assert(size > 0);
    detail::setConstant(data_, size, 0.0);
}

 /* Set data, count and state (intended for serialization and MPI) */
template <typename T>
void mean_acc<T>::set(const mean_type& data, count_type count,
                      bool is_result) {
    data_ = data;
    count_ = count;
    is_result_ = is_result;
}

/* Resize data, set everything to zero and change to accumulating state */
template <typename T>
void mean_acc<T>::reset(std::size_t size) {
    assert(size > 0);
    detail::setConstant(data_, size, 0.0);
    count_ = 0;
    is_result_ = false;
}

/* Stream operator to incorporate another accumulator */
template <typename T>
mean_acc<T>& mean_acc<T>::operator<<(const mean_acc<T>& other) {
    assert(size() == other.size());
    assert(!is_result_ && !other.is_result_);
    data_ += other.data_;
    count_ += other.count_;
    return *this;
}

/* Calculate and get mean */
template <typename T>
typename mean_acc<T>::mean_type mean_acc<T>::mean() const {
    if (is_result_) {
        return data_;
    }
    mean_type res = data_;
    detail::mean(res, count_);
    return res;
}

/* Convert to the result state */
template <typename T>
void mean_acc<T>::convert_to_result() {
    if (!is_result_) {
        detail::mean(data_, count_);
        is_result_ = true;
    }
}

/* Convert to the accumulating state */
template <typename T>
void mean_acc<T>::convert_to_acc() {
    if (is_result_) {
        detail::invmean(data_, count_);
        is_result_ = false;
    }
}

template class mean_acc<double>;
template class mean_acc<std::complex<double>>;

/* Compare two mean accumulators for equality */
template <typename T>
bool operator==(const mean_acc<T>& lhs, const mean_acc<T>& rhs) {
    if (lhs.size() != rhs.size() || lhs.is_result() != rhs.is_result()) {
        return false;
    }
    return (lhs.data() == rhs.data() &&
            lhs.count() == rhs.count());
}

/* Compare two mean accumulators for inequality */
template <typename T>
bool operator!=(const mean_acc<T>& lhs, const mean_acc<T>& rhs) {
    return !(lhs == rhs);
}

template bool operator==(const mean_acc<double>&,
const mean_acc<double>&);
template bool operator!=(const mean_acc<double>&,
const mean_acc<double>&);
template bool operator==(const mean_acc<std::complex<double>>&,
const mean_acc<std::complex<double>>&);
template bool operator!=(const mean_acc<std::complex<double>>&,
const mean_acc<std::complex<double>>&);

/* Write mean accumulator to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const mean_acc<T>& acc) {
    auto mean = acc.mean();
    os << "N = " << acc.count() << "\n"
       << "<X> = [";
    if (acc.size() == 0) {
        os << "]\n";
        return os;
    }
    os << mean[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << mean[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const mean_acc<double>&);
template std::ostream& operator<<(std::ostream&,
const mean_acc<std::complex<double>>&);

/* Write mean accumulator to file (for plotting purposes) */
template <typename T>
void acc_to_file(const mean_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode, bool header) {
    using value_type = typename mean_acc<T>::value_type;
    std::ofstream os(filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20(os, 20);
    simpleMC::utils::fixedwidth_ostream osw10(os, 10);
    os.precision(10);
    os << std::left;

    // write file header
    if (header) {
        os << "#Number of samples = " << acc.count() << "\n";
        osw10 << "#i";
        if constexpr (std::is_same<value_type, double>::value) {
            osw20 << "<X>" << osw20.endl();
        } else {
            osw20 << "<X> real" << "<X> imag" << osw20.endl();
        }
    }

    // write mean values to file
    auto mean = acc.mean();
    for (std::size_t i = 0; i < acc.size(); i++) {
        if constexpr (std::is_same<value_type, double>::value) {
            osw10 << i;
            osw20 << mean[i] << osw20.endl();
        } else {
            osw10 << i;
            osw20 << mean[i].real() << mean[i].imag() << osw20.endl();
        }
    }
}

template void acc_to_file(const mean_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
template void acc_to_file(const mean_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/* Serialize mean accumulator to Json */
template <typename T>
void to_json(nlohmann::json& j, const mean_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    j["size"] = tmp.size();
    j["data"] = tmp.data();
    j["count"] = tmp.count();
}

template void to_json(nlohmann::json&, const mean_acc<double>&);
template void to_json(nlohmann::json&,
const mean_acc<std::complex<double>>&);

/* Deserialize mean accumulator from Json */
template <typename T>
void from_json(const nlohmann::json& j, mean_acc<T>& acc) {
    using traits = typename mean_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using count_type = typename traits::count_type;
    std::size_t size;
    j.at("size").get_to(size);
    mean_type tmp_data;
    detail::setConstant(tmp_data, size, 0.0);
    j.at("data").get_to(tmp_data);
    count_type tmp_count;
    j.at("count").get_to(tmp_count);
    acc.set(tmp_data, tmp_count, /*is_result=*/ false);
}

template void from_json(const nlohmann::json&, mean_acc<double>&);
template void from_json(const nlohmann::json&,
mean_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/* Serialize mean accumulator to HDF5 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO& hio, const mean_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    hio["size"] << tmp.size();
    hio["data"] << tmp.data();
    hio["count"] << tmp.count();
}

template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const mean_acc<double>&);
template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const mean_acc<std::complex<double>>&);

/* Deserialize mean accumulator from HDF5 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO& hio, mean_acc<T>& acc) {
    using traits = typename mean_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using count_type = typename traits::count_type;
    std::size_t size;
    hio["size"] >> size;
    mean_type tmp_data;
    detail::setConstant(tmp_data, size, 0.0);
    hio["data"] >> tmp_data;
    count_type tmp_count;
    hio["count"] >> tmp_count;
    acc.set(tmp_data, tmp_count, /*is_result=*/ false);
}

template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
mean_acc<double>&);
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
mean_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/* MPI reduce/Allreduce for mean accumulators */
template <typename T>
void reduce(const mean_acc<T>& acc, mean_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all, int root) {
    using traits = typename mean_acc<T>::traits;
    using mean_type = typename traits::mean_type;
    using count_type = typename traits::count_type;
    using simpleMC::mpi::reduce;

    // necessary to make sure that acc is in accumulating state
    auto tmp_acc = acc;
    tmp_acc.convert_to_acc();
    mean_type tmp_data;
    detail::setConstant(tmp_data, tmp_acc.size(), 0.0);
    count_type tmp_count = 0;
    reduce(tmp_acc.data(), tmp_data, MPI_SUM, comm, all, root);
    reduce(tmp_acc.count(), tmp_count, MPI_SUM, comm, all, root);
    if (comm.rank() == root || all) {
        res.set(tmp_data, tmp_count, /*is_result=*/ false);
    }
}

template void reduce(const mean_acc<double>&, mean_acc<double>&,
simpleMC::mpi::communicator, bool, int);
template void reduce(const mean_acc<std::complex<double>>&,
mean_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
