/**
 * @file    batch.cpp
 * @brief   Batch accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/accs/batch.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/eigen.h>
#endif

namespace simpleMC {

namespace accs {

/* Construct a batch_acc with certain number of elements, batches and block
 * size */
template <typename T>
batch_acc<T>::batch_acc(std::size_t size, std::size_t num_batches,
                        std::size_t bl_size) :
    data_(num_batches, mean_acc<T>{size}),
    block_size_(bl_size),
    count_(0),
    curr_count_(0),
    curr_(0)
{
    assert(size > 0);
    assert(num_batches > 0);
    assert(bl_size > 0);
}

/* Set status of accumulator (intended for serialization and MPI) */
template <typename T>
void batch_acc<T>::set(const std::vector<mean_acc<T>>& data,
                       std::size_t bl_size, count_type count,
                       count_type curr_count, std::size_t curr) {
    assert(data.size() > 0);
    assert(data[0].size() > 0);
    assert(bl_size > 0);
    assert(curr < data.size());
    data_ = data;
    block_size_ = bl_size;
    count_ = count;
    curr_count_ = curr_count;
    curr_ = curr;
}

/* Resize data, set everything to zero and change to accumulating state */
template <typename T>
void batch_acc<T>::reset(std::size_t size, std::size_t num_batches,
                         std::size_t bl_size) {
    assert(size > 0);
    assert(num_batches > 0);
    assert(bl_size > 0);
    data_ = std::vector<mean_acc<T>>(num_batches, mean_acc<T>{size});
    block_size_ = bl_size;
    count_ = 0;
    curr_count_ = 0;
    curr_ = 0;
}

/* Stream operator to incorporate another accumulator */
template <typename T>
batch_acc<T>& batch_acc<T>::operator<<(const batch_acc<T>& other) {
    assert(size() == other.size());
    assert(num_batches() == other.num_batches());
    for (std::size_t i = 0; i < num_batches(); i++) {
        data_[i] << other.data_[i];
    }
    count_ += other.count_;
    return *this;
}

/* Calculate and get mean */
template <typename T>
typename batch_acc<T>::mean_type batch_acc<T>::mean() const {
    mean_acc<T> res(size());
    for (std::size_t i = 0; i < num_batches(); i++) {
        res << data_[i];
    }
    return res.mean();
}

/* Accumulate data in var_acc */
template <typename T>
var_acc<T> batch_acc<T>::make_var() const {
    var_acc<T> res(size());
    for (std::size_t i = 0; i < num_batches(); i++) {
        auto mean = data_[i].mean();
        res.accumulate(simpleMC::utils::make_view(mean), /*view_count=*/1);
    }
    return res;
}

template class batch_acc<double>;
template class batch_acc<std::complex<double>>;

/* Compare two batch accumulators for equality */
template <typename T>
bool operator==(const batch_acc<T>& lhs, const batch_acc<T>& rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    return (lhs.data() == rhs.data() &&
            lhs.block_size() == rhs.block_size() &&
            lhs.total_count() == rhs.total_count() &&
            lhs.current_count() == rhs.current_count() &&
            lhs.current_batch() == rhs.current_batch());
}

/* Compare two batch accumulators for inequality */
template <typename T>
bool operator!=(const batch_acc<T>& lhs, const batch_acc<T>& rhs) {
    return !(lhs == rhs);
}

template bool operator==(const batch_acc<double>&,
const batch_acc<double>&);
template bool operator!=(const batch_acc<double>&,
const batch_acc<double>&);
template bool operator==(const batch_acc<std::complex<double>>&,
const batch_acc<std::complex<double>>&);
template bool operator!=(const batch_acc<std::complex<double>>&,
const batch_acc<std::complex<double>>&);

/* Write batch accumulator to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const batch_acc<T>& acc) {
    auto mean = acc.mean();
    auto err = acc.stderror_of_mean();
    os << "N = " << acc.total_count() << "\n"
       << "<X> = [";
    if (acc.size() == 0) {
        os << "]\n";
        return os;
    }
    os << mean[0] << " +/- " << err[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << mean[i] << " +/- " << err[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const batch_acc<double>&);
template std::ostream& operator<<(std::ostream&,
const batch_acc<std::complex<double>>&);

/* Write batch accumulator to file (for plotting purposes) */
template <typename T>
void acc_to_file(const batch_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode, bool header) {
    using value_type = typename batch_acc<T>::value_type;
    std::ofstream os(filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20(os, 20);
    simpleMC::utils::fixedwidth_ostream osw10(os, 10);
    os.precision(10);
    os << std::left;

    // write file header
    if (header) {
        os << "#Number of batches = " << acc.num_batches() << "\n"
           << "#Avg. samples in each batch = " << acc.batch_count() << "\n"
           << "#Total number of samples = " << acc.total_count() << "\n"
           << "\n";
        osw10 << "#i";
        if constexpr (std::is_same<value_type, double>::value) {
            osw20 << "<X>" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        } else {
            osw20 << "<X> real" << "<X> imag" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        }
    }

    // write mean values to file
    auto mean = acc.mean();
    auto varm = acc.variance_of_mean();
    auto errm = acc.stderror_of_mean();
    auto var = acc.variance();
    auto err = acc.stderror();
    for (std::size_t i = 0; i < acc.size(); i++) {
        if constexpr (std::is_same<value_type, double>::value) {
            osw10 << i;
            osw20 << mean[i] << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        } else {
            osw10 << i;
            osw20 << mean[i].real() << mean[i].imag() << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        }
    }
}

template void acc_to_file(const batch_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
template void acc_to_file(const batch_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/* Serialize batch accumulator to Json */
template <typename T>
void to_json(nlohmann::json& j, const batch_acc<T>& acc) {
    j["data"] = acc.data();
    j["bl_size"] = acc.block_size();
    j["count"] = acc.total_count();
    j["curr_count"] = acc.current_count();
    j["curr"] = acc.current_batch();
}

template void to_json(nlohmann::json&, const batch_acc<double>&);
template void to_json(nlohmann::json&,
const batch_acc<std::complex<double>>&);

/* Deserialize batch accumulator from Json */
template <typename T>
void from_json(const nlohmann::json& j, batch_acc<T>& acc) {
    using count_type = typename batch_acc<T>::count_type;
    count_type tmp_count, tmp_curr_count;
    std::size_t tmp_bl_size, tmp_curr;
    std::vector<mean_acc<T>> tmp_data;
    j.at("data").get_to(tmp_data);
    j.at("bl_size").get_to(tmp_bl_size);
    j.at("count").get_to(tmp_count);
    j.at("curr_count").get_to(tmp_curr_count);
    j.at("curr").get_to(tmp_curr);
    acc.set(tmp_data, tmp_bl_size, tmp_count, tmp_curr_count, tmp_curr);
}

template void from_json(const nlohmann::json&, batch_acc<double>&);
template void from_json(const nlohmann::json&,
batch_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/* Serialize batch accumulator to HDF5 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO& hio, const batch_acc<T>& acc) {
    hio["bl_size"] << acc.block_size();
    hio["count"] << acc.total_count();
    hio["curr_count"] << acc.current_count();
    hio["curr"] << acc.current_batch();
    hio["num_batches"] << acc.num_batches();
    for (std::size_t i = 0; i < acc.num_batches(); i++) {
        std::string path_data = "data/" + std::to_string(i);
        hio[path_data] << acc.data()[i];
    }
}

template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const batch_acc<double>&);
template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const batch_acc<std::complex<double>>&);

/* Deserialize batch accumulator from HDF5 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO& hio, batch_acc<T>& acc) {
    using count_type = typename batch_acc<T>::count_type;
    count_type tmp_count, tmp_curr_count;
    std::size_t tmp_bl_size, tmp_curr;
    std::size_t tmp_nb;
    hio["bl_size"] >> tmp_bl_size;
    hio["count"] >> tmp_count;
    hio["curr_count"] >> tmp_curr_count;
    hio["curr"] >> tmp_curr;
    hio["num_batches"] >> tmp_nb;
    std::vector<mean_acc<T>> tmp_data(tmp_nb);
    for (std::size_t i = 0; i < tmp_nb; i++) {
        std::string path_data = "data/" + std::to_string(i);
        hio[path_data] >> tmp_data[i];
    }
    acc.set(tmp_data, tmp_bl_size, tmp_count, tmp_curr_count, tmp_curr);
}

template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
batch_acc<double>&);
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
batch_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/* MPI reduce/Allreduce for batch accumulators */
template <typename T>
void reduce(const batch_acc<T>& acc, batch_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all, int root) {
    using count_type = typename batch_acc<T>::count_type;

    // reduce data
    std::vector<mean_acc<T>> tmp_data(acc.num_batches());
    for (std::size_t i = 0; i < acc.num_batches(); i++) {
        reduce(acc.data()[i], tmp_data[i], comm, all, root);
    }

    // reduce also the count
    count_type tmp_count = 0;
    simpleMC::mpi::reduce(acc.total_count(), tmp_count, MPI_SUM, comm, all,
                          root);

    // put it all together
    if (comm.rank() == root || all) {
        res.reset(acc.size(), acc.num_batches(), acc.block_size());
        res.set(tmp_data, res.block_size(), tmp_count, 0, 0);
    }
}

template void reduce(const batch_acc<double>&,
batch_acc<double>&, simpleMC::mpi::communicator, bool, int);
template void reduce(const batch_acc<std::complex<double>>&,
batch_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
