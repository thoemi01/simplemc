/**
 * @file    autocorr_acc.cpp
 * @brief   Autocorrelation accumulator largely based on the ALPSCore::alea
 *          library.
 * @author  Thomas Hahn
 * @todo    -) Fix merge of two accumulators
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/accs/autocorr.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/eigen.h>
#endif

namespace simpleMC {

namespace accs {

namespace detail {

/* Checks the validity of levels of an autocorrelation accumulator */
template <typename T>
void check_levels(std::vector<blockvar_acc<T>> levels,
                  typename autocorr_acc<T>::count_type granularity,
                  typename autocorr_acc<T>::count_type count,
                  typename autocorr_acc<T>::count_type current) {
    // There has to be at least a single level
    if (levels.empty()) {
        throw std::logic_error("Levels in autocorrelation accumulator are "
                               "empty.");
    }
    // Count has to be equal to total count in level 0 + count in current block
    if (count < levels[0].total_count() + levels[0].block_data().count()) {
        throw std::logic_error("Count in autocorrelation accumulator is not "
                               "correct.");
    }
    // Check each level
    auto size = levels[0].size();
    auto current_size = levels[0].block_size();
    for (const auto& level : levels) {
        if (level.size() != size || level.block_size() != current_size) {
            throw std::logic_error("Levels in autocorrelation accumulator are "
                                   "not valid.");
        }
        current_size *= granularity;
    }
    // Check size of next level
    if (current != current_size/granularity) {
        throw std::logic_error("Next level in autocorrelation accumulator "
                               "not correct.");
    }
}

template void check_levels(std::vector<blockvar_acc<double>>,
typename autocorr_acc<double>::count_type,
typename autocorr_acc<double>::count_type,
typename autocorr_acc<double>::count_type);
template void check_levels(std::vector<blockvar_acc<std::complex<double>>>,
typename autocorr_acc<std::complex<double>>::count_type,
typename autocorr_acc<std::complex<double>>::count_type,
typename autocorr_acc<std::complex<double>>::count_type);

} // namespace detail

/* Construct an autocorr_acc */
template <typename T>
autocorr_acc<T>::autocorr_acc(std::size_t size, count_type bl_size,
                              count_type granularity) :
    levels_(),
    granularity_(granularity),
    count_(0),
    current_(bl_size)
{
    assert(granularity_ > 0);

    // initialize the first level
    levels_.push_back(blockvar_type(size, bl_size));
}

/* Set levels, granularity and count (intended for serialization and MPI) */
template <typename T>
void autocorr_acc<T>::set(const std::vector<blockvar_type>& levels,
                          count_type granularity, count_type count,
                          count_type current) {
    assert(granularity_ > 0);
    detail::check_levels(levels, granularity, count, current);
    levels_ = levels;
    granularity_ = granularity;
    count_ = count;
    current_ = current;
}

/* Resize data, set everything to zero and change to accumulating state */
template <typename T>
void autocorr_acc<T>::reset(std::size_t size, count_type bl_size,
                            count_type granularity) {
    assert(granularity_ > 0);
    granularity_ = granularity;
    count_ = 0;
    current_ = bl_size;
    levels_.clear();
    levels_.push_back(blockvar_type(size, bl_size));
}

/* Stream operator to incorporate another accumulator */
template <typename T>
autocorr_acc<T>& autocorr_acc<T>::operator<<(const autocorr_acc<T>& other) {
    assert(size() == other.size());
    assert(block_size() == other.block_size());
    assert(granularity() == other.granularity());

    // make sure enough levels are available
    resize_levels(other.num_levels());

    // merge the levels together
    for (std::size_t i = 0; i < other.num_levels(); i++) {
        levels_[i] << other.levels_[i];
    }
    count_ += other.count();
    return *this;
}

/* Calculate and get mean */
template <typename T>
typename autocorr_acc<T>::mean_type autocorr_acc<T>::mean() const {
    return levels_[0].mean();
}

/* Calculate and get variance */
template <typename T>
typename autocorr_acc<T>::variance_type
autocorr_acc<T>::variance(count_type min) const {
    auto lvl = find_level(min);
    return levels_[lvl].variance();
}

/* Calculate and get variance of mean */
template <typename T>
typename autocorr_acc<T>::variance_type
autocorr_acc<T>::variance_of_mean(count_type min) const {
    auto lvl = find_level(min);
    return levels_[lvl].variance_of_mean();
}

/* Calculate and get std. error */
template <typename T>
typename autocorr_acc<T>::variance_type
autocorr_acc<T>::stderror(count_type min) const {
    auto lvl = find_level(min);
    return levels_[lvl].stderror();
}

/* Calculate and get std. error of mean */
template <typename T>
typename autocorr_acc<T>::variance_type
autocorr_acc<T>::stderror_of_mean(count_type min) const {
    auto lvl = find_level(min);
    return levels_[lvl].stderror_of_mean();
}

/* Calculate and get autocorrelation time */
template <typename T>
typename autocorr_acc<T>::tau_type autocorr_acc<T>::tau(count_type min) const {
    auto lvl = find_level(min);
    const auto& varn = levels_[lvl].variance();
    const auto& var0 = levels_[0].variance();
    return detail::tau(varn, var0);
}

/* Convert to the result state */
template <typename T>
void autocorr_acc<T>::convert_to_result() {
    // ignores all currently in blocks
    if (!is_result()) {
        for (std::size_t i = 0; i < num_levels(); i++) {
            levels_[i].convert_to_result();
        }
    }
}

/* Convert to the accumulating state */
template <typename T>
void autocorr_acc<T>::convert_to_acc() {
    if (is_result()) {
        for (std::size_t i = 0; i < num_levels(); i++) {
            levels_[i].convert_to_acc();
        }
    }
}

/* Find the highest level with at least min samples */
template <typename T>
std::size_t autocorr_acc<T>::find_level(count_type min) const {
    for (std::size_t i = num_levels(); i != 0; i--) {
        if (levels_[i - 1].count() >= min) {
            return i - 1;
        }
    }
    return 0;
}

/* Resize levels */
template <typename T>
void autocorr_acc<T>::resize_levels(std::size_t lvl) {
    for(std::size_t i = num_levels(); i < lvl; i++) {
        add_level();
    }
}

template class autocorr_acc<double>;
template class autocorr_acc<std::complex<double>>;

/* Compare two autocorrelation accumulators for equality */
template <typename T>
bool operator==(const autocorr_acc<T>& lhs, const autocorr_acc<T>& rhs) {
    if (lhs.num_levels() != rhs.num_levels()) {
        return false;
    }
    for (std::size_t i = 0; i < lhs.num_levels(); i++) {
        if (lhs.level(i) != rhs.level(i)) {
            return false;
        }
    }
    return true;
}

/* Compare two autocorrelation accumulators for inequality */
template <typename T>
bool operator!=(const autocorr_acc<T>& lhs, const autocorr_acc<T>& rhs) {
    return !(lhs == rhs);
}

template bool operator==(const autocorr_acc<double>&,
const autocorr_acc<double>&);
template bool operator!=(const autocorr_acc<double>&,
const autocorr_acc<double>&);
template bool operator==(const autocorr_acc<std::complex<double>>&,
const autocorr_acc<std::complex<double>>&);
template bool operator!=(const autocorr_acc<std::complex<double>>&,
const autocorr_acc<std::complex<double>>&);

/* Write autocorrelation accumulator to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const autocorr_acc<T>& acc) {
    auto tmp = acc;
    tmp.convert_to_result();
    auto mean = tmp.mean();
    auto err = tmp.stderror_of_mean();
    auto tau = tmp.tau();
    os << "N = " << acc.count() << "\n"
       << "<X> = [";
    if (acc.size() == 0) {
        os << "]\n"
           << "tau(X) = []\n";
        return os;
    }
    os << mean[0] << " +/- " << err[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << mean[i] << " +/- " << err[i];
    }
    os << "]\n";
    os << "tau(X) = ["
       << tau[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << tau[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const autocorr_acc<double>&);
template std::ostream& operator<<(std::ostream&,
const autocorr_acc<std::complex<double>>&);

/* Write autocorrelation accumulator to file (for plotting purposes) */
template <typename T>
void acc_to_file(const autocorr_acc<T>& acc,
                 const std::string& filename, const std::string& taufile,
                 std::uint64_t minsam, std::ios_base::openmode mode,
                 bool header) {
    std::string tau_filename = taufile;
    if (tau_filename.empty()) {
        tau_filename = filename + std::string(".tau");
    }
    std::ofstream os(filename, mode);
    std::ofstream os_tau(tau_filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20_tau(os_tau, 20);
    simpleMC::utils::fixedwidth_ostream osw10_tau(os_tau, 10);
    os_tau.precision(10);
    os_tau << std::left;

    // get results
    auto tmp_acc = acc;
    tmp_acc.convert_to_result();
    auto lvl = tmp_acc.find_level(minsam);

    // write header
    if (header) {
        os << "#Number of levels = " << acc.num_levels() << "\n"
           << "#Total number of samples = " << acc.count() << "\n"
           << "#Highest level with " << minsam << " samples = " << lvl << "\n";
        os_tau << "#Number of levels = " << acc.num_levels() << "\n"
               << "#Total number of samples = " << acc.count() << "\n"
               << "#Highest level with " << minsam << " samples = " << lvl
               << "\n";
        osw10_tau << "#Level";
        osw20_tau << "Block size";
        for (std::size_t i = 0; i < acc.size(); i++) {
            osw20_tau << i;
        }
        os_tau << "\n";
    }

    // write mean and variance
    os.close();
    acc_to_file(tmp_acc.level(lvl), filename, std::ios_base::app, true);

    // write tau file
    for (std::size_t i = 0; i < acc.num_levels(); i++) {
        osw10_tau << i;
        osw20_tau << tmp_acc.level(i).block_size();
        for (std::size_t j = 0; j < acc.size(); j++) {
            osw20_tau << tmp_acc.level(i).data2()[j];
        }
        os_tau << "\n";
    }
}

template void acc_to_file(const autocorr_acc<double>&,
const std::string&, const std::string&, std::uint64_t,
std::ios_base::openmode, bool);
template void acc_to_file(const autocorr_acc<std::complex<double>>&,
const std::string&, const std::string&, std::uint64_t,
std::ios_base::openmode, bool);

/* Serialize autocorr accumulator to Json */
template <typename T>
void to_json(nlohmann::json& j, const autocorr_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    j["granularity"] = tmp.granularity();
    j["count"] = tmp.count();
    j["current"] = tmp.current_size();
    j["levels"] = tmp.levels();
}

template void to_json(nlohmann::json&, const autocorr_acc<double>&);
template void to_json(nlohmann::json&,
const autocorr_acc<std::complex<double>>&);

/* Deserialize autocorr accumulator from Json */
template <typename T>
void from_json(const nlohmann::json& j, autocorr_acc<T>& acc) {
    using count_type = typename autocorr_acc<T>::count_type;
    using blockvar_type = typename autocorr_acc<T>::blockvar_type;
    count_type tmp_granularity;
    count_type tmp_count;
    count_type tmp_current;
    std::vector<blockvar_type> tmp_levels;
    j.at("granularity").get_to(tmp_granularity);
    j.at("count").get_to(tmp_count);
    j.at("current").get_to(tmp_current);
    j.at("levels").get_to(tmp_levels);
    acc.set(tmp_levels, tmp_granularity, tmp_count, tmp_current);
}

template void from_json(const nlohmann::json&, autocorr_acc<double>&);
template void from_json(const nlohmann::json&,
autocorr_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/* Serialize autocorr accumulator to HDF5 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO& hio, const autocorr_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    hio["granularity"] << tmp.granularity();
    hio["count"] << tmp.count();
    hio["current"] << tmp.current_size();
    hio["num_levels"] << tmp.num_levels();
    for (std::size_t i = 0; i < tmp.num_levels(); i++) {
        std::string path = "levels/" + std::to_string(i);
        hio[path] << tmp.level(i);
    }
}

template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const autocorr_acc<double>&);
template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const autocorr_acc<std::complex<double>>&);

/* Deserialize autocorr accumulator from HDF5 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO& hio, autocorr_acc<T>& acc) {
    using count_type = typename autocorr_acc<T>::count_type;
    using blockvar_type = typename autocorr_acc<T>::blockvar_type;
    count_type tmp_granularity;
    count_type tmp_count;
    count_type tmp_current;
    std::size_t tmp_num_levels;
    hio["granularity"] >> tmp_granularity;
    hio["count"] >> tmp_count;
    hio["current"] >> tmp_current;
    hio["num_levels"] >> tmp_num_levels;
    std::vector<blockvar_type> tmp_levels(tmp_num_levels);
    for (std::size_t i = 0; i < tmp_num_levels; i++) {
        std::string path = "levels/" + std::to_string(i);
        hio[path] >> tmp_levels[i];
    }
    acc.set(tmp_levels, tmp_granularity, tmp_count, tmp_current);
}
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
autocorr_acc<double>&);
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
autocorr_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/* MPI reduce/Allreduce for autocorr accumulators */
template <typename T>
void reduce(const autocorr_acc<T>& acc, autocorr_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all, int root) {
    using count_type = typename autocorr_acc<T>::count_type;
    using blockvar_type = typename autocorr_acc<T>::blockvar_type;

    // necessary to make sure that acc is in accumulating state
    auto tmp_acc = acc;
    tmp_acc.convert_to_acc();

    // get the maximum number of levels and current size
    std::size_t max_levels;
    count_type tmp_current;
    simpleMC::mpi::reduce(tmp_acc.num_levels(), max_levels, MPI_MAX, comm,
                          true, root);
    simpleMC::mpi::reduce(tmp_acc.current_size(), tmp_current, MPI_MAX, comm,
                          true, root);
    tmp_acc.resize_levels(max_levels);

    // reduce only the block variances and do not propagate any data to higher
    // levels
    std::vector<blockvar_type> tmp_levels(max_levels);
    for (std::size_t i = 0; i < max_levels; i++) {
        reduce(tmp_acc.level(i), tmp_levels[i], comm, all, root);
    }

    // reduce also the count (although not really meaningful)
    count_type tmp_count = 0;
    simpleMC::mpi::reduce(tmp_acc.count(), tmp_count, MPI_SUM, comm, all,
                          root);

    // put it all together
    if (comm.rank() == root || all) {
        res.set(tmp_levels, tmp_acc.granularity(), tmp_count, tmp_current);
    }
}

template void reduce(const autocorr_acc<double>&,
autocorr_acc<double>&, simpleMC::mpi::communicator, bool, int);
template void reduce(const autocorr_acc<std::complex<double>>&,
autocorr_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
