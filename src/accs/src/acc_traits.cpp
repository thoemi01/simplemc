/**
 * @file    acc_traits.cpp
 * @brief   Traits classes for accumulators.
 * @author  Thomas Hahn
 */

#include <simpleMC/accs/acc_traits.h>

namespace simpleMC {

namespace accs {

namespace detail {

/* Set an Eigen::VectorXd to a constant */
void setConstant(Eigen::VectorXd& cont, std::size_t size, double constant) {
    cont.setConstant(size, constant);
}

/* Set an Eigen::VectorXcd to a constant */
void setConstant(Eigen::VectorXcd& cont, std::size_t size, double constant) {
    cont.setConstant(size, std::complex<double>{constant, constant});
}

/* Helper function to calculate means */
template <typename M>
void mean(M& data, std::uint64_t count) {
    if (count == 0) {
        setConstant(data, data.size(),
                    std::numeric_limits<double>::quiet_NaN());
    } else {
        data /= count;
    }
}

template void mean(Eigen::VectorXd&, std::uint64_t);
template void mean(Eigen::VectorXcd&, std::uint64_t);

/* Helper function to undo mean calculations */
template <typename M>
void invmean(M& data, std::uint64_t count) {
    if (count == 0) {
        setConstant(data, data.size(), 0.0);
    } else {
        data *= count;
    }
}

template void invmean(Eigen::VectorXd&, std::uint64_t);
template void invmean(Eigen::VectorXcd&, std::uint64_t);

/* Helper function to calculate variances */
template <typename M, typename V>
void variance(M& data, V& data2, std::uint64_t count) {
    mean(data, count);
    if (count <= 1) {
        setConstant(data2, data2.size(),
                    std::numeric_limits<double>::quiet_NaN());
    } else {
        auto unbiased = count - 1;
        data2 = (data2 - count*data.cwiseAbs2())/unbiased;
    }
}

template void variance(Eigen::VectorXd&, Eigen::VectorXd&, std::uint64_t);
template void variance(Eigen::VectorXcd&, Eigen::VectorXd&, std::uint64_t);

/* Helper function to undo variance calculations */
template <typename M, typename V>
void invvariance(M& data, V& data2, std::uint64_t count) {
    if (count == 0) {
        setConstant(data, data.size(), 0.0);
        setConstant(data2, data2.size(), 0.0);
    } else if (count == 1) {
        invmean(data, count);
        data2 = data.cwiseAbs2();
    } else {
        auto unbiased = count - 1;
        data2 = data2*unbiased + count*data.cwiseAbs2();
        invmean(data, count);
    }
}

template void invvariance(Eigen::VectorXd&, Eigen::VectorXd&, std::uint64_t);
template void invvariance(Eigen::VectorXcd&, Eigen::VectorXd&, std::uint64_t);

/* Helper function to calculate autocorrelation times */
Eigen::VectorXd tau(const Eigen::VectorXd& varn, const Eigen::VectorXd& var0) {
    return (0.5*varn.array()/var0.array() - 0.5).matrix();
}

} // namespace detail

} // namespace accs

} // namespace simpleMC
