/**
 * @file    jackknife.cpp
 * @brief   Jackknife resampling.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/accs/batch.h>
#include <simpleMC/accs/jackknife.h>

namespace simpleMC {

namespace accs {

/* Construct jackknife_data from a batch_acc */
template <typename T>
jackknife_data<T>::jackknife_data(const batch_acc<T>& acc) :
    mean_(acc.size()),
    jmean_(acc.size()),
    jdata_(acc.num_batches())
{
    // fill sum
    for (std::size_t i = 0; i < acc.num_batches(); i++) {
        mean_ << acc.data()[i];
    }

    // fill jdata and jmean
    for (std::size_t i = 0; i < acc.num_batches(); i++) {
        auto count = mean_.count() - acc.batch_count(i);
        jdata_[i].set((mean_.data_ - acc.data()[i].data_)/count,
                      /*count=*/1, true);
        jmean_.accumulate(simpleMC::utils::make_view(jdata_[i].data_),
                          /*count=*/1);
    }
    mean_.convert_to_result();
    jmean_.convert_to_result();
}

/* Propagate errors with jackknife resampling */
template <typename T>
jackknife_data<T> jackknife_data<T>::propagate(std::function<T(T)> fn) const {
    jackknife_data<T> res(size(), num_batches());

    // set full mean
    res.mean_.set(mean_.data_.unaryExpr(fn), /*count=*/1, true);

    // set jackknife samples and mean
    for (std::size_t i = 0; i < num_batches(); i++) {
        res.jdata_[i].set(jdata_[i].data_.unaryExpr(fn), /*count=*/1, true);
        res.jmean_.accumulate(simpleMC::utils::make_view(res.jdata_[i].data_),
                              /*count=*/1);
    }
    res.jmean_.convert_to_result();

    return res;
}

/* Calculate and get mean */
template <typename T>
typename jackknife_data<T>::mean_type jackknife_data<T>::mean() const {
    return num_batches()*mean_.data_ - (num_batches() - 1)*jmean_.data_;
}

/* Calculate and get variance */
template <typename T>
typename jackknife_data<T>::variance_type jackknife_data<T>::variance() const {
    var_acc<T> res(size());
    for (std::size_t i = 0; i < num_batches(); i++) {
        res.accumulate(simpleMC::utils::make_view(jdata_[i].data_),
                       /*view_count=*/ 1);
    }
    return (num_batches() - 1)*res.variance();
}

template class jackknife_data<double>;
template class jackknife_data<std::complex<double>>;

/* Write jackknife data to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const jackknife_data<T>& jk) {
    auto mean = jk.mean();
    auto err = jk.stderror();
    os << "Size = " << jk.size() << "\n"
       << "# batches = " << jk.num_batches() << "\n"
       << "<X> = [";
    if (jk.size() == 0) {
        os << "]\n";
        return os;
    }
    os << mean[0] << " +/- " << err[0];
    for (std::size_t i = 1; i < jk.size(); i++) {
        os << ", " << mean[i] << " +/- " << err[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const jackknife_data<double>&);
template std::ostream& operator<<(std::ostream&,
const jackknife_data<std::complex<double>>&);

/* Write jackknife data to file (for plotting purposes) */
template <typename T>
void jackknife_to_file(const jackknife_data<T>& jk, const std::string& filename,
                       std::ios_base::openmode mode, bool header) {
    using value_type = typename jackknife_data<T>::value_type;
    std::ofstream os(filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20(os, 20);
    simpleMC::utils::fixedwidth_ostream osw10(os, 10);
    os.precision(10);
    os << std::left;

    // write file header
    if (header) {
        os << "#Size = " << jk.size() << "\n"
           << "#Number of batches = " << jk.num_batches() << "\n";
        osw10 << "#i";
        if constexpr (std::is_same<value_type, double>::value) {
            osw20 << "f(<X>)" << "Var[f(<X>)]" << "SE[f(<X>)]" << osw20.endl();
        } else {
            osw20 << "f(<X>) real" << "f(<X>) imag" << "Var[f(<X>)]"
                  << "SE[f(<X>)]" << osw20.endl();
        }
    }

    // write to file
    auto mean = jk.mean();
    auto var = jk.variance();
    auto err = jk.stderror();
    for (std::size_t i = 0; i < jk.size(); i++) {
        if constexpr (std::is_same<value_type, double>::value) {
            osw10 << i;
            osw20 << mean[i] << var[i] << err[i] << osw20.endl();
        } else {
            osw10 << i;
            osw20 << mean[i].real() << mean[i].imag() << var[i] << err[i]
                  << osw20.endl();
        }
    }
}

template void jackknife_to_file(const jackknife_data<double>&,
const std::string&, std::ios_base::openmode, bool);
template void jackknife_to_file(const jackknife_data<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

template class jackknife_propagator<double>;
template class jackknife_propagator<std::complex<double>>;

} // namespace accs

} // namespace simpleMC
