/**
 * @file    block_variance.cpp
 * @brief   Blocked variance accumulator largely based on the ALPSCore::alea
 *          library.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/accs/block_variance.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/eigen.h>
#endif

namespace simpleMC {

namespace accs {

/* Construct a blockvar_acc with certain number of elements */
template <typename T>
blockvar_acc<T>::blockvar_acc(std::size_t size, count_type bl_size) :
    vdata_(size),
    block_(size),
    block_size_(bl_size)
{
    assert(block_size_ > 0);
}

/* Set data, block data and block size (intended for serialization and MPI) */
template <typename T>
void blockvar_acc<T>::set(const var_acc<T>& vdata, const mean_acc<T>& block,
                          count_type block_size) {
    assert(vdata.size() == block.size());
    assert(block_size > 0);
    vdata_ = vdata;
    block_ = block;
    block_size_ = block_size;
}

/* Resize data, set everything to zero and change to accumulating state */
template <typename T>
void blockvar_acc<T>::reset(std::size_t size, count_type bl_size) {
    assert(block_size_ > 0);
    vdata_.reset(size);
    block_.reset(size);
    block_size_ = bl_size;
}

/* Add block average to data */
template <typename T>
void blockvar_acc<T>::add_block(blockvar_acc* propagate) {
    // propagate to higher levels in autocorr_acc
    if (propagate) {
        propagate->accumulate(simpleMC::utils::make_view(block_.data()),
                              block_.count(), propagate + 1);
    }

    // add block average and reset
    for (std::size_t i = 0; i < size(); i++) {
        vdata_.data_[i] += block_.data_[i]/static_cast<double>(block_.count());
        vdata_.data2_[i] += traits::abs2(block_.data_[i]/
                                         static_cast<double>(block_.count()));
    }
    vdata_.count_ += 1;
    block_.reset();
}

/* Stream operator to incorporate another blockvar accumulator */
template <typename T>
blockvar_acc<T>& blockvar_acc<T>::operator<<(const blockvar_acc<T>& other) {
    assert(size() == other.size());
    assert(block_size() == other.block_size());
    // ignore the data which is currently in the blocks
    vdata_ << other.vdata_;
    return *this;
}

/* Stream operator to incorporate another mean accumulator */
template <typename T>
blockvar_acc<T>& blockvar_acc<T>::operator<<(const mean_acc<T>& other) {
    accumulate(simpleMC::utils::make_view(other.data()), other.count_);
    return *this;
}

/* Calculate and get mean */
template <typename T>
typename blockvar_acc<T>::mean_type blockvar_acc<T>::mean() const {
    if (is_result()) {
        return vdata_.data();
    }
    mean_type res = vdata_.data();
    detail::mean(res, count());
    return res;
}

/* Calculate and get variance */
template <typename T>
typename blockvar_acc<T>::variance_type blockvar_acc<T>::variance() const {
    if (is_result()) {
        return vdata_.data2();
    }
    mean_type res_mean = vdata_.data();
    variance_type res_var = vdata_.data2();
    detail::variance(res_mean, res_var, count());
    return res_var*block_size_;
}

/* Convert to the result state */
template <typename T>
void blockvar_acc<T>::convert_to_result() {
    if (!is_result()) {
        vdata_.convert_to_result();
        vdata_.data2_ *= block_size_;
    }
}

/* Convert to the accumulating state */
template <typename T>
void blockvar_acc<T>::convert_to_acc() {
    if (is_result()) {
        vdata_.data2_ /= block_size_;
        vdata_.convert_to_acc();
    }
}

template class blockvar_acc<double>;
template class blockvar_acc<std::complex<double>>;

/* Compare two blocked variance accumulators for equality */
template <typename T>
bool operator==(const blockvar_acc<T>& lhs, const blockvar_acc<T>& rhs) {
    return (lhs.var_data() == rhs.var_data() &&
            lhs.block_data() == rhs.block_data() &&
            lhs.block_size() == rhs.block_size());
}

/* Compare two blocked variance accumulators for inequality */
template <typename T>
bool operator!=(const blockvar_acc<T>& lhs, const blockvar_acc<T>& rhs) {
    return !(lhs == rhs);
}

template bool operator==(const blockvar_acc<double>&,
const blockvar_acc<double>&);
template bool operator!=(const blockvar_acc<double>&,
const blockvar_acc<double>&);
template bool operator==(const blockvar_acc<std::complex<double>>&,
const blockvar_acc<std::complex<double>>&);
template bool operator!=(const blockvar_acc<std::complex<double>>&,
const blockvar_acc<std::complex<double>>&);

/* Write blocked variance accumulator to output stream */
template <typename T>
std::ostream& operator<<(std::ostream& os, const blockvar_acc<T>& acc) {
    auto tmp = acc;
    tmp.convert_to_result();
    auto mean = tmp.mean();
    auto err = tmp.stderror_of_mean();
    os << "N = " << acc.count() << "\n"
       << "<X> = [";
    if (acc.size() == 0) {
        os << "]\n";
        return os;
    }
    os << mean[0] << " +/- " << err[0];
    for (std::size_t i = 1; i < acc.size(); i++) {
        os << ", " << mean[i] << " +/- " << err[i];
    }
    os << "]\n";
    return os;
}

template std::ostream& operator<<(std::ostream&,
const blockvar_acc<double>&);
template std::ostream& operator<<(std::ostream&,
const blockvar_acc<std::complex<double>>&);

/* Write blocked variance accumulator to file (for plotting purposes) */
template <typename T>
void acc_to_file(const blockvar_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode, bool header) {
    using value_type = typename blockvar_acc<T>::value_type;
    std::ofstream os(filename, mode);
    simpleMC::utils::fixedwidth_ostream osw20(os, 20);
    simpleMC::utils::fixedwidth_ostream osw10(os, 10);
    os.precision(10);
    os << std::left;

    // write file header
    if (header) {
        os << "#Block size = " << acc.block_size() << "\n"
           << "#Samples in block = " << acc.block_data().count() << "\n"
           << "#Total number of samples = " << acc.total_count() << "\n"
           << "#Number of samples = " << acc.count() << "\n";
        osw10 << "#i";
        if constexpr (std::is_same<value_type, double>::value) {
            osw20 << "<X>" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        } else {
            osw20 << "<X> real" << "<X> imag" << "Var(<X>)" << "SE(<X>)"
                  << "Var(X)" << "SE(X)" << osw20.endl();
        }
    }

    // write mean values to file
    auto tmp = acc;
    tmp.convert_to_result();
    auto mean = tmp.mean();
    auto varm = tmp.variance_of_mean();
    auto errm = tmp.stderror_of_mean();
    auto var = tmp.variance();
    auto err = tmp.stderror();
    for (std::size_t i = 0; i < acc.size(); i++) {
        if constexpr (std::is_same<value_type, double>::value) {
            osw10 << i;
            osw20 << mean[i] << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        } else {
            osw10 << i;
            osw20 << mean[i].real() << mean[i].imag() << varm[i] << errm[i]
                  << var[i] << err[i] << osw20.endl();
        }
    }
}

template void acc_to_file(const blockvar_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
template void acc_to_file(const blockvar_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/* Serialize blockvar accumulator to Json */
template <typename T>
void to_json(nlohmann::json& j, const blockvar_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    j["vdata"] = tmp.var_data();
    j["block"] = tmp.block_data();
    j["bl_size"] = tmp.block_size();
}

template void to_json(nlohmann::json&, const blockvar_acc<double>&);
template void to_json(nlohmann::json&,
const blockvar_acc<std::complex<double>>&);

/* Deserialize blockvar accumulator from Json */
template <typename T>
void from_json(const nlohmann::json& j, blockvar_acc<T>& acc) {
    using count_type = typename blockvar_acc<T>::count_type;
    var_acc<T> tmp_data;
    mean_acc<T> tmp_block;
    count_type tmp_size;
    j.at("vdata").get_to(tmp_data);
    j.at("block").get_to(tmp_block);
    j.at("bl_size").get_to(tmp_size);
    acc.set(tmp_data, tmp_block, tmp_size);
}

template void from_json(const nlohmann::json&, blockvar_acc<double>&);
template void from_json(const nlohmann::json&,
blockvar_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/* Serialize blockvar accumulator to HDF5 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO& hio, const blockvar_acc<T>& acc) {
    // necessary to convert accumulator to accumulating state
    auto tmp = acc;
    tmp.convert_to_acc();
    hio["vdata"] << tmp.var_data();
    hio["block"] << tmp.block_data();
    hio["bl_size"] << tmp.block_size();
}

template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const blockvar_acc<double>&);
template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const blockvar_acc<std::complex<double>>&);

/* Deserialize blockvar accumulator from HDF5 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO& hio, blockvar_acc<T>& acc) {
    using count_type = typename blockvar_acc<T>::count_type;
    var_acc<T> tmp_data;
    mean_acc<T> tmp_block;
    count_type tmp_size;
    hio["vdata"] >> tmp_data;
    hio["block"] >> tmp_block;
    hio["bl_size"] >> tmp_size;
    acc.set(tmp_data, tmp_block, tmp_size);
}

template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
blockvar_acc<double>&);
template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
blockvar_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/* MPI reduce/Allreduce for blockvar accumulators */
template <typename T>
void reduce(const blockvar_acc<T>& acc, blockvar_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all, int root) {
    // necessary to make sure that acc is in accumulating state
    auto tmp_acc = acc;
    tmp_acc.convert_to_acc();

    // reduce only the variance data and ignore any data in the current block
    var_acc<T> tmp_data;
    reduce(tmp_acc.var_data(), tmp_data, comm, all, root);

    // reduced variance but the same block data as before
    if (comm.rank() == root || all) {
        res.set(tmp_data, tmp_acc.block_data(), tmp_acc.block_size());
    }
}

template void reduce(const blockvar_acc<double>&,
blockvar_acc<double>&, simpleMC::mpi::communicator, bool, int);
template void reduce(const blockvar_acc<std::complex<double>>&,
blockvar_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
