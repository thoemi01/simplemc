﻿/**
 * @file    acc_traits.h
 * @brief   Traits classes for accumulators.
 * @author  Thomas Hahn
 */

#pragma once

#include <Eigen/Dense>

namespace simpleMC {

namespace accs {

/**
 * @brief General traits class for accumulators. Variances are calculated
 * as follows:
 *  - double, standard: Var(X) = E[(X - E[X])^2] = E[X^2] - (E[X])^2
 *  - complex, standard: Var(Z) = E[|Z - E[Z]|^2] = E[|Z|^2] - |E[Z]|^2
 *
 * @tparam T Value type
 */
template <typename T>
struct acc_traits {};

template <>
struct acc_traits<double> {

    /* Type definitions */
    using count_type = std::uint64_t;
    using value_type = double;
    using mean_type = Eigen::VectorXd;
    using variance_type = Eigen::VectorXd;
    using tau_type = Eigen::VectorXd;

    /* Operation used in variance calculations */
    static double abs2(double x) {
        return x*x;
    }
};

template <>
struct acc_traits<std::complex<double>> {

    /* Type definitions */
    using count_type = std::uint64_t;
    using value_type = std::complex<double>;
    using mean_type = Eigen::VectorXcd;
    using variance_type = Eigen::VectorXd;
    using tau_type = Eigen::VectorXd;

    /* Operation used in variance calculations */
    template <typename U>
    static double abs2(U u) {
        return std::norm(u);
    }
};

namespace detail {

/**
 * @brief Overloaded function to set an Eigen::Vector to a constant.
 */
void setConstant(Eigen::VectorXd&, std::size_t, double);
void setConstant(Eigen::VectorXcd&, std::size_t, double);

/**
 * @brief Helper function to calculate means.
 */
template <typename M>
void mean(M&, std::uint64_t);

extern template void mean(Eigen::VectorXd&, std::uint64_t);
extern template void mean(Eigen::VectorXcd&, std::uint64_t);

/**
 * @brief Helper function to undo mean calculations.
 */
template <typename M>
void invmean(M&, std::uint64_t);

extern template void invmean(Eigen::VectorXd&, std::uint64_t);
extern template void invmean(Eigen::VectorXcd&, std::uint64_t);

/**
 * @brief Helper function to calculate variances.
 */
template <typename M, typename V>
void variance(M&, V&, std::uint64_t);

extern template void variance(Eigen::VectorXd&, Eigen::VectorXd&,
std::uint64_t);
extern template void variance(Eigen::VectorXcd&, Eigen::VectorXd&,
std::uint64_t);

/**
 * @brief Helper function to undo variance calculations.
 */
template <typename M, typename V>
void invvariance(M&, V&, std::uint64_t);

extern template void invvariance(Eigen::VectorXd&, Eigen::VectorXd&,
std::uint64_t);
extern template void invvariance(Eigen::VectorXcd&, Eigen::VectorXd&,
std::uint64_t);

/**
 * @brief Helper function to calculate autocorrelation times.
 */
Eigen::VectorXd tau(const Eigen::VectorXd&, const Eigen::VectorXd&);

} // namespace detail

} // namespace accs

} // namespace simpleMC
