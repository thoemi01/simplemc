/**
 * @file    batch.h
 * @brief   Batch accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/accs/block_variance.h>

namespace simpleMC {

namespace accs {

/**
 * @brief Batch accumulator.
 *
 * @details Defines an accumulator for batching/blocking data into a fixed number
 * of batches.
 *
 * @tparam T Value type
 */
template <typename T>
class batch_acc {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using value_type = typename traits::value_type;
    using count_type = typename traits::count_type;

public:
    static constexpr std::size_t default_num_batches = 512;
    static constexpr std::size_t default_block_size = 1;

private:

    /**
     * @brief Single-value accumulator for batch_acc.
     *
     * @details Class that holds a reference to a batch accumulator.
     * It can be used to add single values to the accumulator. When it goes out
     * of scope, it increases the count of the accumulator. Therefore it should
     * be used in functions.
     */
    class batch_sva {
    public:

        /* Construct a var_sva */
        batch_sva(batch_acc& acc, std::size_t idx) :
            acc_(acc),
            idx_(idx)
        {
            assert(idx_ < acc_.size());
        }

        /* Subscript operator to set the current index */
        batch_sva& operator[](std::size_t idx) {
            assert(idx < acc_.size());
            idx_ = idx;
            return *this;
        }

        /* Stream operator to accumulate data */
        template <typename U>
        batch_sva& operator<<(U u) {
            acc_.data_[acc_.curr_].data_[idx_] += u;
            return *this;
        }

        /* Destructor increases the count */
        ~batch_sva() {
            acc_.data_[acc_.curr_].count_ += 1;
            acc_.count_ += 1;
            acc_.curr_count_ += 1;
            acc_.check_and_move();
        }

    private:
        batch_acc& acc_;
        std::size_t idx_;
    };

public:

    /* Construct a batch_acc with certain number of elements, batches and block
     * size */
    batch_acc(std::size_t size = 1, std::size_t num_batches = default_num_batches,
              std::size_t bl_size = default_block_size);

    /* Set status of accumulator (intended for serialization and MPI) */
    void set(const std::vector<mean_acc<T>>&, std::size_t, count_type,
             count_type, std::size_t);

    /* Get size */
    std::size_t size() const { return data_[0].size(); }

    /* Get block size */
    std::size_t block_size() const { return block_size_; }

    /* Get number of batches */
    std::size_t num_batches() const { return data_.size(); }

    /* Resize data, set everything to zero and change to accumulating state */
    void reset(std::size_t, std::size_t, std::size_t);

    void reset() { reset(size(), num_batches(), block_size()); }

    /* Get data */
    const std::vector<mean_acc<T>>& data() const { return data_; }

    /* Get total count */
    count_type total_count() const { return count_; }

    /* Get count in certain batch */
    count_type batch_count(std::size_t idx = 0) const {
        return data_[idx].count();
    }

    /* Get current count */
    count_type current_count() const { return curr_count_; }

    /* Check if batch is full */
    bool is_full() const { return curr_count_ >= block_size_; }

    /* Get current batch index */
    std::size_t current_batch() const { return curr_; }

    /* Accumulate data */
    template <typename U>
    void accumulate(const simpleMC::utils::view<U>&, count_type);

    /* Stream operator for accumulation */
    template <typename U>
    batch_acc& operator<<(const U& u) {
        accumulate(simpleMC::utils::make_view(u), /*view_count=*/ 1);
        return *this;
    }

    /* Stream operator to incorporate another accumulator */
    batch_acc& operator<<(const batch_acc& other);

    /* Subscript operator creates a single-value accumulator */
    batch_sva operator[](std::size_t idx) {
        return batch_sva(*this, idx);
    }

    /* Create a single-value accumulator for the acc_data accumulator */
    batch_sva make_sva(std::size_t idx = 0) {
        return batch_sva(*this, idx);
    }

    /* Calculate and get mean */
    mean_type mean() const;

    /* Calculate and get variance */
    typename acc_traits<T>::variance_type variance() const {
        return make_var().variance()*mean_samples();
    }

    /* Calculate and get variance of mean */
    typename acc_traits<T>::variance_type variance_of_mean() const {
        return make_var().variance()/num_batches();
    }

    /* Calculate and get std. error */
    typename acc_traits<T>::variance_type stderror() const {
        return (variance()).cwiseSqrt();
    }

    /* Calculate and get std. error of mean */
    typename acc_traits<T>::variance_type stderror_of_mean() const {
        return (variance_of_mean()).cwiseSqrt();
    }

    /* Accumulate data in var_acc */
    var_acc<T> make_var() const;

    /* Calculate mean number of samples in batches */
    double mean_samples() const {
        return static_cast<double>(count_)/num_batches();
    }

private:

    /* Check and move current index */
    void check_and_move() {
        if (is_full()) {
            curr_ = (curr_ + 1) % num_batches();
            curr_count_ = 0;
        }
    }

private:
    std::vector<mean_acc<T>> data_;
    std::size_t block_size_;
    count_type count_;
    count_type curr_count_;
    std::size_t curr_;
};

/* Accumulate data */
template <typename T>
template <typename U>
void batch_acc<T>::accumulate(const simpleMC::utils::view<U>& view,
                              count_type view_count) {
    data_[curr_].accumulate(view, view_count);
    count_ += view_count;
    curr_count_ += view_count;
    check_and_move();
}

extern template class batch_acc<double>;
extern template class batch_acc<std::complex<double>>;

/**
 * @brief Compare two batch accumulators.
 */
template <typename T>
bool operator==(const batch_acc<T>&, const batch_acc<T>&);

template <typename T>
bool operator!=(const batch_acc<T>&, const batch_acc<T>&);

extern template bool operator==(const batch_acc<double>&,
const batch_acc<double>&);
extern template bool operator!=(const batch_acc<double>&,
const batch_acc<double>&);
extern template bool operator==(const batch_acc<std::complex<double>>&,
const batch_acc<std::complex<double>>&);
extern template bool operator!=(const batch_acc<std::complex<double>>&,
const batch_acc<std::complex<double>>&);

/**
 * @brief Write batch accumulator to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const batch_acc<T>&);

extern template std::ostream& operator<<(std::ostream&,
const batch_acc<double>&);
extern template std::ostream& operator<<(std::ostream&,
const batch_acc<std::complex<double>>&);

/**
 * @brief Write batch accumulator to file (for plotting purposes).
 */
template <typename T>
void acc_to_file(const batch_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode = std::ios_base::out,
                 bool header = true);

extern template void acc_to_file(const batch_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
extern template void acc_to_file(const batch_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/**
 * @brief Serialize batch accumulator to Json.
 */
template <typename T>
void to_json(nlohmann::json&, const batch_acc<T>&);

extern template void to_json(nlohmann::json&, const batch_acc<double>&);
extern template void to_json(nlohmann::json&,
const batch_acc<std::complex<double>>&);

/**
 * @brief Deserialize batch accumulator from Json.
 */
template <typename T>
void from_json(const nlohmann::json&, batch_acc<T>&);

extern template void from_json(const nlohmann::json&, batch_acc<double>&);
extern template void from_json(const nlohmann::json&,
batch_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/**
 * @brief Serialize batch accumulator to HDF5.
 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO&, const batch_acc<T>&);

extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const batch_acc<double>&);
extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const batch_acc<std::complex<double>>&);

/**
 * @brief Deserialize batch accumulator from HDF5.
 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO&, batch_acc<T>&);

extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
batch_acc<double>&);
extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
batch_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief MPI reduce for batch accumulators.
 */
template <typename T>
void reduce(const batch_acc<T>& acc, batch_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all = true,
            int root = 0);

extern template void reduce(const batch_acc<double>&,
batch_acc<double>&, simpleMC::mpi::communicator, bool, int);
extern template void reduce(const batch_acc<std::complex<double>>&,
batch_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
