/**
 * @file    accs.h
 * @brief   Convenience header for simpleMC-accs.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/accs/mean.h>
#include <simpleMC/accs/variance.h>
#include <simpleMC/accs/block_variance.h>
#include <simpleMC/accs/autocorr.h>
#include <simpleMC/accs/batch.h>
#include <simpleMC/accs/jackknife.h>
