/**
 * @file    acc_fwd.h
 * @brief   Forward declarations of accumulators.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json_fwd.hpp>

namespace simpleMC {

namespace accs {

template <typename T> class mean_acc;
template <typename T> class var_acc;
template <typename T> class blockvar_acc;
template <typename T> class autocorr_acc;
template <typename T> class batch_acc;
template <typename T> class jackknife_data;
template <typename T> class jackknife_propagator;

} // namespace accs

} // namespace simpleMC
