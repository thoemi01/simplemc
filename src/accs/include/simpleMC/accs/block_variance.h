/**
 * @file    block_variance.h
 * @brief   Blocked variance accumulator largely based on the ALPSCore::alea
 *          library.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/accs/mean.h>
#include <simpleMC/accs/variance.h>

namespace simpleMC {

namespace accs {

/**
 * @brief Blocked variance accumulator.
 *
 * @details Defines an accumulator for calculating blocked variances. Data is
 * first accumulated into blocks of size M. Then the block averages are
 * calculated and the variance is computed for these block averages. This can
 * speed up the calculation and also reduce the corrleation between successive
 * measurements. Everything else is similar to var_acc.
 *
 * @tparam T Value type
 */
template <typename T>
class blockvar_acc {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using value_type = typename traits::value_type;
    using count_type = typename traits::count_type;

private:

    /**
     * @brief Single-value accumulator for blockvar_acc.
     *
     * @details Class that holds a reference to a blocked variance accumulator.
     * It can be used to add single values to the accumulator. When it goes out
     * of scope, it increases the count of the accumulator. Therefore it should
     * be used in functions.
     */
    class blockvar_sva {
    public:

        /* Construct a var_sva */
        blockvar_sva(blockvar_acc& acc, std::size_t idx,
                     blockvar_acc* prop = nullptr) :
            acc_(acc),
            idx_(idx),
            propagate_(prop)
        {
            assert(idx_ < acc_.size());
        }

        /* Subscript operator to set the current index */
        blockvar_sva& operator[](std::size_t idx) {
            assert(idx < acc_.size());
            idx_ = idx;
            return *this;
        }

        /* Stream operator to accumulate data */
        template <typename U>
        blockvar_sva& operator<<(U u) {
            acc_.block_.data_[idx_] += u;
            return *this;
        }

        /* Destructor increases the count */
        ~blockvar_sva() {
            acc_.block_.count_ += 1;
            if (acc_.is_full()) {
                acc_.add_block(propagate_);
            }
        }

    private:
        blockvar_acc& acc_;
        std::size_t idx_;
        blockvar_acc* propagate_;
    };

public:

    /* Construct a blockvar_acc with certain number of elements */
    blockvar_acc(std::size_t size = 1, count_type bl_size = 1);

    /* Set data, block data and block size (intended for serialization
     * and MPI) */
    void set(const var_acc<T>&, const mean_acc<T>&, count_type);

    /* Get size */
    std::size_t size() const { return vdata_.size(); }

    /* Get block size */
    count_type block_size() const { return block_size_; }

    /* Resize data, set everything to zero and change to accumulating state */
    void reset(std::size_t, count_type);

    void reset() { reset(size(), block_size()); }

    /* Get data */
    const mean_type& data() const { return vdata_.data(); }

    const variance_type& data2() const { return vdata_.data2(); }

    const var_acc<T>& var_data() const { return vdata_; }

    const mean_acc<T>& block_data() const { return block_; }

    /* Get count = effective measurements = number of blocks */
    count_type count() const { return vdata_.count(); }

    /* Get total count */
    count_type total_count() const { return vdata_.count()*block_size_; }

    /* Get current count in block */
    count_type block_count() const { return block_.count(); }

    /* Is data in accumulating or result state */
    bool is_result() const { return vdata_.is_result(); }

    /* Check if block is full */
    bool is_full() const { return block_.count() >= block_size_; }

    /* Accumulate data */
    template <typename U>
    void accumulate(const simpleMC::utils::view<U>&, count_type,
                    blockvar_acc* propagate = nullptr);

    /* Stream operator for accumulation */
    template <typename U>
    blockvar_acc& operator<<(const U& u) {
        accumulate(simpleMC::utils::make_view(u), /*view_count=*/ 1);
        return *this;
    }

    /* Stream operator to incorporate another accumulator */
    blockvar_acc& operator<<(const blockvar_acc&);

    blockvar_acc& operator<<(const mean_acc<T>&);

    /* Subscript operator creates a single-value accumulator */
    blockvar_sva operator[](std::size_t idx) {
        return blockvar_sva(*this, idx);
    }

    /* Create a single-value accumulator for the current accumulator */
    blockvar_sva make_sva(std::size_t idx = 0,
                          blockvar_acc* propagate = nullptr) {
        return blockvar_sva(*this, idx, propagate);
    }

    /* Calculate and get mean */
    mean_type mean() const;

    /* Calculate and get variance */
    variance_type variance() const;

    /* Calculate and get variance of mean */
    variance_type variance_of_mean() const { return variance()/total_count(); }

    /* Calculate and get std. error */
    variance_type stderror() const { return (variance()).cwiseSqrt(); }

    /* Calculate and get std. error of mean */
    variance_type stderror_of_mean() const {
        return (variance_of_mean()).cwiseSqrt();
    }

    /* Convert to the result state */
    void convert_to_result();

    /* Convert to the accumulating state */
    void convert_to_acc();

private:

    /* Add block average to data */
    void add_block(blockvar_acc* propagate = nullptr);

private:
    var_acc<T> vdata_;
    mean_acc<T> block_;
    count_type block_size_;
};

/* Accumulate data */
template <typename T>
template <typename U>
void blockvar_acc<T>::accumulate(const simpleMC::utils::view<U>& view,
                                 count_type view_count,
                                 blockvar_acc<T>* propagate) {
    assert(view.size() == size());
    assert(block_.count() + view_count <= block_size_);
    for (std::size_t i = 0; i < size(); i++) {
        block_.data_[i] += view[i];
    }
    block_.count_ += view_count;

    // add block average to data if block is full
    if (is_full()) {
        add_block(propagate);
    }
}

extern template class blockvar_acc<double>;
extern template class blockvar_acc<std::complex<double>>;

/**
 * @brief Compare two blocked variance accumulators.
 */
template <typename T>
bool operator==(const blockvar_acc<T>&, const blockvar_acc<T>&);

template <typename T>
bool operator!=(const blockvar_acc<T>&, const blockvar_acc<T>&);

extern template bool operator==(const blockvar_acc<double>&,
const blockvar_acc<double>&);
extern template bool operator!=(const blockvar_acc<double>&,
const blockvar_acc<double>&);
extern template bool operator==(const blockvar_acc<std::complex<double>>&,
const blockvar_acc<std::complex<double>>&);
extern template bool operator!=(const blockvar_acc<std::complex<double>>&,
const blockvar_acc<std::complex<double>>&);

/**
 * @brief Write blocked variance accumulator to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const blockvar_acc<T>&);

extern template std::ostream& operator<<(std::ostream&,
const blockvar_acc<double>&);
extern template std::ostream& operator<<(std::ostream&,
const blockvar_acc<std::complex<double>>&);

/**
 * @brief Write blocked variance accumulator to file (for plotting purposes).
 */
template <typename T>
void acc_to_file(const blockvar_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode = std::ios_base::out,
                 bool header = true);

extern template void acc_to_file(const blockvar_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
extern template void acc_to_file(const blockvar_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/**
 * @brief Serialize blocked variance accumulator to Json.
 */
template <typename T>
void to_json(nlohmann::json&, const blockvar_acc<T>&);

extern template void to_json(nlohmann::json&, const blockvar_acc<double>&);
extern template void to_json(nlohmann::json&,
const blockvar_acc<std::complex<double>>&);

/**
 * @brief Deserialize blocked variance accumulator from Json.
 */
template <typename T>
void from_json(const nlohmann::json&, blockvar_acc<T>&);

extern template void from_json(const nlohmann::json&, blockvar_acc<double>&);
extern template void from_json(const nlohmann::json&,
blockvar_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/**
 * @brief Serialize blocked variance accumulator to HDF5.
 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO&, const blockvar_acc<T>&);

extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const blockvar_acc<double>&);
extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const blockvar_acc<std::complex<double>>&);

/**
 * @brief Deserialize blocked variance accumulator from HDF5.
 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO&, blockvar_acc<T>&);

extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
blockvar_acc<double>&);
extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
blockvar_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief MPI reduce for blocked variance accumulators.
 */
template <typename T>
void reduce(const blockvar_acc<T>& acc, blockvar_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all = true,
            int root = 0);

extern template void reduce(const blockvar_acc<double>&,
blockvar_acc<double>&, simpleMC::mpi::communicator, bool, int);
extern template void reduce(const blockvar_acc<std::complex<double>>&,
blockvar_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
