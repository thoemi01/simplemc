/**
 * @file    mean.h
 * @brief   Mean accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/config.h>
#include <simpleMC/utils/view_eigen.h>
#include <simpleMC/accs/acc_fwd.h>
#include <simpleMC/accs/acc_traits.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO_fwd.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace accs {

/**
 * @brief Mean accumulator.
 *
 * @details Defines an accumulator for calculating means. No error estimates
 * are possible. The accumulator can be in one of 2 states: accumulating state
 * or result state. The user is responsible to only accumulate data when the
 * accumulating state is active. Check the state with is_result(). Jump between
 * the different states with convert_to_result() and convert_to_acc().
 *
 * @tparam T Value type
 */
template <typename T>
class mean_acc {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using value_type = typename traits::value_type;
    using count_type = typename traits::count_type;

private:

    /**
     * @brief Single-value accumulator for mean_acc.
     *
     * @details Class that holds a reference to a mean accumulator. It can be
     * used to add single values to the accumulator. When it goes out of scope,
     * it increases the count of the accumulator. Therefore it should be used
     * in functions.
     */
    class mean_sva {
    public:

        /* Construct a mean_sva */
        mean_sva(mean_acc& acc, std::size_t idx) :
            acc_(acc),
            idx_(idx)
        {
            assert(idx_ < acc_.size());
        }

        /* Subscript operator to set the current index */
        mean_sva& operator[](std::size_t idx) {
            assert(idx < acc_.size());
            idx_ = idx;
            return *this;
        }

        /* Stream operator to accumulate data */
        template <typename U>
        mean_sva& operator<<(U u) {
            acc_.data_[idx_] += u;
            return *this;
        }

        /* Destructor increases the count */
        ~mean_sva() { acc_.count_ += 1; }

    private:
        mean_acc& acc_;
        std::size_t idx_;
    };

public:

    /* Construct a mean_acc with certain number of elements */
    mean_acc(std::size_t size = 1);

    /* Set data, count and state (intended for serialization and MPI) */
    void set(const mean_type&, count_type, bool);

    /* Get size */
    std::size_t size() const { return data_.size(); }

    /* Resize data, set everything to zero and change to accumulating state */
    void reset(std::size_t);

    void reset() { reset(size()); }

    /* Get data */
    const mean_type& data() const { return data_; }

    /* Get count */
    count_type count() const { return count_; }

    /* Is data in accumulating or result state */
    bool is_result() const { return is_result_; }

    /* Accumulate data */
    template <typename U>
    void accumulate(const simpleMC::utils::view<U>&, count_type);

    /* Stream operator for accumulation */
    template <typename U>
    mean_acc& operator<<(const U& u) {
        accumulate(simpleMC::utils::make_view(u), /*view_count=*/ 1);
        return *this;
    }

    /* Stream operator to incorporate another accumulator */
    mean_acc& operator<<(const mean_acc& other);

    /* Subscript operator creates a single-value accumulator */
    mean_sva operator[](std::size_t idx) {
        return mean_sva(*this, idx);
    }

    /* Create a single-value accumulator for the current accumulator */
    mean_sva make_sva(std::size_t idx = 0) {
        return mean_sva(*this, idx);
    }

    /* Calculate and get mean */
    mean_type mean() const;

    /* Convert to the result state */
    void convert_to_result();

    /* Convert to the accumulating state */
    void convert_to_acc();

private:
    mean_type data_;
    count_type count_;
    bool is_result_;

    /* Friend declaration */
    friend class blockvar_acc<T>;
    friend class batch_acc<T>;
    friend class jackknife_data<T>;
    friend class jackknife_propagator<T>;
};

/* Accumulate data */
template <typename T>
template <typename U>
void mean_acc<T>::accumulate(const simpleMC::utils::view<U>& view,
                             count_type view_count) {
    assert(view.size() == static_cast<std::size_t>(size()));
    for (std::size_t i = 0; i < size(); i++) {
        data_[i] += view[i];
    }
    count_ += view_count;
}

extern template class mean_acc<double>;
extern template class mean_acc<std::complex<double>>;

/**
 * @brief Compare two mean accumulators.
 */
template <typename T>
bool operator==(const mean_acc<T>&, const mean_acc<T>&);

template <typename T>
bool operator!=(const mean_acc<T>&, const mean_acc<T>&);

extern template bool operator==(const mean_acc<double>&,
const mean_acc<double>&);
extern template bool operator!=(const mean_acc<double>&,
const mean_acc<double>&);
extern template bool operator==(const mean_acc<std::complex<double>>&,
const mean_acc<std::complex<double>>&);
extern template bool operator!=(const mean_acc<std::complex<double>>&,
const mean_acc<std::complex<double>>&);

/**
 * @brief Write mean accumulator to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const mean_acc<T>&);

extern template std::ostream& operator<<(std::ostream&,
const mean_acc<double>&);
extern template std::ostream& operator<<(std::ostream&,
const mean_acc<std::complex<double>>&);

/**
 * @brief Write mean accumulator to file (for plotting purposes).
 */
template <typename T>
void acc_to_file(const mean_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode = std::ios_base::out,
                 bool header = true);

extern template void acc_to_file(const mean_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
extern template void acc_to_file(const mean_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/**
 * @brief Serialize mean accumulator to Json.
 */
template <typename T>
void to_json(nlohmann::json&, const mean_acc<T>&);

extern template void to_json(nlohmann::json&, const mean_acc<double>&);
extern template void to_json(nlohmann::json&,
const mean_acc<std::complex<double>>&);

/**
 * @brief Deserialize mean accumulator from Json.
 */
template <typename T>
void from_json(const nlohmann::json&, mean_acc<T>&);

extern template void from_json(const nlohmann::json&, mean_acc<double>&);
extern template void from_json(const nlohmann::json&,
mean_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/**
 * @brief Serialize mean accumulator to HDF5.
 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO&, const mean_acc<T>&);

extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const mean_acc<double>&);
extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const mean_acc<std::complex<double>>&);

/**
 * @brief Deserialize mean accumulator from HDF5.
 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO&, mean_acc<T>&);

extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
mean_acc<double>&);
extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
mean_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief MPI reduce/Allreduce for mean accumulators.
 */
template <typename T>
void reduce(const mean_acc<T>& acc, mean_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all = true,
            int root = 0);

extern template void reduce(const mean_acc<double>&, mean_acc<double>&,
simpleMC::mpi::communicator, bool, int);
extern template void reduce(const mean_acc<std::complex<double>>&,
mean_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
