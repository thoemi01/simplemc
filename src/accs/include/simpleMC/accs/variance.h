/**
 * @file    variance.h
 * @brief   Variance accumulator largely based on the ALPSCore::alea library.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/config.h>
#include <simpleMC/utils/view_eigen.h>
#include <simpleMC/accs/acc_fwd.h>
#include <simpleMC/accs/acc_traits.h>

// HDF5 support if enabled
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO_fwd.h>
#endif

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace accs {

/**
 * @brief Variance accumulator.
 *
 * @details Defines an accumulator for calculating variances. Variance and
 * standard error estimates for the random data as well as the corresponding
 * means are available. The accumulator can be in one of 2 states: accumulating
 * state or result state. The user is responsible to only accumulate data when
 * the accumulating state is active. Check the state with is_result(). Jump
 * between the different states with convert_to_result() and convert_to_acc().
 *
 * @tparam T Value type
 */
template <typename T>
class var_acc {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using value_type = typename traits::value_type;
    using count_type = typename traits::count_type;

private:

    /**
     * @brief Single-value accumulator for var_acc.
     *
     * @details Class that holds a reference to a variance accumulator. It can
     * be used to add single values to the accumulator. When it goes out of
     * scope, it increases the count of the accumulator. Therefore it should
     * be used in functions.
     */
    class var_sva {
    public:

        /* Construct a var_sva */
        var_sva(var_acc& acc, std::size_t idx) :
            acc_(acc),
            idx_(idx)
        {
            assert(idx_ < acc_.size());
        }

        /* Subscript operator to set the current index */
        var_sva& operator[](std::size_t idx) {
            assert(idx < acc_.size());
            idx_ = idx;
            return *this;
        }

        /* Stream operator to accumulate data */
        template <typename U>
        var_sva& operator<<(U u) {
            acc_.data_[idx_] += u;
            acc_.data2_[idx_] += traits::abs2(u);
            return *this;
        }

        /* Destructor increases the count */
        ~var_sva() { acc_.count_ += 1; }

    private:
        var_acc& acc_;
        std::size_t idx_;
    };

public:

    /* Construct a vac_acc with certain number of elements */
    var_acc(std::size_t size = 1);

    /* Set data, data2, count and state (intended for serialization and MPI) */
    void set(const mean_type&, const variance_type&, count_type, bool);

    /* Get size */
    std::size_t size() const { return data_.size(); }

    /* Resize data, set everything to zero and change to accumulating state */
    void reset(std::size_t);

    void reset() { reset(size()); }

    /* Get data */
    const mean_type& data() const { return data_; }

    const variance_type& data2() const { return data2_; }

    /* Get count */
    count_type count() const { return count_; }

    /* Is data in accumulating or result state */
    bool is_result() const { return is_result_; }

    /* Accumulate data */
    template <typename U>
    void accumulate(const simpleMC::utils::view<U>&, count_type);

    /* Stream operator for accumulation */
    template <typename U>
    var_acc& operator<<(const U& u) {
        accumulate(simpleMC::utils::make_view(u), /*view_count=*/ 1);
        return *this;
    }

    /* Stream operator to incorporate another accumulator */
    var_acc& operator<<(const var_acc& other);

    /* Subscript operator creates a single-value accumulator */
    var_sva operator[](std::size_t idx) {
        return var_sva(*this, idx);
    }

    /* Create a single-value accumulator for the current accumulator */
    var_sva make_sva(std::size_t idx = 0) {
        return var_sva(*this, idx);
    }

    /* Calculate and get mean */
    mean_type mean() const;

    /* Calculate and get variance */
    variance_type variance() const;

    /* Calculate and get variance of mean */
    variance_type variance_of_mean() const { return variance()/count_; }

    /* Calculate and get std. error */
    variance_type stderror() const { return (variance()).cwiseSqrt(); }

    /* Calculate and get std. error of mean */
    variance_type stderror_of_mean() const {
        return (variance_of_mean()).cwiseSqrt();
    }

    /* Convert to the result state */
    void convert_to_result();

    /* Convert to the accumulating state */
    void convert_to_acc();

private:
    mean_type data_;
    variance_type data2_;
    count_type count_;
    bool is_result_;

    /* Friend declaration */
    friend class blockvar_acc<T>;
};

/* Accumulate data */
template <typename T>
template <typename U>
inline void var_acc<T>::accumulate(const simpleMC::utils::view<U>& view,
                                   count_type view_count) {
    assert(view.size() == static_cast<std::size_t>(size()));
    for (std::size_t i = 0; i < size(); i++) {
        data_[i] += view[i];
        data2_[i] += traits::abs2(view[i]);
    }
    count_ += view_count;
}

extern template class var_acc<double>;
extern template class var_acc<std::complex<double>>;

/**
 * @brief Compare two variance accumulators.
 */
template <typename T>
bool operator==(const var_acc<T>&, const var_acc<T>&);

template <typename T>
bool operator!=(const var_acc<T>&, const var_acc<T>&);

extern template bool operator==(const var_acc<double>&,
const var_acc<double>&);
extern template bool operator!=(const var_acc<double>&,
const var_acc<double>&);
extern template bool operator==(const var_acc<std::complex<double>>&,
const var_acc<std::complex<double>>&);
extern template bool operator!=(const var_acc<std::complex<double>>&,
const var_acc<std::complex<double>>&);

/**
 * @brief Write variance accumulator to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const var_acc<T>&);

extern template std::ostream& operator<<(std::ostream&,
const var_acc<double>&);
extern template std::ostream& operator<<(std::ostream&,
const var_acc<std::complex<double>>&);

/**
 * @brief Write variance accumulator to file (for plotting purposes).
 */
template <typename T>
void acc_to_file(const var_acc<T>& acc, const std::string& filename,
                 std::ios_base::openmode mode = std::ios_base::out,
                 bool header = true);

extern template void acc_to_file(const var_acc<double>&,
const std::string&, std::ios_base::openmode, bool);
extern template void acc_to_file(const var_acc<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/**
 * @brief Serialize variance accumulator to Json.
 */
template <typename T>
void to_json(nlohmann::json&, const var_acc<T>&);

extern template void to_json(nlohmann::json&, const var_acc<double>&);
extern template void to_json(nlohmann::json&,
const var_acc<std::complex<double>>&);

/**
 * @brief Deserialize variance accumulator from Json.
 */
template <typename T>
void from_json(const nlohmann::json&, var_acc<T>&);

extern template void from_json(const nlohmann::json&, var_acc<double>&);
extern template void from_json(const nlohmann::json&,
var_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/**
 * @brief Serialize variance accumulator to HDF5.
 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO&, const var_acc<T>&);

extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const var_acc<double>&);
extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const var_acc<std::complex<double>>&);

/**
 * @brief Deserialize variance accumulator from HDF5.
 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO&, var_acc<T>&);

extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
var_acc<double>&);
extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
var_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief MPI reduce/Allreduce for variance accumulators.
 */
template <typename T>
void reduce(const var_acc<T>& acc, var_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all = true,
            int root = 0);

extern template void reduce(const var_acc<double>&, var_acc<double>&,
simpleMC::mpi::communicator, bool, int);
extern template void reduce(const var_acc<std::complex<double>>&,
var_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleAcc
