/**
 * @file    jackknife.h
 * @brief   Jackknife resampling.
 * @author  Thomas Hahn
 */

#pragma once

#include <functional>
#include <utility>
#include <stdexcept>
#include <simpleMC/accs/acc_fwd.h>
#include <simpleMC/accs/mean.h>

namespace simpleMC {

namespace accs {

/**
 * @brief Turns batch accumulator to jackknife data used for resampling.
 * It holds three kinds of data:
 * - mean_: Mean accumulator of all measurements
 * - jdata_: Vector of jackknife data where the ith element holds a mean
 *           accumulator with the measurements from batch i removed
 * - jmean_: Mean accumulator of jdata_ elements
 *
 * @tparam T Value type
 */
template <typename T>
class jackknife_data {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using value_type = typename traits::value_type;

public:

    /* Construct jackknife_data with a given size and number of batches */
    jackknife_data(std::size_t size, std::size_t num_batches) :
        mean_(size),
        jmean_(size),
        jdata_(num_batches, mean_acc<T>(size))
    {
        assert(size > 0);
        assert(num_batches > 0);
    }

    /* Construct jackknife_data from a batch_acc */
    jackknife_data(const batch_acc<T>&);

    /* Get mean data */
    const mean_acc<T>& get_mean() const { return mean_; }

    /* Get ith element of mean (used in variadic error propagation) */
    value_type get_mean(std::size_t i) const {
        assert(size() == 1 || i < size());
        return (size() > 1 ? mean_.data_[i] : mean_.data_[0]);
    }

    /* Get jmean data */
    const mean_acc<T>& get_jmean() const { return jmean_; }

    /* Get ith element of jmean (used in variadic error propagation) */
    value_type get_jmean(std::size_t i) const {
        assert(size() == 1 || i < size());
        return (size() > 1 ? jmean_.data_[i] : jmean_.data_[0]);
    }

    /* Get jdata data */
    const std::vector<mean_acc<T>>& get_jdata() const { return jdata_; }

    /* Get ith element of jth jdata (used in variadic error propagation) */
    value_type get_jdata(std::size_t i, std::size_t j) const {
        assert(size() == 1 || i < size());
        return (size() > 1 ? jdata_[j].data_[i] : jdata_[j].data_[0]);
    }

    /* Get number of jackknife samples = number of batches */
    auto num_batches() const { return jdata_.size(); }

    /* Get size */
    auto size() const { return mean_.size(); }

    /* Get count */
    auto count() const { return mean_.count(); }

    /* Propagate errors with jackknife resampling */
    jackknife_data propagate(std::function<T(T)>) const;

    /* Calculate and get mean */
    mean_type mean() const;

    /* Calculate and get variance */
    variance_type variance() const;

    /* Calclulate and get standard error */
    variance_type stderror() const { return (variance()).cwiseSqrt(); }

private:
    mean_acc<T> mean_;
    mean_acc<T> jmean_;
    std::vector<mean_acc<T>> jdata_;

    /* Friend declarations */
    friend class jackknife_propagator<T>;
};

extern template class jackknife_data<double>;
extern template class jackknife_data<std::complex<double>>;

/**
 * @brief Write jackknife data to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const jackknife_data<T>&);

extern template std::ostream& operator<<(std::ostream&,
const jackknife_data<double>&);
extern template std::ostream& operator<<(std::ostream&,
const jackknife_data<std::complex<double>>&);

/**
 * @brief Write jackknife data to file (for plotting purposes).
 */
template <typename T>
void jackknife_to_file(const jackknife_data<T>& acc,
                       const std::string& filename,
                       std::ios_base::openmode mode = std::ios_base::out,
                       bool header = true);

extern template void jackknife_to_file(const jackknife_data<double>&,
const std::string&, std::ios_base::openmode, bool);
extern template void jackknife_to_file(
const jackknife_data<std::complex<double>>&,
const std::string&, std::ios_base::openmode, bool);

/**
 * @brief Jackknife error propagator.
 *
 * @tparam T Value type
 */
template <typename T>
class jackknife_propagator {
public:
    using traits = acc_traits<T>;
    using value_type = typename traits::value_type;

public:

    /* Construct jackknife_propagator with a given size and number
     * of batches */
    jackknife_propagator(std::size_t size, std::size_t num_batches) :
        size_(size),
        num_batches_(num_batches)
    {
        assert(size > 0);
        assert(num_batches > 0);
    }

    /* Set size and number of batches */
    void set(std::size_t size, std::size_t num_batches) {
        assert(size > 0);
        assert(num_batches > 0);
        size_ = size;
        num_batches_ = num_batches;
    }

    /* Checks if the sizes of the jackknife data are compatible */
    template <typename... Args>
    bool check_sizes(Args&&... as) const {
        return ((as.size() == size_ || as.size() == 1) && ...);
    }

    /* Checks if the number of batches of the jackknife data are compatible */
    template <typename... Args>
    bool check_batches(Args&&... as) const {
        return ((as.num_batches() == num_batches_) && ...);
    }

    /* Performs jackknife resampling */
    template <typename F, typename... Args>
    jackknife_data<T> propagate(F&&, Args&&...) const;

private:
    std::size_t size_;
    std::size_t num_batches_;
};

/**
 * @brief Performs jackknife resampling for an arbitrary number
 *        of jackknife data.
 *
 * @tparam T Value type
 * @tparam F Callable object type
 * @tparam Args Parameter pack, jackknife data
 */
template <typename T>
template <typename F, typename... Args>
jackknife_data<T> jackknife_propagator<T>::propagate(F&& fn, Args&&... as) const {
    // check sizes
    if (!check_sizes(std::forward<Args>(as)...)) {
        throw std::logic_error("Wrong sizes in jackknife resampling.");
    }

    // check number of batches
    if (!check_batches(std::forward<Args>(as)...)) {
        throw std::logic_error("Wrong number of batches in jackknife"
                               " resampling.");
    }

    // do resampling
    jackknife_data<T> res(size_, num_batches_);
    for (std::size_t i = 0; i < size_; i++) {
        res.mean_.data_[i] = fn(as.get_mean(i)...);
        for (std::size_t j = 0; j < num_batches_; j++) {
            auto val = fn(as.get_jdata(i, j)...);
            res.jdata_[j].data_[i] = val;
            res.jmean_.data_[i] += val;
        }
    }
    res.jmean_.count_ = num_batches_;

    // set final accumulator parameters
    res.mean_.count_ = 1;
    res.mean_.is_result_ = true;
    res.jmean_.convert_to_result();
    for (std::size_t j = 0; j < num_batches_; j++) {
        res.jdata_[j].count_ = 1;
        res.jdata_[j].is_result_ = true;
    }

    return res;
}

extern template class jackknife_propagator<double>;
extern template class jackknife_propagator<std::complex<double>>;

} // namespace accs

} // namespace simpleMC
