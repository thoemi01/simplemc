/**
 * @file    autocorr_acc.h
 * @brief   Autocorrelation accumulator largely based on the ALPSCore::alea
 *          library.
 * @author  Thomas Hahn
 * @todo    -) Fix merge of two accumulators
 */

#pragma once

#include <simpleMC/accs/block_variance.h>

namespace simpleMC {

namespace accs {

namespace detail {

/**
 * @brief Checks the validity of levels of an autocorrelation accumulator.
 */
template <typename T>
void check_levels(std::vector<blockvar_acc<T>>,
                  typename autocorr_acc<T>::count_type,
                  typename autocorr_acc<T>::count_type,
                  typename autocorr_acc<T>::count_type);

}

/**
 * @brief Autocorrelation accumulator.
 *
 * @details Defines an accumulator for calculating autocorrelation times. Data
 * is therefore accumulated into blocked variance accumulators of different
 * sizes.The autocorrelation time is obtained from:
 *      1 + 2 * tau_int = var(n)/var(1),
 * where var(n) is the blocked variance for block size n.
 *
 * @tparam T Value type
 */
template <typename T>
class autocorr_acc {
public:
    using traits = acc_traits<T>;
    using mean_type = typename traits::mean_type;
    using variance_type = typename traits::variance_type;
    using value_type = typename traits::value_type;
    using count_type = typename traits::count_type;
    using blockvar_type = blockvar_acc<T>;
    using tau_type = typename traits::tau_type;

public:

    /* Minimum number of samples needed to be considered for error analysis */
    static constexpr count_type default_min_samples = 1024;

    /* Construct an autocorr_acc */
    autocorr_acc(std::size_t size = 1, count_type bl_size = 1,
                 count_type granularity = 2);

    /* Set levels, granularity and count (intended for serialization and
     * MPI) */
    void set(const std::vector<blockvar_type>&, count_type, count_type,
             count_type);

    /* Get size */
    std::size_t size() const { return levels_[0].size(); }

    /* Get block size */
    count_type block_size() const { return levels_[0].block_size(); }

    /* Get granularity */
    count_type granularity() const { return granularity_; }

    /* Get number of current active levels */
    std::size_t num_levels() const { return levels_.size(); }

    /* Get ith level */
    const auto& level(std::size_t i) const { return levels_[i]; }

    /* Get all levels */
    const auto& levels() const { return levels_; }

    /* Get total count */
    count_type count() const { return count_; }

    /* Get current block size */
    count_type current_size() const { return current_; }

    /* Resize data, set everything to zero and change to accumulating state */
    void reset(std::size_t, count_type, count_type);

    void reset() { reset(size(), block_size(), granularity_); }

    /* Is data in accumulating or result state */
    bool is_result() const { return levels_[0].is_result(); }

    /* Accumulate data */
    template <typename U>
    void accumulate(const simpleMC::utils::view<U>&, count_type);

    /* Stream operator for accumulation */
    template <typename U>
    autocorr_acc& operator<<(const U& u) {
        accumulate(simpleMC::utils::make_view(u), /*view_count=*/ 1);
        return *this;
    }

    /* Stream operator to incorporate another accumulator */
    autocorr_acc& operator<<(const autocorr_acc& other);

    /* Subscript operator creates a single-value accumulator */
    auto operator[](std::size_t idx) {
        count_ += 1;
        if (count_ >= current_) {
            add_level();
        }
        return levels_[0].make_sva(idx, levels_.data() + 1);
    }

    /* Create a single-value accumulator for the current accumulator */
    auto make_sva(std::size_t idx = 0) {
        count_ += 1;
        if (count_ >= current_) {
            add_level();
        }
        return levels_[0].make_sva(idx, levels_.data() + 1);
    }

    /* Calculate and get mean */
    mean_type mean() const;

    /* Calculate and get variance */
    variance_type variance(count_type min = default_min_samples) const;

    /* Calculate and get variance of mean */
    variance_type variance_of_mean(count_type min = default_min_samples) const;

    /* Calculate and get std. error */
    variance_type stderror(count_type min = default_min_samples) const;

    /* Calculate and get std. error of mean */
    variance_type stderror_of_mean(count_type min = default_min_samples) const;

    /* Calculatea and get autocorrelation time */
    tau_type tau(count_type min = default_min_samples) const;

    /* Convert to the result state */
    void convert_to_result();

    /* Convert to the accumulating state */
    void convert_to_acc();

    /* Find the highest level with at least min samples */
    std::size_t find_level(count_type) const;

    /* Resize levels */
    void resize_levels(std::size_t lvl);

private:

    /* Add block average to data */
    void add_level() {
        current_ *= granularity_;
        levels_.push_back(blockvar_type(size(), current_));
    }

private:
    std::vector<blockvar_type> levels_;
    count_type granularity_;
    count_type count_;
    count_type current_;
};

/* Accumulate data */
template <typename T>
template <typename U>
void autocorr_acc<T>::accumulate(const simpleMC::utils::view<U>& view,
                                 count_type view_count) {
    // add next level if necessary
    count_ += view_count;
    if (count_ >= current_) {
        add_level();
    }

    // accumulate and propagate
    levels_[0].accumulate(view, view_count, levels_.data() + 1);
}

extern template class autocorr_acc<double>;
extern template class autocorr_acc<std::complex<double>>;

namespace detail {

extern template void check_levels(std::vector<blockvar_acc<double>>,
typename autocorr_acc<double>::count_type,
typename autocorr_acc<double>::count_type,
typename autocorr_acc<double>::count_type);
extern template void check_levels(
std::vector<blockvar_acc<std::complex<double>>>,
typename autocorr_acc<std::complex<double>>::count_type,
typename autocorr_acc<std::complex<double>>::count_type,
typename autocorr_acc<std::complex<double>>::count_type);

} // namespace detail

/**
 * @brief Compare two autocorrelation accumulators.
 */
template <typename T>
bool operator==(const autocorr_acc<T>&, const autocorr_acc<T>&);

template <typename T>
bool operator!=(const autocorr_acc<T>&, const autocorr_acc<T>&);

extern template bool operator==(const autocorr_acc<double>&,
const autocorr_acc<double>&);
extern template bool operator!=(const autocorr_acc<double>&,
const autocorr_acc<double>&);
extern template bool operator==(const autocorr_acc<std::complex<double>>&,
const autocorr_acc<std::complex<double>>&);
extern template bool operator!=(const autocorr_acc<std::complex<double>>&,
const autocorr_acc<std::complex<double>>&);

/**
 * @brief Write autocorrelation accumulator to output stream.
 */
template <typename T>
std::ostream& operator<<(std::ostream&, const autocorr_acc<T>&);

extern template std::ostream& operator<<(std::ostream&,
const autocorr_acc<double>&);
extern template std::ostream& operator<<(std::ostream&,
const autocorr_acc<std::complex<double>>&);

/**
 * @brief Write autocorrelation accumulator to file (for plotting purposes).
 */
template <typename T>
void acc_to_file(const autocorr_acc<T>& acc,
                 const std::string& filename, const std::string& taufile = "",
                 std::uint64_t minsam = autocorr_acc<T>::default_min_samples,
                 std::ios_base::openmode mode = std::ios_base::out,
                 bool header = true);

extern template void acc_to_file(const autocorr_acc<double>&,
const std::string&, const std::string&, std::uint64_t,
std::ios_base::openmode, bool);
extern template void acc_to_file(const autocorr_acc<std::complex<double>>&,
const std::string&, const std::string&, std::uint64_t,
std::ios_base::openmode, bool);

/**
 * @brief Serialize autocorrelation accumulator to Json.
 */
template <typename T>
void to_json(nlohmann::json&, const autocorr_acc<T>&);

extern template void to_json(nlohmann::json&, const autocorr_acc<double>&);
extern template void to_json(nlohmann::json&,
const autocorr_acc<std::complex<double>>&);

/**
 * @brief Deserialize autocorrelation accumulator from Json.
 */
template <typename T>
void from_json(const nlohmann::json&, autocorr_acc<T>&);

extern template void from_json(const nlohmann::json&, autocorr_acc<double>&);
extern template void from_json(const nlohmann::json&,
autocorr_acc<std::complex<double>>&);

#ifdef SIMPLEMC_HAS_HDF5
/**
 * @brief Serialize autocorrelation accumulator to HDF5.
 */
template <typename T>
void to_hdf5(simpleMC::hdf5::hdf5IO&, const autocorr_acc<T>&);

extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const autocorr_acc<double>&);
extern template void to_hdf5(simpleMC::hdf5::hdf5IO&,
const autocorr_acc<std::complex<double>>&);

/**
 * @brief Deserialize autocorrelation accumulator from HDF5.
 */
template <typename T>
void from_hdf5(const simpleMC::hdf5::hdf5IO&, autocorr_acc<T>&);

extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
autocorr_acc<double>&);
extern template void from_hdf5(const simpleMC::hdf5::hdf5IO&,
autocorr_acc<std::complex<double>>&);
#endif

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief MPI reduce for autocorrelation accumulators.
 */
template <typename T>
void reduce(const autocorr_acc<T>& acc, autocorr_acc<T>& res,
            simpleMC::mpi::communicator comm, bool all = true,
            int root = 0);

extern template void reduce(const autocorr_acc<double>&,
autocorr_acc<double>&, simpleMC::mpi::communicator, bool, int);
extern template void reduce(const autocorr_acc<std::complex<double>>&,
autocorr_acc<std::complex<double>>&, simpleMC::mpi::communicator,
bool, int);
#endif

} // namespace accs

} // namespace simpleMC
