/**
 * @file    hdf5IO.cpp
 * @brief   Wrapper for the HDF5 and HighFive library.
 * @author  Thomas Hahn
 */

#include <simpleMC/hdf5/hdf5IO.h>

namespace simpleMC {

namespace hdf5 {

/* Constructs a hdf5IO object by opening/creating a HDF5 file with certain
 * access flags */
hdf5IO::hdf5IO(const std::string& fname, unsigned flags,
               const HighFive::FileAccessProps& fap) :
    file_(fname, flags, fap),
    path_(std::string("/"))
{}

/* Copy hdf5IO object and concatenate path to it */
hdf5IO hdf5IO::copy_and_concat(const hdf5IO& obj, const std::string& path) const {
    hdf5IO copy = obj;
    copy.path_ += delim + path;
    return copy;
}

/* Check if path points to a group, if not create it recursively */
HighFive::Group hdf5IO::check_group(const std::string& path) {
    using namespace simpleMC::utils;
    auto groups = split_at_string(path, delim);
    auto current = file_.getGroup("/");
    for (const auto& gr : groups) {
        if (current.exist(gr)) {
            current = current.getGroup(gr);
        } else {
            current = current.createGroup(gr);
        }
    }
    return current;
}

/* Write std::complex<double> to HDF5 file */
void to_hdf5(hdf5IO& hio, const std::complex<double>& z) {
    using namespace HighFive;
    auto dt = create_datatype<std::complex<double>>();
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write_raw(&z, dt);
    } else {
        auto ds = hio.get().createDataSet(hio.path(), DataSpace{1}, dt);
        ds.write_raw(&z, dt);
    }
}

/* Write std::vector<std::complex<double>> to HDF5 file */
void to_hdf5(hdf5IO& hio, const std::vector<std::complex<double>>& vec) {
    using namespace HighFive;
    using c_t = std::complex<double>;
    auto dt = create_datatype<c_t>();
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write_raw(vec.data(), dt);
    } else {
        auto size = vec.size();
        auto ds = hio.get().createDataSet(hio.path(), DataSpace{size}, dt);
        ds.write_raw(vec.data(), dt);
    }
}

/* Read std::complex<double> from HDF5 file */
void from_hdf5(const hdf5IO& hio, std::complex<double>& z) {
    using namespace HighFive;
    auto dt = create_datatype<std::complex<double>>();
    auto ds = hio.get().getDataSet(hio.path());
    ds.read(&z, dt);
}

/* Read std::vector<std::complex<double>> from HDF5 file */
void from_hdf5(const hdf5IO& hio, std::vector<std::complex<double>>& vec) {
    using namespace HighFive;
    auto dt = create_datatype<std::complex<double>>();
    auto ds = hio.get().getDataSet(hio.path());
    vec.resize(ds.getElementCount());
    ds.read(vec.data(), dt);
}

} // namespace hdf5

} // namespace simpleMC
