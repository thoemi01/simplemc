/**
 * @file    h5IO.cpp
 * @brief   Write/Read objects from simpleMC-utils library to/from HDF5.
 * @author  Thomas Hahn
 */

#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_grids.h>
#include <simpleMC/hdf5/hdf5IO_random.h>
#include <simpleMC/hdf5/hdf5IO_walker_alias.h>

namespace simpleMC {

namespace hdf5 {

/* Write linear grid to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::linear_grid& lg) {
    hio["start"] << lg.start();
    hio["stop"] << lg.stop();
    hio["numpoints"] << lg.numpoints();
}

/* Read linear grid from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::linear_grid& lg) {
    using value_type = simpleMC::utils::linear_grid::value_type;
    using index_type = simpleMC::utils::linear_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    hio["start"] >> start;
    hio["stop"] >> stop;
    hio["numpoints"] >> numpoints;
    lg.set(start, stop, numpoints);
}

/* Write power grid to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::power_grid& pg) {
    hio["start"] << pg.start();
    hio["stop"] << pg.stop();
    hio["numpoints"] << pg.numpoints();
    hio["power"] << pg.power();
}

/* Read power grid from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::power_grid& pg) {
    using value_type = simpleMC::utils::power_grid::value_type;
    using index_type = simpleMC::utils::power_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    std::size_t power;
    hio["start"] >> start;
    hio["stop"] >> stop;
    hio["numpoints"] >> numpoints;
    hio["power"] >> power;
    pg.set(start, stop, numpoints, power);
}

/* Write symmetric grid to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::symmetric_grid& sg) {
    hio["start"] << sg.start();
    hio["stop"] << sg.stop();
    hio["numpoints"] << sg.numpoints();
    hio["power"] << sg.power();
}

/* Read symmetric grid from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::symmetric_grid& sg) {
    using value_type = simpleMC::utils::power_grid::value_type;
    using index_type = simpleMC::utils::power_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    std::size_t power;
    hio["start"] >> start;
    hio["stop"] >> stop;
    hio["numpoints"] >> numpoints;
    hio["power"] >> power;
    sg.set(start, stop, numpoints, power);
}

/* Write index grid to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::index_grid& ig) {
    hio["numpoints"] << ig.numpoints();
}

/* Read index grid from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::index_grid& ig) {
    using index_type = simpleMC::utils::index_grid::index_type;
    index_type numpoints;
    hio["numpoints"] >> numpoints;
    ig.set(numpoints);
}

/* Write splitmix64 PRNG to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::splitmix64& sm64) {
    hio["state"] << sm64.state();
}

/* Read splitmix64 PRNG from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::splitmix64& sm64) {
    using namespace simpleMC::utils;
    splitmix64::state_type state;
    hio["state"] >> state;
    sm64.seed(state);
}

/* Write uniform real distribution to HDF5 file */
void to_hdf5(hdf5IO& hio,
             const simpleMC::utils::uniform_real_distribution& urd) {
    hio["min"] << urd.min();
    hio["max"] << urd.max();
}

/* Write variate_generator to HDF5 file */
void from_hdf5(const hdf5IO& hio,
               simpleMC::utils::uniform_real_distribution& urd) {
    using namespace simpleMC::utils;
    double min, max;
    hio["min"] >> min;
    hio["max"] >> max;
    urd.param(uniform_real_distribution::param_type{min, max});
}

/* Write Walker-Alias to HDF5 file */
void to_hdf5(hdf5IO& hio, const simpleMC::utils::walker_alias& wa) {
    hio["weights"] << wa.weights();
    hio["probs"] << wa.probabilities();
    hio["alias"] << wa.alias();
}

/* Read Walker-Alias from HDF5 file */
void from_hdf5(const hdf5IO& hio, simpleMC::utils::walker_alias& wa) {
    std::vector<double> weights, probs;
    std::vector<std::size_t> alias;
    hio["weights"] >> weights;
    hio["probs"] >> probs;
    hio["alias"] >> alias;
    wa.set(weights, probs, alias);
}

} // namespace hdf5

} // namespace simpleMC
