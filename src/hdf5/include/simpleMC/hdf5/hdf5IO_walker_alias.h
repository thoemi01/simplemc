/**
 * @file    hdf5IO_grids.h
 * @brief   Write/Read walker_alias objects to/from HDF5.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/walker_alias.h>
#include <simpleMC/hdf5/hdf5IO.h>

namespace simpleMC {

namespace hdf5 {

/**
 * @brief Write walker_alias to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param wa walker_alias object
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::walker_alias&);

/**
 * @brief Read walker_alias from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param wa walker_alias object
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::walker_alias&);

} // namespace hdf5

} // namespace simpleMC
