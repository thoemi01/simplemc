/**
 * @file    hdf5IO_grids.h
 * @brief   Read and write grids to HDF5 file.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/grids.h>
#include <simpleMC/hdf5/hdf5IO.h>

namespace simpleMC {

namespace hdf5 {

/**
 * @brief Write linear grid to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param lg Linear grid
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::linear_grid&);

/**
 * @brief Read linear grid from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param lg Linear grid
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::linear_grid&);

/**
 * @brief Write power grid to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param pg Power grid
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::power_grid&);

/**
 * @brief Read power grid from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param pg Power grid
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::power_grid&);

/**
 * @brief Write symmetric grid to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param sg Symmetric grid
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::symmetric_grid&);

/**
 * @brief Read symmetric grid from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param sg Symmetric grid
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::symmetric_grid&);

/**
 * @brief Write index grid to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param lg Index grid
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::index_grid&);

/**
 * @brief Read index grid from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param lg Index grid
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::index_grid&);

/**
 * @brief Write N-dimensional grid to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param ndg N-dimensional grid
 */
template <typename Index, typename... Gs>
void to_hdf5(hdf5IO& hio, const simpleMC::utils::nd_grid<Index, Gs...>& ndg) {
    using namespace simpleMC::utils;
    int i = 0;
    for_each_tuple([&i, &hio](auto&& grid) {
        std::string path = std::to_string(i++);
        hio[path] << grid;
    }, ndg.get_grids());
}

/**
 * @brief Read N-dimensional grid from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param ndg N-dimensional grid
 */
template <typename Index, typename... Gs>
void from_hdf5(const hdf5IO& hio, simpleMC::utils::nd_grid<Index, Gs...>& ndg) {
    using namespace simpleMC::utils;
    int i = 0;
    for_each_tuple([&i, &hio](auto&& grid) {
        std::string path = std::to_string(i++);
        hio[path] >> grid;
    }, ndg.get_grids());
}

} // namespace hdf5

} // namespace simpleMC
