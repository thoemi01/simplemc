/**
 * @file    hdf5IO_eigen.h
 * @brief   Read and write Eigen vector, matrices, arrays to HDF5 file.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/traits_eigen.h>
#include <simpleMC/hdf5/hdf5IO.h>

namespace simpleMC {

namespace hdf5 {

namespace detail {

/* Declare Eigen types as native HDF5 type */
template <typename T>
struct is_native_hdf5<T, typename std::enable_if<
        simpleMC::utils::is_eigen_v<T>>::type> : public std::true_type {};

} // namespace detail

/**
 * @brief Write Eigen type to HDF5 file.
 *
 * @details Write Eigen::DenseBase<D> or Eigen::TensorBase<D> derived
 * objects to HDF5 file. Any object is written as a one dimensional array.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param derived Object derived from Eigen::DenseBase<D> or
 * Eigen::TensorBase<D>
 * @tparam D Derived type from Eigen::DenseBase<D>
 */
template <typename D>
typename std::enable_if<simpleMC::utils::is_eigen_v<D>>::type
to_hdf5(hdf5IO& hio, const D& derived) {
    using namespace HighFive;
    using scalar_type = typename D::Scalar;
    auto dt = create_datatype<scalar_type>();
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write_raw(derived.data(), dt);
    } else {
        auto size = static_cast<std::size_t>(derived.size());
        auto ds = hio.get().createDataSet(hio.path(), DataSpace{size}, dt);
        ds.write_raw(derived.data(), dt);
    }
}

/**
 * @brief Read Eigen type from HDF5 file.
 *
 * @details Read Eigen objects from HDF5 file. No checks are performed. The
 * object that is read into should be of the same type as the object which has
 * been written.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param derived Object derived from Eigen::DenseBase<D> or
 * Eigen::TensorBase<D>
 * @tparam D Derived type from Eigen::DenseBase<D> or Eigen::TensorBase<D>
 */
template <typename D>
typename std::enable_if<simpleMC::utils::is_eigen_v<D>>::type
from_hdf5(const hdf5IO& hio, D& derived) {
    using namespace HighFive;
    using scalar_type = typename D::Scalar;
    auto dt = create_datatype<scalar_type>();
    auto ds = hio.get().getDataSet(hio.path());
    ds.read(derived.data(), dt);
}

} // namespace hdf5

} // namespace simpleMC
