/**
 * @file    hdf5IO_random.h
 * @brief   Read and write RNGs and distributions HDF5 file.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/random.h>
#include <simpleMC/hdf5/hdf5IO.h>

namespace simpleMC {

namespace hdf5 {

/**
 * @brief Write splitmix64 PRNG to HDF5 file.
 *
 * @param hio hdf5IO object
 * @param sm64 Splitmix64 PRNG
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::splitmix64&);

/**
 * @brief Read splitmix64 PRNG from HDF5 file.
 *
 * @param hio hdf5IO object
 * @param sm64 Splitmix64 PRNG
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::splitmix64&);

/**
 * @brief Write xoshiro256 PRNG to HDF5 file.
 *
 * @param hio hdf5IO object
 * @param xo256 xoshiro256 PRNG
 * @tparam X Xoshiro_type
 */
template <simpleMC::utils::Xoshiro_type X>
void to_hdf5(hdf5IO& hio, const simpleMC::utils::xoshiro256<X>& xo256) {
    hio["state"] << xo256.state();
}

/**
 * @brief Read xoshiro256 PRNG from HDF5 file.
 *
 * @param hio hdf5IO object
 * @param xo256 xoshiro256 PRNG
 * @tparam X Xoshiro_type
 */
template <simpleMC::utils::Xoshiro_type X>
void from_hdf5(const hdf5IO& hio, simpleMC::utils::xoshiro256<X>& xo256) {
    using namespace HighFive;
    using namespace simpleMC::utils;
    typename xoshiro256<X>::array_type state;
    hio["state"] >> state;
    xo256.seed(state[0], state[1], state[2], state[3]);
}

/**
 * @brief Write uniform real distribution to HDF5 file.
 *
 * @param hio hdf5IO object
 * @param urd Uniform real distribution
 */
void to_hdf5(hdf5IO&, const simpleMC::utils::uniform_real_distribution&);

/**
 * @brief Read uniform real distribution  from HDF5 file.
 *
 * @param hio hdf5IO object
 * @param urd Uniform real distribution
 */
void from_hdf5(const hdf5IO&, simpleMC::utils::uniform_real_distribution&);

/**
 * @brief Write variate_generator to HDF5 file.
 *
 * @param hio hdf5IO object
 * @param vg variate_generator
 * @tparam E Engine type
 * @tparam D distribution type
 */
template <typename E, typename D>
void to_hdf5(hdf5IO& hio,
             const simpleMC::utils::variate_generator<E, D>& vg) {
    hio["eng"] << vg.engine();
    hio["dist"] << vg.distribution();
}

/**
 * @brief Read variate_generator from HDF5 file.
 *
 * @param hio hdf5IO object
 * @param vg variate_generator
 * @tparam E Engine type
 * @tparam D distribution type
 */
template <typename E, typename D>
void from_hdf5(const hdf5IO& hio,
               simpleMC::utils::variate_generator<E, D>& vg) {
    hio["eng"] >> vg.engine();
    hio["dist"] >> vg.distribution();
}

} // namespace hdf5

} // namespace simpleMC
