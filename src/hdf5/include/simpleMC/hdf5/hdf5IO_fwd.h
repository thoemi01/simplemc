/**
 * @file    hdf5IO_fwd.h
 * @brief   Forward declarations for HDF5 IO utils.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleMC {

namespace hdf5 {

class hdf5IO;

} // namespace hdf5

} // namespace simpleMC
