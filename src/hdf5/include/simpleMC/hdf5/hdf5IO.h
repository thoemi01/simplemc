/**
 * @file    hdf5IO.h
 * @brief   Wrapper for the HDF5 and HighFive library.
 * @author  Thomas Hahn
 */

#pragma once

#include <exception>
#include <highfive/H5File.hpp>
#include <highfive/H5DataSpace.hpp>
#include <simpleMC/utils/string.h>
#include <simpleMC/utils/traits.h>

namespace simpleMC {

namespace hdf5 {

namespace detail {

/**
 * @brief Traits class to indicate native scalar types supported
 * by hdf5IO.
 */
template <typename T>
struct is_native_scalar_hdf5 : public std::false_type {};

/**
 * @brief Traits class to indicate native vector types supported
 * by hdf5IO.
 */
template <typename T>
struct is_native_vector_hdf5 : public std::false_type {};

/**
 * @brief Traits class to indicate native array types supported
 * by hdf5IO.
 */
template <typename T>
struct is_native_array_hdf5 : public std::false_type {};

/* Supported native scalar types */
#define FOREACH_NATIVE_SCALAR(CALLBACK) \
    CALLBACK(char) \
    CALLBACK(signed char) \
    CALLBACK(unsigned char) \
    CALLBACK(short) \
    CALLBACK(unsigned short) \
    CALLBACK(int) \
    CALLBACK(unsigned int) \
    CALLBACK(long) \
    CALLBACK(unsigned long) \
    CALLBACK(long long) \
    CALLBACK(unsigned long long) \
    CALLBACK(float) \
    CALLBACK(double) \
    CALLBACK(bool) \
    CALLBACK(std::string) \
    CALLBACK(std::complex<double>)

/* Supported are all scalar types T (see above) as well as std::vector<T> and
 * std::array<T, N> */
#define IS_NATIVE(__type__) \
    template <> \
    struct is_native_scalar_hdf5<__type__> : std::true_type {}; \
    template <typename A> \
    struct is_native_vector_hdf5<std::vector<__type__, A>> : std::true_type {}; \
    template <std::size_t N> \
    struct is_native_array_hdf5<std::array<__type__, N>> : std::true_type{};

FOREACH_NATIVE_SCALAR(IS_NATIVE)

#undef IS_NATIVE
#undef FOREACH_SUPPORTED

/**
 * @brief Traits class to indicate native types. Native types are written
 * directly into HDF5 datasets, whereas non-native types are written into groups.
 */
template <typename T, typename = void>
struct is_native_hdf5 : public std::false_type {};

template <typename T>
struct is_native_hdf5<T, typename std::enable_if<
        is_native_scalar_hdf5<T>::value ||
        is_native_vector_hdf5<T>::value ||
        is_native_array_hdf5<T>::value>::type> : public std::true_type {};

} // namespace detail

/**
 * @brief Wrapper around HighFive and HDF5 library.
 *
 * @details
 * This is a simple wrapper class around the HighFive and HDF5 library.
 * It should simplify writing/reading HDF5 files. This class
 * does not support all the functionality from the HighFive or HDF5 library.
 * If more functionality is needed, one can use the underlying HighFive::File
 * object directly by calling the get() function.
 */
class hdf5IO {
public:
    using File = HighFive::File;
    using Group = HighFive::Group;

public:

    /* Constructs a hdf5IO object by opening/creating a HDF5 file with certain
     * access flags */
    explicit hdf5IO(const std::string&,
                    unsigned = File::ReadWrite | File::Create | File::Truncate,
                    const HighFive::FileAccessProps& = HighFive::FileDriver());

    /* Get a reference to the underlying HighFive::File object */
    File& get() { return file_; }

    const File& get() const { return file_; }

    /* Flushes all buffers associated with the HDF5 file */
    void dump_to_file() { file_.flush(); }

    /* Get file name */
    const std::string& name() const { return file_.getName(); }

    /* Get ID of the HDF5 file */
    hid_t id() const { return file_.getId(); }

    /* Get current path in the file structure */
    const std::string& path() const { return path_; }

    /* Returns a copy of the current this object but with the
     * paths concatenated */
    hdf5IO operator[](const std::string& path) {
        return copy_and_concat(*this, path);
    }

    const hdf5IO operator[](const std::string& path) const {
        return copy_and_concat(*this, path);
    }

    /* Output stream operator */
    template <typename T>
    hdf5IO& operator<<(const T&);

    /* Input stream operator */
    template <typename T>
    const hdf5IO& operator>>(T&) const;

private:

    /* Copy hdf5IO object and concatenate path to it */
    hdf5IO copy_and_concat(const hdf5IO&, const std::string&) const;

    /* Check if path points to a group, if not create it recursively */
    HighFive::Group check_group(const std::string&);

private:
    File file_;
    std::string path_;
    const std::string delim = "/";
};

/* Output stream operators */
template <typename T>
hdf5IO& hdf5IO::operator<<(const T& t) {
    using namespace simpleMC::utils;
    if constexpr(detail::is_native_hdf5<T>::value) {
        auto base = get_base(path_, delim);
        check_group(base);
        to_hdf5(*this, t);
    } else {
        check_group(path_);
        to_hdf5(*this, t);
    }
    return *this;
}

/* Input stream operator */
template <typename T>
const hdf5IO& hdf5IO::operator>>(T& t) const {
    using namespace simpleMC::utils;
    if (file_.exist(path_)) {
        from_hdf5(*this, t);
    } else {
        throw std::invalid_argument("Reading from HDF5 file: Path does not"
                                    " exist: " + path_);
    }
    return *this;
}

/**
 * @brief Write supported scalar types to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param t Scalar to write
 * @tparam T Scalar type
 */
template <typename T>
typename std::enable_if<detail::is_native_scalar_hdf5<T>::value>::type
to_hdf5(hdf5IO& hio, const T& t) {
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write(t);
    } else {
        hio.get().createDataSet(hio.path(), t);
    }
}

/**
 * @brief Write std::complex<double> to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param z Complex number to be written
 */
void to_hdf5(hdf5IO&, const std::complex<double>&);

/**
 * @brief Write std::vector<T> of supported scalar types T to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param vec Vector to write
 * @tparam T Scalar type
 */
template <typename T>
typename std::enable_if<detail::is_native_vector_hdf5<T>::value>::type
to_hdf5(hdf5IO& hio, const T& vec) {
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write(vec);
    } else {
        using HighFive::DataSpace;
        using value_type = typename T::value_type;
        auto size = vec.size();
        auto ds = hio.get().createDataSet<value_type>(hio.path(),
                                                      DataSpace{size});
        ds.write(vec);
    }
}

/**
 * @brief Write std::vector<std::complex<double>> to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param vec Vector of complex numbers to be written
 */
void to_hdf5(hdf5IO&, const std::vector<std::complex<double>>&);

/**
 * @brief Write std::array<T> of supported scalar types T to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param arr Array to write
 * @tparam T Scalar type
 * @tparam N Size of array
 */
template <typename T>
typename std::enable_if<detail::is_native_array_hdf5<T>::value>::type
to_hdf5(hdf5IO& hio, const T& arr) {
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write(arr);
    } else {
        using HighFive::DataSpace;
        using value_type = typename T::value_type;
        auto size = arr.size();
        auto ds = hio.get().createDataSet<value_type>(hio.path(),
                                                      DataSpace{size});
        ds.write(arr);
    }
}

/**
 * @brief Write std::array<std::complex<double>, N> to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param arr Array of complex numbers to be written
 * @tparam N Size of array
 */
template <std::size_t N>
void to_hdf5(hdf5IO& hio, const std::array<std::complex<double>, N>& arr) {
    using namespace HighFive;
    using c_t = std::complex<double>;
    auto dt = create_datatype<c_t>();
    if (hio.get().exist(hio.path())) {
        auto ds = hio.get().getDataSet(hio.path());
        ds.write_raw(arr.data(), dt);
    } else {
        auto size = arr.size();
        auto ds = hio.get().createDataSet(hio.path(), DataSpace{size}, dt);
        ds.write_raw(arr.data(), dt);
    }
}

/**
 * @brief Write std::array<std::string, N> to HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param arr Array of strings to be written
 * @tparam N Size of array
 */
template <std::size_t N>
void to_hdf5(hdf5IO& hio, const std::array<std::string, N>& arr) {
    std::vector<std::string> vec(arr.begin(), arr.end());
    to_hdf5(hio, vec);
}

/**
 * @brief Read supported scalar types T, std::vector<T> and std::array<T>
 * from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param t Object to read
 * @tparam T Type of the object to be read
 */
template <typename T>
typename std::enable_if<detail::is_native_scalar_hdf5<T>::value ||
                        detail::is_native_vector_hdf5<T>::value ||
                        detail::is_native_array_hdf5<T>::value>::type
from_hdf5(const hdf5IO& hio, T& t) {
    auto ds = hio.get().getDataSet(hio.path());
    ds.read(t);
}

/**
 * @brief Read std::complex<double> from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param z Complex number
 */
void from_hdf5(const hdf5IO&, std::complex<double>&);

/**
 * @brief Read std::vector<std::complex<double>> from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param vec Vector of complex numbers
 */
void from_hdf5(const hdf5IO&, std::vector<std::complex<double>>&);

/**
 * @brief Read std::array<std::complex<double>, N> from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param arr Array of complex numbers
 * @tparam N Size of array
 */
template <std::size_t N>
void from_hdf5(const hdf5IO& hio, std::array<std::complex<double>, N>& arr) {
    using namespace HighFive;
    auto dt = create_datatype<std::complex<double>>();
    auto ds = hio.get().getDataSet(hio.path());
    ds.read(arr.data(), dt);
}

/**
 * @brief Read std::array<std::string, N> from HDF5 file.
 *
 * @param hio hdf5IO object which holds the HDF5 file
 * @param arr Array of strings
 * @tparam N Size of array
 */
template <std::size_t N>
void from_hdf5(const hdf5IO& hio, std::array<std::string, N>& arr) {
    std::vector<std::string> vec;
    from_hdf5(hio, vec);
    std::copy(vec.begin(), vec.end(), arr.begin());
}

} // namespace hdf5

} // namespace simpleMC
