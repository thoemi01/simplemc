/**
 * @file    jsonIO.cpp
 * @brief   Wrapper for the nlohmann-json library.
 * @author  Thomas Hahn
 */

#include <fstream>
#include <iomanip>
#include <simpleMC/utils/string.h>
#include <simpleMC/json/jsonIO.h>

namespace simpleMC {

namespace json {

/* Constructs a default jsonIO object */
jsonIO::jsonIO() :
    jptr_(new json),
    path_(std::string("/"))
{}

/* Get json_pointer to the current path */
nlohmann::json_pointer<nlohmann::json>
jsonIO::current_json_pointer() const {
    auto fixed_path = fix_path(path_);
    if (fixed_path == delim) {
        fixed_path = std::string();
    }
    return json_pointer(fixed_path);
}

/* Write json object to file */
void jsonIO::dump_to_file(const std::string& filename,
                          std::ios_base::openmode mode,
                          jsonIO_mode jmode) const {
    using detail::check_ios_binary;
    using nlohmann::detail::output_adapter;
    std::ofstream os(filename, mode);
    if (jmode == jsonIO_mode::text && !check_ios_binary(mode)) {
        os << *jptr_;
    } else if (jmode == jsonIO_mode::text2 && !check_ios_binary(mode)) {
        os << std::setw(2) << *jptr_;
    } else if (jmode == jsonIO_mode::text4 && !check_ios_binary(mode)) {
        os << std::setw(4) << *jptr_;
    } else if (jmode == jsonIO_mode::bson && check_ios_binary(mode)) {
        json::to_bson(*jptr_, output_adapter<char>(os));
    } else if (jmode == jsonIO_mode::cbor && check_ios_binary(mode)) {
        json::to_cbor(*jptr_, output_adapter<char>(os));
    } else if (jmode == jsonIO_mode::msgpack && check_ios_binary(mode)) {
        json::to_msgpack(*jptr_, output_adapter<char>(os));
    } else if (jmode == jsonIO_mode::ubjson && check_ios_binary(mode)) {
        json::to_ubjson(*jptr_, output_adapter<char>(os));
    } else {
        throw std::invalid_argument("Wrong jsonIO_mode or "
                                    "std::ios_base::openmode");
    }
}

/* Write json object to file */
void jsonIO::dump_to_file(const std::string& filename, jsonIO_mode jmode) const {
    using detail::check_text_mode;
    std::ios_base::openmode om;
    if (check_text_mode(jmode)) {
        om = std::ios_base::out;
    } else {
        om = std::ios_base::binary;
    }
    dump_to_file(filename, om, jmode);
}

/* Read json object from file */
void jsonIO::load_from_file(const std::string& filename,
                            std::ios_base::openmode mode,
                            jsonIO_mode jmode) {
    using detail::check_ios_binary;
    using detail::check_text_mode;
    std::ifstream is(filename, mode);
    if (check_text_mode(jmode) && !check_ios_binary(mode)) {
        is >> *jptr_;
    } else if (jmode == jsonIO_mode::bson && check_ios_binary(mode)) {
        *jptr_ = json::from_bson(is);
    } else if (jmode == jsonIO_mode::cbor && check_ios_binary(mode)) {
        *jptr_ = json::from_cbor(is);
    } else if (jmode == jsonIO_mode::msgpack && check_ios_binary(mode)) {
        *jptr_ = json::from_msgpack(is);
    } else if (jmode == jsonIO_mode::ubjson && check_ios_binary(mode)) {
        *jptr_ = json::from_ubjson(is);
    } else {
        throw std::invalid_argument("Wrong jsonIO_mode or "
                                    "std::ios_base::openmode");
    }
}

/* Read json object from file */
void jsonIO::load_from_file(const std::string& filename, jsonIO_mode jmode) {
    using detail::check_text_mode;
    std::ios_base::openmode om;
    if (check_text_mode(jmode)) {
        om = std::ios_base::in;
    } else {
        om = std::ios_base::binary;
    }
    load_from_file(filename, om, jmode);
}

/* Fixes path so that json_pointers can be used */
std::string jsonIO::fix_path(const std::string& path) const {
    using namespace simpleMC::utils;
    auto groups = split_at_string(path, delim);
    return concat_tokens(groups, delim, delim);
}

} // namespace json

} // namespace simpleMC
