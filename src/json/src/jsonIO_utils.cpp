/**
 * @file    jsonIO_utils.cpp
 * @brief   Read and write grids to json file.
 * @author  Thomas Hahn
 */

#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/json/jsonIO_random.h>
#include <simpleMC/json/jsonIO_walker_alias.h>

namespace simpleMC {

namespace utils {

/* Write linear grid to json file */
void to_json(nlohmann::json& j, const linear_grid& lg) {
    j["start"] = lg.start();
    j["stop"] = lg.stop();
    j["numpoints"] = lg.numpoints();
}

/* Read linear grid from json file */
void from_json(const nlohmann::json& j, linear_grid& lg) {
    using value_type = linear_grid::value_type;
    using index_type = linear_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    j.at("start").get_to(start);
    j.at("stop").get_to(stop);
    j.at("numpoints").get_to(numpoints);
    lg.set(start, stop, numpoints);
}

/* Write power grid to json file */
void to_json(nlohmann::json& j, const power_grid& pg) {
    j["start"] = pg.start();
    j["stop"] = pg.stop();
    j["numpoints"] = pg.numpoints();
    j["power"] = pg.power();
}

/* Read power grid from json file */
void from_json(const nlohmann::json& j, power_grid& pg) {
    using value_type = power_grid::value_type;
    using index_type = power_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    std::size_t power;
    j.at("start").get_to(start);
    j.at("stop").get_to(stop);
    j.at("numpoints").get_to(numpoints);
    j.at("power").get_to(power);
    pg.set(start, stop, numpoints, power);
}

/* Write symmetric grid to json file */
void to_json(nlohmann::json& j, const symmetric_grid& sg) {
    j["start"] = sg.start();
    j["stop"] = sg.stop();
    j["numpoints"] = sg.numpoints();
    j["power"] = sg.power();
}

/* Read symmetric grid from json file */
void from_json(const nlohmann::json& j, symmetric_grid& sg) {
    using value_type = power_grid::value_type;
    using index_type = power_grid::index_type;
    value_type start, stop;
    index_type numpoints;
    std::size_t power;
    j.at("start").get_to(start);
    j.at("stop").get_to(stop);
    j.at("numpoints").get_to(numpoints);
    j.at("power").get_to(power);
    sg.set(start, stop, numpoints, power);
}

/* Write index grid to json file */
void to_json(nlohmann::json& j, const index_grid& ig) {
    j["numpoints"] = ig.numpoints();
}

/* Read index grid from json file */
void from_json(const nlohmann::json& j, index_grid& ig) {
    using index_type = index_grid::index_type;
    index_type numpoints;
    j.at("numpoints").get_to(numpoints);
    ig.set(numpoints);
}

/* Write splitmix64 PRNG to json file */
void to_json(nlohmann::json& j, const splitmix64& sm64) {
    j["state"] = sm64.state();
}

/* Read splitmix64 PRNG from json file */
void from_json(const nlohmann::json& j, splitmix64& sm64) {
    splitmix64::state_type state;
    j.at("state").get_to(state);
    sm64.seed(state);
}

/* Write uniform real distribution to json file */
void to_json(nlohmann::json& j, const uniform_real_distribution& urd) {
    j["min"] = urd.min();
    j["max"] = urd.max();
}

/* Read uniform real distribution  from json file */
void from_json(const nlohmann::json& j, uniform_real_distribution& urd) {
    double min, max;
    j.at("min").get_to(min);
    j.at("max").get_to(max);
    urd.param(uniform_real_distribution::param_type{min, max});
}

/* Write Walker-Alias to json file */
void to_json(nlohmann::json& j, const walker_alias& wa) {
    j["weights"] = wa.weights();
    j["probs"] = wa.probabilities();
    j["alias"] = wa.alias();
}

/* Read Walker-Alias from json file */
void from_json(const nlohmann::json& j, walker_alias& wa) {
    std::vector<double> weights, probs;
    std::vector<std::size_t> alias;
    j.at("weights").get_to(weights);
    j.at("probs").get_to(probs);
    j.at("alias").get_to(alias);
    wa.set(weights, probs, alias);
}

} // namespace utils

} // namespace simpleMC
