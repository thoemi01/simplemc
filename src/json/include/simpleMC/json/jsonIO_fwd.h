/**
 * @file    jsonIO_fwd.h
 * @brief   Forward declarations for jsonIO utils.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleMC {

namespace json {

enum class jsonIO_mode;
class jsonIO;

} // namespace json

} // namespace simpleMC
