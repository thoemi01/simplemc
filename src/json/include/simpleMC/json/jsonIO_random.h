/**
 * @file    jsonIO_random.h
 * @brief   Read and write RNGs and distributions to json file.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json.hpp>
#include <simpleMC/utils/random.h>

namespace simpleMC {

namespace utils {

/**
 * @brief Write splitmix64 PRNG to json file.
 *
 * @param j nlohmann::json object
 * @param sm64 Splitmix64 PRNG
 */
void to_json(nlohmann::json&, const splitmix64&);

/**
 * @brief Read splitmix64 PRNG from json file.
 *
 * @param j nlohmann::json object
 * @param sm64 Splitmix64 PRNG
 */
void from_json(const nlohmann::json&, splitmix64&);

/**
 * @brief Write xoshiro256 PRNG to json file.
 *
 * @param j nlohmann::json object
 * @param xo256 xoshiro256 PRNG
 * @tparam X Xoshiro_type
 */
template <Xoshiro_type X>
void to_json(nlohmann::json& j, const xoshiro256<X>& xo256) {
    j["state"] = xo256.state();
}

/**
 * @brief Read xoshiro256 PRNG from json file.
 *
 * @param j nlohmann::json object
 * @param xo256 xoshiro256 PRNG
 * @tparam X Xoshiro_type
 */
template <Xoshiro_type X>
void from_json(const nlohmann::json& j, xoshiro256<X>& xo256) {
    typename xoshiro256<X>::array_type state;
    j.at("state").get_to(state);
    xo256.seed(state[0], state[1], state[2], state[3]);
}

/**
 * @brief Write uniform real distribution to json file.
 *
 * @param j nlohmann::json object
 * @param urd Uniform real distribution
 */
void to_json(nlohmann::json&, const uniform_real_distribution&);

/**
 * @brief Read uniform real distribution  from json file.
 *
 * @param j nlohmann::json object
 * @param urd Uniform real distribution
 */
void from_json(const nlohmann::json& j, uniform_real_distribution& urd);

/**
 * @brief Write variate_generator to json file.
 *
 * @param j nlohmann::json object
 * @param vg variate_generator
 * @tparam E Engine type
 * @tparam D distribution type
 */
template <typename E, typename D>
void to_json(nlohmann::json& j, const variate_generator<E, D>& vg) {
    j["eng"] = vg.engine();
    j["dist"] = vg.distribution();
}

/**
 * @brief Read variate_generator from json file.
 *
 * @param j nlohmann::json object
 * @param vg variate_generator
 * @tparam E Engine type
 * @tparam D distribution type
 */
template <typename E, typename D>
void from_json(const nlohmann::json& j, variate_generator<E, D>& vg) {
    j.at("eng").get_to(vg.engine());
    j.at("dist").get_to(vg.distribution());
}

} // namespace utils

} // namespace simpleMC
