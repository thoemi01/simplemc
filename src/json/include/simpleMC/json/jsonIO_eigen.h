/**
 * @file    jsonIO_eigen.h
 * @brief   Read and write Eigen vector, matrices, arrays to json file.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json.hpp>
#include <simpleMC/utils/traits_eigen.h>

namespace Eigen {

/**
 * @brief Write Eigen type to json file.
 *
 * @details Write Eigen::DenseBase<D> or Eigen::TensorBase<D> derived objects to
 * json file. Any object is written as a one dimensional array.
 *
 * @param j nlohman::json object
 * @param derived Object derived from Eigen::DenseBase<D> or Eigen::TensorBase<D>
 * @tparam D Derived type from Eigen::DenseBase<D> or Eigen::TensorBase<D>
 */
template <typename D>
typename std::enable_if<simpleMC::utils::is_eigen_v<D>>::type
to_json(nlohmann::json& j, const D& derived) {
    using scalar_type = typename D::Scalar;
    std::vector<scalar_type> vec(derived.data(), derived.data() + derived.size());
    j = vec;
}

/**
 * @brief Read Eigen type from json file.
 *
 * @details Read Eigen objects from json file. No checks are performed. The object
 * that is read into should be of the same type as the object which has been
 * written.
 *
 * @param j nlohmann::json object
 * @param derived Object derived from Eigen::DenseBase<D> or Eigen::TensorBase<D>
 * @tparam D Derived type from Eigen::DenseBase<D> or Eigen::TensorBase<D>
 */
template <typename D>
typename std::enable_if<simpleMC::utils::is_eigen_v<D>>::type
from_json(const nlohmann::json& j, D& derived) {
    using scalar_type = typename D::Scalar;
    std::vector<scalar_type> vec;
    j.get_to(vec);
    if constexpr (simpleMC::utils::is_eigen_dense_v<D>) {
        derived = Eigen::Map<D>(vec.data(), derived.rows(), derived.cols());
    } else {
        derived = Eigen::TensorMap<D>(vec.data(), derived.dimensions());
    }
}

} // namespace Eigen
