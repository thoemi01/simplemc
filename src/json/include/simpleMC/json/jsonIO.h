/**
 * @file    jsonIO.h
 * @brief   Wrapper for the nlohmann-json library.
 * @author  Thomas Hahn
 */

#pragma once

#include <ios>
#include <memory>
#include <complex>
#include <nlohmann/json.hpp>

namespace simpleMC {

namespace json {

/**
 * @brief Enum class for different output strategies.
 */
enum class jsonIO_mode {
    text,
    text2,
    text4,
    bson,
    cbor,
    msgpack,
    ubjson
};

namespace detail {

/**
 * @brief Checks if file is opened in binary mode.
 */
inline bool check_ios_binary(std::ios_base::openmode mode) {
    return (mode & std::ios::binary);
}

/**
 * @brief Checks if jsonIO_mode is some text.
 */
inline bool check_text_mode(jsonIO_mode jmode) {
    return (jmode == jsonIO_mode::text ||
            jmode == jsonIO_mode::text2 ||
            jmode == jsonIO_mode::text4);
}

} // namespace detail

/**
 * @brief Wrapper around nlohmann-json library.
 *
 * @details
 * This is a simple wrapper class around the nlohmann-json library. It should
 * simplify writing/reading json files. This class does not support
 * all the functionality from the nlohmann-json library. If more functionality is
 * needed, one can use the underlying json object directly by calling
 * the get() function.
 */
class jsonIO {
public:
    using json = nlohmann::json;
    using json_pointer = nlohmann::json_pointer<json>;

public:

    /* Constructs a default jsonIO object */
    explicit jsonIO();

    /* Get a reference to the root nlohmann::json object */
    json& get() { return *jptr_; }

    const json& get() const { return *jptr_; }

    /* Get json_pointer to the current path */
    json_pointer current_json_pointer() const;

    /* Get a reference to the current nlohmann::json object */
    json& current_json() {
        return (*jptr_)[current_json_pointer()];
    }

    const json& current_json() const {
        return (*jptr_)[current_json_pointer()];
    }

    json& current_json_at() {
        return (*jptr_).at(current_json_pointer());
    }

    const json& current_json_at() const {
        return (*jptr_).at(current_json_pointer());
    }

    /* Convert to and from strings */
    std::string to_string() const {
        return jptr_->dump();
    }

    void from_string(const std::string& str) {
        *jptr_ = json::parse(str);
    }

    /* Write json object to file */
    void dump_to_file(const std::string& filename,
                      std::ios_base::openmode mode = std::ios_base::out,
                      jsonIO_mode jmode = jsonIO_mode::text) const;

    void dump_to_file(const std::string& filename, jsonIO_mode jmode) const;

    /* Read json object from file */
    void load_from_file(const std::string& filename,
                        std::ios_base::openmode mode = std::ios_base::in,
                        jsonIO_mode jmode = jsonIO_mode::text);

    void load_from_file(const std::string& filename, jsonIO_mode jmode);

    /* Get current path in the json structure */
    const std::string& path() const { return path_; }

    /* Returns a copy of the current this object but with the paths
     * concatenated */
    jsonIO operator[](const std::string& path) {
        return copy_and_concat(*this, path);
    }

    const jsonIO operator[](const std::string& path) const {
        return copy_and_concat(*this, path);
    }

    /* Output stream operator */
    template <typename T>
    jsonIO& operator<<(const T&);

    /* Input stream operator */
    template <typename T>
    const jsonIO& operator>>(T&) const;

    /* Conversion operators to nlohmann::json objects */
    operator json&() { return current_json(); }

    operator const json&() const { return current_json(); }

private:

    /* Copy jsonIO object and concatenate path to it */
    jsonIO copy_and_concat(const jsonIO& obj, const std::string& path) const {
        jsonIO copy = obj;
        copy.path_ += delim + path;
        return copy;
    }

    /* Fixes path so that json_pointers can be used */
    std::string fix_path(const std::string&) const;

private:
    std::shared_ptr<json> jptr_;
    std::string path_;
    const std::string delim = "/";
};

/* Output stream operator */
template <typename T>
jsonIO& jsonIO::operator<<(const T& t) {
    current_json() = t;
    return *this;
}

/* Input stream operator */
template <typename T>
const jsonIO& jsonIO::operator>>(T& t) const {
    current_json_at().get_to(t);
    return *this;
}

} // namespace json

} // namespace simpleMC

namespace nlohmann {

/**
 * @brief Specialization of nlohmann::adl_serializer to read/write
 * std::complex<T> to json file.
 *
 * @tparam T Scalar type
 */
template <typename T>
struct adl_serializer<std::complex<T>> {
    static void to_json(json& j, const std::complex<T>& z) {
        std::array<T, 2> z_arr{z.real(), z.imag()};
        j = z_arr;
    }

    static void from_json(const json& j, std::complex<T>& z) {
        std::array<T, 2> z_arr;
        j.get_to(z_arr);
        z.real(z_arr[0]);
        z.imag(z_arr[1]);
    }
};

} // namespace nlohmann
