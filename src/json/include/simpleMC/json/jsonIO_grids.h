/**
 * @file    jsonIO_grids.h
 * @brief   Read and write grids to json file.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json.hpp>
#include <simpleMC/utils/grids.h>

namespace simpleMC {

namespace utils {

/**
 * @brief Write linear grid to json file.
 *
 * @param j nlohmann::json object
 * @param lg Linear grid
 */
void to_json(nlohmann::json&, const linear_grid&);

/**
 * @brief Read linear grid from json file.
 *
 * @param j nlohmann::json object
 * @param lg Linear grid
 */
void from_json(const nlohmann::json&, linear_grid&);

/**
 * @brief Write power grid to json file.
 *
 * @param j nlohmann::json object
 * @param pg Power grid
 */
void to_json(nlohmann::json&, const power_grid&);

/**
 * @brief Read power grid from json file.
 *
 * @param j nlohmann::json object
 * @param pg Power grid
 */
void from_json(const nlohmann::json&, power_grid&);

/**
 * @brief Write symmetric grid to json file.
 *
 * @param j nlohmann::json object
 * @param sg Symmetric grid
 */
void to_json(nlohmann::json&, const power_grid&);

/**
 * @brief Read symmetric grid from json file.
 *
 * @param j nlohmann::json object
 * @param sg Symmetric grid
 */
void from_json(const nlohmann::json&, power_grid&);

/**
 * @brief Write index grid to json file.
 *
 * @param j nlohmann::json object
 * @param ig Index grid
 */
void to_json(nlohmann::json&, const index_grid&);

/**
 * @brief Read index grid from json file.
 *
 * @param j nlohmann::json object
 * @param ig Index grid
 */
void from_json(const nlohmann::json&, index_grid&);

/**
 * @brief Write N-dimensional grid to json file.
 *
 * @param j nlohmann::json object
 * @param ndg N-dimensional grid
 */
template <typename Index, typename... Gs>
void to_json(nlohmann::json& j, const nd_grid<Index, Gs...>& ndg) {
    int i = 0;
    for_each_tuple([&i, &j](auto&& grid) {
        std::string path = std::to_string(i++);
        j[path] = grid;
    }, ndg.get_grids());
}

/**
 * @brief Read N-dimensional grid from json file.
 *
 * @param j nlohmann::json object
 * @param ndg N-dimensional grid
 */
template <typename Index, typename... Gs>
void from_json(const nlohmann::json& j, nd_grid<Index, Gs...>& ndg) {
    int i = 0;
    for_each_tuple([&i, &j](auto&& grid) {
        std::string path = std::to_string(i++);
        j.at(path).get_to(grid);
    }, ndg.get_grids());
}

} // namespace utils

} // namespace simpleMC
