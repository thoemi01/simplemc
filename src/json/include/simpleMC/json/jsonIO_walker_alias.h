/**
 * @file    jsonIO_walker_alias.h
 * @brief   Read and write Walker-Alias class to json file.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json_fwd.hpp>
#include <simpleMC/utils/walker_alias.h>

namespace simpleMC {

namespace utils {

/**
 * @brief Write Walker-Alias to json file.
 *
 * @param j nlohmann::json object
 * @param wa walker_alias object
 */
void to_json(nlohmann::json&, const walker_alias&);

/**
 * @brief Read Walker-Alias from json file.
 *
 * @param j nlohmann::json object
 * @param wa walker_alias object
 */
void from_json(const nlohmann::json&, walker_alias&);

} // namespace utils

} // namespace simpleMC
