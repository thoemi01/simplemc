/**
 * @file    basic_jsonIO.h
 * @brief   Basic interface to support Json IO.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json_fwd.hpp>

namespace simpleMC {

namespace json {

/**
 * @brief Basic interface to support Json IO.
 *
 * @details Classes can derive from this class if they want to support
 * Json IO.
 */
struct basic_jsonIO {
public:
    /* Serialize to Json */
    virtual void to_json(nlohmann::json&) const {}

    /* Deserialize from Json */
    virtual void from_json(const nlohmann::json&) {}

    /* Read Json input */
    virtual void read_input_json(const nlohmann::json&) {}

    /* Write Json input */
    virtual void write_input_json(nlohmann::json&) const {}

    /* Virtual destructor */
    virtual ~basic_jsonIO() = default;
};

/**
 * @brief Serialize to Json.
 */
inline void to_json(nlohmann::json& j, const basic_jsonIO& io) {
    io.to_json(j);
}

/**
 * @brief Deserialize from Json.
 */
inline void from_json(const nlohmann::json& j, basic_jsonIO& io) {
    io.from_json(j);
}

/**
 * @brief Read Json input.
 */
inline void read_input_json(const nlohmann::json& j, basic_jsonIO& io) {
    io.read_input_json(j);
}

/**
 * @brief Write Json input.
 */
inline void write_input_json(nlohmann::json& j, const basic_jsonIO& io) {
    io.write_input_json(j);
}

} // namespace json

} // namespace simpleMC
