/**
 * @file    mpi.cpp
 * @brief   Implementation of MPI routines.
 * @author  Thomas Hahn
 */

#include <simpleMC/mpi/broadcast.h>
#include <simpleMC/mpi/gather.h>
#include <simpleMC/mpi/scatter.h>
#include <simpleMC/mpi/send_recv.h>
#include <simpleMC/mpi/json.h>

namespace simpleMC {

namespace mpi {

/* MPI broadcast for single string values */
void broadcast(std::string& value, int root, communicator comm) {
    using char_type = std::string::value_type;
    std::size_t size = value.size();
    broadcast(&size, /*count=*/ 1, root, comm);
    if (comm.rank() != root) {
        value.resize(size);
    }
    // use const_cast for older compilers
    broadcast(const_cast<char_type*>(value.data()), size, root, comm);
}

/* MPI broadcast for std::vector<string> */
void broadcast(std::vector<std::string>& vec, int root,
               communicator comm) {
    std::size_t size = vec.size();
    broadcast(&size, /*count=*/ 1, root, comm);
    if (comm.rank() != root) {
        vec.resize(size);
    }
    for (std::string& str : vec) {
        broadcast(str, root, comm);
    }
}

/* MPI gather for single std::strings */
std::vector<std::string> gather(const std::string& send_str, int root,
                                communicator comm) {
    std::vector<std::string> res;
    int tag = 0;
    if (comm.rank() == root) {
        res.resize(comm.size());
        for (int i = 0; i < comm.size(); i++) {
            if (root == i) {
                res[i] = send_str;
                continue;
            }
            receive(res[i], i, tag, comm);
        }
    } else {
        send(send_str, root, tag, comm);
    }
    return res;
}

/* MPI gather for std::vector<std::string> */
std::vector<std::string> gather(const std::vector<std::string>& send_vec,
                                int root, communicator comm) {
    std::vector<std::string> res;
    int tag = 0;
    auto size = send_vec.size();
    if (comm.rank() == root) {
        res.reserve(comm.size()*size);
        for (int i = 0; i < comm.size(); i++) {
            if (root == i) {
                res.insert(res.end(), send_vec.begin(), send_vec.end());
                continue;
            }
            std::vector<std::string> tmp;
            receive(tmp, i, tag, comm);
            res.insert(res.end(), tmp.begin(), tmp.end());
        }
    } else {
        send(send_vec, root, tag, comm);
    }
    return res;
}

/* MPI Allgather for single std::strings */
void all_gather(const std::string& send_str,
                std::vector<std::string>& rec_vec, communicator comm) {
    int root = 0;
    rec_vec = gather(send_str, root, comm);
    broadcast(rec_vec, root, comm);
}

/* MPI Allgather for std::vector<std::string> */
void all_gather(const std::vector<std::string>& send_vec,
                std::vector<std::string>& rec_vec, communicator comm) {
    int root = 0;
    rec_vec = gather(send_vec, root, comm);
    broadcast(rec_vec, root, comm);
}

/* MPI scatter for std::vector<std::string> */
void scatter(const std::vector<std::string>& send_vec,
             std::vector<std::string>& rec_vec, int root,
             communicator comm) {
    int rank = comm.rank();
    int procs = comm.size();
    auto send_size = send_vec.size();
    auto chunk_size = send_size/procs;
    if (send_size % procs != 0) {
        chunk_size += 1;
    }
    rec_vec.resize(chunk_size);
    int tag = 0;
    if (rank == root) {
        for (int i = 0; i < procs; i++) {
            auto begin = send_vec.begin() + i*chunk_size;
            auto end = (i + 1 < procs) ? begin + chunk_size : send_vec.end();
            std::vector<std::string> tmp_vec(begin, end);
            tmp_vec.resize(chunk_size);
            if (i == root) {
                rec_vec = tmp_vec;
            } else {
                send(tmp_vec, i, tag, comm);
            }
        }
    } else {
        receive(rec_vec, root, tag, comm);
    }
}

/* MPI send for single string values */
void send(const std::string& value, int dest, int tag,
          communicator comm) {
    std::size_t size = value.size();
    send(size, dest, tag, comm);
    send(value.data(), size, dest, tag, comm);
}

/* MPI receive for single string values */
void receive(std::string& value, int source, int tag,
             communicator comm, MPI_Status* status) {
    using char_type = std::string::value_type;
    std::size_t size;
    receive(size, source, tag, comm, status);
    value.resize(size);
    // use const_cast for older compilers
    receive(const_cast<char_type*>(value.data()), size, source, tag, comm, status);
}

/* MPI send for std::vector<string> */
void send(const std::vector<std::string>& vec, int dest, int tag,
          communicator comm) {
    std::size_t size = vec.size();
    send(size, dest, tag, comm);
    for (const std::string& str : vec) {
        send(str, dest, tag, comm);
    }
}

/* MPI receive for std::vector<string> */
void receive(std::vector<std::string>& vec, int source, int tag,
             communicator comm, MPI_Status* status) {
    std::size_t size;
    receive(size, source, tag, comm, status);
    vec.resize(size);
    for (std::string& str : vec) {
        receive(str, source, tag, comm, status);
    }
}

/* MPI gather routine for jsonIO objects */
std::vector<std::string> gather_jsonIO(const simpleMC::json::jsonIO& jio,
                                       int root, communicator comm) {
    auto jio_string = jio.to_string();
    return gather(jio_string, root, comm);
}

/* Gather jsonIO objects and write them to a single file */
void gather_write_jsonIO(const simpleMC::json::jsonIO& jio,
                         const std::string& filename,
                         simpleMC::json::jsonIO_mode mode, communicator comm,
                         int root, const std::string& base) {
    using simpleMC::json::detail::check_text_mode;
    auto jio_strings = gather_jsonIO(jio, root, comm);
    if (comm.rank() == root) {
        simpleMC::json::jsonIO gio;
        for (int i = 0; i < comm.size(); i++) {
            std::string path = base + std::to_string(i);
            gio[path] << nlohmann::json::parse(jio_strings[i]);
        }
        if (check_text_mode(mode)) {
            gio.dump_to_file(filename);
        } else {
            gio.dump_to_file(filename, std::ios_base::binary, mode);
        }
    }
}

/* Read jsonIO from file and scatter them to processes */
void read_scatter_jsonIO(simpleMC::json::jsonIO& jio,
                         const std::string& filename,
                         simpleMC::json::jsonIO_mode mode, communicator comm,
                         int root, const std::string& base) {
    using simpleMC::json::detail::check_text_mode;
    std::vector<std::string> jio_strings(comm.size());
    if (comm.rank() == root) {
        simpleMC::json::jsonIO gio;
        if (check_text_mode(mode)) {
            gio.load_from_file(filename);
        } else {
            gio.load_from_file(filename, std::ios_base::binary, mode);
        }
        for (int i = 0; i < comm.size(); i++) {
            std::string path = base + std::to_string(i);
            jio_strings[i] = gio[path].current_json().dump();
        }
    }
    std::vector<std::string> myjson;
    scatter(jio_strings, myjson, root, comm);
    jio.from_string(myjson[0]);
}

} // namespace mpi

} // namespace simpleMC
