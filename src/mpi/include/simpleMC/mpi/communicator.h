/**
 * @file    communicator.h
 * @brief   Wrapper class for MPI communicators.
 * @author  Thomas Hahn
 */

#pragma once

#include <mpi.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Wrapper around MPI communicator.
 *
 * @details Abstracts a set of commmunicating processes in MPI. It is closely
 * based on boost::mpi::communicator.
 */
class communicator {
public:

    /* Construct communicator with attached MPI_COMM_WORLD */
    communicator(MPI_Comm comm = MPI_COMM_WORLD) :
        comm_(comm)
    {}

    /* Get rank */
    int rank() const {
        int rank;
        MPI_Comm_rank(comm_, &rank);
        return rank;
    }

    /* Get size */
    int size() const {
        int size;
        MPI_Comm_size(comm_, &size);
        return size;
    }

    /* Wait for all processes */
    void barrier() const {
        MPI_Barrier(comm_);
    }

    /* Cast to MPI_Comm */
    operator MPI_Comm() const {
        return comm_;
    }

private:
    MPI_Comm comm_;
};

} // namespace mpi

} // namespace simpleMC
