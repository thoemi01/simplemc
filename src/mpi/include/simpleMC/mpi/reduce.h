/**
 * @file    reduce.h
 * @brief   MPI reduce routines.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/utils/view.h>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/datatypes.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic MPI reduce for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
reduce(const T* send, T* rec, std::size_t count, MPI_Op op, int root,
       communicator comm) {
    if (count == 0) return;
    MPI_Reduce(send, rec, count, mpi_type<T>::get(), op, root, comm);
}

/**
 * @brief Generic MPI Allreduce for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
all_reduce(const T* send, T* rec, std::size_t count, MPI_Op op,
           communicator comm) {
    if (count == 0) return;
    MPI_Allreduce(send, rec, count, mpi_type<T>::get(), op, comm);
}

/**
 * @brief Generic MPI reduce/Allreduce for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
reduce(const T& send, T& rec, MPI_Op op, communicator comm,
       bool all = true, int root = 0) {
    if (all) {
        all_reduce(&send, &rec, /*count=*/ 1, op, comm);
    } else {
        reduce(&send, &rec, /*count=*/ 1, op, root, comm);
    }
}

/**
 * @brief Generic MPI reduce/Allreduce for std::vectors of supported MPI types.
 */
template <typename T>
void reduce(const std::vector<T>& send, std::vector<T>& rec, MPI_Op op,
            communicator comm, bool all = true, int root = 0) {
    std::size_t size = send.size();
    if (all) {
        rec.resize(size);
        all_reduce(send.data(), rec.data(), size, op, comm);
    } else {
        if (comm.rank() == root) {
            rec.resize(size);
        }
        reduce(send.data(), rec.data(), size, op, root, comm);
    }
}

/**
 * @brief Generic MPI reduce/Allreduce for views of supported MPI types.
 */
template <typename T>
void reduce(const simpleMC::utils::view<T>& send,
            simpleMC::utils::view<T>& rec, MPI_Op op,
            communicator comm, bool all = true, int root = 0) {
    if (all) {
        all_reduce(send.data(), rec.data(), send.size(), op, comm);
    } else {
        reduce(send.data(), rec.data(), send.size(), op, root, comm);
    }
}

} // namespace mpi

} // namespace simpleMC
