/**
 * @file    only_on_rank.h
 * @brief   Things that should be done only one MPI process.
 * @author  Thomas Hahn
 */

#pragma once

#include <iosfwd>
#include <utility>
#include <simpleMC/mpi/communicator.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic functor to be only called on a certain MPI rank.
 */
struct only_on_rank {
public:

    /* Constructor */
    explicit only_on_rank(int rank_, int root_ = 0):
        rank(rank_),
        root(root_)
    {}

    /* Callable */
    template <typename F, typename... Args>
    constexpr void operator()(F&& f, Args&&... args) {
        if (rank == root) {
            std::forward<F>(f)(std::forward<Args>(args)...);
        }
    }

public:
    int rank;
    int root;
};

/**
 * @brief Output stream class for MPI processes.
 */
template <typename O = std::ostream>
class mpi_ostream {
public:
    using ostream_type = O;

public:

    /* Construct a fixedwidth_stream class */
    mpi_ostream(ostream_type& os, int rank, int root = 0) :
        os_(os),
        rank_(rank),
        root_(root)
    {}

    /* Get rank */
    int rank() { return rank_; }

    /* Get root */
    int root() { return root_; }

    /* Stream operator and overloads */
    template <typename T>
    mpi_ostream& operator<<(const T& t) {
        if (rank_ == root_)
            os_ << t;
        return *this;
    }

private:
    ostream_type& os_;
    int rank_;
    int root_;
};

} // namespace mpi

} // namespace simpleMC
