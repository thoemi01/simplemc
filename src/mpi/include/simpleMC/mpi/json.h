/**
 * @file    json.h
 * @brief   MPI routines to support jsonIO.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/mpi/communicator.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief MPI gather routine for jsonIO objects.
 */
std::vector<std::string> gather_jsonIO(const simpleMC::json::jsonIO&, int,
                                       communicator);

/**
 * @brief Gather jsonIO objects and write them to a single file.
 */
void gather_write_jsonIO(const simpleMC::json::jsonIO& jio,
                         const std::string& filename,
                         simpleMC::json::jsonIO_mode mode, communicator comm,
                         int root = 0, const std::string& base = "rank");

/**
 * @brief Read jsonIO from file and scatter them to processes.
 */
void read_scatter_jsonIO(simpleMC::json::jsonIO& jio,
                         const std::string& filename,
                         simpleMC::json::jsonIO_mode mode, communicator comm,
                         int root = 0, const std::string& base = "rank");

} // namespace mpi

} // namespace simpleMC
