/**
 * @file    gather.h
 * @brief   MPI gather routines.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/utils/view.h>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/datatypes.h>
#include <simpleMC/mpi/send_recv.h>
#include <simpleMC/mpi/broadcast.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic MPI gather for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
gather(const T* send_buff, T* rec_buff, std::size_t count, int root,
       communicator comm) {
    if (count == 0) return;
    MPI_Gather(send_buff, count, mpi_type<T>::get(), rec_buff,
               count, mpi_type<T>::get(), root, comm);
}

/**
 * @brief Generic MPI gather for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>, std::vector<T>>::type
gather(const T& send_val, int root, communicator comm) {
    std::vector<T> res;
    if (comm.rank() == root) {
        res.resize(comm.size());
        gather(&send_val, res.data(), /*count=*/ 1, root, comm);
    } else {
        gather(&send_val, static_cast<T*>(nullptr), /*count=*/ 1, root, comm);
    }
    return res;
}

/**
 * @brief MPI gather for single std::strings.
 */
std::vector<std::string> gather(const std::string&, int,
                                communicator);

/**
 * @brief Generic MPI gather for std::vectors of supported MPI types.
 */
template <typename T>
std::vector<T> gather(const std::vector<T>& send_vec, int root,
                      communicator comm) {
    std::vector<T> res;
    auto size = send_vec.size();
    if (comm.rank() == root) {
        res.resize(comm.size()*size);
        gather(send_vec.data(), res.data(), size, root, comm);
    } else {
        gather(send_vec.data(), static_cast<T*>(nullptr), size, root, comm);
    }
    return res;
}

/**
 * @brief MPI gather for std::vector<std::string>.
 */
std::vector<std::string> gather(const std::vector<std::string>&,
                                int, communicator);

/**
 * @brief Generic MPI AlLgather for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
all_gather(const T* send_buff, T* rec_buff, std::size_t count,
           communicator comm) {
    if (count == 0) return;
    MPI_Allgather(send_buff, count, mpi_type<T>::get(), rec_buff, count,
                  mpi_type<T>::get(), comm);
}

/**
 * @brief Generic MPI Allgather for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
all_gather(const T& send_val, std::vector<T>& rec_vec, communicator comm) {
    rec_vec.resize(comm.size());
    all_gather(&send_val, rec_vec.data(), /*count=*/ 1, comm);
}

/**
 * @brief MPI Allgather for single std::strings.
 */
void all_gather(const std::string&, std::vector<std::string>&,
                communicator);

/**
 * @brief Generic MPI Allgather for std::vectors of supported MPI types.
 */
template <typename T>
void all_gather(const std::vector<T>& send_vec, std::vector<T>& rec_vec,
                communicator comm) {
    auto size = send_vec.size();
    rec_vec.resize(comm.size()*size);
    all_gather(send_vec.data(), rec_vec.data(), size, comm);
}

/**
 * @brief MPI Allgather for std::vector<std::string>.
 */
void all_gather(const std::vector<std::string>&,
                std::vector<std::string>&, communicator);

} // namespace mpi

} // namespace simpleMC
