/**
 * @file    mpi_fwd.h
 * @brief   Forward declarations of MPI tools.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleUtils {

namespace mpi {

class environment;
class communicator;

} // namespace mpi

} // namespace simpleMC
