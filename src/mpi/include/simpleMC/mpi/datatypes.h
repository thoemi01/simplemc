/**
 * @file    datatypes.h
 * @brief   Mappings of MPI datatypes to C++ types.
 * @author  Thomas Hahn
 */

#pragma once

#include <complex>
#include <mpi.h>

namespace simpleMC {

namespace mpi {

namespace detail {

/* Build bool type for MPI, since it is not supported directly */
inline MPI_Datatype build_bool_type() {
    MPI_Datatype type;
    MPI_Type_contiguous(sizeof(bool), MPI_BYTE, &type);
    MPI_Type_commit(&type);
    return type;
}

} // namespace detail

/**
 * @brief Traits class to indicate a native MPI datatype.
 */
template <typename T>
struct is_mpi_native : public std::false_type {};

/**
 * @brief Map C++ types to MPI datatypes.
 */
template <typename T>
struct mpi_type {};

#define MAKE_MPI_DATATYPE(__cxx_type__, __mpi_type__) \
    template <> \
    struct mpi_type<__cxx_type__> { \
        static MPI_Datatype get() { \
            return __mpi_type__; \
        } \
    }; \
    template <> \
    struct is_mpi_native<__cxx_type__> : public std::true_type {};

MAKE_MPI_DATATYPE(char, MPI_CHAR)
MAKE_MPI_DATATYPE(signed char, MPI_SIGNED_CHAR)
MAKE_MPI_DATATYPE(unsigned char, MPI_UNSIGNED_CHAR)
MAKE_MPI_DATATYPE(short, MPI_SHORT)
MAKE_MPI_DATATYPE(unsigned short, MPI_UNSIGNED_SHORT)
MAKE_MPI_DATATYPE(int, MPI_INT)
MAKE_MPI_DATATYPE(unsigned int, MPI_UNSIGNED)
MAKE_MPI_DATATYPE(long, MPI_LONG)
MAKE_MPI_DATATYPE(unsigned long, MPI_UNSIGNED_LONG)
MAKE_MPI_DATATYPE(long long, MPI_LONG_LONG)
MAKE_MPI_DATATYPE(unsigned long long, MPI_UNSIGNED_LONG_LONG)
MAKE_MPI_DATATYPE(float, MPI_FLOAT)
MAKE_MPI_DATATYPE(double, MPI_DOUBLE)
MAKE_MPI_DATATYPE(long double, MPI_LONG_DOUBLE)
MAKE_MPI_DATATYPE(std::complex<double>, MPI_DOUBLE_COMPLEX)

#undef MAKE_MPI_DATATYPE

template <>
struct mpi_type<bool> {
    static MPI_Datatype get() {
        static MPI_Datatype type = detail::build_bool_type();
        return type;
    }
};

template <>
struct is_mpi_native<bool> : public std::true_type {};

template <typename T>
inline constexpr bool is_mpi_native_v = is_mpi_native<T>::value;

} // namespace mpi

} // namespace simpleMC
