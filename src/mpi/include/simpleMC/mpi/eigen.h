/**
 * @file    eigen.h
 * @brief   MPI routines for Eigen types.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/view_eigen.h>
#include <simpleMC/mpi/broadcast.h>
#include <simpleMC/mpi/reduce.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Broadcast an Eigen type without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>>::type
broadcast(T& value, int root, communicator comm) {
    auto view = simpleMC::utils::make_view(value);
    broadcast(view, root, comm);
}

/**
 * @brief Reduce/Allreduce on an Eigen type without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>>::type
reduce(const T& send, T& rec, MPI_Op op, communicator comm,
       bool all = true, int root = 0) {
    auto send_view = simpleMC::utils::make_view(send);
    auto rec_view = simpleMC::utils::make_view(rec);
    reduce(send_view, rec_view, op, comm, all, root);
}

/**
 * @brief Send an Eigen type without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>>::type
send(const T& value, int dest, int tag, communicator comm) {
    auto view = simpleMC::utils::make_view(value);
    send(view, dest, tag, comm);
}

/**
 * @brief Receive an Eigen type without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>>::type
receive(T& value, int source, int tag, communicator comm,
        MPI_Status* status = MPI_STATUS_IGNORE) {
    auto view = simpleMC::utils::make_view(value);
    receive(view, source, tag, comm, status);
}

/**
 * @brief Gather Eigen types without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>, std::vector<T>>::type
gather(const T& send_val, int root, communicator comm) {
    std::vector<T> res;
    int tag = 0;
    if (comm.rank() == root) {
        // we have to initialize the array, otherwise dynamic type won't work
        res.resize(comm.size(), send_val);
        for (int i = 0; i < comm.size(); i++) {
            if (root == i) {
                res[i] = send_val;
                continue;
            }
            receive(res[i], i, tag, comm);
        }
    } else {
        send(send_val, root, tag, comm);
    }
    return res;
}

/**
 * @brief Allgather Eigen types without resizing or size checks.
 */
template <typename T>
typename std::enable_if<simpleMC::utils::is_eigen_v<T>>::type
all_gather(const T& send_val, std::vector<T>& rec_vec, communicator comm) {
    std::vector<T> tmp;
    for (int i = 0; i < comm.size(); i++) {
        tmp = gather(send_val, i, comm);
        if (i == comm.rank()) {
            rec_vec = tmp;
        }
    }
}

} // namespace mpi

} // namespace simpleMC
