/**
 * @file    scatter.h
 * @brief   MPI scatter routines.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/datatypes.h>
#include <simpleMC/mpi/send_recv.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic MPI scatter for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
scatter(const T* send_buff, T* rec_buff, std::size_t count, int root,
        communicator comm) {
    if (count == 0) return;
    MPI_Scatter(send_buff, count, mpi_type<T>::get(), rec_buff,
                count, mpi_type<T>::get(), root, comm);
}

/**
 * @brief Generic MPI scatter for std::vectors of supported MPI types.
 */
template <typename T>
void scatter(const std::vector<T>& send_vec, std::vector<T>& rec_vec,
             int root, communicator comm) {
    auto send_size = send_vec.size();
    auto chunk_size = send_size/comm.size();
    if (send_size % comm.size() != 0) {
        chunk_size += 1;
        std::vector<T> tmp_vec(send_vec.begin(), send_vec.end());
        tmp_vec.resize(chunk_size*comm.size());
        scatter(tmp_vec.data(), rec_vec.data(), chunk_size, root, comm);
    } else {
        rec_vec.resize(chunk_size);
        scatter(send_vec.data(), rec_vec.data(), chunk_size, root, comm);

    }
}

/**
 * @brief MPI scatter for std::vector<std::string>.
 */
void scatter(const std::vector<std::string>&, std::vector<std::string>&,
             int, communicator);

} // namespace mpi

} // namespace simpleMC
