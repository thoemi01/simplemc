/**
 * @file    send_recv.h
 * @brief   Blocking MPI send and receive routines.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/utils/view.h>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/datatypes.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic MPI send for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
send(const T* value, std::size_t count, int dest, int tag,
     communicator comm) {
    if (count == 0) return;
    MPI_Send(value, count, mpi_type<T>::get(), dest, tag, comm);
}

/**
 * @brief Generic MPI receive for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
receive(T* value, std::size_t count, int source, int tag,
        communicator comm,
        MPI_Status* status = MPI_STATUS_IGNORE) {
    if (count == 0) return;
    MPI_Recv(value, count, mpi_type<T>::get(), source, tag, comm, status);
}

/**
 * @brief Generic MPI send for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
send(const T& value, int dest, int tag, communicator comm) {
    send(&value, /*count=*/ 1, dest, tag, comm);
}

/**
 * @brief Generic MPI receive for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
receive(T& value, int source, int tag, communicator comm,
        MPI_Status* status = MPI_STATUS_IGNORE) {
    receive(&value, /*count=*/ 1, source, tag, comm, status);
}

/**
 * @brief MPI send for single string values.
 */
void send(const std::string&, int, int, communicator);

/**
 * @brief MPI receive for single string values.
 */
void receive(std::string& value, int source, int tag,
             communicator comm,
             MPI_Status* status = MPI_STATUS_IGNORE);

/**
 * @brief Generic MPI send for std::vectors of supported MPI types.
 */
template <typename T>
void send(const std::vector<T>& vec, int dest, int tag,
          communicator comm) {
    std::size_t size = vec.size();
    send(size, dest, tag, comm);
    send(vec.data(), size, dest, tag, comm);
}

/**
 * @brief Generic MPI receive for std::vectors of supported MPI types.
 */
template <typename T>
void receive(std::vector<T>& vec, int source, int tag,
             communicator comm,
             MPI_Status* status = MPI_STATUS_IGNORE) {
    std::size_t size;
    receive(size, source, tag, comm, status);
    vec.resize(size);
    receive(vec.data(), size, source, tag, comm, status);
}

/**
 * @brief MPI send for std::vector<string>.
 */
void send(const std::vector<std::string>&, int, int, communicator);

/**
 * @brief MPI receive for std::vector<string>.
 */
void receive(std::vector<std::string>& vec, int source, int tag,
             communicator comm,
             MPI_Status* status = MPI_STATUS_IGNORE);

/**
 * @brief Generic MPI send for views of supported MPI types.
 */
template <typename T>
void send(const simpleMC::utils::view<T>& view, int dest, int tag,
          communicator comm) {
    send(view.data(), view.size(), dest, tag, comm);
}

/**
 * @brief Generic MPI receive for views of supported MPI types.
 */
template <typename T>
void receive(simpleMC::utils::view<T>& view, int source, int tag,
             communicator comm, MPI_Status* status = MPI_STATUS_IGNORE) {
    receive(view.data(), view.size(), source, tag, comm, status);
}

} // namespace mpi

} // namespace simpleMC
