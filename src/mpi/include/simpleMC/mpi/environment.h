/**
 * @file    environment.h
 * @brief   Wrapper class for MPI execution environment.
 * @author  Thomas Hahn
 */

#pragma once

#include <exception>
#include <mpi.h>
#include <simpleMC/utils/noncopyable.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief RAII MPI execution environment.
 *
 * @details Initializes and finalizes a MPI exectution environment.
 * It is closely based on boost::mpi::environment.
 */
class environment : public simpleMC::utils::noncopyable<environment> {
public:

    /* Checks if the MPI execution environment has already been initialized */
    static bool is_initialized() {
        int ini;
        MPI_Initialized(&ini);
        return ini != 0;
    }

    /* Checks if the MPI execution environment has already been finalized */
    static bool is_finalized() {
        int fin;
        MPI_Finalized(&fin);
        return fin != 0;
    }

    /* Terminate MPI execution environment */
    static void abort(int ec = 0) {
        MPI_Abort(MPI_COMM_WORLD, ec);
    }

    /* Finalize MPI execution manually */
    static void finalize() {
        if (!is_finalized()) {
            MPI_Finalize();
        }
    }

public:

    /* Constructs and initalizes a MPI environment */
    environment(int& argc, char**& argv,
                bool abort_on_exception = true) :
        init_(false),
        abort_on_exception_(abort_on_exception)
    {
        if (!is_initialized()) {
            MPI_Init(&argc, &argv);
            init_ = true;
        }
    }

    /* Destructor finalizes the MPI environment */
    ~environment() {
        if (init_) {
            if (abort_on_exception_ && std::uncaught_exceptions()) {
                abort(-1);
            } else if (!is_finalized()) {
                MPI_Finalize();
            }
        }
    }

private:
    bool init_;
    bool abort_on_exception_;
};

} // namespace mpi

} // namespace simpleMC
