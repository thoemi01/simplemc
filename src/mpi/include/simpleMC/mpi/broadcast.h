/**
 * @file    broadcast.h
 * @brief   MPI broadcasting routines.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/datatypes.h>
#include <simpleMC/utils/view.h>

namespace simpleMC {

namespace mpi {

/**
 * @brief Generic MPI broadcast for contiguous data of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
broadcast(T* value, std::size_t count, int root, communicator comm) {
    if (count == 0) return;
    MPI_Bcast(value, count, mpi_type<T>::get(), root, comm);
}

/**
 * @brief Generic MPI broadcast for single values of supported MPI types.
 */
template <typename T>
typename std::enable_if<is_mpi_native_v<T>>::type
broadcast(T& value, int root, communicator comm) {
    broadcast(&value, /*count=*/ 1, root, comm);
}

/**
 * @brief MPI broadcast for single string values.
 */
void broadcast(std::string&, int, communicator);

/**
 * @brief Generic MPI broadcast for std::vectors of supported MPI types.
 */
template <typename T>
void broadcast(std::vector<T>& vec, int root, communicator comm) {
    std::size_t size = vec.size();
    broadcast(&size, /*count=*/ 1, root, comm);
    if (comm.rank() != root) {
        vec.resize(size);
    }
    broadcast(vec.data(), size, root, comm);
}

/**
 * @brief MPI broadcast for std::vector<string>.
 */
void broadcast(std::vector<std::string>&, int, communicator);

/**
 * @brief Generic MPI broadcast for views of supported MPI types.
 */
template <typename T>
void broadcast(simpleMC::utils::view<T>& view, int root, communicator comm) {
    broadcast(view.data(), view.size(), root, comm);
}

} // namespace mpi

} // namespace simpleMC
