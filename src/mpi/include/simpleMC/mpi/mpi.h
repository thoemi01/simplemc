/**
 * @file    mpi.h
 * @brief   Convenience header for mpi library.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/mpi/environment.h>
#include <simpleMC/mpi/communicator.h>
#include <simpleMC/mpi/broadcast.h>
#include <simpleMC/mpi/reduce.h>
#include <simpleMC/mpi/send_recv.h>
#include <simpleMC/mpi/gather.h>
#include <simpleMC/mpi/scatter.h>
