/**
 * @file    walker_alias.cpp
 * @brief   Walker alias algorithms for generating discrete random numbers.
 * @author  Thomas Hahn
 */

#include <numeric>
#include <simpleMC/utils/walker_alias.h>

namespace simpleMC {

namespace utils {

/* Creates the alias and probability table */
void walker_alias::initialize(const std::vector<double>& weights) {
    // initialize tables
    weights_ = weights;
    probs_.resize(weights_.size(), 0.0);
    alias_.resize(weights_.size(), 0);

    // helper vectors/arrays
    std::vector<std::size_t> e;
    std::vector<std::size_t> n;
    double norm = std::accumulate(weights_.begin(), weights_.end(), 0.0);
    for (std::size_t i = 0; i < weights_.size(); i++) {
        probs_[i] = weights_[i]*weights_.size()/norm;
        if (probs_[i] < 1) {
            e.push_back(i);
        } else {
            n.push_back(i);
        }
    }

    // create tables
    while (e.size() != 0 && n.size() != 0) {
        auto j = e.back();
        auto k = n.back();
        alias_[j] = k;
        e.pop_back();
        probs_[k] += probs_[j] - 1;
        if (probs_[k] < 1) {
            n.pop_back();
            e.push_back(k);
        }
    }
    for (std::size_t i = 0; i < e.size(); i++) {
        probs_[e[i]] = 1;
    }
    for (std::size_t i = 0; i < n.size(); i++) {
        probs_[n[i]] = 1;
    }
}

/* Set member variables (used for serialization) */
void walker_alias::set(const std::vector<double>& weights,
                       const std::vector<double>& probs,
                       const std::vector<std::size_t>& alias) {
    weights_ = weights;
    probs_ = probs;
    alias_ = alias;
}

} // namespace utils

} // namespace simpleMC
