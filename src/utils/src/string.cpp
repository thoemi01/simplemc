/**
 * @file    string.cpp
 * @brief   Useful functions to deal with strings.
 * @author  Thomas Hahn
 */

#include <sstream>
#include <simpleMC/utils/string.h>

namespace simpleMC {

namespace utils {

/* Split string into tokens by a certain delimiter character */
std::vector<std::string> split_at_char(const std::string& in,
                                       char delim,
                                       bool keep_empty) {
    std::vector<std::string> res;
    std::string token;
    std::stringstream ss(in);
    while(std::getline(ss, token, delim)) {
        if (!token.empty()) {
            res.push_back(token);
        } else if (keep_empty) {
            res.push_back(token);
        }
    }
    if (token.empty() && keep_empty) {
        res.push_back(token);
    }
    return res;
}

/* Split string into tokens by a certain delimiter string */
std::vector<std::string> split_at_string(const std::string& in,
                                         const std::string& delim,
                                         bool keep_empty) {
    std::vector<std::string> res;
    std::string current = in;
    auto delim_len = delim.size();
    auto split_at = in.size();
    std::string token;
    while((split_at = current.find(delim)) != std::string::npos) {
        token = current.substr(0, split_at);
        if (!token.empty()) {
            res.push_back(token);
        } else if (keep_empty) {
            res.push_back(token);
        }
        current = current.substr(split_at + delim_len);
    }
    if (!current.empty()) {
        res.push_back(current);
    } else if (keep_empty) {
        res.push_back(current);
    }
    return res;
}

/* Concatenate tokens into one string */
std::string concat_tokens(const std::vector<std::string>& cont,
                          const std::string& delim,
                          const std::string& starts_with,
                          const std::string& ends_with) {
    std::string res = starts_with;
    if (cont.size() == 0) {
        res += ends_with;
        return res;
    }
    res += cont[0];
    for (std::size_t i = 1; i < cont.size(); i++) {
        res += delim + cont[i];
    }
    res += ends_with;
    return res;
}

/* Finds the last occurence of one of the characters specified and returns
 * the substring up to that character */
std::string get_base(const std::string& in, const std::string& search,
                     bool keep_full) {
    auto pos = in.find_last_of(search);
    if (pos == std::string::npos && !keep_full)
        pos = 0;
    return in.substr(0, pos);
}

/* Finds the last occurence of one of the characters specified and returns
 * the substring from that character to the end */
std::string get_leaf(const std::string& in, const std::string& search,
                     bool keep_full) {
    auto pos = in.find_last_of(search);
    if (pos == std::string::npos) {
        return keep_full ? in : std::string{};
    }
    if (pos == in.size() - 1) {
        return std::string{};
    }
    return in.substr(pos + 1);
}

} // namespace utils

} // namespace simpleMC
