/**
 * @file    signals.cpp
 * @brief   Simple signal listener. Based on triqs::signal_handler.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <csignal>
#include <vector>
#include <simpleMC/utils/signals.h>

namespace simpleMC {

namespace signals {

namespace {

std::vector<int> signals_;
bool init_;

} // namespace anonymous

/* Default signal handler */
void default_handler(int signal) {
    std::cerr << "\nSIGNAL RECEIVED: " << signal << "\n";
    signals_.push_back(signal);
}

/* Specify a handling function and start listening to signals */
void start_listening(void(*handler)(int)) {
    if (!init_) {
        std::signal(SIGINT, handler);
        std::signal(SIGTERM, handler);
        std::signal(SIGQUIT, handler);
        std::signal(SIGXCPU, handler);
        std::signal(SIGUSR1, handler);
        std::signal(SIGUSR2, handler);
        signals_.clear();
        init_ = true;
    }
}

/* Stop listening to the signal */
void stop_listening() {
    signals_.clear();
    init_ = false;
}

/* Determines if a signal has been received */
bool empty() {
    return signals_.empty();
}

/* Get the last signal that has been received */
int last() {
    if (!empty()) {
        return signals_.back();
    }
    return -1;
}

/* Pop the last signal that has been received */
void pop() {
    if (!empty()) {
        signals_.pop_back();
    }
}

} // namespace signals

} // namespace simpleMC
