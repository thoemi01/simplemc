/**
 * @file    special_functions.cpp
 * @brief   Special functions, e.g. Legendre polynomials, ...
 * @details Implementations of special functions and especially corresponding
 *          recurrence relations for those functions. These can be used to
 *          efficiently estimate coefficients of a generalized Fourier series:
 *          f(x) = sum_n f_n e_n(x), where {e_n(x)} is a complete set of
 *          orthogonal functions on some interval [a, b].
 * @author  Thomas Hahn
 */

#include <simpleMC/utils/special_functions.h>

namespace simpleMC {

namespace utils {

/* Set values and calculate alpha and beta */
void map_interval::set(double a, double b, double c, double d) {
    using namespace math_constants;
    assert(a < b && c < d);
    if (a > minus_inf && b < inf) {
        assert(c > minus_inf && d < inf);
        alpha_ = (c - d)/(a - b);
        beta_ = c - a*alpha_;
    } else if (a == minus_inf && b != inf) {
        assert(c == minus_inf && d != inf);
        alpha_ = 1.0;
        beta_ = d - b;
    } else if (a != minus_inf && b == inf) {
        assert(c != minus_inf && d == inf);
        alpha_ = 1.0;
        beta_ = c - a;
    } else if (a == minus_inf && b == inf) {
        assert(c == minus_inf && d == inf);
        alpha_ = 1.0;
        beta_ = 0.0;
    } else {
        throw std::out_of_range("Range in map_interval not supported");
    }
}

/* Calculate lth Legendre polynomial at x */
double legendre_poly(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 1;
        case 1:
            return x;
    }
    double pl = x;
    double plm1 = 1;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = legendre_next(i, x, pl, plm1);
        plm1 = pl;
        pl = tmp;
    }
    return pl;
}

/* Calculate the derivative of lth Legendre polynomial at x */
double legendre_poly_prime(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 0;
        case 1:
            return 1;
    }
    double pl = x;
    double plm1 = 1;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = legendre_next(i, x, pl, plm1);
        plm1 = pl;
        pl = tmp;
    }
    return l/(x*x - 1)*(x*pl - plm1);
}

/* Calculate lth Chebyshev polynomial of 1st kind at x */
double chebyshev_poly_t(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 1;
        case 1:
            return x;
    }
    double tl = x;
    double tlm1 = 1;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = chebyshev_next(x, tl, tlm1);
        tlm1 = tl;
        tl = tmp;
    }
    return tl;
}

/* Calculate lth Chebyshev polynomial of 2nd kind at x */
double chebyshev_poly_u(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 1;
        case 1:
            return 2.0*x;
    }
    double ul = 2.0*x;
    double ulm1 = 1;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = chebyshev_next(x, ul, ulm1);
        ulm1 = ul;
        ul = tmp;
    }
    return ul;
}

/* Calculate the derivative of lth hebyshev polynomial of 1st kind at x */
double chebyshev_poly_t_prime(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 0;
        case 1:
            return 1;
    }
    double ul = 2.0*x;
    double ulm1 = 1;
    for (unsigned int i = 2; i < l; i++) {
        double tmp = chebyshev_next(x, ul, ulm1);
        ulm1 = ul;
        ul = tmp;
    }
    return l*ul;
}

/* Calculate the derivative of lth hebyshev polynomial of 2nd kind at x */
double chebyshev_poly_u_prime(unsigned int l, double x) {
    assert(std::abs(x) <= 1);
    switch (l) {
        case 0:
            return 0;
        case 1:
            return 2;
    }
    double ul = 2.0*x;
    double ulm1 = 1;
    for (unsigned int i = 0; i < l; i++) {
        double tmp = chebyshev_next(x, ul, ulm1);
        ulm1 = ul;
        ul = tmp;
    }
    double tl = ul - x*ulm1;
    return ((l + 1)*tl - x*ulm1)/(x*x - 1);
}

/* Calculate lth Laguerre polynomial at x */
double laguerre_poly(unsigned int l, double x) {
    assert(x >= 0);
    switch (l) {
        case 0:
            return 1;
        case 1:
            return 1.0 - x;
    }
    double ll = 1.0 - x;
    double llm1 = 1.0;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = laguerre_next(i, x, ll, llm1);
        llm1 = ll;
        ll = tmp;
    }
    return ll;
}

/* Calculate lth Hermite polynomial at x */
double hermite_poly(unsigned int l, double x) {
    switch (l) {
        case 0:
            return 1;
        case 1:
            return 2.0*x;
    }
    double hl = 2.0*x;
    double hlm1 = 1.0;
    for (unsigned int i = 1; i < l; i++) {
        double tmp = hermite_next(i, x, hl, hlm1);
        hlm1 = hl;
        hl = tmp;
    }
    return hl;
}

} // namespace utils

} // namespace simpleMC
