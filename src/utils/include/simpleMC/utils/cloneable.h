/**
 * @file    cloneable.h
 * @brief   CRTP base class to automate cloning.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleMC {

namespace utils {

/**
 * @brief CRTP base class to automate cloning.
 *
 * @details Inherit from this class to provide an automatic clone() function.
 * B is the actual base class which may contain a pure virtual clone() member
 * with the following signature: virtual B* clone() const = 0. Then the
 * derived class D can use class D : public cloneable<B, D> {...} to
 * automatically define a clone function.
 *
 * @tparam B Base type
 * @tparam D Derived type
 */
template <typename B, typename D>
class cloneable : public B {
public:
    /* Virtual clone method */
    virtual B* clone() const {
        return new D(static_cast<const D&>(*this));
    }

protected:
    /* This class needs to be inherited */
    cloneable() = default;
    cloneable(const cloneable&) = default;
};

} // namespace utils

} // namespace simpleMC
