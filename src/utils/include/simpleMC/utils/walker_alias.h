/**
 * @file    walker_alias.h
 * @brief   Walker alias algorithms for generating discrete random numbers.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>

namespace simpleMC {

namespace utils {

/**
 * @brief Implementation of the Walker-Alias algorithm
 * (see https://en.wikipedia.org/wiki/Alias_method).
 */
class walker_alias {
public:

    /* Default constructor */
    walker_alias() = default;

    /* Construct walker_alias with weights vector */
    walker_alias(const std::vector<double>& weights) {
        initialize(weights);
    }

    /* Set member variables (used for serialization) */
    void set(const std::vector<double>&, const std::vector<double>&,
             const std::vector<std::size_t>&);

    /* Initialize alias table */
    void initialize(const std::vector<double>&);

    /* Get weights */
    const std::vector<double>& weights() const { return weights_; }

    /* Get probablilites */
    const std::vector<double>& probabilities() const { return probs_; }

    /* Get alias table */
    const std::vector<std::size_t>& alias() const { return alias_; }

    /* Generate random sample from a single uniform random number */
    std::size_t gen(double u) const {
        std::size_t j = static_cast<std::size_t>(u*probs_.size());
        double r = j + 1 - u*probs_.size();
        return (r <= probs_[j] ? j : alias_[j]);
    }

    /* Generate random sample from two uniform random numbers */
    std::size_t gen(double u1, double u2) const {
        std::size_t j = static_cast<std::size_t>(u1*probs_.size());
        return (u2 <= probs_[j] ? j : alias_[j]);
    }

private:
    std::vector<double> weights_;
    std::vector<double> probs_;
    std::vector<std::size_t> alias_;
};

} // namespace utils

} // namespace simpleMC
