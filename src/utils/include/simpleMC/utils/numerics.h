﻿/**
 * @file    numerics.h
 * @brief   Simple numerical interpolation and integration routines. Largely
 *          based on Press, Numerical Recipes (2007)
 * @author  Thomas Hahn
 */

#pragma once

#include <stdexcept>
#include <iostream>
#include <simpleMC/utils/view.h>
#include <simpleMC/utils/view_eigen.h>
#include <simpleMC/utils/algorithms.h>

namespace simpleMC {

namespace utils {

namespace detail {

/**
 * @brief Check number of grid points and y-values.
 *
 * @tparam G Grid type
 */
template <typename G>
void check_number_points(const G& grid, const view<double>& vi) {
    if (grid.numpoints() != vi.size()) {
        throw std::out_of_range("Number of grid points not equal to number"
                                " of y values.");
    }
}

} // namespace detail

/**
 * @brief 1-dimension linear interpolation.
 *
 * @details Uses linear interpolation on an arbitrary grid.
 *
 * @tparam G Grid type
 */
template <typename G>
class linear_interpolation {
public:
    using grid_type = G;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Construct linear interpolation with grid and view */
    linear_interpolation(const grid_type&, const view<double>&);

    /* Linear interpolation */
    double interpolate(double x) const {
        std::size_t idx = inbetween_m_points(grid_.index(x), grid_.numpoints(), 2);
        return yvals_[idx] + (yvals_[idx + 1] - yvals_[idx])*
                ((x - grid_.value(idx))/(grid_.value(idx+1) - grid_.value(idx)));
    }

private:
    grid_type grid_;
    view<double> yvals_;
};

/* Construct linear interpolation with grid and view */
template <typename G>
linear_interpolation<G>::linear_interpolation(const grid_type& grid,
                                              const view<double>& yvals) :
    grid_(grid),
    yvals_(yvals)
{
    detail::check_number_points(grid_, yvals_);
}

/**
 * @brief 1-dimension polynomial interpolation.
 *
 * @details Uses Neville's algorithm to perform a polynomial interpolation
 * on an arbitrary grid. If the degree of the polynomial is m, then this
 * interpolation scheme uses m+1 points, e.g. cubic interpolation uses
 * 4 points.
 *
 * @tparam G Grid type
 */
template <typename G>
class polynomial_interpolation {
public:
    using grid_type = G;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Construct polynomial interpolation with grid, view and degree of
     * polynomial */
    polynomial_interpolation(const grid_type&, const view<double>&,
                             std::size_t);

    /* Polynomial interpolation */
    double interpolate(double) const;

private:
    grid_type grid_;
    view<double> yvals_;
    std::size_t npoints_;
    mutable std::vector<double> pcoeff_;
    mutable std::vector<double> xcoeff_;
};

/* Construct polynomial interpolation with grid, view and degree of polynomial */
template <typename G>
polynomial_interpolation<G>::polynomial_interpolation(const grid_type& grid,
                                                      const view<double>& yvals,
                                                      std::size_t degree) :
    grid_(grid),
    yvals_(yvals),
    npoints_(degree + 1),
    pcoeff_(npoints_, 0.0),
    xcoeff_(npoints_, 0.0)
{
    detail::check_number_points(grid_, yvals_);
}

/* Polynomial interpolation using Neville's algorithm */
template <typename G>
double polynomial_interpolation<G>::interpolate(double x) const {
    auto idx = grid_.index(x);
    std::size_t lidx = inbetween_m_points(idx, grid_.numpoints(), npoints_);
    for (std::size_t i = 0; i < npoints_; i++) {
        pcoeff_[i] = yvals_[lidx+i];
        xcoeff_[i] = x - grid_.value(lidx+i);
    }
    for (std::size_t i = 1; i < npoints_; i++) {
        for (std::size_t j = 0; j < npoints_ - i; j++) {
            pcoeff_[j] = (xcoeff_[j+i]*pcoeff_[j] - xcoeff_[j]*pcoeff_[j+1])/
                    (xcoeff_[j+i] - xcoeff_[j]);
        }
    }
    return pcoeff_[0];
}

/**
 * @brief 1-dimensional cubic spline interpolation.
 *
 * @details Cubic spline interpolation of data points on an arbitrary grid. One can
 * either use natural cubic splines, specify the first derivatives at the end points
 * or use an arbitrary system of linear equations for the second derivaties used in
 * the spline interpolation.
 *
 * @tparam G Grid type
 */
template <typename G>
class cbspline_interpolation {
public:
    using grid_type = G;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

    /* Get basic spline matrix, i.e. with the last two equations left
     * unspecified (Eq. 3.3.7 times 6 in Press, Numerical Recipes) */
    static Eigen::MatrixXd spline_matrix(const grid_type&);

    /* Get basic spline vector, i.e. with the last two entries left
     * unspecified (Eq. 3.3.7 times 6 in Press, Numerical Recipes) */
    static Eigen::VectorXd spline_vector(const grid_type&, const view<double>&);

public:

    /* Construct natural cubic spline interpolation with grid and view */
    cbspline_interpolation(const grid_type&, const view<double>&);

    /* Construct cubic spline interpolation with grid, view and specified first
     * derivatives at end points */
    cbspline_interpolation(const grid_type&, const view<double>&, double, double);

    /* Construct cubic spline interpolation with grid, view, spline matrix
     * and spline vector */
    cbspline_interpolation(const grid_type&, const view<double>&,
                           const Eigen::MatrixXd&, const Eigen::VectorXd&);

    /* Polynomial interpolation */
    double interpolate(double) const;

    /* Set second derivatives by solving the linear system mat*y2 = vec */
    void set_y2(const Eigen::MatrixXd&, const Eigen::VectorXd&);

private:
    grid_type grid_;
    view<double> yvals_;
    Eigen::VectorXd y2_;
};

/* Get basic spline matrix, i.e. with the last two equations left unspecified
 * (Eq. 3.3.7 times 6 in Press, Numerical Recipes) */
template <typename G>
Eigen::MatrixXd cbspline_interpolation<G>::spline_matrix(const grid_type& grid) {
    using namespace Eigen;
    MatrixXd mat = MatrixXd::Zero(grid.numpoints(), grid.numpoints());
    for (std::size_t i = 1; i < grid.numpoints() - 1; i++) {
        auto xjm1 = grid.value(i-1);
        auto xj = grid.value(i);
        auto xjp1 = grid.value(i+1);
        mat(i-1,i-1) = xj - xjm1;
        mat(i-1,i) = 2.0*(xjp1 - xjm1);
        mat(i-1,i+1) = xjp1 - xj;
    }
    return mat;
}

/* Get basic spline vector, i.e. with the last entries left
 * unspecified (Eq. 3.3.7 times 6 in Press, Numerical Recipes) */
template <typename G>
Eigen::VectorXd
cbspline_interpolation<G>::spline_vector(const grid_type& grid,
                                         const view<double>& yvals) {
    using namespace Eigen;
    VectorXd vec = VectorXd::Zero(grid.numpoints());
    for (std::size_t i = 1; i < grid.numpoints() - 1; i++) {
        auto xjm1 = grid.value(i-1);
        auto xj = grid.value(i);
        auto xjp1 = grid.value(i+1);
        vec(i-1) = 6.0*(yvals[i+1] - yvals[i])/(xjp1 - xj) -
                (yvals[i] - yvals[i-1])/(xj - xjm1);
    }
    return vec;
};

/* Construct natural cubic spline interpolation with grid and view */
template <typename G>
cbspline_interpolation<G>::cbspline_interpolation(const grid_type& grid,
                                                  const view<double>& yvals) :
    grid_(grid),
    yvals_(yvals),
    y2_(Eigen::VectorXd::Zero(grid_.numpoints()))
{
    detail::check_number_points(grid_, yvals_);
    auto np = grid_.numpoints();
    auto mat = spline_matrix(grid_);
    auto vec = spline_vector(grid_, yvals_);
    mat(np-2,0) = 1;
    vec(np-2) = 0;
    mat(np-1,np-1) = 1;
    vec(np-1) = 0;
    set_y2(mat, vec);
}

/* Construct cubic spline interpolation with grid, view and specified first
 * derivatives at end points */
template <typename G>
cbspline_interpolation<G>::cbspline_interpolation(const grid_type& grid,
                                                  const view<double>& yvals,
                                                  double yp0, double ypnm1) :
    grid_(grid),
    yvals_(yvals),
    y2_(Eigen::VectorXd::Zero(grid_.numpoints()))
{
    detail::check_number_points(grid_, yvals_);
    auto np = grid_.numpoints();
    auto mat = spline_matrix(grid_);
    auto vec = spline_vector(grid_, yvals_);
    mat(np-2,0) = -1.0/3.0*(grid_.value(1) - grid_.value(0));
    vec(np-2) = yp0 - (yvals_[1] - yvals_[0])/(grid_.value(1) - grid_.value(0));
    mat(np-1,np-1) = 1.0/3.0*(grid_.value(np-1) - grid_.value(np-2));
    vec(np-1) = ypnm1 - (yvals_[np-1] - yvals_[np-2])/
            (grid_.value(np-1) - grid_.value(np-2));
    set_y2(mat, vec);
}

/* Construct cubic spline interpolation with grid, view, spline matrix
 * and spline vector */
template <typename G>
cbspline_interpolation<G>::cbspline_interpolation(const grid_type& grid,
                                                  const view<double>& yvals,
                                                  const Eigen::MatrixXd& mat,
                                                  const Eigen::VectorXd& vec) :
    grid_(grid),
    yvals_(yvals),
    y2_(Eigen::VectorXd::Zero(grid_.numpoints()))
{
    detail::check_number_points(grid_, yvals_);
    set_y2(mat, vec);
}

/* Set second derivatives by solving the linear system mat*y2 = vec */
template <typename G>
void cbspline_interpolation<G>::set_y2(const Eigen::MatrixXd& mat,
                                       const Eigen::VectorXd& vec) {
    y2_ = mat.colPivHouseholderQr().solve(vec);
}

/* Cubic spline interpolation */
template <typename G>
double cbspline_interpolation<G>::interpolate(double x) const {
    std::size_t idx = inbetween_m_points(grid_.index(x), grid_.numpoints(), 2);
    double h = grid_.value(idx+1) - grid_.value(idx);
    double a = (grid_.value(idx+1) - x)/h;
    double b = (x - grid_.value(idx))/h;
    double y = a*yvals_[idx] + b*yvals_[idx+1] + ((a*a*a - a)*y2_[idx] +
        (b*b*b - b)*y2_[idx+1])*h*h/6.0;
    return y;
}

/**
 * @brief Extended trapezoidal quadrature rule.
 *
 * @details Implements the extended trapezoidal quadrature rule (see
 * Press, Numerical Recipes, p. 163).
 *
 * @tparam F Function type
 */
template <typename F>
class basic_quadrature {
public:
    using func_type = F;

public:

    /* Constructor */
    basic_quadrature(F&& f, double a, double b) :
        f_(std::forward<F>(f)),
        a_(a),
        b_(b),
        n_(0),
        curr_(0.0)
    {}

    /* Get current value of integral */
    double current() const { return curr_; }

    /* Get current level of accuracy */
    double level() const { return n_; }

    /* Calculate next level of accuracy by doubling the points */
    double next();

private:
    func_type f_;
    double a_;
    double b_;
    int n_;
    double curr_;
};

/* Calculate next level of accuracy */
template <typename F>
double basic_quadrature<F>::next() {
    n_++;
    if (n_ == 1) {
        curr_ = 0.5*(b_ - a_)*(f_(a_) + f_(b_));
    } else {
        int np = 1;
        for (int i = 1; i < n_ - 1; i++) {
            np <<= 1;
        }
        double step = (b_ - a_)/np;
        double x = a_ + 0.5*step;
        double sum = 0.0;
        for (int i = 0; i < np; i++) {
            sum += f_(x);
            x += step;
        }
        curr_ = 0.5*(curr_ + (b_ - a_)*sum/np);
    }
    return curr_;
}

/**
 * @brief Use trapezoidal quadrature to integrate a function until a certain
 * accuracy is achieved or the maximum number of iterations is reached.
 *
 * @tparam F Function type
 */
template <typename F>
double trapez_quad(F&& f, double a, double b, double eps = 1e-7, int max = 20,
                   int min = 1) {
    double current = 0.0;
    double old = 0.0;
    basic_quadrature quad(std::forward<F>(f), a, b);
    for (int i = 0; i < min; i++) {
        current = quad.next();
    }
    old = current;
    for (int i = min; i < max; i++) {
        current = quad.next();
        if (std::abs(current - old) < eps*std::abs(old) ||
            (current == 0.0 && old == 0.0))
            return current;
        old = current;
    }
    return current;
}

/**
 * @brief Use trapezoidal quadrature to integrate a discrete set of data defined
 * on an arbitrary grid.
 *
 * @tparam G Grid type
 */
template <typename G>
double trapez_quad(const G& grid, const view<double>& data) {
    // check data and grid
    detail::check_number_points(grid, data);

    // perform trapezoidal integration
    double res = 0.0;
    for (std::size_t i = 1; i < grid.numpoints(); i++) {
        res += (data[i] + data[i-1])*0.5*(grid.value(i) - grid.value(i-1));
    }

    return res;
}

/**
 * @brief Use Simpson quadrature to integrate a function until a certain
 * accuracy is achieved or the maximum number of iterations is reached.
 *
 * @tparam F Function type
 */
template <typename F>
double simpson_quad(F&& f, double a, double b, double eps = 1e-7, int max = 20,
                    int min = 1) {
    double current = 0.0;
    double old = 0.0;
    double tra_current = 0.0;
    double tra_old = 0.0;
    basic_quadrature quad(std::forward<F>(f), a, b);
    for (int i = 0; i < min; i++) {
        tra_current = quad.next();
        current = (4.0*tra_current - tra_old)/3.0;
        tra_old = tra_current;
    }
    old = current;
    for (int i = min; i < max; i++) {
        tra_current = quad.next();
        current = (4.0*tra_current - tra_old)/3.0;
        if (std::abs(current - old) < eps*std::abs(old) ||
            (current == 0.0 && old == 0.0))
            return current;
        old = current;
        tra_old = tra_current;
    }
    return current;
}

/**
 * @brief Use Simpson quadrature to integrate a discrete set of data defined
 * on an arbitrary grid.
 *
 * @tparam G Grid type
 */
template <typename G>
double simpson_quad(const G& grid, const view<double>& data) {
    // check data and grid
    detail::check_number_points(grid, data);

    // get difference array
    std::size_t n = grid.numpoints() - 1;
    std::vector<double> h(n);
    for (std::size_t i = 0; i < n; i++) {
        h[i] = grid.value(i+1) - grid.value(i);
    }

    // perform Simpson integration
    double res = 0.0;
    for (std::size_t i = 1; i < n; i += 2) {
        double hph = h[i] + h[i-1];
        res += data[i]*(h[i]*h[i]*h[i] + h[i-1]*h[i-1]*h[i-1] +
                3*h[i]*h[i-1]*hph)/(6*h[i]*h[i-1]);
        res += data[i-1]*(2*h[i-1]*h[i-1]*h[i-1] - h[i]*h[i]*h[i] +
                3*h[i]*h[i-1]*h[i-1])/(6*h[i-1]*hph);
        res += data[i+1]*(2*h[i]*h[i]*h[i] - h[i-1]*h[i-1]*h[i-1] +
                3*h[i-1]*h[i]*h[i])/(6*h[i]*hph);
    }
    if (grid.numpoints() % 2 == 0) {
        res += data[n]*(2*h[n-1]*h[n-1] + 3*h[n-2]*h[n-1])/
                (6*(h[n-2] + h[n-1]));
        res += data[n-1]*(h[n-1]*h[n-1] + 3*h[n-1]*h[n-2])/(6*h[n-2]);
        res -= data[n-2]*h[n-1]*h[n-1]*h[n-1]/(6*h[n-2]*(h[n-2] + h[n-1]));
    }

    return res;
}

} // namespace utils

} // namespace simpleMC
