/**
 * @file    random.h
 * @brief   Utilities for random number generation.
 * @author  Thomas Hahn
 */

#pragma once

#include <cstdint>
#include <cassert>
#include <array>
#include <istream>
#include <iostream>
#include <simpleMC/utils/traits.h>

namespace simpleMC {

namespace utils {

namespace detail {

/**
 * @brief Fills a 64-bit state with 32-bit numbers generated from a SeedSeq.
 *
 * @param seq Seed sequence
 * @param it Iterator to the beginning of the state
 * @tparam N Size of the state
 * @tparam SeedSeq Seed sequence type
 * @tparam Iterator Iterator type
 */
template <std::size_t N, typename SeedSeq, typename Iterator>
void fill_64bit_state(SeedSeq& seq, Iterator it) {
    std::array<std::uint32_t, N*2> arr;
    seq.generate(arr.begin(), arr.end());
    for (std::size_t i = 0; i < N; i++) {
        *it = static_cast<std::uint64_t>(arr[i*2]) << 32;
        *it += static_cast<std::uint64_t>(arr[i*2+1]);
        it++;
    }
}

/**
 * @brief Traits classes to disable seed() and ctors for certain types.
 */
template <typename T>
struct disable_random_seed :
        public disable_if<std::is_arithmetic<T>::value>
{};

template <typename E, typename T>
struct disable_random_ctor : public disable_random_seed<T> {};

template <typename E>
struct disable_random_ctor<E, E> {};

} // namespace detail

/**
 * @brief Variate generator to join a random number generator with a distribution.
 *
 * @details
 * Simple helper class to combine a random number generator with a certain
 * random number distribution. It is nearly a one-to-one copy from boost::random.
 * Therefore it is only needed if no boost dependency is wanted. Otherwise it is
 * recommended to use boost::random::variate_generator. The class should work at
 * least with engines and distributions from the standard library and
 * boost::random.
 *
 * @tparam Engine Random number generator
 * @tparam Distribution Random number distribution
 */
template <typename Engine, typename Distribution>
class variate_generator {
public:
    using engine_type = Engine;
    using distribution_type = Distribution;
    using result_type = typename distribution_type::result_type;

public:

    /* Constructor for the given engine and distribution */
    variate_generator(engine_type eng, distribution_type dist) :
        eng_(eng),
        dist_(dist)
    {}

    /* Generate a random number */
    result_type operator()() {
        return dist_(eng_);
    }

    template <typename T>
    result_type operator()(const T& value) {
        return dist_(eng_, value);
    }

    /* Getter methods for the engine and distribution */
    engine_type& engine() {
        return eng_;
    }

    const engine_type& engine() const {
        return eng_;
    }

    distribution_type& distribution() {
        return dist_;
    }

    const distribution_type& distribution() const {
        return dist_;
    }

    /* Minimum and maximum values of the underlying distribution */
    result_type min() const {
        return dist_.min();
    }

    result_type max() const {
        return dist_.max();
    }

private:
    engine_type eng_;
    distribution_type dist_;
};


/**
 * @brief Splitmix64 random number generator.
 *
 * @details
 * Splitmix64 is a very fast random number generator that passes the BigCrush
 * test. The following class is derived from the C code by Sebastiano Vigna.
 * The original C code can be found at http://xorshift.di.unimi.it/splitmix64.c.
 * We use it to seed the xoshiro256 class of PRNGs (see below).
 */
class splitmix64 {
public:
    using state_type = std::uint64_t;
    using result_type = std::uint64_t;

public:
    static constexpr std::uint64_t default_seed = 0x8a34e2345234fdb1;
    static constexpr std::size_t state_size = 1;
    static constexpr std::size_t word_size = 64;

    /* Smallest value that the generator can produce */
    static constexpr result_type min() {
        return 0;
    }

    /* Biggest value that the generator can produce */
    static constexpr result_type max() {
        return UINT64_MAX;
    }

    /* Constructs a splitmix64 with a seed */
    explicit splitmix64(std::uint64_t s = default_seed) :
        state_(s)
    {}

    /* Constructs a splitmix64 engine with a SeedSeq (maybe not correct!) */
    template <typename SeedSeq,
              typename = typename detail::disable_random_ctor<splitmix64,
                                                              SeedSeq>::type>
    explicit splitmix64(SeedSeq& seq) {
        seed(seq);
    }

    /* Seed the engine */
    void seed(std::uint64_t s = default_seed) {
        state_ = s;
    }

    template <typename SeedSeq,
              typename = typename detail::disable_random_seed<SeedSeq>::type>
    void seed(SeedSeq& seq) {
        detail::fill_64bit_state<state_size>(seq, &state_);
    }

    /* Generate a random number and advance state */
    result_type operator()() {
        result_type z = (state_ += static_cast<std::uint64_t>(0x9e3779b97f4a7c15));
        z = (z ^ (z >> 30)) * static_cast<std::uint64_t>(0xbf58476d1ce4e5b9);
        z = (z ^ (z >> 27)) * static_cast<std::uint64_t>(0x94d049bb133111eb);
        return z ^ (z >> 31);
    }

    /* Advance the state z times */
    void discard(std::uintmax_t z) {
        for(std::uintmax_t i = 0; i < z; i++) {
            this->operator()();
        }
    }

    /* Comparison operators */
    friend bool operator==(const splitmix64& lhs, const splitmix64& rhs) {
        return lhs.state_ == rhs.state_;
    }

    friend bool operator!=(const splitmix64& lhs, const splitmix64& rhs) {
        return !(lhs == rhs);
    }

    /* Stream operators */
    template <typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>& os, const splitmix64& eng) {
        os << eng.state_;
        return os;
    }

    template <typename CharT, typename Traits>
    friend std::basic_istream<CharT, Traits>&
    operator>>(std::basic_istream<CharT, Traits>& is, splitmix64& eng) {
        is >> eng.state_;
        return is;
    }

    /* Get state */
    std::uint64_t state() const {
        return state_;
    }

private:
    std::uint64_t state_;
};


/**
 * @brief Different types of xoshiro256 engines.
 */
enum class Xoshiro_type {
    plus,
    plusplus,
    starstar
};


/**
 * @brief Xoshiro256 random number generator.
 *
 * @details
 * Xoshiro256 are fast random number generators with a state of 256 bits.
 * The following class is derived from the C codes by David Blackman and
 * Sebastiano Vigna. The original C codes can be found at
 * http://prng.di.unimi.it/.
 *
 * @tparam X Type of xoshiro256 generator
 */
template <Xoshiro_type X>
class xoshiro256 {
public:
    using state_type = std::uint64_t;
    using result_type = std::uint64_t;
    using array_type = std::array<state_type, 4>;

public:
    static constexpr std::size_t state_size = 4;
    static constexpr std::size_t word_size = 64;

    /* Smallest value that the generator can produce */
    static constexpr result_type min() {
        return 0;
    }

    /* Biggest value that the generator can produce */
    static constexpr result_type max() {
        return UINT64_MAX;
    }

    /* Constructs a xoshiro256 engine with the help of splitmix64 */
    explicit xoshiro256(std::uint64_t s = splitmix64::default_seed) {
        seed(s);
    }

    /* Constructs a xoshiro256 engine with an array */
    explicit xoshiro256(std::uint64_t s0, std::uint64_t s1,
                        std::uint64_t s2, std::uint64_t s3) :
        state_(array_type{s0, s1, s2, s3})
    {}

    /* Constructs a xoshiro256 engine with a SeedSeq (maybe not correct!) */
    template <typename SeedSeq,
              typename = typename detail::disable_random_ctor<xoshiro256,
                                                              SeedSeq>::type>
    explicit xoshiro256(SeedSeq& seq) {
        seed(seq);
    }

    /* Seed the engine */
    void seed(std::uint64_t s = splitmix64::default_seed);

    void seed(std::uint64_t s0, std::uint64_t s1,
              std::uint64_t s2, std::uint64_t s3) {
        state_ = array_type{s0, s1, s2, s3};
    }

    template <typename SeedSeq,
              typename = typename detail::disable_random_seed<SeedSeq>::type>
    void seed(SeedSeq& seq) {
        detail::fill_64bit_state<state_size>(seq, state_.begin());
    }

    /* Generate a random number and advance state */
    result_type operator()() {
        std::uint64_t result = type_specific();
        std::uint64_t t = state_[1] << 17;
        state_[2] ^= state_[0];
        state_[3] ^= state_[1];
        state_[1] ^= state_[2];
        state_[0] ^= state_[3];
        state_[2] ^= t;
        state_[3] ^= rotl(state_[3], 45);
        return result;
    }

    /* Advance the state z times */
    void discard(std::uintmax_t z) {
        for(std::uintmax_t i = 0; i < z; i++) {
            this->operator()();
        }
    }

    /* Comparison operators */
    friend bool operator==(const xoshiro256& lhs, const xoshiro256& rhs) {
        return lhs.state_ == rhs.state_;
    }

    friend bool operator!=(const xoshiro256& lhs, const xoshiro256& rhs) {
        return !(lhs == rhs);
    }

    /* Stream operators */
    template <typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>& os, const xoshiro256& eng) {
        os << eng.state_[0] << ' '
           << eng.state_[1] << ' '
           << eng.state_[2] << ' '
           << eng.state_[3];
        return os;
    }

    template <typename CharT, typename Traits>
    friend std::basic_istream<CharT, Traits>&
    operator>>(std::basic_istream<CharT, Traits>& is, xoshiro256& eng) {
        is >> eng.state_[0] >> std::ws
           >> eng.state_[1] >> std::ws
           >> eng.state_[2] >> std::ws
           >> eng.state_[3];
        return is;
    }

    /* Jumps ahead 2^128 states */
    void jump() {
        static const array_type arr = array_type{
            0x180ec6d33cfd0aba, 0xd5a61266f0c9392c,
            0xa9582618e03fc9aa, 0x39abdc4529b1661c};
        base_jump(arr);
    }

    /* Jumps ahead 2^192 states */
    void long_jump() {
        static const array_type arr = array_type{
                0x76e15d3efefdcbbf, 0xc5004e441c522fb3,
                0x77710069854ee241, 0x39109bb02acbe635};
        base_jump(arr);
    }

    /* Get state */
    const array_type& state() const {
        return state_;
    }

private:
    /* Perform a left rotation */
    std::uint64_t rotl(std::uint64_t x, int k) {
        return (x << k) | (x >> (64 - k));
    }

    /* Generates a new random number from the current state (dependent on the
     * Xoshiro_type */
    std::uint64_t type_specific();

    /* Performs the actual jumps */
    void base_jump(const array_type&);

private:
    array_type state_;
};

/* Seed the engine */
template <Xoshiro_type X>
void xoshiro256<X>::seed(std::uint64_t s) {
    splitmix64 sm(s);
    state_[0] = sm();
    state_[1] = sm();
    state_[2] = sm();
    state_[3] = sm();
}

/* Performs the actual jumps */
template <Xoshiro_type X>
void xoshiro256<X>::base_jump(const array_type& arr) {
    std::uint64_t s0 = 0;
    std::uint64_t s1 = 0;
    std::uint64_t s2 = 0;
    std::uint64_t s3 = 0;
    for(std::size_t i = 0; i < arr.size(); i++) {
        for(int b = 0; b < 64; b++) {
            if (arr[i] & static_cast<std::uint64_t>(1) << b) {
                s0 ^= state_[0];
                s1 ^= state_[1];
                s2 ^= state_[2];
                s3 ^= state_[3];
            }
            this->operator()();
        }
    }
    state_[0] = s0;
    state_[1] = s1;
    state_[2] = s2;
    state_[3] = s3;
}

template <>
inline std::uint64_t xoshiro256<Xoshiro_type::plus>::type_specific() {
    return state_[0] + state_[3];
}

template <>
inline std::uint64_t xoshiro256<Xoshiro_type::plusplus>::type_specific() {
    return rotl(state_[0] + state_[3], 23) + state_[0];
}

template <>
inline std::uint64_t xoshiro256<Xoshiro_type::starstar>::type_specific() {
    return rotl(state_[1] * 5, 7) * 9;
}

/**
 * @brief Use xoshiro256p (= xoshiro256+) only for floating-point generation.
 */
using xoshiro256p = xoshiro256<Xoshiro_type::plus>;

/**
 * @brief Use xoshiro256pp (= xoshiro256++) for all purposes.
 */
using xoshiro256pp = xoshiro256<Xoshiro_type::plusplus>;

/**
 * @brief Use xoshiro256ss (= xoshiro256**) for all purposes.
 */
using xoshiro256ss = xoshiro256<Xoshiro_type::starstar>;


/**
 * @brief Uniform real distribution between a and b.
 *
 * @details
 * Models a uniform distribution on the interval [min_, max_). It is basically
 * a copy from boost::random::uniform_real_distribution, except the way
 * floating-point numbers are generated. Floating-point number generation is
 * taken from http://prng.di.unimi.it/ and it is intended to be used with
 * xoshrio256 generators (although any other 64-bit engine can be used as well
 * if it generates std::uint64_t numbers). On each vocation it generates a
 * random double uniformly distributed between [min_,max_).
 */
class uniform_real_distribution {
public:
    using result_type = double;

    class param_type {
    public:
        using distribution_type = uniform_real_distribution;

    public:

        /* Constructs the parameters of the distribution */
        explicit param_type(double min_arg = 0.0, double max_arg = 1.0) :
            min_(min_arg),
            max_(max_arg)
        {
            assert(min_ < max_);
        }

        /* Return minimum and maximum values of the distribution */
        double a() const {
            return min_;
        }

        double b() const {
            return max_;
        }

        /* Stream operators */
        template <typename CharT, typename Traits>
        friend std::basic_ostream<CharT, Traits>&
        operator<<(std::basic_ostream<CharT, Traits>& os,
                   const param_type& parm) {
            os << parm.min_ << ' '
               << parm.max_;
            return os;
        }

        template <typename CharT, typename Traits>
        friend std::basic_istream<CharT, Traits>&
        operator>>(std::basic_istream<CharT, Traits>& is, param_type& parm) {
            is >> parm.min_ >> std::ws
               >> parm.max_;
            assert(parm.min_ < parm.max_);
            return is;
        }

        /* Comparison operators */
        friend bool operator==(const param_type& lhs, const param_type& rhs) {
            return (lhs.min_ == rhs.min_ && lhs.max_ == rhs.max_);
        }

        friend bool operator!=(const param_type& lhs, const param_type& rhs) {
            return !(lhs == rhs);
        }

    private:
        double min_;
        double max_;
    };

public:

    /* Constructs a uniform_real_distribution on [min_arg, max_arg) */
    explicit uniform_real_distribution(double min_arg = 0.0,
                                       double max_arg = 1.0) :
        min_(min_arg),
        max_(max_arg)
    {}

    /* Constructs a uniform_real_distribution from given parameters */
    explicit uniform_real_distribution(const param_type& parm) :
        min_(parm.a()),
        max_(parm.b())
    {}

    /* Minimum and maximum values of the distribution */
    double min() const {
        return min_;
    }

    double a() const {
        return min_;
    }

    double max() const {
        return max_;
    }

    double b() const {
        return max_;
    }

    /* Get and set parameters of the disbtribution */
    param_type param() const {
        return param_type(min_, max_);
    }

    void param(const param_type& parm) {
        min_ = parm.a();
        max_ = parm.b();
    }

    /* Resets the internal state of the distribution. The next call to operator()
     * on the distribution will not depend on values produced by any engine prior
     * to reset(). */
    void reset() {}

    /* Random number in the range [min_,max_) */
    template <typename Engine>
    double operator()(Engine& eng) const {
        return generate_uniform_real(eng, min_, max_);
    }

    /* Random number in the range [parm.a(),parm.b()) */
    template <typename Engine>
    double operator()(Engine& eng, const param_type& parm) const {
        return generate_uniform_real(eng, parm.a(), parm.b());
    }

    /* Stream operators */
    template <typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>& os,
               const uniform_real_distribution& ud) {
        os << ud.param();
        return os;
    }

    template <typename CharT, typename Traits>
    friend std::basic_istream<CharT, Traits>&
    operator>>(std::basic_istream<CharT, Traits>& is,
               uniform_real_distribution& ud) {
        param_type parm;
        if (is >> parm) {
            ud.param(parm);
        }
        return is;
    }

    /* Comparison operators */
    friend bool operator==(const uniform_real_distribution& lhs,
                           const uniform_real_distribution& rhs) {
        return (lhs.min_ == rhs.min_ && lhs.max_ == rhs.max_);
    }

    friend bool operator!=(const uniform_real_distribution& lhs,
                           const uniform_real_distribution& rhs) {
        return !(lhs == rhs);
    }

private:
    /* Generate a double on [min_value, max_value) given a 64-bit engine */
    template <typename Engine>
    double generate_uniform_real(Engine& eng, double min_value,
                                 double max_value) const {
        static_assert(std::is_same<typename Engine::result_type,
                std::uint64_t>::value,
                "Engine::result_type must be std::uint64_t");
        assert(min_value < max_value);
        return min_value + (max_value - min_value)*((eng() >> 11) * 0x1.0p-53);
    }

private:
    double min_;
    double max_;
};

} // namespace utils

} // namespace simpleMC
