/**
 * @file    noncopyable.h
 * @brief   CRTP base class for making derived classes non-copyable.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleMC {

namespace utils {

/**
 * @brief CRTP base class for making derived classes non-copyable.
 *
 * @details Inherit from this class to prevent the derived class from being
 * copyable.
 *
 * @tparam D Derived type
 */
template <class D>
class noncopyable
{
public:
    /* Delete copy constructor and copy assignment */
    noncopyable(const noncopyable&) = delete;
    D& operator=(const D&) = delete;

protected:
    /* This class needs to be inherited */
    noncopyable() = default;
    ~noncopyable() = default;
};

} // namespace utils

} // namespace simpleMC
