/**
 * @file    grids.h
 * @brief   Different kinds of 1-dimensional grids and a generic n-dimensional
 *          grid.
 * @details We use the following definition of a grid: A grid is a strictly
 *          monotonic function from the natural numbers (0, 1, ..., N-1) to
 *          some interval [a,b]. Elements of the domain are of type index_type
 *          and elements of the range are of type value_type. Let t(j) be a grid.
 *          Then we always have t(0) = a and t(N-1) = b. A bin is the area
 *          between to adjacent grid points. The size of a bin is
 *          abs(t(j+1) - t(j)). A grid therefore defines N-1 bins.
 * @author  Thomas Hahn
 */

#pragma once

#include <cstddef>
#include <cassert>
#include <cmath>
#include <simpleMC/utils/algorithms.h>
#include <simpleMC/utils/tuple.h>

namespace simpleMC {

namespace utils {

/**
 * @brief 1-dimensional linear grid.
 *
 * @details
 * Defines a linear 1-dimensional grid on the interval [a,b]. Grid points are
 * equally spaced, i.e. t(j) = x + y*j, where x = t(0) and
 * y = (t(N-1) - t(0))/(N - 1).
 */
class linear_grid {
public:
    using value_type = double;
    using index_type = std::size_t;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:
    /* Default constructor */
    linear_grid() = default;

    /* Construct a grid between [start, stop] with numpoints points */
    linear_grid(value_type start, value_type stop, index_type numpoints) :
        start_(start),
        stop_(stop),
        numpoints_(numpoints),
        step_(calculate_step()),
        binsize_(std::abs(step_))
    {
        assert(start_ != stop_ && numpoints_ > 1);
    }

    /* Set start, stop and number of points */
    void set(value_type start, value_type stop, index_type numpoints) {
        assert(start != stop && numpoints > 1);
        start_ = start;
        stop_ = stop;
        numpoints_ = numpoints;
        step_ = calculate_step();
        binsize_ = std::abs(step_);
    }

    /* Get start */
    value_type start() const { return start_; }

    /* Get stop */
    value_type stop() const { return stop_; }

    /* Get number of grid points */
    index_type numpoints() const { return numpoints_; }

    /* Given an index, get the grid value */
    value_type value(index_type index) const {
        assert(index < numpoints_);
        return start_ + step_*index;
    }

    /* Given two indices, return the value between the grid points */
    value_type half_value(index_type index1, index_type index2) const {
        assert(index1 < numpoints_ && index2 < numpoints_);
        return (value(index2) + value(index1))*0.5;
    }

    /* Given a point, get the index of the closest (left) grid point */
    index_type index(value_type value) const {
        assert((value >= start_ && value <= stop_) ||
               (value <= start_ && value >= stop_));
        return static_cast<index_type>((value - start_)/step_);
    }

    /* Get bin size of specific bin */
    value_type binsize(index_type /*index*/) const {
        return binsize_;
    }

private:

    /* Calculate step parameter */
    value_type calculate_step() const {
        return (stop_ - start_)/(numpoints_ - 1);
    }

private:
    value_type start_{ 0.0 };
    value_type stop_{ 0.0 };
    index_type numpoints_{ 0 };
    value_type step_{ 0.0 };
    value_type binsize_{ 0.0 };
};

/**
 * @brief 1-dimensional power grid.
 *
 * @details
 * Defines a 1-dimensional power grid on the interval [a,b]. Grid points are
 * calculated as t(j) = x + y*j**d, with d >= 1 being the specified
 * power and where x = t(0) and y = (t(N-1) - t(0))/(N - 1)**d.
 */
class power_grid {
public:
    using value_type = double;
    using index_type = std::size_t;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Default constructor */
    power_grid() = default;

    /* Construct a grid between [start, stop] with numpoints points */
    power_grid(value_type start, value_type stop, index_type numpoints,
               std::size_t power = 2) :
        start_(start),
        stop_(stop),
        numpoints_(numpoints),
        power_(power),
        step_(calculate_step())
    {
        assert(start_ != stop_ && numpoints_ > 1);
        assert(power >= 1);
    }

    /* Set start, stop and number of points */
    void set(value_type start, value_type stop, index_type numpoints,
             std::size_t power = 2) {
        assert(start != stop && numpoints > 1);
        assert(power >= 1);
        start_ = start;
        stop_ = stop;
        numpoints_ = numpoints;
        power_ = power;
        step_ = calculate_step();
    }

    /* Get start */
    value_type start() const { return start_; }

    /* Get stop */
    value_type stop() const { return stop_; }

    /* Get number of grid points */
    index_type numpoints() const { return numpoints_; }

    /* Get power */
    std::size_t power() const { return power_; }

    /* Given an index, get the grid value */
    value_type value(index_type index) const {
        assert(index < numpoints_);
        return start_ + step_*std::pow(index, power_);
    }

    /* Given two indices, return the value between the grid points */
    value_type half_value(index_type index1, index_type index2) const {
        assert(index1 < numpoints_ && index2 < numpoints_);
        return (value(index2) + value(index1))*0.5;
    }

    /* Given a point, get the index of the closest (left) grid point */
    index_type index(value_type value) const {
        assert((value >= start_ && value <= stop_) ||
               (value <= start_ && value >= stop_));
        return static_cast<index_type>(std::pow((value - start_)/step_,
                                       1.0/power_));
    }

    /* Get bin size of specific bin */
    value_type binsize(index_type index) const {
        assert(index < numpoints_ - 1);
        return std::abs(value(index + 1) - value(index));
    }

private:

    /* Calculate step parameter */
    value_type calculate_step() const {
        return (stop_ - start_)/std::pow(numpoints_ - 1.0, power_);
    }

private:
    value_type start_{ 0.0 };
    value_type stop_{ 0.0 };
    index_type numpoints_{ 0 };
    std::size_t power_{ 0 };
    value_type step_{ 0.0 };
};

/**
 * @brief 1-dimensional symmetric grid, based on power_grid.
 *
 * @details
 * Defines a 1-dimensional symmetric grid on the interval [a,b] with N points
 * (N is odd). Let c be the midpoint of the interval, i.e. c = 0.5*(a + b),
 * and j_c = floor(N/2) be the corresponding index. Let g1 be the grid
 * defined on [a,c] with j_c points and let g2 be the descending grid defined
 * on [c,b] with j_c points such that g1(j_c) = g2(j_c). The grid points
 * are calculated as follows:
 * -) j <= j_c: t(j) = g1(j)
 * -) j > j_c: t(j) = g2(2*jc - j)
 */
class symmetric_grid {
public:
    using grid_type = power_grid;
    using value_type = grid_type::value_type;
    using index_type = grid_type::index_type;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Default constructor */
    symmetric_grid() = default;

    /* Construct a grid between [start, stop] with numpoints points */
    symmetric_grid(value_type start, value_type stop, index_type numpoints,
                   std::size_t power = 2) :
        mid_(start + (stop - start)*0.5),
        numpoints_(numpoints + ((numpoints + 1) % 2)),
        jc_(static_cast<index_type>(numpoints/2)),
        g1_(start, mid_, jc_ + 1, power),
        g2_(stop, mid_, jc_ + 1, power)
    {}

    /* Set start, stop and number of points */
    void set(value_type start, value_type stop, index_type numpoints,
             std::size_t power = 2) {
        mid_ = start + (stop - start)*0.5;
        numpoints_ = numpoints + ((numpoints + 1) % 2);
        jc_ = static_cast<index_type>(numpoints/2);
        g1_.set(start, mid_, jc_ + 1, power);
        g2_.set(stop, mid_, jc_ + 1, power);
    }

    /* Get start */
    value_type start() const { return g1_.start(); }

    /* Get stop */
    value_type stop() const { return g2_.start(); }

    /* Get number of grid points */
    index_type numpoints() const { return numpoints_; }

    /* Get power */
    std::size_t power() const { return g1_.power(); }

    /* Given an index, get the grid value */
    value_type value(index_type index) const {
        assert(index < numpoints_);
        if (index <= jc_) {
            return g1_.value(index);
        }
        return g2_.value(2*jc_ - index);
    }

    /* Given two indices, return the value between the grid points */
    value_type half_value(index_type index1, index_type index2) const {
        assert(index1 < numpoints_ && index2 < numpoints_);
        return (value(index2) + value(index1))*0.5;
    }

    /* Given a point, get the index of the closest (left) grid point */
    index_type index(value_type value) const {
        assert((value >= start() && value <= stop()) ||
               (value <= start() && value >= stop()));
        if (value <= mid_) {
            return g1_.index(value);
        } else if (value == stop()) {
            return 2*jc_;
        }
        return numpoints_ - 2 - g2_.index(value);
    }

    /* Get bin size of specific bin */
    value_type binsize(index_type index) const {
        assert(index < numpoints_ - 1);
        if (index < jc_) {
            return g1_.binsize(index);
        }
        return g2_.binsize(numpoints_ - 2 - index);
    }

private:
    value_type mid_{ 0.0 };
    index_type numpoints_{ 0 };
    index_type jc_{ 0 };
    grid_type g1_;
    grid_type g2_;
};

/**
 * @brief Index grid.
 *
 * @details
 * Defines an index grid on the natural numbers. It is defined as t(j) = j,
 * where j = 0, ..., N-1. The bin size is always 1.
 */
class index_grid {
public:
    using value_type = std::size_t;
    using index_type = std::size_t;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Default constructor */
    index_grid() = default;

    /* Construct a grid between [start, stop] with numpoints points */
    index_grid(index_type numpoints) :
        start_(0),
        stop_(numpoints - 1),
        numpoints_(numpoints)
    {
        assert(numpoints_ != 0);
    }

    /* Set number of points */
    void set(index_type numpoints) {
        assert(numpoints != 0);
        start_ = 0;
        stop_ = numpoints - 1;
        numpoints_ = numpoints;
    }

    /* Get start */
    value_type start() const { return start_; }

    /* Get stop */
    value_type stop() const { return stop_; }

    /* Get number of grid points */
    index_type numpoints() const { return numpoints_; }

    /* Given an index, get the grid value */
    value_type value(index_type index) const {
        assert(index < numpoints_);
        return index;
    }

    /* Given two indices, return the value between the grid points */
    value_type half_value(index_type index1, index_type index2) const {
        assert(index1 < numpoints_ && index2 < numpoints_);
        return (value(index2) + value(index1))/2;
    }

    /* Given a point, get the index of the closest (left) grid point */
    index_type index(value_type value) const {
        assert(value < numpoints_);
        return value;
    }

    /* Get bin size of specific bin */
    value_type binsize(index_type /*index*/) const {
        return 1;
    }

private:
    value_type start_{ 0 };
    value_type stop_{ 0 };
    index_type numpoints_{ 0 };
};

/**
 * @brief Arbitrary 1-dimensional grid.
 *
 * @details
 * Defines a class template for arbitrary 1-dimensional grids. It is constructed
 * from a container containing the grid points and a function object used for
 * finding the correct index given a value.
 *
 * @tparam V Value type
 * @tparam I Index type
 * @tparam C Containter type
 * @tparam S Function object used for searching
 */
template <typename V, typename I, typename C = std::vector<V>,
          typename S = bisector>
class custom_grid {
public:
    using value_type = V;
    using index_type = I;
    using container_type = C;
    using searcher_type = S;

public:

    /* Get dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Default constructor */
    custom_grid() = default;

    /* Construct a grid with grid points given in container */
    custom_grid(const container_type& container, searcher_type searcher) :
        container_(container),
        searcher_(searcher),
        numpoints_(container_.size())
    {}

    /* Set grid points and search function */
    void set(const container_type& container, searcher_type searcher) {
        container_ = container;
        searcher_ = searcher;
        numpoints_ = container_.size();
    }

    /* Get start */
    value_type start() const { return container_[0]; }

    /* Get stop */
    value_type stop() const { return container_[numpoints_ - 1]; }

    /* Get number of grid points */
    index_type numpoints() const { return numpoints_; }

    /* Given an index, get the grid value */
    value_type value(index_type index) const {
        assert(index < numpoints_);
        return container_[index];
    }

    /* Given two indices, return the point half way inbetween */
    value_type half_value(index_type index1, index_type index2) const {
        assert(index1 < numpoints_ && index2 < numpoints_);
        return (value(index2) + value(index1))*0.5;
    }

    /* Given a point, get the index of the closest (left) grid point */
    index_type index(value_type value) const {
        assert((value >= start() && value <= stop()) ||
               (value <= start() && value >= stop()));
        return searcher_(container_, value);
    }

    /* Get bin size of specific bin */
    value_type binsize(index_type index) const {
        assert(index < numpoints_ - 1);
        return std::abs(value(index) - value(index + 1));
    }

private:
    container_type container_;
    searcher_type searcher_;
    index_type numpoints_{ 0 };
};

/**
 * @brief Generic n-dimensional grid.
 *
 * @details
 * Defines a class template for a generic n-dimensional grid. The underlying
 * 1-dimensional grids are assumed to be derived classes from the
 * gridInterface class.
 *
 * @tparam Gs Grid types
 */
template <typename Index, typename... Gs>
class nd_grid {
public:
    using tuple_type = std::tuple<Gs...>;
    using index_type = Index;
    using value_type = double;
    using indexarray_type = std::array<index_type, std::tuple_size<tuple_type>::value>;
    using valuearray_type = std::array<value_type, std::tuple_size<tuple_type>::value>;
    static_assert (sizeof...(Gs) > 0, "0 dimensions in nd_grid not allowed");

public:

    /* Get dimensions */
    static constexpr index_type dim() {
        return std::tuple_size<tuple_type>::value;
    }

public:

    /* Default constructor */
    nd_grid() :
        grids_()
    {}

    /* Construct a ndGrid with n grids */
    nd_grid(const Gs&... gs) :
        grids_(gs...)
    {}

    /* Get ith grid */
    template <std::size_t I>
    auto& get() { return std::get<I>(grids_); }

    template <std::size_t I>
    const auto& get() const { return std::get<I>(grids_); }

    /* Get all grids as tuple */
    tuple_type& get_grids() { return grids_; }

    const tuple_type& get_grids() const { return grids_; }

    /* Get start */
    valuearray_type start() const {
        std::size_t i = 0;
        valuearray_type res;
        for_each_tuple([&i, &res](auto&& elem) {
            res[i++] = elem.start();
        }, grids_);
        return res;
    }

    /* Get stop */
    valuearray_type stop() const {
        std::size_t i = 0;
        valuearray_type res;
        for_each_tuple([&i, &res](auto&& elem) {
            res[i++] = elem.stop();
        }, grids_);
        return res;
    }

    /* Get number of grid points */
    index_type numpoints() const {
        index_type res = 1;
        for_each_tuple([&res](auto&& elem) {
            res *= elem.numpoints();
        }, grids_);
        return res;
    }

    /* Given indices, get the grid point */
    valuearray_type value(const indexarray_type& indices) const {
        std::size_t i = 0;
        valuearray_type res;
        for_each_tuple([&i, &res, &indices](auto&& elem) {
            res[i] = elem.value(indices[i]);
            i++;
        }, grids_);
        return res;
    }

    template <typename... Idx>
    typename std::enable_if<sizeof...(Idx) == dim(), valuearray_type>::type
    value(Idx... idx) const {
        return value(indexarray_type{idx...});
    }

    /* Given two index arrays, get the point half way in between */
    valuearray_type half_value(const indexarray_type& indices1,
                               const indexarray_type& indices2) const {
        std::size_t i = 0;
        valuearray_type res;
        for_each_tuple([&i, &res, &indices1, &indices2](auto&& elem) {
            res[i] = elem.half_value(indices1[i], indices2[i]);
            i++;
        }, grids_);
        return res;
    }

    /* Given a point, get the indices of the closest (left) grid point */
    indexarray_type index(const valuearray_type& values) const {
        std::size_t i = 0;
        indexarray_type res;
        for_each_tuple([&i, &res, &values](auto&& elem) {
            res[i] = elem.index(values[i]);
            i++;
        }, grids_);
        return res;
    }

    template <typename... Vals>
    typename std::enable_if<sizeof...(Vals) == dim(), indexarray_type>::type
    index(Vals... vals) const {
        return index(valuearray_type{vals...});
    }

    /* Get bin size of specific bin */
    value_type binsize(const indexarray_type& indices) const {
        std::size_t i = 0;
        value_type res = 1;
        for_each_tuple([&i, &res, &indices](auto&& elem) {
            res *= elem.binsize(indices[i]);
            i++;
        }, grids_);
        return res;
    }

    template <typename... Idx>
    typename std::enable_if<sizeof...(Idx) == dim(), value_type>::type
    binsize(Idx... idx) const {
        return binsize(indexarray_type{idx...});
    }

private:
    tuple_type grids_;
};

} // namespace utils

} // namespace simpleMC
