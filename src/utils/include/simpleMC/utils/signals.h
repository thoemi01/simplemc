/**
 * @file    signals.h
 * @brief   Simple signal listener. Based on triqs::signal_handler.
 * @author  Thomas Hahn
 */

#pragma once

namespace simpleMC {

namespace signals {

/**
 * @brief Default signal handler.
 */
void default_handler(int);

/**
 * @brief Start listening to signals and specify a handling function.
 */
void start_listening(void(*handler)(int) = &default_handler);

/**
 * @brief Stop listening to signals.
 */
void stop_listening();

/**
 * @brief Determines if a signal has been received.
 */
bool empty();

/**
 * @brief Get the last signal that has been received.
 */
int last();

/**
 * @brief Pop the last signal that has been received.
 */
void pop();

} // namespace signals

} // namespace simpleMC
