/**
 * @file    algorithms.h
 * @brief   Some useful algorithms.
 * @author  Thomas Hahn
 */

#pragma once

#include <algorithm>
#include <vector>
#include <numeric>

namespace simpleMC {

namespace utils {

/**
 * @brief Returns a permutation vector according to the comparison function
 * applied to the elements of the container.
 *
 * @tparam C Container type
 * @tparam F Comparison operation
 * @param cont Container to be permutated
 * @param func Comparison function
 * @return Permutation vector
 */
template <typename C, typename F>
std::vector<std::size_t> sort_permutation(const C& cont, F&& func) {
    std::vector<std::size_t> p(cont.size());
    std::iota(p.begin(), p.end(), 0);
    std::sort(p.begin(), p.end(), [&cont, &func](std::size_t i, std::size_t j){
        return func(cont[i], cont[j]);
    });
    return p;
}

/**
 * @brief Apply a permutation vector to a container and return a new container
 * with its elements sorted according to the permutation.
 *
 * @tparam C Container type
 * @param cont Container to be permutated
 * @param p Permutation vector
 * @return Copy of cont with its elements permutated according to p
 */
template <typename C>
C apply_permutation(const C& cont, const std::vector<std::size_t>& p) {
    C sorted_cont(cont.size());
    std::transform(p.begin(), p.end(), sorted_cont.begin(),
                   [&cont](std::size_t i){
        return cont[i];
    });
    return sorted_cont;
}

/**
 * @brief Uses bisection to find a position in a sorted container.
 *
 * @details A binary search algorithm is used to find the position j in the container C
 * such that
 * C[j] <= value < C[j+1] if the container is sorted in ascending order, else
 * C[j] >= value > C[j+1].
 *
 * @tparam C Container type
 * @tparam T Value type
 * @param container Sorted container with elements of type T
 * @param value     Value to be searched for
 * @return Position/Index j
 */
template <typename C, typename T>
decltype(auto) bisection(const C& container, const T& value) {
    auto numelem = container.size();
    decltype(numelem) left = 0;
    decltype(numelem) right = numelem - 1;
    // elements of container are in ascending order
    if (container[0] < container[numelem - 1]) {
        while (right - left > 1) {
            auto mid = (right + left)/2;
            if (value >= container[mid])
                left = mid;
            else
                right = mid;
        }
    // elements of container are in descending order
    } else {
        while (right - left > 1) {
            auto mid = (right + left)/2;
            if (value >= container[mid])
                right = mid;
            else
                left = mid;
        }
    }
    // return the exact point j if value == container[j]
    if (value == container[right])
        left = right;
    return left;
}

/**
 * @brief Functor wrapping the bisection function.
 */
class bisector {
public:
    template <typename C, typename T>
    decltype(auto) operator()(const C& container, const T& value) const {
        return bisection(container, value);
    }
};

/**
 * @brief Calculates the index of a subset such that the current position lies
 * in the middle of it.
 *
 * @details
 * Let I = {0, 1, ..., n-1}. Given the current position l in the set, we want
 * to find the position j of the subset {j, j+1, ..., j+m-1} such that l lies
 * in the middle of this subset (insofar as this is possible considering the
 * boundaries, i.e. j >= 0 and j <= n-1).
 *
 * @param l Current position
 * @param n Length/Size of the array/grid
 * @param m Size of the subinterval
 */
inline int inbetween_m_points(int l, int n, int m) {
    return std::max(0, std::min(n - m, l - (m - 2)/2));
}

} // namespace utils

} // namespace simpleMC
