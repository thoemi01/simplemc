/**
 * @file    traits.h
 * @brief   Type traits utilities.
 * @author  Thomas Hahn
 */

#pragma once

#include <type_traits>

namespace simpleMC {

namespace utils {

namespace detail {

/**
 * @brief Implementation of void_t since older compilers don't recognize it.
 */
template<typename... Ts> struct make_void { typedef void type;};
template<typename... Ts> using void_t = typename make_void<Ts...>::type;

template <template <typename...> typename Z, typename = void, typename... Ts>
struct can_apply: public std::false_type {};

template <template <typename...> typename Z, typename... Ts>
struct can_apply<Z, void_t<Z<Ts...>>, Ts...> : public std::true_type {};

} // namespace detail

/**
 * @brief Disable a type if condition is fulfilled. (Opposite of std::enable_if)
 *
 * @tparam B Boolean value
 * @tparam T Enabled type if B is false
 */
template <bool B, typename T = void>
struct disable_if {
    using type = T;
};

template <typename T>
struct disable_if<true, T> {};

template <bool B, typename T = void>
using disable_if_t = typename disable_if<B, T>::type;

/**
 * @brief Check if type Z<Ts...> is well formed.
 */
template <template <typename...> typename Z, typename... Ts>
using can_apply = detail::can_apply<Z, void, Ts...>;

} // namespace utils

} // namespace simpleMC
