/**
 * @file    view_eigen.h
 * @brief   Create views on Eigen types.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/traits_eigen.h>
#include <simpleMC/utils/view.h>

namespace simpleMC {

namespace utils {

/**
 * @brief Create view on type derived from Eigen::DenseBase<D> or
 * Eigen::TensorBase<D>
 */
template <typename D>
inline typename
std::enable_if<is_eigen_v<D>, view<typename D::Scalar>>::type
make_view(const D& obj) {
    using value_type = typename D::Scalar;
    // const_cast feels suspicious
    return view<value_type>(const_cast<value_type*>(obj.data()), obj.size());
}

/* Disallow temporaries */
template <typename D>
inline typename
std::enable_if<is_eigen_v<D>, const view<typename D::Scalar>>::type
make_view(const D&& obj) = delete;


/**
 * @brief Create ndview on type derived from Eigen::DenseBase<D>
 */
template <typename D>
inline typename
std::enable_if<is_eigen_dense_v<D>, nd_view<typename D::Scalar, 2>>::type
make_ndview(const D& obj) {
    using value_type = typename D::Scalar;
    // const_cast feels suspicious
    return nd_view<value_type, 2>(const_cast<value_type*>(obj.data()),
                                 {obj.rows, obj.cols});
}

/* Disallow temporaries */
template <typename D>
inline typename
std::enable_if<is_eigen_dense_v<D>, nd_view<typename D::Scalar, 2>>::type
make_ndview(const D&& obj) = delete;


/**
 * @brief Create ndview on type derived from Eigen::TensorBase<D>
 */
template <typename D>
typename std::enable_if<is_eigen_tensor_v<D>,
nd_view<typename D::Scalar, D::NumDimensions>>::type
make_ndview(const D& obj) {
    using value_type = typename D::Scalar;
    using result_type = nd_view<value_type, D::NumDimensions>;
    typename result_type::shape_type shape;
    const auto& dims = obj.dimensions();
    for (std::size_t i = 0; i < D::NumDimensions; i++) {
        shape[i] = dims[i];
    }
    // const_cast feels suspicious
    return result_type(const_cast<value_type*>(obj.data()), shape);
}

/* Disallow temporaries */
template <typename D>
inline typename std::enable_if<is_eigen_tensor_v<D>,
nd_view<typename D::Scalar, D::NumDimensions>>::type
make_ndview(const D&& obj) = delete;

} // namespace utils

} // namespace simpleMC
