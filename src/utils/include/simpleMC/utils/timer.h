/**
 * @file    timer.h
 * @brief   Timer built on std::chrono.
 * @author  Thomas Hahn
 */

#pragma once

#include <chrono>

namespace simpleMC {

namespace utils {

/**
 * @brief Convenience typdefs for duration casting.
 */
struct duration {
public:
    using sec_d = std::chrono::duration<double, std::ratio<1>>;
    using millisec_d = std::chrono::duration<double, std::milli>;
    using microsec_d = std::chrono::duration<double, std::micro>;
    using nanosec_d = std::chrono::duration<double, std::nano>;
};

/**
 * @brief Timer for measuring time intervals.
 *
 * @details Simple timer class to measure time intervals built on top of
 * std::chrono clock types. It only supports starting and stoping the timer,
 * storing a single interim time point and retrieving corresponding time
 * intervals.
 *
 * @tparam C Clock type from std::chrono (defaults to std::chrono::steady_clock)
 */
template <typename C = std::chrono::steady_clock>
class timer {
public:
    using clock_type = C;
    using duration_type = typename clock_type::duration;
    using timepoint_type = typename clock_type::time_point;

public:

    /* Default constructor */
    timer() :
        start_(now()),
        stop_(start_),
        interim_(start_)
    {}

    /* Get the current time */
    static timepoint_type now() {
        return clock_type::now();
    }

    /* Start time measurement */
    void start() {
        start_ = now();
    }

    /* Stop time measurement */
    void stop() {
        stop_ = now();
    }

    /* Set interim time point */
    void interim() {
        interim_ = now();
    }

    /* Get elapsed time since the start */
    template <typename D = duration_type>
    auto since_start() const {
        return std::chrono::duration_cast<D>(now() - start_).count();
    }

    /* Get elapsed time since the interim */
    template <typename D = duration_type>
    auto since_interim() const {
        return std::chrono::duration_cast<D>(now() - interim_).count();
    }

    /* Get elapsed time between stop and start */
    template <typename D = duration_type>
    auto start_to_stop() const {
        return std::chrono::duration_cast<D>(stop_ - start_).count();
    }

    /* Get elapsed time between interim and start */
    template <typename D = duration_type>
    auto start_to_interim() const {
        return std::chrono::duration_cast<D>(interim_ - start_).count();
    }

    /* Get elapsed time between stop and interim */
    template <typename D = duration_type>
    auto interim_to_stop() const {
        return std::chrono::duration_cast<D>(stop_ - interim_).count();
    }

    /* Get starting time point */
    timepoint_type get_start() const {
        return start_;
    }

    /* Get stopping time point */
    timepoint_type get_stop() const {
        return stop_;
    }

    /* Get interim time point */
    timepoint_type get_interim() const {
        return interim_;
    }

private:
    timepoint_type start_;
    timepoint_type stop_;
    timepoint_type interim_;
};

} // namespace utils

} // namespace simpleMC
