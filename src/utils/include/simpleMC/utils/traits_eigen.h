/**
 * @file    traits_eigen.h
 * @brief   Type traits utilities for Eigen types.
 * @author  Thomas Hahn
 */

#pragma once

#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <simpleMC/utils/traits.h>

namespace simpleMC {

namespace utils {

namespace detail {

template <typename T>
constexpr std::true_type
is_eigen_dense_f(const Eigen::DenseBase<T>&) {
    return {};
}

template <typename T>
constexpr std::true_type
is_eigen_tensor_f(const Eigen::TensorBase<T, Eigen::ReadOnlyAccessors>&) {
    return {};
}

template <typename T>
using is_eigen_dense_r = decltype(is_eigen_dense_f(std::declval<const T&>()));

template <typename T>
using is_eigen_tensor_r = decltype(is_eigen_tensor_f(std::declval<const T&>()));

} // namespace detail

/**
 * @brief Traits class to determine if a type D is derived from
 * Eigen::DenseBase<D>.
 */
template <typename T>
using is_eigen_dense = can_apply<detail::is_eigen_dense_r, T>;

template <typename T>
inline constexpr bool is_eigen_dense_v = is_eigen_dense<T>::value;

/**
 * @brief Traits class to determine if a type D is derived from
 * Eigen::TensorBase<D>.
 */
template <typename T>
using is_eigen_tensor = can_apply<detail::is_eigen_tensor_r, T>;

template <typename T>
inline constexpr bool is_eigen_tensor_v = is_eigen_tensor<T>::value;

/**
 * @brief Traits class to determine if a type D is derived from
 * Eigen::DenseBase<D> or from Eigen::TensorBase<D>.
 */
template <typename T, typename = void>
struct is_eigen : public std::false_type {};

template <typename T>
struct is_eigen<T, typename std::enable_if<
        is_eigen_dense_v<T> ||
        is_eigen_tensor_v<T>>::type> : public std::true_type {};

template <typename T>
inline constexpr bool is_eigen_v = is_eigen<T>::value;

} // namespace utils

} // namespace simpleMC
