/**
 * @file    tuple.h
 * @brief   Utility functions to deal with tuples.
 * @author  Thomas Hahn
 */

#pragma once

#include <tuple>

namespace simpleMC {

namespace utils {

/**
 * @brief for_each function for tuples.
 *
 * @param func Function object to apply
 * @param tup Tuple object
 * @tparam N Current tuple item to be processed
 * @tparam F Function object
 * @tparam T Tuple object
 */
template <std::size_t N = 0, typename F, typename T>
inline void for_each_tuple(F&& func, T&& tup) {
    if constexpr (N == std::tuple_size<typename std::decay<T>::type>::value - 1) {
        func(std::get<N>(std::forward<T>(tup)));
    } else {
        func(std::get<N>(std::forward<T>(tup)));
        for_each_tuple<N+1>(std::forward<F>(func), std::forward<T>(tup));
    }
}

/**
 * @brief Reverse for_each function for tuples.
 *
 * @param func Function object to apply
 * @param tup Tuple object
 * @tparam F Function object
 * @tparam T Tuple object
 * @tparam N Current tuple item to be processed
 */
template <typename F, typename T,
          std::size_t N = std::tuple_size<typename std::decay<T>::type>::value - 1>
inline void for_each_tuple_rev(F func, T&& tup) {
    if constexpr (N == 0) {
        func(std::get<N>(std::forward<T>(tup)));
    } else {
        func(std::get<N>(std::forward<T>(tup)));
        for_each_tuple_rev<F, T, N-1>(std::forward<F>(func), std::forward<T>(tup));
    }
}

/**
 * @brief Apply function to a certain tuple element specified at runtime.
 *
 * @param idx Index of tuple element
 * @param func Function object to apply
 * @param tup Tuple object
 * @tparam N Current tuple item
 * @tparam F Function object
 * @tparam T Tuple object
 */
template <std::size_t N = 0, typename F, typename T>
inline void for_index_tuple(std::size_t idx, F&& func, T&& tup) {
    if constexpr (N < std::tuple_size<typename std::decay<T>::type>::value) {
        if (idx == 0) {
            func(std::get<N>(std::forward<T>(tup)));
        } else {
            for_index_tuple<N+1, F, T>(idx - 1, std::forward<F>(func),
                                       std::forward<T>(tup));
        }
    }
}

} // namespace utils

} // namespace simpleMC
