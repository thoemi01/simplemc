/**
 * @file    math.h
 * @brief   Useful mathematical functions and constants.
 * @author  Thomas Hahn
 */

#pragma once

#include <cmath>
#include <limits>
#include <simpleMC/utils/traits_eigen.h>

namespace simpleMC {

namespace utils {

namespace math_constants {

/* infinities */
inline constexpr double inf = std::numeric_limits<double>::infinity();
inline constexpr double minus_inf = -inf;

/* pi */
inline constexpr double pi = M_PI;
inline constexpr double pi2 = 2.0*pi;
inline constexpr double pi_2 = pi/2.0;
inline const double pi_sqrt = std::sqrt(pi);
inline const double pi2_sqrt = std::sqrt(pi2);

} // namespace math_constants

namespace math_functions {

/**
 * @brief Calculate the factorial n!.
 *
 * @param n unsigned integer
 */
inline double factorial(unsigned int n) {
    double res = 1;
    for (unsigned int i = 2; i <= n; i++) {
        res *= i;
    }
    return res;
}

/**
 * @brief Checks if an arithmetic value, a complex number or an
 * Eigen::DenseBase object is either inf or nan.
 *
 * @param t arithmetic value, std::complex<T> or Eigen::DenseBase<T> object
 */
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, bool>::type
inf_nan(const T& t) {
    return std::isinf(t) || std::isnan(t);
}

template <typename T>
inline bool inf_nan(const std::complex<T>& t) {
    return inf_nan(t.real()) || inf_nan(t.imag());
}

template <typename T>
inline typename std::enable_if<simpleMC::utils::is_eigen_dense<T>::value,
bool>::type inf_nan(const T& t) {
    return t.array().isInf().any() || t.array().isNaN().any();
}

/**
 * @brief Absolute difference between two arithmetic values, complex numbers
 * or Eigen::DenseBase objects using the 2-norm.
 *
 * @param t1 value #1
 * @param t2 value #2
 */
template <typename T1, typename T2>
inline typename std::enable_if<std::is_arithmetic<T1>::value &&
std::is_arithmetic<T2>::value, double>::type
absolute_diff(const T1& t1, const T2& t2) {
    return std::abs(t1 - t2);
}

template <typename T>
inline double absolute_diff(const std::complex<T>& t1,
                            const std::complex<T>& t2) {
    return std::abs(t1 - t2);
}

template <typename T1, typename T2>
inline typename std::enable_if<simpleMC::utils::is_eigen_dense<T1>::value &&
simpleMC::utils::is_eigen_dense<T2>::value, double>::type
absolute_diff(const T1& t1, const T2& t2) {
    return (t1 - t2).norm();
}

/**
 * @brief Relative difference between two arithmetic values, complex numbers
 * or Eigen::DenseBase objects using the 2-norm.
 *
 * @param t1 value #1
 * @param t2 value #2
 */
template <typename T1, typename T2>
inline typename std::enable_if<std::is_arithmetic<T1>::value &&
std::is_arithmetic<T2>::value, double>::type
relative_diff(const T1& t1, const T2& t2) {
    auto a = t1;
    if (t1 == 0) {
        a = std::numeric_limits<double>::min();
    }
    auto b = t2;
    if (t2 == 0) {
        b = std::numeric_limits<double>::min();
    }
    return std::max(std::abs((a - b)/a), std::abs((a - b)/b));
}

template <typename T>
inline double relative_diff(const std::complex<T>& t1,
                            const std::complex<T>& t2) {
    double a = std::abs(t1);
    double b = std::abs(t2);
    if (a == 0) {
        a = std::numeric_limits<double>::min();
    }
    if (b == 0) {
        b = std::numeric_limits<double>::min();
    }
    return std::max(std::abs((t1 - t2)/a), std::abs((t1 - t2)/b));
}

template <typename T1, typename T2>
inline typename std::enable_if<simpleMC::utils::is_eigen_dense<T1>::value &&
simpleMC::utils::is_eigen_dense<T2>::value, double>::type
relative_diff(const T1& t1, const T2& t2) {
    double n = (t1 - t2).norm();
    double a = t1.norm();
    double b = t2.norm();
    if (a == 0) {
        a = std::numeric_limits<double>::min();
    }
    if (b == 0) {
        b = std::numeric_limits<double>::min();
    }
    return std::max(n/a, n/b);
}

} // namespace math_functions

namespace detail {

/**
 * @brief Type traits for 1d, 2d and 3d double Eigen vectors.
 */
template <int DIM>
struct vector_traits {};

template <>
struct vector_traits<1> {
    using type = Eigen::Matrix<double, 1, 1>;
};

template <>
struct vector_traits<2> {
    using type = Eigen::Vector2d;
};

template <>
struct vector_traits<3> {
    using type = Eigen::Vector3d;
};

} // namespace detail

/**
 * @brief Convenience type alias for 1d, 2d and 3d double Eigen vectors.
 */
template <int DIM>
using vector = typename detail::vector_traits<DIM>::type;

} // namespace utils

} // namespace simpleMC
