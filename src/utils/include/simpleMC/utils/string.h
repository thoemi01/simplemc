/**
 * @file    string.h
 * @brief   Some string functions.
 * @author  Thomas Hahn
 */

#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include <vector>

namespace simpleMC {

namespace utils {

/**
 * @brief Split string into tokens at a certain delimiter character.
 *
 * @param in Input string
 * @param delim Delimiter character
 * @param keep_empty Keep empty tokens
 */
std::vector<std::string> split_at_char(const std::string& in,
                                       char delim,
                                       bool keep_empty = false);

/**
 * @brief Split string into tokens at a certain delimiter string.
 *
 * @param in Input string
 * @param delim Delimiter string
 * @param keep_empty Keep empty tokens
 */
std::vector<std::string> split_at_string(const std::string& in,
                                         const std::string& delim,
                                         bool keep_empty = false);

/**
 * @brief Concatenate tokens into one string.
 *
 * @param in Input tokens
 * @param delim Delimiter string
 * @param starts_with String at the front
 * @param ends_with String at the end
 */
std::string concat_tokens(const std::vector<std::string>& cont,
                          const std::string& delim,
                          const std::string& starts_with = "",
                          const std::string& ends_with = "");

/**
 * @brief Finds the last occurence of one of the characters specified and returns
 * the substring up to that character.
 *
 * @param in Input string
 * @param search Characters to search for
 */
std::string get_base(const std::string& in, const std::string& search,
                     bool keep_full = false);

/**
 * @brief Finds the last occurence of one of the characters specified and returns
 * the substring from that character to the end.
 *
 * @param in Input string
 * @param search Characters to search for
 */
std::string get_leaf(const std::string& in, const std::string& search,
                     bool keep_full = true);

/**
 * @brief Convert arbitrary type to string using stringstreams.
 *
 * @param t instance of type T
 * @param p precision for numeric values
 */
template <typename T>
std::string to_string(const T& t, int p = 10) {
    std::stringstream ss;
    ss << std::setprecision(p);
    ss << t;
    return ss.str();
}

} // namespace utils

} // namespace simpleMC
