/**
 * @file    fixedwidth_ostream.h
 * @brief   Output stream class to write with fixed widths.
 * @author  Thomas Hahn
 */

#pragma once

#include <iosfwd>
#include <iomanip>

namespace simpleMC {

namespace utils {

/**
 * @brief Empty structs to write certain tokens to fixedwidth_ostream.
 */
struct fixedwidth_endl {};
struct fixedwidth_tab {};
struct fixedwidth_space {};

/**
 * @brief Different alignment options.
 */
enum class fixedwidth_alignment : unsigned char {
    left,
    right,
    def
};

/**
 * @brief Output stream class to write with fixed width.
 */
class fixedwidth_ostream {
public:

    /* Convenience functions */
    static fixedwidth_endl endl() { return fixedwidth_endl{}; }
    static fixedwidth_tab tab() { return fixedwidth_tab{}; }
    static fixedwidth_space space() { return fixedwidth_space{}; }
    static fixedwidth_alignment left() { return fixedwidth_alignment::left; }
    static fixedwidth_alignment right() { return fixedwidth_alignment::right; }
    static fixedwidth_alignment def() { return fixedwidth_alignment::def; }

public:

    /* Construct a fixedwidth_ostream class */
    explicit fixedwidth_ostream(std::ostream& os, int width = 0,
                                fixedwidth_alignment align =
            fixedwidth_alignment::left) :
        os_(os),
        width_(width),
        align_(align)
    {}

    /* Get/Set width */
    int width() const { return width_; }

    int width(int width) {
        int old = width_;
        width_ = width;
        return old;
    }

    /* Get/Set alignment */
    fixedwidth_alignment align() const { return align_; }

    fixedwidth_alignment align(fixedwidth_alignment align) {
        auto old = align_;
        align_ = align;
        return old;
    }

    /* General ostream operator */
    template <typename U>
    fixedwidth_ostream& operator<<(const U&);

    /* Write new line */
    fixedwidth_ostream& operator<<(fixedwidth_endl /*eol*/) {
        os_ << "\n";
        return *this;
    }

    /* Write a single tab */
    fixedwidth_ostream& operator<<(fixedwidth_tab /*tab*/) {
        os_ << "\t";
        return *this;
    }

    /* Write a single space */
    fixedwidth_ostream& operator<<(fixedwidth_space /*space*/) {
        os_ << " ";
        return *this;
    }

    /* Overload function call operator */
    fixedwidth_ostream& operator()(int width) {
        width_ = width;
        return *this;
    }

    fixedwidth_ostream& operator()(fixedwidth_alignment align) {
        align_ = align;
        return *this;
    }

    fixedwidth_ostream& operator()(int width, fixedwidth_alignment align) {
        width_ = width;
        align_ = align;
        return *this;
    }

private:
    std::ostream& os_;
    int width_;
    fixedwidth_alignment align_;
};

/* General ostream operator */
template <typename U>
inline fixedwidth_ostream& fixedwidth_ostream::operator<<(const U& u) {
    switch (align_) {
        case fixedwidth_alignment::left:
            os_ << std::left << std::setw(width_) << u;
            break;
        case fixedwidth_alignment::right:
            os_ << std::right << std::setw(width_) << u;
            break;
        default:
            os_ << std::setw(width_) << u;
    }
    return *this;
}

/* Specialize ostream operator for fixedwidth_ostreams */
template <>
inline fixedwidth_ostream&
fixedwidth_ostream::operator<<<fixedwidth_ostream>(const fixedwidth_ostream& fos) {
    width_ = fos.width_;
    align_ = fos.align_;
    return *this;
}

} // namespace utils

} // namespace simpleMC
