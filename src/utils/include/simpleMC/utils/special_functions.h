﻿/**
 * @file    special_functions.h
 * @brief   Special functions, e.g. Legendre polynomials, ...
 * @details Implementations of special functions and especially corresponding
 *          recurrence relations for those functions. These can be used to
 *          efficiently estimate coefficients of a generalized Fourier series:
 *          f(x) = sum_n f_n e_n(x), where {e_n(x)} is a complete set of
 *          orthogonal functions on some interval [a, b].
 * @author  Thomas Hahn
 */

#pragma once

#include <cmath>
#include <cassert>
#include <stdexcept>
#include <simpleMC/utils/math.h>

namespace simpleMC {

namespace utils {

/**
 * @brief Map an interval [a, b] to another interval [c, d] via a linear
 * function y(x) = alpha*x + beta sucht that y(a) = c and y(b) = d.
 */
class map_interval {
public:

    /* Default constructor */
    map_interval() = default;

    /* Constructor */
    map_interval(double a, double b, double c, double d) {
        set(a, b, c, d);
    }

    /* Set values and calculate alpha and beta */
    void set(double, double, double, double);

    /* Map a value x from [a, b] to [c, d] */
    double map(double x) const { return alpha_*x + beta_; }

    /* Map a value x from [c, d] to [a, b] */
    double inv_map(double x) const { return (x - beta_)/alpha_; }

    /* Get alpha */
    double alpha() const { return alpha_; }

    /* Get beta */
    double beta() const { return beta_; }

private:
    double alpha_;
    double beta_;
};

/**
 * @brief Recurrence relation for Legendre polynomials.
 */
inline double legendre_next(unsigned int l, double x, double pl, double plm1) {
    assert(l > 0);
    assert(std::abs(x) <= 1);
    return ((2*l + 1)*x*pl - l*plm1)/(l + 1);
}

/**
 * @brief Calculate lth Legendre polynomial at x.
 */
double legendre_poly(unsigned int, double);

/**
 * @brief Calculate the derivative of lth Legendre polynomial at x.
 */
double legendre_poly_prime(unsigned int, double);

/**
 * @brief Legendre polynomials for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l p_l(x), with the lth
 * Legendre polynomial p_l(x). It is instantiated at a specific x and order
 * l. Calling the function next(), calculates p_l+1(x) and returns p_l(x).
 */
class legendre {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = -1.0;

    /* Upper bound of domain */
    static constexpr double upper_bound = 1.0;

    /* Normalization factor */
    static double norm_factor(unsigned int l) { return 2.0/(2.0*l + 1); }

    /* Weight function */
    static constexpr double weight(double /*x*/) { return 1.0; }

public:

    /* Constructor for a specific x and order l */
    legendre(double x = 0.0, unsigned int l = 0);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Increase order l and return last value for order l-1 */
    double next();

    /* Return value for current order l */
    double current() const { return pl_; }

    /* Return value for order l-1 */
    double last() const { return plm1_; }

    /* Normalization factor for current order l */
    double norm_factor() const { return 2.0/(2.0*l_ + 1); }

    /* Weight function for current x */
    constexpr double weight() const { return 1.0; }

private:
    double x_;
    unsigned int l_;
    double pl_;
    double plm1_;
};

/* Constructor for a specific x and order l */
inline legendre::legendre(double x, unsigned int l) :
    x_(x),
    l_(l)
{
    if (std::abs(x_) > 1) {
        throw std::out_of_range("Legendre polynomials are only defined on the "
                                "interval [-1, 1]");
    }
    switch (l_) {
        case 0:
            pl_ = 1;
            break;
        default:
            pl_ = x_;
            plm1_ = 1;
            for (unsigned int i = 1; i < l_; i++) {
                double tmp = legendre_next(i, x_, pl_, plm1_);
                plm1_ = pl_;
                pl_ = tmp;
            }
    }
}

/* Increase order l and return last value for order l-1 */
inline double legendre::next() {
    double tmp;
    switch (l_) {
        case 0:
            tmp = x_;
            break;
        default:
            tmp = legendre_next(l_, x_, pl_, plm1_);
    }
    plm1_ = pl_;
    pl_ = tmp;
    ++l_;
    return plm1_;
}

/**
 * @brief Distiguish between different kinds of Chebyshev polynomials.
 */
enum class chebyshev_kind {
    first,
    second
};

/**
 * @brief Recurrence relation for Chebyshev polynomials.
 */
inline double chebyshev_next(double x, double vl, double vlm1) {
    assert(std::abs(x) <= 1);
    return 2.0*x*vl - vlm1;
}

/**
 * @brief Calculate lth Chebyshev polynomial of 1st kind at x.
 */
double chebyshev_poly_t(unsigned int, double);

/**
 * @brief Calculate lth Chebyshev polynomial of 2nd kind at x.
 */
double chebyshev_poly_u(unsigned int, double);

/**
 * @brief Calculate the derivative of lth Chebyshev polynomial of 1st kind at x.
 */
double chebyshev_poly_t_prime(unsigned int, double);

/**
 * @brief Calculate the derivative of lth Chebyshev polynomial of 2nd kind at x.
 */
double chebyshev_poly_u_prime(unsigned int, double);

/**
 * @brief Chebyshev polynomials for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l v_l(x), with the lth
 * Chebyshev polynomial v_l(x). It is instantiated at a specific x and order
 * l. Calling the function next(), calculates v_l+1(x) and returns v_l(x).
 */
class chebyshev {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = -1.0;

    /* Upper bound of domain */
    static constexpr double upper_bound = 1.0;

    /* Normalization factor */
    static double norm_factor(unsigned int,
                              chebyshev_kind kind = chebyshev_kind::first);

    /* Weight function */
    static double weight(double,
                         chebyshev_kind kind = chebyshev_kind::first);

public:

    /* Constructor for a specific x, order l and kind */
    chebyshev(double x = 0.0, unsigned int l = 0,
              chebyshev_kind kind = chebyshev_kind::first);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Get kind */
    chebyshev_kind kind() const { return kind_; }

    /* Increase order l and return last value for order l-1 */
    double next();

    /* Return value for current order l */
    double current() const { return vl_; }

    /* Return value for order l-1 */
    double last() const { return vlm1_; }

    /* Normalization factor for current order l */
    double norm_factor() const { return norm_factor(l_, kind_); }

    /* Weight function for current x */
    double weight() const { return weight_; }

private:
    double x_;
    unsigned int l_;
    chebyshev_kind kind_;
    double weight_;
    double vl_;
    double vlm1_;
    double ini_factor_;
};

/* Normalization factor */
inline double chebyshev::norm_factor(unsigned int l, chebyshev_kind kind) {
    using namespace math_constants;
    if (kind == chebyshev_kind::first && l == 0) {
        return pi;
    }
    return pi_2;
}

/* Weight function */
inline double chebyshev::weight(double x, chebyshev_kind kind) {
    if (kind == chebyshev_kind::first) {
        return 1.0/std::sqrt(1 - x*x);
    }
    return std::sqrt(1 - x*x);
}

/* Constructor for a specific x, order l and kind */
inline chebyshev::chebyshev(double x, unsigned int l, chebyshev_kind kind) :
    x_(x),
    l_(l),
    kind_(kind),
    weight_(weight(x_, kind_))
{
    if (std::abs(x_) > 1) {
        throw std::out_of_range("Chebyshev polynomials are only defined on the "
                                "interval [-1, 1]");
    }
    ini_factor_ = 1.0;
    if (kind_ == chebyshev_kind::second) {
        ini_factor_ = 2.0;
    }
    switch (l_) {
        case 0:
            vl_ = 1;
            vlm1_ = 0;
            break;
        default:
            vl_ = ini_factor_*x_;
            vlm1_ = 1;
            for (unsigned int i = 1; i < l_; i++) {
                double tmp = chebyshev_next(x_, vl_, vlm1_);
                vlm1_ = vl_;
                vl_ = tmp;
            }
    }
}

/* Increase order l and return last value for order l-1 */
inline double chebyshev::next() {
    double tmp;
    switch (l_) {
        case 0:
            tmp = ini_factor_*x_;
            break;
        default:
            tmp = chebyshev_next(x_, vl_, vlm1_);
    }
    vlm1_ = vl_;
    vl_ = tmp;
    ++l_;
    return vlm1_;
}

/**
 * @brief Recurrence relation for Laguerre polynomials.
 */
inline double laguerre_next(unsigned int l, double x, double ll, double llm1) {
    assert(l > 0);
    assert(x >= 0);
    return ((2*l + 1 - x)*ll - l*llm1)/(l + 1);
}

/**
 * @brief Calculate lth Laguerre polynomial at x.
 */
double laguerre_poly(unsigned int, double);

/**
 * @brief Laguerre polynomials for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l l_l(x), with the lth
 * Laguerre polynomial l_l(x). It is instantiated at a specific x and order
 * l. Calling the function next(), calculates l_l+1(x) and returns l_l(x).
 */
class laguerre {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = 0.0;

    /* Upper bound of domain */
    static constexpr double upper_bound = math_constants::inf;

    /* Normalization factor */
    static constexpr double norm_factor(unsigned int /*l*/) { return 1.0; }

    /* Weight function */
    static double weight(double x) { return std::exp(-x); }

public:

    /* Constructor for a specific x and order l */
    laguerre(double x = 0.0, unsigned int l = 0);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Increase order l and return last value for order l-1 */
    double next();

    /* Return value for current order l */
    double current() const { return ll_; }

    /* Return value for order l-1 */
    double last() const { return llm1_; }

    /* Normalization factor for current order l */
    constexpr double norm_factor() const { return 1.0; }

    /* Weight function for current x */
    double weight() const { return weight_; }

private:
    double x_;
    unsigned int l_;
    double weight_;
    double ll_;
    double llm1_;
};

/* Constructor for a specific x and order l */
inline laguerre::laguerre(double x, unsigned int l) :
    x_(x),
    l_(l),
    weight_(weight(x_))
{
    if (x < 0) {
        throw std::out_of_range("Laguerre polynomials are only defined on the "
                                "interval [0, inf)");
    }
    switch (l_) {
        case 0:
            ll_ = 1;
            break;
        default:
            ll_ = 1 - x_;
            llm1_ = 1;
            for (unsigned int i = 1; i < l_; i++) {
                double tmp = laguerre_next(i, x_, ll_, llm1_);
                llm1_ = ll_;
                ll_ = tmp;
            }
    }
}

/* Increase order l and return last value for order l-1 */
inline double laguerre::next() {
    double tmp;
    switch (l_) {
        case 0:
            tmp = 1 - x_;
            break;
        default:
            tmp = laguerre_next(l_, x_, ll_, llm1_);
    }
    llm1_ = ll_;
    ll_ = tmp;
    ++l_;
    return llm1_;
}

/**
 * @brief Recurrence relation for Hermite polynomials.
 */
inline double hermite_next(unsigned int l, double x, double hl, double hlm1) {
    assert(l > 0);
    return 2.0*x*hl - 2.0*l*hlm1;
}

/**
 * @brief Calculate lth Hermite polynomial at x.
 */
double hermite_poly(unsigned int, double);

/**
 * @brief Hermite polynomials for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l h_l(x), with the lth
 * Hermite polynomial h_l(x). It is instantiated at a specific x and order
 * l. Calling the function next(), calculates h_l+1(x) and returns h_l(x).
 */
class hermite {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = math_constants::minus_inf;

    /* Upper bound of domain */
    static constexpr double upper_bound = math_constants::inf;

    /* Normalization factor */
    static double norm_factor(unsigned int l) {
        using namespace math_constants;
        using namespace math_functions;
        return pi_sqrt*std::pow(2.0, l)*factorial(l);
    }

    /* Weight function */
    static double weight(double x) { return std::exp(-x*x); }

public:

    /* Constructor for a specific x and order l */
    hermite(double x = 0.0, unsigned int l = 0);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Increase order l and return last value for order l-1 */
    double next();

    /* Return value for current order l */
    double current() const { return hl_; }

    /* Return value for order l-1 */
    double last() const { return hlm1_; }

    /* Normalization factor for current order l */
    double norm_factor() const { return norm_factor(l_); }

    /* Weight function for current x */
    double weight() const { return weight_; }

private:
    double x_;
    unsigned int l_;
    double weight_;
    double hl_;
    double hlm1_;
};

/* Constructor for a specific x and order l */
inline hermite::hermite(double x, unsigned int l) :
    x_(x),
    l_(l),
    weight_(weight(x))
{
    switch (l_) {
        case 0:
            hl_ = 1;
            break;
        default:
            hl_ = 2.0*x_;
            hlm1_ = 1;
            for (unsigned int i = 1; i < l_; i++) {
                double tmp = hermite_next(i, x_, hl_, hlm1_);
                hlm1_ = hl_;
                hl_ = tmp;
            }
    }
}

/* Increase order l and return last value for order l-1 */
inline double hermite::next() {
    double tmp;
    switch (l_) {
        case 0:
            tmp = 2.0*x_;
            break;
        default:
            tmp = hermite_next(l_, x_, hl_, hlm1_);
    }
    hlm1_ = hl_;
    hl_ = tmp;
    ++l_;
    return hlm1_;
}

/**
 * @brief Cosine functions for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l c_l(x), with the lth
 * cosine function c_l(x) = cos(l*x). It is instantiated at a specific x
 * and order l. Calling the function next(), calculates c_l+1(x) and returns
 * c_l(x) using Chebyshev polynomials.
 */
class cosine {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = -math_constants::pi;

    /* Upper bound of domain */
    static constexpr double upper_bound = math_constants::pi;

    /* Normalization factor */
    static double norm_factor(unsigned int l) {
        using namespace math_constants;
        if (l == 0) {
            return pi2_sqrt;
        }
        return pi_sqrt;
    }

    /* Weight function */
    static constexpr double weight(double /*x*/) { return 1.0; }

public:

    /* Constructor for a specific x and order l */
    cosine(double x = 0.0, unsigned int l = 0);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Increase order l and return last value for order l-1 */
    double next();

    /* Return value for current order l */
    double current() const { return cl_; }

    /* Return value for order l-1 */
    double last() const { return clm1_; }

    /* Normalization factor for current order l */
    double norm_factor() const { return norm_factor(l_); }

    /* Weight function for current x */
    constexpr double weight() const { return 1.0; }

private:
    double x_;
    unsigned int l_;
    double cosx_;
    double cl_;
    double clm1_;
};

/* Constructor for a specific x and order l */
inline cosine::cosine(double x, unsigned int l) :
    x_(x),
    l_(l),
    cosx_(std::cos(x))
{
    switch (l_) {
        case 0:
            cl_ = 1;
            break;
        default:
            cl_ = cosx_;
            clm1_ = 1;
            for (unsigned int i = 1; i < l_; i++) {
                double tmp = chebyshev_next(cosx_, cl_, clm1_);
                clm1_ = cl_;
                cl_ = tmp;
            }
    }
}

/* Increase order l and return last value for order l-1 */
inline double cosine::next() {
    double tmp;
    switch (l_) {
        case 0:
            tmp = cosx_;
            break;
        default:
            tmp = chebyshev_next(cosx_, cl_, clm1_);
    }
    clm1_ = cl_;
    cl_ = tmp;
    ++l_;
    return clm1_;
}

/**
 * @brief Sine functions for generalized Fourier series.
 *
 * @details This class is intented to be used for estimating coefficients
 * in a generalized Fourier series: f(x) = sum_l f_l s_l(x), with the lth
 * sine function s_l(x) = sin(l*x). It is instantiated at a specific x
 * and order l. Calling the function next(), calculates s_l+1(x) and returns
 * s_l(x) using Chebyshev polynomials.
 */
class sine {
public:

    /* Lower bound of domain */
    static constexpr double lower_bound = -math_constants::pi;

    /* Upper bound of domain */
    static constexpr double upper_bound = math_constants::pi;

    /* Normalization factor */
    static double norm_factor(unsigned int /*l*/) {
        using namespace math_constants;
        return pi_sqrt;
    }

    /* Weight function */
    static constexpr double weight(double /*x*/) { return 1.0; }

public:

    /* Construct a sine for a specific x */
    sine(double x, unsigned int l = 0);

    /* Get current order */
    unsigned int order() const { return l_; }

    /* Get x value */
    double x() const { return x_; }

    /* Increase order l and return last value for order l-1 */
    double next() {
        double tmp;
        switch (l_) {
            case 0:
                tmp = 1;
                break;
            case 1:
                tmp = chebyshev_poly_u(1, cosx_);
                break;
            default:
                tmp = chebyshev_next(cosx_, sl_, slm1_);
        }
        slm1_ = sl_;
        sl_ = tmp;
        ++l_;
        return slm1_*sinx_;
    }

    /* Return value for current order l */
    double current() const { return sl_*sinx_; }

    /* Return value for order l-1 */
    double last() const { return slm1_*sinx_; }

    /* Normalization factor for current order l */
    double norm_factor() const { return norm_factor(l_); }

    /* Weight function for current x */
    constexpr double weight() const { return 1.0; }

private:
    double x_;
    unsigned int l_;
    double cosx_;
    double sinx_;
    double sl_;
    double slm1_;
};

/* Construct a sine for a specific x */
inline sine::sine(double x, unsigned int l) :
    x_(x),
    l_(l),
    cosx_(std::cos(x)),
    sinx_(std::sin(x))
{
    switch (l_) {
        case 0:
            sl_ = 0;
            break;
        case 1:
            sl_ = 1;
            slm1_ = 0;
            break;
        default:
            sl_ = chebyshev_poly_u(1, cosx_);
            slm1_ = 1;
            for (unsigned int i = 2; i < l_; i++) {
                double tmp = chebyshev_next(cosx_, sl_, slm1_);
                slm1_ = sl_;
                sl_ = tmp;
            }
    }
}

} // namespace utils

} // namespace simpleMC
