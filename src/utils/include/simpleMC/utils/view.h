﻿/**
 * @file    view.h
 * @brief   Views on contiguous data.
 * @author  Thomas Hahn
 *
 * @todo    -) const_cast in make_view functions seems inappropriate
 */

#pragma once

#include <complex>
#include <array>
#include <vector>

namespace simpleMC {

namespace utils {

/**
 * @brief View on a contiguous data structure.
 *
 * @details Holds a pointer to the actual data. The view class does not own
 * the data, so the user has to make sure that the pointer is valid as long as
 * the view class is used. Also be careful with views on const objects,
 * since the object might be modified through the view. Advice: Create views
 * with the supported make_view() (or write youre own) and use views in
 * functions which either accept view<T>& or const view<T>& depending on
 * wheter you want to modify the underlying data.
 *
 * @tparam T Type of the underlying data
 */
template <typename T>
class view {
public:
    using value_type = T;

public:

    /* Get number of dimensions */
    static constexpr std::size_t dim() { return 1; }

public:

    /* Construct a view */
    view(T* data, std::size_t size) :
        data_(data),
        size_(size)
    {}

    /* Get the raw pointer */
    T* data() { return data_; }

    const T* data() const { return data_; }

    /* Get the total number of elements */
    std::size_t size() const { return size_; }

    /* Is the number of elements == 0 */
    bool empty() const { return size_ == 0; }

    /* Access nth element */
    T& operator[](std::size_t n) { return data_[n]; }

    const T& operator[](std::size_t n) const { return data_[n]; }

    /* Access nth element with bounds checking */
    T& at(std::size_t n) {
        if (n >= size_) {
            throw std::out_of_range("Out of bounds access in view class");
        }
        return data_[n];
    }

    const T& at(std::size_t n) const {
        if (n >= size_) {
            throw std::out_of_range("Out of bounds access in view class");
        }
        return data_[n];
    }

private:
    T* data_;
    std::size_t size_;
};


/**
 * @brief View on an N-dimensional contiguous data structure.
 *
 * @details Holds a pointer to the actual data. The view class does not own
 * the data, so the user has to make sure that the pointer is valid as long as
 * the view class is used. Also be careful with views on const objects,
 * since the object might be modified through the view. Advice: Create views
 * with the supported make_view() (or write youre own) and use views in
 * functions which either accept view<T>& or const view<T>& depending
 * on wheter you want to modify the underlying data.
 *
 * @tparam T Type of the underlying data
 * @tparam N Number of dimensions
 */
template <typename T, std::size_t N>
class nd_view {
public:
    using value_type = T;
    using shape_type = std::array<std::size_t, N>;

public:

    /* Get number of dimensions */
    static constexpr std::size_t dim() { return N; }

public:

    /* Construct a ndview */
    nd_view(T* data, const shape_type& shape) :
        view_(data, compute_size(shape)),
        shape_(shape)
    {}

    /* Get 1-dimension view */
    view<T>& view1d() { return view_; }

    const view<T>& view1d() const { return view_; }

    /* Get the raw pointer */
    T* data() { return view_.data(); }

    const T* data() const { return view_.data(); }

    /* Get the total number of elements */
    std::size_t size() const { return view_.size(); }

    /* Get the shape */
    const shape_type& shape() const { return shape_; }

    /* Is the number of elements == 0 */
    bool empty() const { return view_.empty(); }

    /* Access nth element */
    T& operator[](std::size_t n) { return view_[n]; }

    const T& operator[](std::size_t n) const { return view_[n]; }

    /* Access nth elements with bounds checking */
    T& at(std::size_t n) { return view_.at(n); }

    const T& at(std::size_t n) const { return view_.at(n); }

private:

    /* Compute the number of elements (= size) from the given shape */
    std::size_t compute_size(const shape_type& shape) const {
        std::size_t res = 1;
        for (std::size_t i = 0; i < N; i++) {
            res *= shape[i];
        }
        return res;
    }

private:
    view<T> view_;
    shape_type shape_;
};

/**
 * @brief Create view on a single arithmetic type.
 */
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const T& val) {
    // const_cast feels suspicious
    return view<T>(const_cast<T*>(&val), 1);
}

/* Disallow temporaries */
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const T&& val) = delete;

/**
 * @brief Create view on a single std::complex<double> value.
 */
inline view<std::complex<double>>
make_view(const std::complex<double>& val) {
    using c_t = std::complex<double>;
    // const_cast feels suspicious
    return view<c_t>(const_cast<c_t*>(&val), 1);
}

/* Disallow temporaries */
inline view<std::complex<double>>
make_view(const std::complex<double>&& val) = delete;

/**
 * @brief Create view on a std::vector<T>, with T an arithmetic type.
 */
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const std::vector<T>& vec) {
    // const_cast feels suspicious
    return view<T>(const_cast<T*>(vec.data()), vec.size());
}

/* Disallow temporaries */
template <typename T>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const std::vector<T>&& vec) = delete;

/**
 * @brief Create view on a std::vector<std::complex<double>>.
 */
inline view<std::complex<double>>
make_view(const std::vector<std::complex<double>>& vec) {
    using c_t = std::complex<double>;
    // const_cast feels suspicious
    return view<c_t>(const_cast<c_t*>(vec.data()), vec.size());
}

/* Disallow temporaries */
inline view<std::complex<double>>
make_view(const std::vector<std::complex<double>>&& vec) = delete;

/**
 * @brief Create view on a std::array<double, N>.
 */
template <typename T, std::size_t N>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const std::array<T, N>& arr) {
    // const_cast feels suspicious
    return view<T>(const_cast<T*>(arr.data()), arr.size());
}

/* Disallow temporaries */
template <typename T, std::size_t N>
inline typename std::enable_if<std::is_arithmetic<T>::value, view<T>>::type
make_view(const std::array<T, N>&& arr) = delete;

/**
 * @brief Create view on a std::array<std::complex<double>, N>.
 */
template <std::size_t N>
inline view<std::complex<double>>
make_view(const std::array<std::complex<double>, N>& arr) {
    using c_t = std::complex<double>;
    // const_cast feels suspicious
    return view<c_t>(const_cast<c_t*>(arr.data()), arr.size());
}

/* Disallow temporaries */
template <std::size_t N>
inline view<std::complex<double>>
make_view(const std::array<std::complex<double>, N>&& arr) = delete;

} // namespace utils

} // namespace simpleMC
