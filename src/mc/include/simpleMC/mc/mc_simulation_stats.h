/**
 * @file    mc_simulation_stats.h
 * @brief   Statistics and information of MC simulations.
 * @author  Thomas Hahn
 */

#pragma once

#include <cstdint>
#include <iosfwd>
#include <simpleMC/config.h>
#include <simpleMC/utils/timer.h>
#include <simpleMC/utils/signals.h>
#include <simpleMC/mc/mc_fwd.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace mc {

/**
 * @brief Enum class to specify update search algorithm.
 */
enum class mc_update_search {
    linear,
    walker_alias
};

namespace detail {

/**
 * @brief Convert mc_update_search to string.
 */
std::string mus_to_string(mc_update_search);

/**
 * @brief Convert string to mc_update_search.
 */
mc_update_search string_to_mus(const std::string&);

} // namespace detail

/**
 * @brief Simple struct to hold statistics and information on a MC
 * simulation.
 *
 * @details
 * - timer: Run time since the start of the current simulation
 * - total_time: Total run time, including previous runs
 * - max_time: Maximum run time of current simulation
 * - warmup_time: Maximum warm up time of current simulation
 * - warmup_steps: Maximum warm up steps of current simulation
 * - total_steps_done: Total number of MC steps, including previous runs
 * - steps_done: Number of MC steps in the current simulation
 * - max_steps: Maximum number of MC steps in the current simulation
 * - cycle_length: Number of MC steps between measurements
 * - do_n_cycles: Number of cycles between checks (check_steps() &
 *   check_time()) are clled
 * - checkpoint_after_t: Perform checkpoint after t seconds
 * - lastcheckpoint_t: Last time a checkpoint was performed
 * - checkpoint_after_s: Perform checkpoint after s steps
 * - lastcheckpoint_s: Steps done last time a checkpoint was performed
 * - load_checkpoint: Start simulation from checkpoint
 * - update_search: Specify update search algorithm
 */
struct mc_simulation_stats {
public:
    using timer_type = simpleMC::utils::timer<>;
    using sec_d = simpleMC::utils::duration::sec_d;
    using count_type = std::uint64_t;

public:

    /* Return limit on runtime */
    static constexpr double limit_runtime() {
        return std::numeric_limits<double>::max();
    }

    /* Return limit on steps */
    static constexpr count_type limit_steps() {
        return std::numeric_limits<count_type>::max();
    }

public:

    /* Default constructor */
    mc_simulation_stats();

    /* Get current run time */
    double runtime() const { return timer.since_start<sec_d>(); }

    /* Check if max. number of MC steps are done */
    bool check_steps() const { return steps_done < max_steps; }

    /* Check if max. run time is reached */
    bool check_time() const { return runtime() < max_time; }

    /* Check if MC simulation should stop */
    bool keep_running() const { return check_steps() && check_time(); }

    /* Check if it is time for checkpoint */
    bool check_checkpoint_time() const {
        return checkpoint_after_t < runtime() - last_checkpoint_t;
    }

    bool check_checkpoint_steps() const {
        return checkpoint_after_s < steps_done - last_checkpoint_s;
    }

    bool check_checkpoint() const {
        return check_checkpoint_time() || check_checkpoint_steps();
    }

    /* Check if warm up steps are done */
    bool check_warmup_steps() const { return steps_done < warmup_steps; }

    /* Check if warm up time is reached */
    bool check_warmup_time() const { return runtime() < warmup_time; }

    /* Check if warm up is finished */
    bool keep_warmingup() const {
        return check_warmup_time() && check_warmup_steps();
    }

    /* Check if a signal has been catched */
    bool signals_catched() const { return !simpleMC::signals::empty(); }

    /* Start simulation by resetting some variables */
    void start();

    /* End simulation by stopping timer */
    void stop() { timer.stop(); }

public:
    timer_type timer;
    double total_time;
    double max_time;
    double warmup_time;
    count_type warmup_steps;
    count_type total_steps;
    count_type steps_done;
    count_type max_steps;
    count_type cycle_length;
    count_type do_n_cycles;
    double checkpoint_after_t;
    double last_checkpoint_t;
    count_type checkpoint_after_s;
    count_type last_checkpoint_s;
    bool load_checkpoint;
    mc_update_search update_search;
};

/**
 * @brief Write mc_simulation_stats to output stream.
 */
std::ostream& operator<<(std::ostream&, const mc_simulation_stats&);

/**
 * @brief Serialize mc_simulation_stats to Json.
 */
void to_json(nlohmann::json&, const mc_simulation_stats&);

/**
 * @brief Deserialize mc_simulation_stats from Json.
 */
void from_json(const nlohmann::json&, mc_simulation_stats&);

/**
 * @brief Read Json input file.
 */
void read_input_json(const nlohmann::json&, mc_simulation_stats&);

/**
 * @brief Write Json input file.
 */
void write_input_json(nlohmann::json&, const mc_simulation_stats&);

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief Collect simulation stats from MPI processes.
 */
mc_simulation_stats
mpi_collect_simulation_stats(const mc_simulation_stats&,
                             simpleMC::mpi::communicator);
#endif

} // namespace mc

} // namespace simpleMC
