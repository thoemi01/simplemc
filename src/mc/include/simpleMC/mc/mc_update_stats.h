/**
 * @file    mc_update_stats.h
 * @brief   Statistics and information on MC updates.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <string>
#include <cstdint>
#include <simpleMC/config.h>
#include <simpleMC/utils/walker_alias.h>
#include <simpleMC/mc/mc_fwd.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace mc {

namespace detail {

/**
 * @brief Helper function to create default names for updates.
 */
std::string default_update_name(std::size_t);
std::vector<std::string> default_update_names(std::size_t);

} // namespace detail

/**
 * @brief Simple struct to hold information on a single MC update.
 */
struct mc_update_info {
public:
    using count_type = std::uint64_t;

public:

    /* Constructor for mc_update_info */
    mc_update_info(std::size_t, double, double, count_type,
                   count_type, const std::string&, const std::string&);

    /* Returns a mc_update_info for invalid indexes */
    static mc_update_info invalid(std::size_t);

public:
    std::size_t index;
    double weight;
    double prob;
    count_type nprop;
    count_type nacc;
    std::string name;
    std::string name_inv;
};

/**
 * @brief Simple struct to hold information on all MC updates.
 */
struct mc_update_stats {
public:
    using count_type = std::uint64_t;
    using walker_type = simpleMC::utils::walker_alias;

public:

    /* Default constructor */
    mc_update_stats() = default;

    /* Constructor for a given number of updates */
    mc_update_stats(std::size_t);

    /* Number of updates */
    std::size_t num_updates() const { return weights.size(); }

    /* Normalization factor = Sum of weights */
    double norm_factor() const;

    /* Initialize probabilities */
    void init_probs();

    /* Initialize Walker-Alias */
    void init_walias() { walias.initialize(weights); }

    /* Sort updates by weight and return permutation vector */
    std::vector<std::size_t> sort();

    /* Add an update */
    void add_update(double, const std::string& = std::string(),
                    const std::string& = std::string(),
                    count_type = 0, count_type = 0);

    /* Find update by name and return its index */
    std::size_t find_by_name(const std::string&) const ;

    /* Get info on a single update by name */
    mc_update_info get_info(const std::string&) const ;

    /* Get info on a single update by index */
    mc_update_info get_info(std::size_t) const;

    /* Increase number of proposed for current update */
    void increase_proposed() { nprops[current] += 1; }

    /* Increase number of accepted for current update */
    void increase_accepted() { naccs[current] += 1; }

    /* Start simulation by resetting variables */
    void start();

public:
    std::vector<double> weights;
    std::vector<double> probs;
    std::vector<count_type> nprops;
    std::vector<count_type> naccs;
    std::vector<std::string> names;
    std::vector<std::string> names_inv;
    walker_type walias;
    std::size_t current;
};

/**
 * @brief Write mc_update_info to output stream.
 */
std::ostream& operator<<(std::ostream&, const mc_update_info&);

/**
 * @brief Write mc_update_stats to output stream.
 */
std::ostream& operator<<(std::ostream&, const mc_update_stats&);

/**
 * @brief Serialize mc_update_stats to Json.
 */
void to_json(nlohmann::json&, const mc_update_stats&);

/**
 * @brief Deserialize mc_update_stats from Json.
 */
void from_json(const nlohmann::json&, mc_update_stats&);

/**
 * @brief Read Json input file.
 */
void read_input_json(const nlohmann::json&, mc_update_stats&);

/**
 * @brief Write Json input file.
 */
void write_input_json(nlohmann::json&, const mc_update_stats&);

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief Collect update stats from MPI processes.
 */
mc_update_stats
mpi_collect_update_stats(const mc_update_stats&,
                         simpleMC::mpi::communicator);
#endif

} // namespace mc

} // namespace simpleMC
