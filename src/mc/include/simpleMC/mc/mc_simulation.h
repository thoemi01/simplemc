/**
 * @file    mc_simulation.h
 * @brief   Generic Monte Carlo simulation.
 * @author  Thomas Hahn
 * @todo    -) make public interface smaller
 */

#pragma once

#include <memory>
#include <functional>
#include <simpleMC/utils/algorithms.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_random.h>
#include <simpleMC/mc/mc_configuration.h>
#include <simpleMC/mc/mc_update.h>
#include <simpleMC/mc/mc_update_stats.h>
#include <simpleMC/mc/mc_measurement.h>
#include <simpleMC/mc/mc_measurement_stats.h>
#include <simpleMC/mc/mc_simulation_stats.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/environment.h>
#endif

namespace simpleMC {

namespace mc {

/**
 * @brief Default callback for doing a checkpoint during a MC simulation.
 */
template <typename C>
void default_checkpoint_callback(const mc_simulation<C>& mc) {
    using namespace simpleMC::json;
    std::string filename = "mc_checkpoint_" + std::to_string(mc.rank());
    jsonIO jio;
    jio << mc;
    jio.dump_to_file(filename, jsonIO_mode::cbor);
}

/**
 * @brief Default implementation for loading a MC simulation.
 */
template <typename C>
void default_load_checkpoint(mc_simulation<C>& mc) {
    using namespace simpleMC::json;
    std::string filename = "mc_checkpoint_" + std::to_string(mc.rank());
    jsonIO jio;
    jio.load_from_file(filename, jsonIO_mode::cbor);
    jio >> mc;
}

/**
 * @brief Default callback when a signal is catched during a MC simulation.
 */
template <typename C>
void default_signal_callback(const mc_simulation<C>& /*mc*/) {
#ifdef SIMPLEMC_HAS_MPI
    using namespace simpleMC::mpi;
    if (environment::is_initialized()) {
        environment::abort();
    } else {
        std::exit(1);
    }
#else
    std::exit(1);
#endif
}

/**
 * @brief A generic Monte Carlo simulation.
 *
 * @tparam C Configuration type
 */
template <typename C>
class mc_simulation : public json::basic_jsonIO {
public:
    using config_type = C;
    using gen_type = mc_traits::gen_type;
    using update_type = mc_update<config_type>;
    using update_ptr = std::shared_ptr<update_type>;
    using measurement_type = mc_measurement<config_type>;
    using measurement_ptr = std::shared_ptr<measurement_type>;
    using count_type = std::uint64_t;
    using callback_type = std::function<void(mc_simulation&)>;

public:

    /* Default constructor */
    mc_simulation(int rank = 0, std::uint64_t seed = mc_traits::default_seed);

    /* Get rank */
    int rank() const { return rank_; }

    /* Set checkpoint callback */
    void set_checkpoint_callback(const callback_type& callback) {
        checkpoint_callback_ = callback;
    }

    /* Set checkpoint callback */
    void set_signal_callback(const callback_type& callback) {
        signal_callback_ = callback;
    }

    /* Get random number generator */
    const gen_type& generator() const { return uni01_; }

    /* Get/Set configuration */
    const config_type& configuration() const { return config_; }

    void set_configuration(const config_type& config) { config_ = config; }

    /* Get update pointers */
    const std::vector<update_ptr>& updates() const { return updates_; }

    /* Get measurement pointers */
    const std::vector<measurement_ptr>& measurements() const { return measurements_; }

    /* Get update stats */
    const mc_update_stats& update_stats() const { return ustat_; }

    /* Get measurement stats */
    const mc_measurement_stats& measurement_stats() const { return mstat_; }

    /* Get simulation stats */
    const mc_simulation_stats& simulation_stats() const { return sstat_; }

    /* Set warm up specific variables */
    void set_warmup_vars(double wtime, count_type wsteps,
                         count_type cyclen = 1, count_type ncycs = 1,
                         mc_update_search mus = mc_update_search::linear);

    /* Set run specific variables */
    void set_run_vars(double mtime, count_type msteps,
                      count_type cyclen = 1, count_type ncycs = 1,
                      double cp_t = mc_simulation_stats::limit_runtime(),
                      count_type cp_s = mc_simulation_stats::limit_steps(),
                      mc_update_search mus = mc_update_search::linear);

    /* Sort and initialize probabilities of updates */
    void sort_and_init_updates();

    /* Calls the initialize() method for all updates and measurements */
    void initialize();

    /* Calls the finalize() method for all updates and measurements */
    void finalize();

    /* Add an update to MC simulation */
    template <typename U>
    mc_simulation& add_update(const U&, double, const std::string&,
                              const std::string& = std::string(), bool = true);

    /* Add a measurement to MC simulation */
    template <typename M>
    mc_simulation& add_measurement(const M&, const std::string&);

    /* Get update by name */
    template <typename T>
    T& get_update(const std::string&);

    template <typename T>
    const T& get_update(const std::string&) const;

    /* Get measurement by name */
    template <typename T>
    T& get_measurement(const std::string&);

    template <typename T>
    const T& get_measurement(const std::string&) const;

    /* Perform basic MC step */
    void basic_metropolis_step(update_type&);

    /* Perform a MC step */
    template <mc_update_search US = mc_update_search::linear>
    void do_step();

    /* Warm up the MC simulation */
    template <mc_update_search US = mc_update_search::linear>
    void do_warmup();

    void warmup();

    /* Run the MC simulation */
    template <mc_update_search US = mc_update_search::linear>
    void do_run(bool, bool);

    void run(bool do_init = true, bool do_final = true);

    /* Perform measurements */
    void do_measurements();

    /* Serialize to Json */
    void to_json(nlohmann::json&) const override;

    /* Deserialize from Json */
    void from_json(const nlohmann::json&) override;

    /* Read Json input */
    void read_input_json(const nlohmann::json&) override;

    /* Write Json input */
    void write_input_json(nlohmann::json&) const override;

#ifdef SIMPLEMC_HAS_MPI
    /* Collect results from MPI processes */
    mc_simulation mpi_collect_results(simpleMC::mpi::communicator) const;
#endif

private:
    int rank_;
    gen_type uni01_;
    config_type config_;
    std::vector<update_ptr> updates_;
    mc_update_stats ustat_;
    std::vector<measurement_ptr> measurements_;
    mc_measurement_stats mstat_;
    mc_simulation_stats sstat_;
    callback_type checkpoint_callback_;
    callback_type signal_callback_;
};

/* Default constructor */
template <typename C>
mc_simulation<C>::mc_simulation(int rank, std::uint64_t seed) :
    rank_(rank),
    uni01_(mc_traits::eng_type(seed), mc_traits::urd_type()),
    config_(),
    updates_(),
    ustat_(),
    measurements_(),
    mstat_(),
    sstat_(),
    checkpoint_callback_(default_checkpoint_callback<C>),
    signal_callback_(default_signal_callback<C>)
{
    assert(rank_ >= 0);
    for (int i = 0; i < rank_; i++) {
        uni01_.engine().long_jump();
    }
}

/* Set warm up specific variables */
template <typename C>
void mc_simulation<C>::set_warmup_vars(double wtime, count_type wsteps,
                                       count_type cyclen, count_type ncycs,
                                       mc_update_search mus) {
    sstat_.warmup_time = wtime;
    sstat_.warmup_steps = wsteps;
    sstat_.cycle_length = cyclen;
    sstat_.do_n_cycles = ncycs;
    sstat_.update_search = mus;
}

/* Set run specific variables */
template <typename C>
void mc_simulation<C>::set_run_vars(double mtime, count_type msteps,
                                    count_type cyclen, count_type ncycs,
                                    double cp_t, count_type cp_s,
                                    mc_update_search mus) {
    sstat_.max_time = mtime;
    sstat_.max_steps = msteps;
    sstat_.cycle_length = cyclen;
    sstat_.do_n_cycles = ncycs;
    sstat_.checkpoint_after_t = cp_t;
    sstat_.checkpoint_after_s = cp_s;
    sstat_.update_search = mus;
}

/* Sort and initialize probabilities of updates */
template <typename C>
void mc_simulation<C>::sort_and_init_updates() {
    using simpleMC::utils::apply_permutation;
    auto perm = ustat_.sort();
    updates_ = apply_permutation(updates_, perm);
    ustat_.init_probs();
    ustat_.init_walias();
}

/* Calls the initialize() method for all updates and measurements */
template <typename C>
void mc_simulation<C>::initialize() {
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        updates_[i]->initialize(*this);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        measurements_[i]->initialize(*this);
    }
}

/* Calls the finalize() method for all updates and measurements */
template <typename C>
void mc_simulation<C>::finalize() {
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        updates_[i]->finalize(*this);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        measurements_[i]->finalize(*this);
    }
}

/* Add an update to MC simulation */
template <typename C>
template <typename U>
mc_simulation<C>& mc_simulation<C>::add_update(const U& up, double weight,
                                               const std::string& name,
                                               const std::string& name_inv,
                                               bool so_ini) {
    updates_.push_back(update_ptr(new U(up)));
    ustat_.add_update(weight, name, name_inv);
    if (so_ini) {
        sort_and_init_updates();
    }
    return *this;
}

/* Add a measurement to MC simulation */
template <typename C>
template <typename M>
mc_simulation<C>& mc_simulation<C>::add_measurement(const M& me,
                                                    const std::string& name) {
    measurements_.push_back(measurement_ptr(new M(me)));
    mstat_.add_measurement(name);
    return *this;
}

/* Get update by name */
template <typename C>
template <typename T>
T& mc_simulation<C>::get_update(const std::string& name) {
    auto idx = ustat_.find_by_name(name);
    if (idx >= ustat_.num_updates()) {
        throw std::out_of_range("Update " + name + " not found.");
    }
    return dynamic_cast<T&>(*updates_[idx]);
}

template <typename C>
template <typename T>
const T& mc_simulation<C>::get_update(const std::string& name) const {
    auto idx = ustat_.find_by_name(name);
    if (idx >= ustat_.num_updates()) {
        throw std::out_of_range("Update " + name + " not found.");
    }
    return dynamic_cast<const T&>(*updates_[idx]);
}

/* Get measurement by name */
template <typename C>
template <typename T>
T& mc_simulation<C>::get_measurement(const std::string& name) {
    auto idx = mstat_.find_by_name(name);
    if (idx >= mstat_.num_measurements()) {
        throw std::out_of_range("Measurement " + name + " not found.");
    }
    return dynamic_cast<T&>(*measurements_[idx]);
}

template <typename C>
template <typename T>
const T& mc_simulation<C>::get_measurement(const std::string& name) const {
    auto idx = mstat_.find_by_name(name);
    if (idx >= mstat_.num_measurements()) {
        throw std::out_of_range("Measurement " + name + " not found.");
    }
    return dynamic_cast<const T&>(*measurements_[idx]);
}

/* Perform basic MC step */
template <typename C>
inline void mc_simulation<C>::basic_metropolis_step(update_type& up) {
    double ratio = up.attempt(config_, uni01_);
    ustat_.increase_proposed();
    if (ratio >= 1.0 || ratio >= uni01_()) {
        up.accept(config_, uni01_);
        ustat_.increase_accepted();
    } else {
        up.reject(config_, uni01_);
    }
}

/* Perform a MC step */
template <typename C>
template <mc_update_search US>
inline void mc_simulation<C>::do_step() {
    double prob = uni01_();
    if constexpr (US == mc_update_search::linear) {
        ustat_.current = 0;
        while (prob >= ustat_.probs[ustat_.current]) {
            ustat_.current++;
        }
    } else {
        ustat_.current = ustat_.walias.gen(prob);
    }
    basic_metropolis_step(*updates_[ustat_.current]);
}

/* Warm up the MC simulation */
template <typename C>
template <mc_update_search US>
void mc_simulation<C>::do_warmup() {
    simpleMC::signals::start_listening();
    ustat_.start();
    mstat_.start();
    sstat_.start();
    while (sstat_.keep_warmingup()) {
        for (std::size_t i = 0; i < sstat_.do_n_cycles; i++) {
            for (std::size_t j = 0; j < sstat_.cycle_length; j++) {
                do_step<US>();
                sstat_.steps_done++;
            }
        }
    }
    sstat_.stop();
    simpleMC::signals::stop_listening();
}

/* Warm up the MC simulation */
template <typename C>
void mc_simulation<C>::warmup() {
    if (sstat_.update_search == mc_update_search::linear) {
        do_warmup<mc_update_search::linear>();
    } else {
        do_warmup<mc_update_search::walker_alias>();
    }
}

/* Run the MC simulation */
template <typename C>
template <mc_update_search US>
void mc_simulation<C>::do_run(bool do_init, bool do_final) {
    simpleMC::signals::start_listening();
    if (!sstat_.load_checkpoint) {
        ustat_.start();
        mstat_.start();
        if (do_init) {
            initialize();
        }
    }
    sstat_.start();
    while (sstat_.keep_running()) {
        for (std::size_t i = 0; i < sstat_.do_n_cycles; i++) {
            for (std::size_t j = 0; j < sstat_.cycle_length; j++) {
                do_step<US>();
                sstat_.steps_done++;
            }
            do_measurements();
        }
        if (sstat_.check_checkpoint()) {
            checkpoint_callback_(*this);
            sstat_.last_checkpoint_s = sstat_.steps_done;
            sstat_.last_checkpoint_t = sstat_.runtime();
        }
        if (sstat_.signals_catched()) {
            signal_callback_(*this);
        }
    }
    checkpoint_callback_(*this);
    sstat_.stop();
    if (do_final) {
        finalize();
    }
    simpleMC::signals::stop_listening();
}

/* Run the MC simulation */
template <typename C>
void mc_simulation<C>::run(bool do_init, bool do_final) {
    if (sstat_.update_search == mc_update_search::linear) {
        do_run<mc_update_search::linear>(do_init, do_final);
    } else {
        do_run<mc_update_search::walker_alias>(do_init, do_final);
    }
}

/* Perform measurements */
template <typename C>
inline void mc_simulation<C>::do_measurements() {
    for (const auto& measurement : measurements_) {
        measurement->measure(config_);
    }
    mstat_.total++;
}

/* Serialize to Json */
template <typename C>
void mc_simulation<C>::to_json(nlohmann::json& j) const {
    j["gen"] = uni01_;
    j["config"] = config_;
    j["ustat"] = ustat_;
    j["mstat"] = mstat_;
    j["sstat"] = sstat_;
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        j["updates"][ustat_.names[i]] = *updates_[i];
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        j["measurements"][mstat_.names[i]] = *measurements_[i];
    }
}

/* Deserialize from Json */
template <typename C>
void mc_simulation<C>::from_json(const nlohmann::json& j) {
    j.at("gen").get_to(uni01_);
    j.at("config").get_to(config_);
    j.at("ustat").get_to(ustat_);
    j.at("mstat").get_to(mstat_);
    j.at("sstat").get_to(sstat_);
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        j.at("updates").at(ustat_.names[i]).get_to(*updates_[i]);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        j.at("measurements").at(mstat_.names[i]).get_to(*measurements_[i]);
    }
}

/* Read Json input */
template <typename C>
void mc_simulation<C>::read_input_json(const nlohmann::json& j) {
    using simpleMC::json::read_input_json;
    read_input_json(j.at("configuration"), config_);
    read_input_json(j.at("updates"), ustat_);
    read_input_json(j.at("simulation"), sstat_);
    read_input_json(j.at("measurements"), mstat_);
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        read_input_json(j.at("updates").at(ustat_.names[i]), *updates_[i]);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        read_input_json(j.at("measurements").at(mstat_.names[i]),
                        *measurements_[i]);
    }
    sort_and_init_updates();
}

/* Write Json input */
template <typename C>
void mc_simulation<C>::write_input_json(nlohmann::json& j) const {
    using simpleMC::json::write_input_json;
    write_input_json(j["configuration"], config_);
    write_input_json(j["updates"], ustat_);
    write_input_json(j["simulation"], sstat_);
    write_input_json(j["measurements"], mstat_);
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        write_input_json(j["updates"][ustat_.names[i]], *updates_[i]);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        write_input_json(j["measurements"][mstat_.names[i]], *measurements_[i]);
    }
}

#ifdef SIMPLEMC_HAS_MPI
/* Collect results from MPI processes */
template <typename C>
mc_simulation<C> mc_simulation<C>::mpi_collect_results(
        simpleMC::mpi::communicator comm) const {
    mc_simulation<C> mc_red = *this;
    mc_red.ustat_ = mpi_collect_update_stats(ustat_, comm);
    mc_red.mstat_ = mpi_collect_measurement_stats(mstat_, comm);
    mc_red.sstat_ = mpi_collect_simulation_stats(sstat_, comm);
    for (std::size_t i = 0; i < ustat_.num_updates(); i++) {
        mc_red.updates_[i].reset(updates_[i]->clone());
        mc_red.updates_[i]->mpi_collect(comm);
    }
    for (std::size_t i = 0; i < mstat_.num_measurements(); i++) {
        mc_red.measurements_[i].reset(measurements_[i]->clone());
        mc_red.measurements_[i]->mpi_collect(comm);
    }
    return mc_red;
}
#endif

} // namespace mc

} // namespace simpleMC
