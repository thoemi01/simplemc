/**
 * @file    mc_configuration.h
 * @brief   Base class for MC configurations.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/json/basic_jsonIO.h>

namespace simpleMC {

namespace mc {

/**
 * @brief Base class for MC configurations.
 *
 * @details The user should derived his/her own configuration from this
 * base class and override the virtual functions if needed.
 */
struct mc_configuration : public json::basic_jsonIO {
public:
    /* Virtual destructor */
    virtual ~mc_configuration() = default;
};

} // namespace mc

} // namespace simpleMC
