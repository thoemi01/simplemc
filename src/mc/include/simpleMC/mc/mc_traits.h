/**
 * @file    mc_traits.h
 * @brief   Traits class for MC tools.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/utils/random.h>

namespace simpleMC {

namespace mc {

/**
 * @brief Basic traits, typedefs, ... for MC tools.
 */
struct mc_traits {
public:
    using eng_type = simpleMC::utils::xoshiro256ss;
    using urd_type = simpleMC::utils::uniform_real_distribution;
    using gen_type = simpleMC::utils::variate_generator<eng_type, urd_type>;
    using splitmix = simpleMC::utils::splitmix64;

    static constexpr auto default_seed = splitmix::default_seed;
};

} // namespace mc

} // namespace simpleMC
