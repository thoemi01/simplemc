/**
 * @file    mc_fwd.h
 * @brief   Forward declarations of MC tools.
 * @author  Thomas Hahn
 */

#pragma once

#include <nlohmann/json_fwd.hpp>

namespace simpleMC {

namespace mc {

enum class mc_update_search;
struct mc_update_stats;
struct mc_measurement_stats;
struct mc_simulation_stats;
struct mc_configuration;
template <typename C> struct mc_update;
template <typename C> struct mc_measurement;
template <typename C> class mc_simulation;

} // namespace mc

} // namespace simpleMC
