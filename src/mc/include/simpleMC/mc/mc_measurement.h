/**
 * @file    mc_measurement.h
 * @brief   Base class for MC measurements.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/config.h>
#include <simpleMC/json/basic_jsonIO.h>
#include <simpleMC/mc/mc_fwd.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace mc {

/**
 * @brief Base class for MC measurements.
 *
 * @details The user should derived his/her own updates from this base class
 * and override the virtual functions if needed.
 *
 * @tparam C Configuration type
 */
template <typename C>
struct mc_measurement : public json::basic_jsonIO {
public:
    using config_type = C;
    using mc_type = mc_simulation<config_type>;

public:
    /* Virtual clone method */
    virtual mc_measurement* clone() const = 0;

    /* Initialize measurement */
    virtual void initialize(const mc_type&) {}

    /* Finalize measurement */
    virtual void finalize(const mc_type&) {}

    /* Perform measurement */
    virtual void measure(const config_type&) = 0;

#ifdef SIMPLEMC_HAS_MPI
    /* Collect measurements from MPI processes */
    virtual void mpi_collect(simpleMC::mpi::communicator /*comm*/) {}
#endif

    /* Virtual destructor */
    virtual ~mc_measurement() = default;
};

} // namespace mc

} // namespace simpleMC
