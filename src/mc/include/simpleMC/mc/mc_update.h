﻿/**
 * @file    mc_update.h
 * @brief   Base class for MC updates.
 * @author  Thomas Hahn
 */

#pragma once

#include <simpleMC/config.h>
#include <simpleMC/json/basic_jsonIO.h>
#include <simpleMC/mc/mc_fwd.h>
#include <simpleMC/mc/mc_traits.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace mc {

/**
 * @brief Base class for MC updates.
 *
 * @details The user should derived his/her own updates from this base class
 * and override the virtual functions if needed.
 *
 * @tparam C Configuration type
 */
template <typename C>
struct mc_update : public json::basic_jsonIO {
public:
    using config_type = C;
    using mc_type = mc_simulation<config_type>;
    using gen_type = mc_traits::gen_type;

public:
    /* Virtual clone method */
    virtual mc_update* clone() const = 0;

    /* Initialize update */
    virtual void initialize(const mc_type&) {}

    /* Finalize update */
    virtual void finalize(const mc_type&) {}

    /* Attempt update */
    virtual double attempt(const config_type&, gen_type&) = 0;

    /* Accept update */
    virtual void accept(config_type&, gen_type&) {};

    /* Reject update */
    virtual void reject(config_type&, gen_type&) {};

#ifdef SIMPLEMC_HAS_MPI
    /* Collect updates from MPI processes */
    virtual void mpi_collect(simpleMC::mpi::communicator /*comm*/) {}
#endif

    /* Virtual destructor */
    virtual ~mc_update() = default;
};

} // namespace mc

} // namespace simpleMC
