/**
 * @file    mc_measurement_stats.h
 * @brief   Statistics and information on MC measurements.
 * @author  Thomas Hahn
 */

#pragma once

#include <vector>
#include <string>
#include <cstdint>
#include <simpleMC/config.h>
#include <simpleMC/mc/mc_fwd.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/communicator.h>
#endif

namespace simpleMC {

namespace mc {

namespace detail {

/**
 * @brief Helper function to create default names for measurements.
 */
std::string default_measurement_name(std::size_t);
std::vector<std::string> default_measurement_names(std::size_t);

} // namespace detail

/**
 * @brief Simple struct to hold information on a single MC measurement.
 */
struct mc_measurement_info {
public:

    /* Constructor for mc_measurement_info */
    mc_measurement_info(std::size_t, const std::string&);

    /* Returns a mc_measurement_info for invalid indexes */
    static mc_measurement_info invalid(std::size_t);

public:
    std::size_t index;
    std::string name;
};

/**
 * @brief Simple struct to hold information on all MC measurement.
 */
struct mc_measurement_stats {
public:
    using count_type = std::uint64_t;

public:

    /* Default constructor */
    mc_measurement_stats();

    /* Constructor for a given number of measurements */
    mc_measurement_stats(std::size_t);

    /* Number of measurements */
    std::size_t num_measurements() const { return names.size(); }

    /* Add a measurement */
    void add_measurement(const std::string& = std::string());

    /* Find measurement by name and return its index */
    std::size_t find_by_name(const std::string&) const ;

    /* Get info on a single measurement by name */
    mc_measurement_info get_info(const std::string&) const ;

    /* Get info on a single measurement by index */
    mc_measurement_info get_info(std::size_t) const;

    /* Start simulation by resetting variables */
    void start() { total = 0; }

public:
    std::vector<std::string> names;
    count_type total;
};

/**
 * @brief Write mc_measurement_info to output stream.
 */
std::ostream& operator<<(std::ostream&, const mc_measurement_info&);

/**
 * @brief Write mc_measurement_stats to output stream.
 */
std::ostream& operator<<(std::ostream&, const mc_measurement_stats&);

/**
 * @brief Serialize mc_measurement_stats to Json.
 */
void to_json(nlohmann::json&, const mc_measurement_stats&);

/**
 * @brief Deserialize mc_update_stats from Json.
 */
void from_json(const nlohmann::json&, mc_measurement_stats&);

/**
 * @brief Read Json input file.
 */
void read_input_json(const nlohmann::json&, mc_measurement_stats&);

/**
 * @brief Write Json input file.
 */
void write_input_json(nlohmann::json&, const mc_measurement_stats&);

#ifdef SIMPLEMC_HAS_MPI
/**
 * @brief Collect measurement stats from MPI processes.
 */
mc_measurement_stats
mpi_collect_measurement_stats(const mc_measurement_stats&,
                              simpleMC::mpi::communicator);
#endif

} // namespace mc

} // namespace simpleMC
