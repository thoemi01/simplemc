/**
 * @file    mc_update_stats.cpp
 * @brief   Statistics and information on MC updates.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <iterator>
#include <nlohmann/json.hpp>
#include <simpleMC/utils/algorithms.h>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO_walker_alias.h>
#include <simpleMC/mc/mc_update_stats.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/reduce.h>
#endif

namespace simpleMC {

namespace mc {

namespace detail {

/* Helper function to create default names for updates */
std::string default_update_name(std::size_t i) {
    return "Update " + std::to_string(i);
}

std::vector<std::string> default_update_names(std::size_t num) {
    std::vector<std::string> res(num);
    for (std::size_t i = 0; i < num; i++) {
        res[i] = default_update_name(i);
    }
    return res;
}

} // namespace detail

/* Constructor for mc_update_info */
mc_update_info::mc_update_info(std::size_t idx, double we, double pr,
                               count_type np, count_type nac,
                               const std::string& nam,
                               const std::string& nam_inv) :
    index(idx),
    weight(we),
    prob(pr),
    nprop(np),
    nacc(nac),
    name(nam),
    name_inv(nam_inv)
{}

/* Returns a mc_update_info for invalid indexes */
mc_update_info mc_update_info::invalid(std::size_t idx) {
    return mc_update_info(idx, 0, 0, 0, 0, "INVALID", "INVALID");
}

/* Constructor for a given number of updates */
mc_update_stats::mc_update_stats(std::size_t num) :
    weights(num, 1.0),
    probs(num, 1.0/num),
    nprops(num, 0),
    naccs(num, 0),
    names(detail::default_update_names(num)),
    names_inv(detail::default_update_names(num)),
    walias(weights),
    current(0)
{}

/* Normalization factor = Sum of weights */
double mc_update_stats::norm_factor() const {
    return std::accumulate(weights.begin(), weights.end(), 0.0);
}

/* Initialize probabilities */
void mc_update_stats::init_probs() {
    double norm = norm_factor();
    double current = 0.0;
    for (std::size_t i = 0; i < num_updates(); i++) {
        current += weights[i]/norm;
        probs[i] = current;
    }
}

/* Sort updates by weight */
std::vector<std::size_t> mc_update_stats::sort() {
    using namespace simpleMC::utils;

    // get permutation vector
    auto perm = sort_permutation(weights, [](double i, double j) {
        return i > j;
    });

    // rearrange according to permutation
    weights = apply_permutation(weights, perm);
    probs = apply_permutation(probs, perm);
    nprops = apply_permutation(nprops, perm);
    naccs = apply_permutation(naccs, perm);
    names = apply_permutation(names, perm);
    names_inv = apply_permutation(names_inv, perm);

    return perm;
}

/* Add an update */
void mc_update_stats::add_update(double weight, const std::string& name,
                                 const std::string& name_inv,
                                 count_type nprop, count_type nacc) {
    // add entries
    weights.push_back(weight);
    probs.push_back(weight);
    nprops.push_back(nprop);
    naccs.push_back(nacc);
    if (name.empty()) {
        names.push_back(detail::default_update_name(num_updates()));
    } else {
        names.push_back(name);
    }
    if (name_inv.empty()) {
        names_inv.push_back(names[num_updates() - 1]);
    } else {
        names_inv.push_back(name_inv);
    }
}

/* Find update by name and return its index */
std::size_t mc_update_stats::find_by_name(const std::string& name) const {
    auto it = std::find(names.begin(), names.end(), name);
    return std::distance(names.begin(), it);
}

/* Get info on a single update by name */
mc_update_info mc_update_stats::get_info(const std::string& name) const {
    auto idx = find_by_name(name);
    return get_info(idx);
}

/* Get info on a single update by index */
mc_update_info mc_update_stats::get_info(std::size_t idx) const {
    if (idx < num_updates()) {
        return mc_update_info(idx, weights[idx], probs[idx], nprops[idx],
                              naccs[idx], names[idx], names_inv[idx]);
    }
    return mc_update_info::invalid(idx);
}

/* Start simulation by resetting variables */
void mc_update_stats::start() {
    std::fill(nprops.begin(), nprops.end(), 0);
    std::fill(naccs.begin(), naccs.end(), 0);
}

/* Write mc_update_info to output stream */
std::ostream& operator<<(std::ostream& os, const mc_update_info& uinfo) {
    simpleMC::utils::fixedwidth_ostream fos20(os, 20);
    fos20 << "Update" << "Inverse" << "Weight"  << "Proposed"
          << "Accepted" << "Acc. ratio" << fos20.endl();
    os << "------------------------------------------------------------------"
       << "---------------------------------------------\n";
    fos20 << uinfo.name.substr(0, 17) << uinfo.name_inv.substr(0, 17)
          << uinfo.weight << uinfo.nprop << uinfo.nacc
          << static_cast<double>(uinfo.nacc)/uinfo.nprop
          << fos20.endl();
    return os;
}

/* Write mc_update_stats to output stream */
std::ostream& operator<<(std::ostream& os, const mc_update_stats& ustat) {
    simpleMC::utils::fixedwidth_ostream fos20(os, 20);
    auto nf = ustat.norm_factor();
    os << "============================\n"
       << "UPDATE STATISTICS:\n"
       << "============================\n";
    fos20 << "Update" << "Inverse" << "Probablility"
          << "Proposed" << "Accepted" << "Acc. ratio" << fos20.endl();
    os << "------------------------------------------------------------------"
       << "---------------------------------------------\n";
    for (std::size_t i = 0; i < ustat.num_updates(); i++) {
        fos20 << ustat.names[i].substr(0, 17) << ustat.names_inv[i].substr(0, 17)
              << ustat.weights[i]/nf << ustat.nprops[i] << ustat.naccs[i]
              << static_cast<double>(ustat.naccs[i])/ustat.nprops[i]
              << fos20.endl();
    }
    return os;
}

/* Serialize mc_update_stats to Json */
void to_json(nlohmann::json& j, const mc_update_stats& ustat) {
    j["weights"] = ustat.weights;
    j["probs"] = ustat.probs;
    j["nprops"] = ustat.nprops;
    j["naccs"] = ustat.naccs;
    j["names"] = ustat.names;
    j["names_inv"] = ustat.names_inv;
    j["walias"] = ustat.walias;
}

/* Deserialize mc_update_stats from Json */
void from_json(const nlohmann::json& j, mc_update_stats& ustat) {
    j.at("weights").get_to(ustat.weights);
    j.at("probs").get_to(ustat.probs);
    j.at("nprops").get_to(ustat.nprops);
    j.at("naccs").get_to(ustat.naccs);
    j.at("names").get_to(ustat.names);
    j.at("names_inv").get_to(ustat.names_inv);
    j.at("walias").get_to(ustat.walias);
}

/* Read Json input file */
void read_input_json(const nlohmann::json& j, mc_update_stats& ustat) {
    for (std::size_t i = 0; i < ustat.num_updates(); i++) {
        j.at(ustat.names[i]).at("weight").get_to(ustat.weights[i]);
    }
}

/* Write Json input file */
void write_input_json(nlohmann::json& j, const mc_update_stats& ustat) {
    for (std::size_t i = 0; i < ustat.num_updates(); i++) {
        j[ustat.names[i]]["weight"] = ustat.weights[i];
    }
}

#ifdef SIMPLEMC_HAS_MPI
/* Collect update stats from MPI processes */
mc_update_stats
mpi_collect_update_stats(const mc_update_stats& ustat,
                         simpleMC::mpi::communicator comm) {
    using simpleMC::mpi::reduce;
    mc_update_stats ustat_red = ustat;
    reduce(ustat.nprops, ustat_red.nprops, MPI_SUM, comm, /*all=*/true);
    reduce(ustat.naccs, ustat_red.naccs, MPI_SUM, comm, /*all=*/true);
    return ustat_red;
}
#endif

} // namespace mc

} // namespace simpleMC
