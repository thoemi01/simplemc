/**
 * @file    mc_simulation_stats.cpp
 * @brief   Statistics and information of MC simulations.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <limits>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/mc/mc_simulation_stats.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/reduce.h>
#endif

namespace simpleMC {

namespace mc {

namespace detail {

/* Convert mc_update_search to string */
std::string mus_to_string(mc_update_search mus) {
    switch (mus) {
        case mc_update_search::linear:
            return "linear";
        case mc_update_search::walker_alias:
            return "walker-alias";
    }
    // suppress compiler warning
    return std::string();
}

/* Convert string to mc_update_search */
mc_update_search string_to_mus(const std::string& str) {
    if (str == "linear") {
        return mc_update_search::linear;
    } else if (str == "walker-alias") {
        return mc_update_search::walker_alias;
    } else {
        throw std::invalid_argument("String " + str + " does not name an"
                                                      " update search algorithm");
    }
}

} // namespace detail

/* Default constructor */
mc_simulation_stats::mc_simulation_stats() :
    timer(),
    total_time(0.0),
    max_time(limit_runtime()),
    warmup_time(0.0),
    warmup_steps(0),
    total_steps(0),
    steps_done(0),
    max_steps(limit_steps()),
    cycle_length(1),
    do_n_cycles(1),
    checkpoint_after_t(limit_runtime()),
    last_checkpoint_t(0),
    checkpoint_after_s(limit_steps()),
    last_checkpoint_s(0),
    load_checkpoint(false),
    update_search(mc_update_search::linear)
{}

/* Reset stats */
void mc_simulation_stats::start() {
    timer.start();
    steps_done = 0;
    last_checkpoint_s = 0;
    last_checkpoint_t = 0.0;
}

/* Write mc_simulation_stats to output stream */
std::ostream& operator<<(std::ostream& os, const mc_simulation_stats& sstat) {
    using sec_d = simpleMC::utils::duration::sec_d;
    using detail::mus_to_string;
    double run_time = sstat.timer.since_start<sec_d>();
    os << "============================\n"
       << "SIMULATION STATISTICS:\n"
       << "============================\n";
    os << "Run time = " << run_time << "\n"
       << "Total time = " << sstat.total_time + run_time << "\n"
       << "Steps done = " << sstat.steps_done << "\n"
       << "Steps per sec = " << sstat.steps_done/run_time << "\n"
       << "Total steps = " << sstat.total_steps + sstat.steps_done << "\n"
       << "Cycles done = " << sstat.steps_done/sstat.cycle_length << "\n"
       << "Max. time = " << sstat.max_time << "\n"
       << "Max. steps = " << sstat.max_steps << "\n"
       << "Warm up time = " << sstat.warmup_time << "\n"
       << "Warm up steps = " << sstat.warmup_steps << "\n"
       << "Cycle length = " << sstat.cycle_length << "\n"
       << "Do n cycles = " << sstat.do_n_cycles << "\n"
       << "Checkpoint after n sec = " << sstat.checkpoint_after_t << "\n"
       << "Checkpoint after n steps = " << sstat.checkpoint_after_s << "\n"
       << "Load checkpoint = " << sstat.load_checkpoint << "\n"
       << "Update search = " << mus_to_string(sstat.update_search) << "\n";
    return os;
}

/* Serialize mc_simulation_stats to Json */
void to_json(nlohmann::json& j, const mc_simulation_stats& sstat) {
    j["total_time"] = sstat.total_time + sstat.runtime();
    j["total_steps_done"] = sstat.total_steps + sstat.steps_done;
    j["cycle_length"] = sstat.cycle_length;
}

/* Deserialize mc_simulation_stats from Json */
void from_json(const nlohmann::json& j, mc_simulation_stats& sstat) {
    j.at("total_time").get_to(sstat.total_time);
    j.at("total_steps_done").get_to(sstat.total_steps);
    j.at("cycle_length").get_to(sstat.cycle_length);
}

/* Read Json input file */
void read_input_json(const nlohmann::json& j, mc_simulation_stats& sstat) {
    using detail::string_to_mus;
    j.at("max_time").get_to(sstat.max_time);
    j.at("max_steps").get_to(sstat.max_steps);
    j.at("warmup_time").get_to(sstat.warmup_time);
    j.at("warmup_steps").get_to(sstat.warmup_steps);
    j.at("cycle_length").get_to(sstat.cycle_length);
    j.at("do_n_cycles").get_to(sstat.do_n_cycles);
    j.at("checkpoint_after_t").get_to(sstat.checkpoint_after_t);
    j.at("checkpoint_after_s").get_to(sstat.checkpoint_after_s);
    j.at("load_checkpoint").get_to(sstat.load_checkpoint);
    std::string mus;
    j.at("update_search").get_to(mus);
    sstat.update_search = string_to_mus(mus);
}

/* Write Json input file */
void write_input_json(nlohmann::json& j, const mc_simulation_stats& sstat) {
    using detail::mus_to_string;
    j["max_time"] = sstat.max_time;
    j["max_steps"] = sstat.max_steps;
    j["warmup_time"] = sstat.warmup_time;
    j["warmup_steps"] = sstat.warmup_steps;
    j["cycle_length"] = sstat.cycle_length;
    j["do_n_cycles"] = sstat.do_n_cycles;
    j["checkpoint_after_t"] = sstat.checkpoint_after_t;
    j["checkpoint_after_s"] = sstat.checkpoint_after_s;
    j["load_checkpoint"] = sstat.load_checkpoint;
    j["update_search"] = mus_to_string(sstat.update_search);
}

#ifdef SIMPLEMC_HAS_MPI
/* Collect simulation stats from MPI processes */
mc_simulation_stats
mpi_collect_simulation_stats(const mc_simulation_stats& sstat,
                             simpleMC::mpi::communicator comm) {
    using simpleMC::mpi::reduce;
    using sec_d = simpleMC::utils::duration::sec_d;
    mc_simulation_stats sstat_red = sstat;
    sstat_red.total_time += sstat_red.timer.since_start<sec_d>();
    sstat_red.total_steps += sstat_red.steps_done;
    reduce(sstat.total_time, sstat_red.total_time, MPI_MAX, comm,
           /*all=*/true);
    reduce(sstat.total_steps, sstat_red.total_steps, MPI_SUM,
           comm, /*all=*/true);
    reduce(sstat.steps_done, sstat_red.steps_done, MPI_SUM,
           comm, /*all=*/true);
    return sstat_red;
}
#endif

} // namespace mc

} // namespace simpleMC
