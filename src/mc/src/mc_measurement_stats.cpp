/**
 * @file    mc_measurement_stats.cpp
 * @brief   Statistics and information on MC measurements.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <iterator>
#include <algorithm>
#include <nlohmann/json.hpp>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/mc/mc_measurement_stats.h>

// MPI support if enabled
#ifdef SIMPLEMC_HAS_MPI
#include <simpleMC/mpi/reduce.h>
#endif

namespace simpleMC {

namespace mc {

namespace detail {

/* Helper function to create default names for measurements */
std::string default_measurement_name(std::size_t i) {
    return "Measurement " + std::to_string(i);
}

std::vector<std::string> default_measurement_names(std::size_t num) {
    std::vector<std::string> res(num);
    for (std::size_t i = 0; i < num; i++) {
        res[i] = default_measurement_name(i);
    }
    return res;
}

} // namespace detail

/* Constructor for mc_measurement_info */
mc_measurement_info::mc_measurement_info(std::size_t idx,
                                         const std::string& nam) :
    index(idx),
    name(nam)
{}

/* Returns a mc_measurement_info for invalid indexes */
mc_measurement_info mc_measurement_info::invalid(std::size_t idx) {
    return mc_measurement_info(idx, "INVALID");
}

/* Default constructor */
mc_measurement_stats::mc_measurement_stats() :
    names(),
    total(0)
{}

/* Constructor for a given number of measurements */
mc_measurement_stats::mc_measurement_stats(std::size_t num) :
    names(detail::default_measurement_names(num)),
    total(0)
{}

/* Add a measurement */
void mc_measurement_stats::add_measurement(const std::string& name) {
    // add entry
    if (name.empty()) {
        names.push_back(detail::default_measurement_name(num_measurements()));
    } else {
        names.push_back(name);
    }
}

/* Find measurement by name and return its index */
std::size_t mc_measurement_stats::find_by_name(const std::string& name) const {
    auto it = std::find(names.begin(), names.end(), name);
    return std::distance(names.begin(), it);
}

/* Get info on a single measurement by name */
mc_measurement_info mc_measurement_stats::get_info(const std::string& name) const {
    auto idx = find_by_name(name);
    if (idx < num_measurements()) {
        return mc_measurement_info(idx, names[idx]);
    }
    return mc_measurement_info::invalid(idx);
}

/* Get info on a single measurement by index */
mc_measurement_info mc_measurement_stats::get_info(std::size_t idx) const {
    if (idx < num_measurements()) {
        return mc_measurement_info(idx, names[idx]);
    }
    return mc_measurement_info::invalid(idx);
}

/* Write mc_measurement_info to output stream */
std::ostream& operator<<(std::ostream& os, const mc_measurement_info& minfo) {
    simpleMC::utils::fixedwidth_ostream fos20(os, 20);
    fos20 << "Measurement" << "Index" << fos20.endl();
    fos20 << minfo.name.substr(0.17) <<  minfo.index << fos20.endl();
    return os;
}

/* Write mc_measurement_stats to output stream */
std::ostream& operator<<(std::ostream& os, const mc_measurement_stats& mstat) {
    simpleMC::utils::fixedwidth_ostream fos20(os, 20);
    os << "#Number of measurements = " << mstat.num_measurements() << "\n"
       << "#Total done = " << mstat.total << "\n";
    fos20 << "Measurement" << "Index" << fos20.endl();
    for (std::size_t i = 0; i < mstat.num_measurements(); i++) {
        fos20 << mstat.names[i].substr(0, 17) << i << fos20.endl();
    }
    return os;
}

/* Serialize mc_measurement_stats to Json */
void to_json(nlohmann::json& j, const mc_measurement_stats& mstat) {
    j["names"] = mstat.names;
    j["total"] = mstat.total;
}

/* Deserialize mc_measurement_stats from Json */
void from_json(const nlohmann::json& j, mc_measurement_stats& mstat) {
    j.at("names").get_to(mstat.names);
    j.at("total").get_to(mstat.total);
}

/* Read Json input file */
void read_input_json(const nlohmann::json& /*j*/,
                     mc_measurement_stats& /*ustat*/) {}

/* Write Json input file */
void write_input_json(nlohmann::json& j,
                      const mc_measurement_stats& mstat) {
    for (std::size_t i = 0; i < mstat.num_measurements(); i++) {
        j[mstat.names[i]] = nullptr;
    }
}

#ifdef SIMPLEMC_HAS_MPI
/* Collect measurement stats from MPI processes */
mc_measurement_stats
mpi_collect_measurement_stats(const mc_measurement_stats& mstat,
                              simpleMC::mpi::communicator comm) {
    using simpleMC::mpi::reduce;
    mc_measurement_stats mstat_red = mstat;
    reduce(mstat.total, mstat_red.total, MPI_SUM, comm, /*all=*/true);
    return mstat_red;
}
#endif

} // namespace mc

} // namespace simpleMC
