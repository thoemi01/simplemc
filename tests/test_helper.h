/**
 * @file    test_helper.h
 * @brief   Helper functions for testing.
 * @author  Thomas Hahn
 */

#pragma once

#include <iostream>
#include <string>

template <typename C>
void print_container(const C& cont, const std::string& delim = ", ",
                     const std::string& start = "[",
                     const std::string& end = "]\n") {
    std::cout << start;
    if (cont.size() == 0) {
        std::cout << end;
        return;
    }
    std::cout << cont[0];
    for (std::size_t i = 1; i < cont.size(); i++) {
        std::cout << delim << cont[i];
    }
    std::cout << end;
}
