/**
 * @file    fixtures.h
 * @brief   Fixture class for unit testing accumulators.
 * @author  Thomas Hahn
 */

#pragma once

#include <fstream>
#include <random>
#include "acc_test_helper.h"
#include <simpleMC/accs/accs.h>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/utils/grids.h>

using namespace simpleMC::accs;

class MeanAccumulatorTest : public ::testing::Test {
protected:
    using ctype = std::complex<double>;
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = simpleMC::utils::uniform_real_distribution;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    MeanAccumulatorTest() : vg(rng_type{}, dist_type{0.0, 5.0}) {
        // resize accumulators
        vec_size = 5;
        acc_vec_d.reset(vec_size);
        acc_vec_cd.reset(vec_size);

        // expected values
        exp_val_d = 2.5;
        exp_val_cd = {2.5, 2.5};
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void fill_accumulators(std::size_t num) {
        for (std::size_t i = 0; i < num; i++) {
            acc_d << random_num<double>(vg);
            acc_vec_d << random_vec<double>(vec_size, vg);
            acc_cd << random_num<ctype>(vg);
            acc_vec_cd << random_vec<ctype>(vec_size, vg);
        }
    }

    void fill_with_sva(std::size_t num, std::size_t idx) {
        for (std::size_t i = 0; i < num; i++) {
            acc_vec_d[idx] << random_num<double>(vg);
            acc_vec_cd[idx] << random_num<ctype>(vg);
        }
    }

    void print_accumulators() {
        std::cout << acc_d << "\n"
                  << acc_vec_d << "\n"
                  << acc_cd << "\n"
                  << acc_vec_cd << "\n";
    }

    vg_type vg;
    std::size_t vec_size;
    mean_acc<double> acc_d;
    mean_acc<double> acc_vec_d;
    mean_acc<ctype> acc_cd;
    mean_acc<ctype> acc_vec_cd;
    double exp_val_d;
    ctype exp_val_cd;
};

class VarAccumulatorTest : public ::testing::Test {
protected:
    using ctype = std::complex<double>;
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = std::normal_distribution<double>;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    VarAccumulatorTest() : vg(rng_type{}, dist_type{2.0, 1.0}) {
        // resize accumulators
        vec_size = 3;
        acc_vec_d.reset(vec_size);
        acc_vec_cd.reset(vec_size);

        // expected values
        exp_mean_d = 2.0;
        exp_var_d = 1.0;
        exp_mean_cd = {2.0, 2.0};
        exp_var_cd = 2.0;
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void fill_accumulators(std::size_t num) {
        for (std::size_t i = 0; i < num; i++) {
            acc_d << random_num<double>(vg);
            acc_vec_d << random_vec<double>(vec_size, vg);
            acc_cd << random_num<ctype>(vg);
            acc_vec_cd << random_vec<ctype>(vec_size, vg);
        }
    }

    void fill_with_sva(std::size_t num, std::size_t idx) {
        for (std::size_t i = 0; i < num; i++) {
            acc_vec_d[idx] << random_num<double>(vg);
            acc_vec_cd[idx] << random_num<ctype>(vg);
        }
    }

    void print_accumulators() {
        std::cout << acc_d
                  << "Var(X) = " << acc_d.variance() << "\n\n"
                  << acc_vec_d
                  << "Var(X) = " << acc_vec_d.variance() << "\n\n"
                  << acc_cd
                  << "Var(X) = " << acc_cd.variance() << "\n\n"
                  << acc_vec_cd
                  << "Var(X) = " << acc_vec_cd.variance() << "\n\n";
    }

    vg_type vg;
    std::size_t vec_size;
    var_acc<double> acc_d;
    var_acc<double> acc_vec_d;
    var_acc<ctype> acc_cd;
    var_acc<ctype> acc_vec_cd;
    double exp_mean_d;
    double exp_var_d;
    ctype exp_mean_cd;
    double exp_var_cd;
};

class BlockvarAccumulatorTest : public ::testing::Test {
protected:
    using ctype = std::complex<double>;
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = std::normal_distribution<double>;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    BlockvarAccumulatorTest() : vg(rng_type{}, dist_type{2.0, 1.0}) {
        // resize accumulators
        vec_size = 3;
        bl_size = 16;
        acc_d.reset(1, bl_size);
        acc_cd.reset(1, bl_size);
        acc_vec_d.reset(vec_size, bl_size);
        acc_vec_cd.reset(vec_size, bl_size);

        // expected values
        exp_mean_d = 2.0;
        exp_var_d = 1.0;
        exp_mean_cd = {2.0, 2.0};
        exp_var_cd = 2.0;
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void fill_accumulators(std::size_t num) {
        for (std::size_t i = 0; i < num; i++) {
            acc_d << random_num<double>(vg);
            acc_vec_d << random_vec<double>(vec_size, vg);
            acc_cd << random_num<ctype>(vg);
            acc_vec_cd << random_vec<ctype>(vec_size, vg);
        }
    }

    void fill_with_sva(std::size_t num, std::size_t idx) {
        for (std::size_t i = 0; i < num; i++) {
            acc_vec_d[idx] << random_num<double>(vg);
            acc_vec_cd[idx] << random_num<ctype>(vg);
        }
    }

    void print_accumulators() {
        std::cout << acc_d
                  << "Var(X) = " << acc_d.variance() << "\n\n"
                  << acc_vec_d
                  << "Var(X) = " << acc_vec_d.variance() << "\n\n"
                  << acc_cd
                  << "Var(X) = " << acc_cd.variance() << "\n\n"
                  << acc_vec_cd
                  << "Var(X) = " << acc_vec_cd.variance() << "\n\n";
    }

    vg_type vg;
    std::size_t vec_size;
    std::size_t bl_size;
    blockvar_acc<double> acc_d;
    blockvar_acc<double> acc_vec_d;
    blockvar_acc<ctype> acc_cd;
    blockvar_acc<ctype> acc_vec_cd;
    double exp_mean_d;
    double exp_var_d;
    ctype exp_mean_cd;
    double exp_var_cd;
};

class AutocorrAccumulatorTest : public ::testing::Test {
protected:
    using ctype = std::complex<double>;
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = std::normal_distribution<double>;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    AutocorrAccumulatorTest() : vg(rng_type{}, dist_type{2.0, 1.0}) {
        // resize accumulators
        vec_size = 3;
        bl_size = 1;
        gran = 2;
        acc_d.reset(1, bl_size, gran);
        acc_cd.reset(1, bl_size, gran);
        acc_vec_d.reset(vec_size, bl_size, gran);
        acc_vec_cd.reset(vec_size, bl_size, gran);

        // expected values
        exp_mean_d = 2.0;
        exp_var_d = 1.0;
        exp_mean_cd = {2.0, 2.0};
        exp_var_cd = 2.0;
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void fill_accumulators(std::size_t num) {
        for (std::size_t i = 0; i < num; i++) {
            acc_d << random_num<double>(vg);
            acc_vec_d << random_vec<double>(vec_size, vg);
            acc_cd << random_num<ctype>(vg);
            acc_vec_cd << random_vec<ctype>(vec_size, vg);
        }
    }

    void fill_with_sva(std::size_t num, std::size_t idx) {
        for (std::size_t i = 0; i < num; i++) {
            acc_vec_d[idx] << random_num<double>(vg);
            acc_vec_cd[idx] << random_num<ctype>(vg);
        }
    }

    template <typename A>
    void print_all_levels(const A& acc, const std::string& name = "Acc.") {
        std::cout << "Accumulator: " << name << "\n"
                  << "# levels: " << acc.num_levels() << "\n"
                  << "count: " << acc.count() << "\n"
                  << "--------------------------\n";
        for (std::size_t i = 0; i < acc.num_levels(); i++) {
            std::cout << "LEVEL " << i << ":\n";
            std::cout << "count = " << acc.level(i).count() << "\n"
                      << "data = ";
            print_container(acc.level(i).data());
            std::cout << "\n"
                      << "data2 = ";
            print_container(acc.level(i).data2());
            std::cout << "\n"
                      << "block size = " << acc.level(i).block_size() << "\n"
                      << "block count = " << acc.level(i).block_data().count() << "\n"
                      << "block data = ";
            print_container(acc.level(i).block_data().data());
            std::cout << "\n\n";
        }
    }

    vg_type vg;
    std::size_t vec_size;
    std::size_t bl_size;
    std::size_t gran;
    autocorr_acc<double> acc_d;
    autocorr_acc<double> acc_vec_d;
    autocorr_acc<ctype> acc_cd;
    autocorr_acc<ctype> acc_vec_cd;
    double exp_mean_d;
    double exp_var_d;
    ctype exp_mean_cd;
    double exp_var_cd;
};

class BatchAccumulatorTest : public ::testing::Test {
protected:
    using ctype = std::complex<double>;
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = std::normal_distribution<double>;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    BatchAccumulatorTest() : vg(rng_type{}, dist_type{2.0, 1.0}) {
        // resize accumulators
        vec_size = 3;
        batch_size = 1024;
        bl_size = 16;
        acc_d.reset(1, batch_size, bl_size);
        acc_cd.reset(1, batch_size, bl_size);
        acc_vec_d.reset(vec_size, batch_size, bl_size);
        acc_vec_cd.reset(vec_size, batch_size, bl_size);

        // expected values
        exp_mean_d = 2.0;
        exp_var_d = 1.0;
        exp_mean_cd = {2.0, 2.0};
        exp_var_cd = 2.0;
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void fill_accumulators(std::size_t num) {
        for (std::size_t i = 0; i < num; i++) {
            acc_d << random_num<double>(vg);
            acc_vec_d << random_vec<double>(vec_size, vg);
            acc_cd << random_num<ctype>(vg);
            acc_vec_cd << random_vec<ctype>(vec_size, vg);
        }
    }

    void fill_with_sva(std::size_t num, std::size_t idx) {
        for (std::size_t i = 0; i < num; i++) {
            acc_vec_d[idx] << random_num<double>(vg);
            acc_vec_cd[idx] << random_num<ctype>(vg);
        }
    }

    template <typename T>
    void print_accumulator(const batch_acc<T>& acc) {
        std::cout << acc
                  << "Var(X) = " << acc.variance() << "\n\n";
    }

    void print_accumulator_detail(const batch_acc<double>& acc) {
        auto i = 0;
        for (const auto& macc : acc.data()) {
            std::cout << ++i
                      << ": N = " << macc.count()
                      << ", data = " << macc.data()
                      << ", mean = " << macc.mean()
                      << "\n";
        }
    }

    vg_type vg;
    std::size_t vec_size;
    std::size_t batch_size;
    std::size_t bl_size;
    batch_acc<double> acc_d;
    batch_acc<double> acc_vec_d;
    batch_acc<ctype> acc_cd;
    batch_acc<ctype> acc_vec_cd;
    double exp_mean_d;
    double exp_var_d;
    ctype exp_mean_cd;
    double exp_var_cd;
};

class MonteCarloTest : public ::testing::Test {
protected:
    using rng_type = simpleMC::utils::xoshiro256pp;
    using dist_type = simpleMC::utils::uniform_real_distribution;
    using vg_type = simpleMC::utils::variate_generator<rng_type, dist_type>;

    MonteCarloTest() : vg(rng_type{}, dist_type{}) {
        // resize accumulators
        hist_acc.reset(hist_size);
    }

    void jump(int n) {
        for (int i = 0; i < n; i++) {
            vg.engine().jump();
        }
    }

    void reset() {
        acc = 0;
        rej = 0;
        hist_acc.reset();
        int1_acc.reset();
    }

    double uniform(double a, double b) {
        return a + b*vg();
    }

    double func(double x, double alpha) {
        return std::exp(-alpha*x);
    }

    double normalization(double a, double b, double alpha) {
        return (func(a, alpha) - func(b, alpha))/alpha;
    }

    double exact_int1(double a, double b, double alpha) {
        double af = std::exp(-a*alpha)*(a*alpha + 1)/(alpha*alpha);
        double bf = std::exp(-b*alpha)*(b*alpha + 1)/(alpha*alpha);
        return af - bf;
    }

    double exact_int2(double a, double b, double alpha) {
        double af = std::exp(-a*alpha)*(a*a*alpha*alpha + 2*a*alpha + 2)/(alpha*alpha*alpha);
        double bf = std::exp(-b*alpha)*(b*b*alpha*alpha + 2*b*alpha + 2)/(alpha*alpha*alpha);
        return af - bf;
    }

    double mcstep(double curr, double a, double b, double alpha) {
        double prop = uniform(a, b);
        double ratio = func(prop, alpha)/func(curr, alpha);
        if (ratio < vg()) {
            rej += 1;
        } else {
            acc += 1;
            return prop;
        }
        return curr;
    }

    void mcmc(std::uint64_t nsamples, std::uint64_t ntherm, double a,
              double b, double alpha) {
        // info
        std::cout << "Starting MC simulation...\n"
                  << "# MC steps = " << nsamples << "\n"
                  << "# therm. steps = " << ntherm << "\n"
                  << "alpha = " << alpha << "\n"
                  << "a = " << a << "\n"
                  << "b = " << b << "\n\n";

        // inital config
        double curr = uniform(a, b);

        // thermalization
        std::cout << "Starting " << ntherm << " thermalization steps...\n";
        for (std::uint64_t i = 0; i < ntherm; i++) {
            curr = mcstep(curr, a, b, alpha);
        }
        std::cout << "Finished thermalization.\n"
                  << "Acceptance ratio = " << acc/ntherm << "\n"
                  << "Rejectance ratio = " << rej/ntherm << "\n";
        reset();

        // simulation
        lg.set(a, b, hist_size + 1);
        double norm = normalization(a, b, alpha);
        std::cout << "Starting " << nsamples << " MC steps...\n";
        for (std::uint64_t i = 0; i < nsamples; i++) {
            curr = mcstep(curr, a, b, alpha);
            auto idx = lg.index(curr);
            hist_acc[idx] << norm/lg.binsize(i);
            int1_acc << curr*norm;
            int2_acc << curr*curr*norm;
        }
        std::cout << "Finished simulation.\n"
                  << "Acceptance ratio = " << acc/nsamples << "\n"
                  << "Rejectance ratio = " << rej/nsamples << "\n";
    }

    void write_to_file(double alpha) {
        auto mean = hist_acc.mean();
        std::string filename = "mc_histogram.dat";
        std::ofstream os(filename);
        simpleMC::utils::fixedwidth_ostream osw(os, 25);
        osw << "#tau" << "mean" << "exact" << osw.endl();
        for (std::size_t i = 0; i < hist_size; i++) {
            auto x = (lg.value(i) + lg.value(i+1))*0.5;
            osw << x << mean[i] << func(x, alpha) << osw.endl();
        }
    }

    vg_type vg;
    std::size_t hist_size = 100;
    simpleMC::utils::linear_grid lg;
    mean_acc<double> hist_acc;
    autocorr_acc<double> int1_acc;
    autocorr_acc<double> int2_acc;
    double acc = 0;
    double rej = 0;
};
