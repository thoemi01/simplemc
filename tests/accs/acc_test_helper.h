/**
 * @file    acc_test_helper.h
 * @brief   Helper functions for testing accumulators.
 * @author  Thomas Hahn
 */

#pragma once

#include <type_traits>
#include <complex>
#include <vector>
#include <gtest/gtest.h>
#include <simpleMC/utils/random.h>

template <typename T, typename VG>
T random_num(VG& vg) {
    if constexpr (std::is_same<T, double>::value)
        return vg();
    else if (std::is_same<T, std::complex<double>>::value) {
        return {vg(), vg()};
    }
    return T();
}

template <typename T, typename VG>
std::vector<T> random_vec(std::size_t size, VG& vg) {
    std::vector<T> vec(size);
    for (std::size_t i = 0; i < size; i++) {
        vec[i] = random_num<T>(vg);
    }
    return vec;
}

template <typename T>
void check_num(T val, T exp, double tol) {
    if constexpr (std::is_same<T, double>::value)
        ASSERT_NEAR(val, exp, tol);
    else if (std::is_same<T, std::complex<double>>::value) {
        ASSERT_NEAR(val.real(), exp.real(), tol);
        ASSERT_NEAR(val.imag(), exp.imag(), tol);
    }
}

template <typename C, typename T>
void check_container(const C& cont, T exp, double tol) {
    for (int i = 0; i < cont.size(); i++) {
        check_num(cont[i], exp, tol);
    }
}

template <typename T>
void check_nan(T val) {
    if constexpr (std::is_same<T, double>::value)
        ASSERT_TRUE(std::isnan(val));
    else if (std::is_same<T, std::complex<double>>::value) {
        ASSERT_TRUE(std::isnan(val.real()));
        ASSERT_TRUE(std::isnan(val.imag()));
    }
}

template <typename C1>
void check_container_nan(const C1& cont) {
    for (int i = 0; i < cont.size(); i++) {
        check_nan(cont[i]);
    }
}

template <typename S, typename A>
void check_serialization(S& ser, const A& acc, const std::string& path) {
    A acc_copy;
    ser[path] << acc;
    ser[path] >> acc_copy;
    ASSERT_EQ(acc, acc_copy);
}
