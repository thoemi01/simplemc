/**
 * @file    acc_mpi_test.cpp
 * @brief   Unit tests for MPI support for accumulators.
 * @author  Thomas Hahn
 */

#include "../gtest-mpi-listener.hpp"
#include "acc_fixtures.h"
#include <simpleMC/mpi/mpi.h>

using namespace simpleMC::mpi;

TEST_F(MeanAccumulatorTest, MPISupport) {
    communicator world;
    int root = 0;
    int size = world.size();
    double exp_d = (size + 1)*0.5;
    auto exp_cd = std::complex<double>(exp_d, exp_d);
    double tol = 0.00001;

    // set rng
    jump(world.rank());

    // fill accumulator
    auto val = world.rank() + 1;
    acc_d << val;
    acc_cd << std::complex<double>(val, val);

    // reduce
    auto acc_red_d = acc_d;
    auto acc_red_cd = acc_cd;
    reduce(acc_d, acc_red_d, world, false, root);
    reduce(acc_cd, acc_red_cd, world, false, root);

    // check results
    if (world.rank() == root) {
        ASSERT_EQ(acc_red_d.count(), world.size());
        ASSERT_EQ(acc_red_cd.count(), world.size());
        check_num(acc_red_d.mean()[0], exp_d, tol);
        check_num(acc_red_cd.mean()[0], exp_cd, tol);
    }

    // all reduce
    world.barrier();
    reduce(acc_d, acc_red_d, world);
    reduce(acc_cd, acc_red_cd, world);

    // check results
    ASSERT_EQ(acc_red_d.count(), world.size());
    ASSERT_EQ(acc_red_cd.count(), world.size());
    check_num(acc_red_d.mean()[0], exp_d, tol);
    check_num(acc_red_cd.mean()[0], exp_cd, tol);

    // fill vector accumulators
    world.barrier();
    fill_accumulators(10000);

    // all reduce
    auto acc_red_vec_d = acc_vec_d;
    auto acc_red_vec_cd = acc_vec_cd;
    reduce(acc_vec_d, acc_red_vec_d, world);
    reduce(acc_vec_cd, acc_red_vec_cd, world);

    // check results
    check_container(acc_vec_d.mean(), exp_val_d, 0.05);
    check_container(acc_vec_cd.mean(), exp_val_cd, 0.05);
}

TEST_F(VarAccumulatorTest, MPISupport) {
    communicator world;
    int root = 0;
    double tol = 0.05;
    std::size_t nsam = 10000;

    // set rng
    jump(world.rank());

    // fill accumulators
    fill_accumulators(nsam);

    // reduce
    auto acc_red_d = acc_d;
    auto acc_red_cd = acc_cd;
    reduce(acc_d, acc_red_d, world, false, root);
    reduce(acc_cd, acc_red_cd, world, false, root);

    // check results
    if (world.rank() == root) {
        ASSERT_EQ(acc_red_d.count(), world.size()*acc_d.count());
        ASSERT_EQ(acc_red_cd.count(), world.size()*acc_d.count());
        check_num(acc_red_d.mean()[0], exp_mean_d, tol);
        check_num(acc_red_cd.mean()[0], exp_mean_cd, tol);
        check_num(acc_red_d.variance()[0], exp_var_d, tol);
        check_num(acc_red_cd.variance()[0], exp_var_cd, tol);
    }

    // all reduce
    auto acc_red_vec_d = acc_vec_d;
    auto acc_red_vec_cd = acc_vec_cd;
    reduce(acc_vec_d, acc_red_vec_d, world);
    reduce(acc_vec_cd, acc_red_vec_cd, world);

    // check results
    ASSERT_EQ(acc_red_vec_d.count(), world.size()*acc_d.count());
    ASSERT_EQ(acc_red_vec_cd.count(), world.size()*acc_d.count());
    check_container(acc_red_vec_d.mean(), exp_mean_d, tol);
    check_container(acc_red_vec_cd.mean(), exp_mean_cd, tol);
    check_container(acc_red_vec_d.variance(), exp_var_d, tol);
    check_container(acc_red_vec_cd.variance(), exp_var_cd, tol);
}

TEST_F(BlockvarAccumulatorTest, MPISupport) {
    communicator world;
    int root = 0;
    double tol = 0.05;
    std::size_t nsam = 100000;

    // set rng
    jump(world.rank());

    // fill accumulators
    fill_accumulators(nsam);

    // reduce
    auto acc_red_d = acc_d;
    auto acc_red_cd = acc_cd;
    reduce(acc_d, acc_red_d, world, false, root);
    reduce(acc_cd, acc_red_cd, world, false, root);

    // check results
    if (world.rank() == root) {
        ASSERT_EQ(acc_red_d.count(), world.size()*acc_d.count());
        ASSERT_EQ(acc_red_cd.count(), world.size()*acc_d.count());
        check_num(acc_red_d.mean()[0], exp_mean_d, tol);
        check_num(acc_red_cd.mean()[0], exp_mean_cd, tol);
        check_num(acc_red_d.variance()[0], exp_var_d, tol);
        check_num(acc_red_cd.variance()[0], exp_var_cd, tol);
    }

    // all reduce
    auto acc_red_vec_d = acc_vec_d;
    auto acc_red_vec_cd = acc_vec_cd;
    reduce(acc_vec_d, acc_red_vec_d, world);
    reduce(acc_vec_cd, acc_red_vec_cd, world);

    // check results
    ASSERT_EQ(acc_red_vec_d.count(), world.size()*acc_d.count());
    ASSERT_EQ(acc_red_vec_cd.count(), world.size()*acc_d.count());
    check_container(acc_red_vec_d.mean(), exp_mean_d, tol);
    check_container(acc_red_vec_cd.mean(), exp_mean_cd, tol);
    check_container(acc_red_vec_d.variance(), exp_var_d, tol);
    check_container(acc_red_vec_cd.variance(), exp_var_cd, tol);
}

TEST_F(AutocorrAccumulatorTest, MPISupport) {
    communicator world;
    int root = 0;
    double tol = 0.1;
    std::size_t nsam = 100000;
    std::size_t minsam = 5000;

    // set rng
    jump(world.rank());

    // fill accumulators
    fill_accumulators(nsam);

    // reduce
    auto acc_red_d = acc_d;
    auto acc_red_cd = acc_cd;
    reduce(acc_d, acc_red_d, world, false, root);
    reduce(acc_cd, acc_red_cd, world, false, root);

    // check results
    if (world.rank() == root) {
        ASSERT_EQ(acc_red_d.count(), world.size()*acc_d.count());
        ASSERT_EQ(acc_red_cd.count(), world.size()*acc_d.count());
        check_num(acc_red_d.mean()[0], exp_mean_d, tol);
        check_num(acc_red_cd.mean()[0], exp_mean_cd, tol);
        check_num(acc_red_d.variance(minsam)[0], exp_var_d, tol);
        check_num(acc_red_cd.variance(minsam)[0], exp_var_cd, tol);
    }

    // all reduce
    auto acc_red_vec_d = acc_vec_d;
    auto acc_red_vec_cd = acc_vec_cd;
    reduce(acc_vec_d, acc_red_vec_d, world);
    reduce(acc_vec_cd, acc_red_vec_cd, world);

    // some output
    if (world.rank() == root) {
        acc_to_file(acc_vec_cd, "mpi_before.dat", "mpi_before.dat.tau", minsam);
        acc_to_file(acc_red_vec_cd, "mpi_after.dat", "mpi_after.dat.tau", minsam);
    }

    // check results
    ASSERT_EQ(acc_red_vec_d.count(), world.size()*acc_d.count());
    ASSERT_EQ(acc_red_vec_cd.count(), world.size()*acc_d.count());
    check_container(acc_red_vec_d.mean(), exp_mean_d, tol);
    check_container(acc_red_vec_cd.mean(), exp_mean_cd, tol);
    check_container(acc_red_vec_d.variance(minsam), exp_var_d, tol);
    check_container(acc_red_vec_cd.variance(minsam), exp_var_cd, tol);
}

TEST_F(BatchAccumulatorTest, MPISupport) {
    communicator world;
    int root = 0;
    double tol = 0.1;
    std::size_t nsam = 100000;

    // set rng
    jump(world.rank());

    // fill accumulators
    fill_accumulators(nsam);

    // reduce
    auto acc_red_d = acc_d;
    auto acc_red_cd = acc_cd;
    reduce(acc_d, acc_red_d, world, false, root);
    reduce(acc_cd, acc_red_cd, world, false, root);

    // check results
    if (world.rank() == root) {
        check_num(acc_red_d.mean()[0], exp_mean_d, tol);
        check_num(acc_red_cd.mean()[0], exp_mean_cd, tol);
        check_num(acc_red_d.variance()[0], exp_var_d, tol);
        check_num(acc_red_cd.variance()[0], exp_var_cd, tol);
    }

    // all reduce
    auto acc_red_vec_d = acc_vec_d;
    auto acc_red_vec_cd = acc_vec_cd;
    reduce(acc_vec_d, acc_red_vec_d, world);
    reduce(acc_vec_cd, acc_red_vec_cd, world);

    // check results
    check_container(acc_red_vec_d.mean(), exp_mean_d, tol);
    check_container(acc_red_vec_cd.mean(), exp_mean_cd, tol);
    check_container(acc_red_vec_d.variance(), exp_var_d, tol);
    check_container(acc_red_vec_cd.variance(), exp_var_cd, tol);
}

int main(int argc, char** argv) {
    // Filter out Google Test arguments
    ::testing::InitGoogleTest(&argc, argv);

    // Initialize MPI
    //MPI_Init(&argc, &argv);
    environment env(argc, argv);

    // Add object that will finalize MPI on exit; Google Test owns this pointer
    ::testing::AddGlobalTestEnvironment(new GTestMPIListener::MPIEnvironment);

    // Get the event listener list.
    ::testing::TestEventListeners& listeners =
            ::testing::UnitTest::GetInstance()->listeners();

    // Remove default listener: the default printer and the default XML printer
    ::testing::TestEventListener *l =
            listeners.Release(listeners.default_result_printer());

    // Adds MPI listener; Google Test owns this pointer
    listeners.Append(new GTestMPIListener::MPIWrapperPrinter(l, MPI_COMM_WORLD));

    // Run tests, then clean up and exit. RUN_ALL_TESTS() returns 0 if all tests
    // pass and 1 if some test fails.
    int result = RUN_ALL_TESTS();

    return result;
}
