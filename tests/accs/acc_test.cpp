﻿/**
 * @file    acc_test.cpp
 * @brief   Unit tests for simpleMC-accs.
 * @author  Thomas Hahn
 */

#include "acc_fixtures.h"
#include <simpleMC/config.h>
#include <simpleMC/json/jsonIO.h>
#ifdef SIMPLEMC_HAS_HDF5
#include <simpleMC/hdf5/hdf5IO.h>
#endif

TEST_F(MeanAccumulatorTest, EmptyAccumulator) {
    check_container_nan(acc_d.mean());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_cd.mean());
    check_container_nan(acc_vec_d.mean());
}

TEST_F(MeanAccumulatorTest, MeanValues) {
    std::size_t nsam = 10000;
    fill_accumulators(nsam);
    check_container(acc_d.mean(), exp_val_d, 0.05);
    check_container(acc_vec_d.mean(), exp_val_d, 0.05);
    check_container(acc_cd.mean(), exp_val_cd, 0.05);
    check_container(acc_vec_cd.mean(), exp_val_cd, 0.05);
    acc_to_file(acc_d, "mean_double.dat");
    acc_to_file(acc_vec_cd, "mean_complex_vec.dat");
}

TEST_F(MeanAccumulatorTest, SingleValueAccumulator) {
    std::size_t idx = 2;
    fill_with_sva(10000, idx);
    check_num(acc_vec_d.mean()[idx], exp_val_d, 0.05);
    check_num(acc_vec_cd.mean()[idx], exp_val_cd, 0.05);
}

TEST_F(MeanAccumulatorTest, ConvertBetweenStates) {
    auto acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    acc_d << 2.0;
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    fill_accumulators(1000);
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
}

TEST_F(MeanAccumulatorTest, JsonIO) {
    using namespace simpleMC::json;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    jsonIO jio;
    check_serialization(jio, acc_d, "acc_d");
    check_serialization(jio, acc_vec_d, "acc_vec_d");
    check_serialization(jio, acc_cd, "acc_cd");
    check_serialization(jio, acc_vec_cd, "acc_vec_cd");
    jio.dump_to_file("mean_acc.json");
}

TEST_F(VarAccumulatorTest, EmptyAccumulator) {
    check_container_nan(acc_d.mean());
    check_container_nan(acc_d.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
    check_container_nan(acc_cd.mean());
    check_container_nan(acc_cd.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
}

TEST_F(VarAccumulatorTest, MeanAndVariance) {
    std::size_t nsam = 10000;
    fill_accumulators(nsam);
    check_container(acc_d.mean(), exp_mean_d, 0.05);
    check_container(acc_vec_d.mean(), exp_mean_d, 0.05);
    check_container(acc_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_vec_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_d.variance(), exp_var_d, 0.05);
    check_container(acc_vec_d.variance(), exp_var_d, 0.05);
    check_container(acc_cd.variance(), exp_var_cd, 0.05);
    check_container(acc_vec_cd.variance(), exp_var_cd, 0.05);
    acc_to_file(acc_cd, "var_complex.dat");
    acc_to_file(acc_vec_d, "var_double_vec.dat");
}

TEST_F(VarAccumulatorTest, SingleValueAccumulator) {
    std::size_t idx = 2;
    fill_with_sva(10000, idx);
    check_num(acc_vec_d.mean()[idx], exp_mean_d, 0.05);
    check_num(acc_vec_d.variance()[idx], exp_var_d, 0.05);
    check_num(acc_vec_cd.mean()[idx], exp_mean_cd, 0.05);
    check_num(acc_vec_cd.variance()[idx], exp_var_cd, 0.05);
}

TEST_F(VarAccumulatorTest, ConvertBetweenStates) {
    auto acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    acc_d << 2.0;
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    fill_accumulators(1000);
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
}

TEST_F(VarAccumulatorTest, JsonIO) {
    using namespace simpleMC::json;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    jsonIO jio;
    check_serialization(jio, acc_d, "acc_d");
    check_serialization(jio, acc_vec_d, "acc_vec_d");
    check_serialization(jio, acc_cd, "acc_cd");
    check_serialization(jio, acc_vec_cd, "acc_vec_cd");
    jio.dump_to_file("var_acc.json");
}

TEST_F(BlockvarAccumulatorTest, EmptyAccumulator) {
    check_container_nan(acc_d.mean());
    check_container_nan(acc_d.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
    check_container_nan(acc_cd.mean());
    check_container_nan(acc_cd.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
}

TEST_F(BlockvarAccumulatorTest, MeanAndVariance) {
    std::size_t nsam = 100000;
    fill_accumulators(nsam);
    check_container(acc_d.mean(), exp_mean_d, 0.05);
    check_container(acc_vec_d.mean(), exp_mean_d, 0.05);
    check_container(acc_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_vec_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_d.variance(), exp_var_d, 0.05);
    check_container(acc_vec_d.variance(), exp_var_d, 0.05);
    check_container(acc_cd.variance(), exp_var_cd, 0.05);
    check_container(acc_vec_cd.variance(), exp_var_cd, 0.05);
    acc_to_file(acc_cd, "blockvar_complex.dat");
    acc_to_file(acc_vec_d, "blockvar_double_vec.dat");
}

TEST_F(BlockvarAccumulatorTest, SingleValueAccumulator) {
    std::size_t idx = 2;
    fill_with_sva(100000, idx);
    check_num(acc_vec_d.mean()[idx], exp_mean_d, 0.05);
    check_num(acc_vec_d.variance()[idx], exp_var_d, 0.05);
    check_num(acc_vec_cd.mean()[idx], exp_mean_cd, 0.05);
    check_num(acc_vec_cd.variance()[idx], exp_var_cd, 0.05);
}

TEST_F(BlockvarAccumulatorTest, ConvertBetweenStates) {
    auto acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    fill_accumulators(bl_size);
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    fill_accumulators(1000);
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
}

TEST_F(BlockvarAccumulatorTest, JsonIO) {
    using namespace simpleMC::json;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    jsonIO jio;
    check_serialization(jio, acc_d, "acc_d");
    check_serialization(jio, acc_vec_d, "acc_vec_d");
    check_serialization(jio, acc_cd, "acc_cd");
    check_serialization(jio, acc_vec_cd, "acc_vec_cd");
    jio.dump_to_file("blockvar_acc.json");
}

TEST_F(AutocorrAccumulatorTest, EmptyAccumulator) {
    check_container_nan(acc_d.level(0).mean());
    check_container_nan(acc_d.level(0).variance());
    check_container_nan(acc_vec_d.level(0).mean());
    check_container_nan(acc_vec_d.level(0).variance());
    check_container_nan(acc_cd.level(0).mean());
    check_container_nan(acc_cd.level(0).variance());
    check_container_nan(acc_vec_d.level(0).mean());
    check_container_nan(acc_vec_d.level(0).variance());
    ASSERT_EQ(acc_d.num_levels(), 1);
    ASSERT_EQ(acc_vec_d.num_levels(), 1);
    ASSERT_EQ(acc_cd.num_levels(), 1);
    ASSERT_EQ(acc_vec_cd.num_levels(), 1);
}

TEST_F(AutocorrAccumulatorTest, MeanAndVariance) {
    std::size_t nsam = 100000;
    std::size_t minsam = 5000;
    fill_accumulators(nsam);
    check_container(acc_d.mean(), exp_mean_d, 0.05);
    check_container(acc_vec_d.mean(), exp_mean_d, 0.05);
    check_container(acc_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_vec_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_d.variance(minsam), exp_var_d, 0.05);
    check_container(acc_vec_d.variance(minsam), exp_var_d, 0.05);
    check_container(acc_cd.variance(minsam), exp_var_cd, 0.05);
    check_container(acc_vec_cd.variance(minsam), exp_var_cd, 0.05);
}

TEST_F(AutocorrAccumulatorTest, SingleValueAccumulator) {
    std::size_t nsam = 100000;
    std::size_t minsam = 5000;
    std::size_t idx = 2;
    fill_with_sva(nsam, idx);
    check_num(acc_vec_d.mean()[idx], exp_mean_d, 0.05);
    check_num(acc_vec_d.variance(minsam)[idx], exp_var_d, 0.05);
    check_num(acc_vec_cd.mean()[idx], exp_mean_cd, 0.05);
    check_num(acc_vec_cd.variance(minsam)[idx], exp_var_cd, 0.05);
}

TEST_F(AutocorrAccumulatorTest, ConvertBetweenStates) {
    auto acc_copy = acc_d;
    ASSERT_EQ(acc_copy, acc_d);
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
    fill_accumulators(1000);
    acc_copy = acc_d;
    acc_d.convert_to_result();
    acc_d.convert_to_acc();
    ASSERT_EQ(acc_copy, acc_d);
}

TEST_F(AutocorrAccumulatorTest, JsonIO) {
    using namespace simpleMC::json;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    jsonIO jio;
    check_serialization(jio, acc_d, "acc_d");
    check_serialization(jio, acc_vec_d, "acc_vec_d");
    check_serialization(jio, acc_cd, "acc_cd");
    check_serialization(jio, acc_vec_cd, "acc_vec_cd");
    jio.dump_to_file("autocorr_acc.json");
}

TEST_F(BatchAccumulatorTest, EmptyAccumulator) {
    check_container_nan(acc_d.mean());
    check_container_nan(acc_d.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
    check_container_nan(acc_cd.mean());
    check_container_nan(acc_cd.variance());
    check_container_nan(acc_vec_d.mean());
    check_container_nan(acc_vec_d.variance());
}

TEST_F(BatchAccumulatorTest, MeanAndVariance) {
    std::size_t nsam = 100000;
    fill_accumulators(nsam);
    check_container(acc_d.mean(), exp_mean_d, 0.05);
    check_container(acc_vec_d.mean(), exp_mean_d, 0.05);
    check_container(acc_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_vec_cd.mean(), exp_mean_cd, 0.05);
    check_container(acc_d.variance(), exp_var_d, 0.1);
    check_container(acc_vec_d.variance(), exp_var_d, 0.1);
    check_container(acc_cd.variance(), exp_var_cd, 0.1);
    check_container(acc_vec_cd.variance(), exp_var_cd, 0.1);
    acc_to_file(acc_cd, "batch_complex.dat");
    acc_to_file(acc_vec_d, "batch_double_vec.dat");
}

TEST_F(BatchAccumulatorTest, SingleValueAccumulator) {
    std::size_t idx = 2;
    fill_with_sva(100000, idx);
    check_num(acc_vec_d.mean()[idx], exp_mean_d, 0.05);
    check_num(acc_vec_d.variance()[idx], exp_var_d, 0.1);
    check_num(acc_vec_cd.mean()[idx], exp_mean_cd, 0.05);
    check_num(acc_vec_cd.variance()[idx], exp_var_cd, 0.1);
}

TEST_F(BatchAccumulatorTest, JsonIO) {
    using namespace simpleMC::json;
    std::size_t nsam = 100000;
    fill_accumulators(nsam);
    jsonIO jio;
    check_serialization(jio, acc_d, "acc_d");
    check_serialization(jio, acc_vec_d, "acc_vec_d");
    check_serialization(jio, acc_cd, "acc_cd");
    check_serialization(jio, acc_vec_cd, "acc_vec_cd");
    jio.dump_to_file("batch_acc.json");
}

TEST_F(MonteCarloTest, Simulation) {
    double a = 0;
    double b = 5;
    double alpha = 1;
    std::uint64_t nsamples = 1000000;
    std::uint64_t ntherm = 1000;
    mcmc(nsamples, ntherm, a, b, alpha);
    hist_acc.convert_to_result();
    int1_acc.convert_to_result();
    int2_acc.convert_to_result();
    double res1 = int1_acc.mean()[0];
    double res2 = int2_acc.mean()[0];
    ASSERT_NEAR(res1, exact_int1(a, b, alpha), 0.01);
    ASSERT_NEAR(res2, exact_int2(a, b, alpha), 0.01);
    write_to_file(alpha);
    acc_to_file(int1_acc, "mc_int1.dat");
    acc_to_file(int2_acc, "mc_int2.dat");
}

#ifdef SIMPLEMC_HAS_HDF5
TEST_F(MeanAccumulatorTest, H5IO) {
    using namespace simpleMC::hdf5;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    hdf5IO hio("mean_acc.h5");
    check_serialization(hio, acc_d, "acc_d");
    check_serialization(hio, acc_vec_d, "acc_vec_d");
    check_serialization(hio, acc_cd, "acc_cd");
    check_serialization(hio, acc_vec_cd, "acc_vec_cd");
}

TEST_F(VarAccumulatorTest, H5IO) {
    using namespace simpleMC::hdf5;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    hdf5IO hio("var_acc.h5");
    check_serialization(hio, acc_d, "acc_d");
    check_serialization(hio, acc_vec_d, "acc_vec_d");
    check_serialization(hio, acc_cd, "acc_cd");
    check_serialization(hio, acc_vec_cd, "acc_vec_cd");
}

TEST_F(BlockvarAccumulatorTest, H5IO) {
    using namespace simpleMC::hdf5;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    hdf5IO hio("blockvar_acc.h5");
    check_serialization(hio, acc_d, "acc_d");
    check_serialization(hio, acc_vec_d, "acc_vec_d");
    check_serialization(hio, acc_cd, "acc_cd");
    check_serialization(hio, acc_vec_cd, "acc_vec_cd");
}

TEST_F(AutocorrAccumulatorTest, H5IO) {
    using namespace simpleMC::hdf5;
    std::size_t nsam = 10;
    fill_accumulators(nsam);
    hdf5IO hio("autocorr_acc.h5");
    check_serialization(hio, acc_d, "acc_d");
    check_serialization(hio, acc_vec_d, "acc_vec_d");
    check_serialization(hio, acc_cd, "acc_cd");
    check_serialization(hio, acc_vec_cd, "acc_vec_cd");
}

TEST_F(BatchAccumulatorTest, H5IO) {
    using namespace simpleMC::hdf5;
    std::size_t nsam = 100000;
    fill_accumulators(nsam);
    hdf5IO hio("batch_acc.h5");
    check_serialization(hio, acc_d, "acc_d");
    check_serialization(hio, acc_vec_d, "acc_vec_d");
    check_serialization(hio, acc_cd, "acc_cd");
    check_serialization(hio, acc_vec_cd, "acc_vec_cd");
}
#endif
