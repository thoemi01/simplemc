﻿/**
 * @file    hdf5_test.cpp
 * @brief   Unit tests for HDF5 IO support.
 * @author  Thomas Hahn
 */

#include <gtest/gtest.h>
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_grids.h>
#include <simpleMC/hdf5/hdf5IO_random.h>
#include <simpleMC/hdf5/hdf5IO_eigen.h>
#include <simpleMC/hdf5/hdf5IO_walker_alias.h>

using namespace simpleMC::hdf5;

template <typename T>
void write_check(hdf5IO& hio, const std::string& path, const T& t) {
    hio[path] << t;
}

template <typename T>
void write_read_check(hdf5IO& hio, const std::string& path, const T& t) {
    T copy;
    hio[path] << t;
    hio[path] >> copy;
    ASSERT_EQ(t, copy);
}

template <typename T>
void write_read_check(hdf5IO& hio, const std::string& path, const T& t, T& copy) {
    hio[path] << t;
    hio[path] >> copy;
    ASSERT_EQ(t, copy);
}

namespace mytype_space {

using namespace Eigen;
using namespace simpleMC::utils;

bool compare_tensors(const TensorFixedSize<double, Sizes<2, 2>>& lhs,
                     const TensorFixedSize<double, Sizes<2, 2>>& rhs) {
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 2; i++) {
            if (lhs(i,j) != rhs(i,j))
                return false;
        }
    }
    return true;
}

struct mytype {
    mytype() : vg_(xoshiro256pp{}, uniform_real_distribution{}), ten_() {
        ten_.setZero();
    }

    friend bool operator==(const mytype& lhs, const mytype& rhs) {
        bool result = lhs.vg_.engine() == rhs.vg_.engine() &&
                lhs.vg_.distribution() == rhs.vg_.distribution() &&
                compare_tensors(lhs.ten_, rhs.ten_);
        return result;
    }

    friend bool operator!=(const mytype& lhs, const mytype& rhs) {
        return !(lhs == rhs);
    }

    variate_generator<xoshiro256pp, uniform_real_distribution> vg_;
    TensorFixedSize<double, Sizes<2, 2>> ten_;
};

void to_hdf5(hdf5IO& hio, const mytype& f) {
    hio["vg"] << f.vg_;
    hio["tensor"] << f.ten_;
}

void from_hdf5(const hdf5IO& hio, mytype& f) {
    hio["vg"] >> f.vg_;
    hio["tensor"] >> f.ten_;
}

}

TEST(HDF5IO, LinkingAndCompilation) {
    using namespace HighFive;
    const std::string filename = "compile_and_link.hdf5";
    File file(filename, File::ReadWrite | File::Create | File::Truncate);
    file.createDataSet("int", 100);
}

TEST(HDF5IO, WriteAndReadScalars) {
    const std::string filename = "scalars.hdf5";
    hdf5IO hio(filename);
    write_read_check<char>(hio, "char", 'c');
    write_read_check<signed char>(hio, "signed_char", 'u');
    write_read_check<unsigned char>(hio, "unsigned_char", 'u');
    write_read_check<short>(hio, "short", -1234);
    write_read_check<unsigned short>(hio, "unsigned_short", 1234);
    write_read_check<int>(hio, "int", -123456);
    write_read_check<unsigned int>(hio, "unsigned_int", 123456);
    write_read_check<long>(hio, "long", -123456789);
    write_read_check<unsigned long>(hio, "unsigned_long", 123456789);
    write_read_check<long long>(hio, "long_long", -12345678912);
    write_read_check<unsigned long long>(hio, "unsigned_long_long", 12345678912);
    write_read_check<float>(hio, "float", -12.34567);
    write_read_check<double>(hio, "double", 12.34567);
    write_read_check<bool>(hio, "bool", true);
    write_read_check<std::string>(hio, "string", "test string");
}

TEST(HDF5IO, WriteAndReadVectors) {
    const std::string filename = "vectors.hdf5";
    hdf5IO hio(filename);
    write_read_check<std::vector<char>>(hio, "char", {'c', 'a'});
    write_read_check<std::vector<signed char>>(hio, "signed_char", {'u', 'a'});
    write_read_check<std::vector<unsigned char>>(hio, "unsigned_char", {'u'});
    write_read_check<std::vector<short>>(hio, "short", {-1234, 9183, 821});
    write_read_check<std::vector<unsigned short>>(hio, "unsigned_short", {1234, 87, 8123});
    write_read_check<std::vector<int>>(hio, "int", {1,-2,3,4,-5,6});
    write_read_check<std::vector<unsigned int>>(hio, "unsigned_int", {0,291,2});
    write_read_check<std::vector<long>>(hio, "long", {9, 1823, 948183, -918238});
    write_read_check<std::vector<unsigned long>>(hio, "unsigned_long", {9, 1, 3243, 12388213});
    write_read_check<std::vector<long long>>(hio, "long_long", {-921323, 838412, 1, -123921});
    write_read_check<std::vector<unsigned long long>>(hio, "unsigned_long_long", {18, 1923});
    write_read_check<std::vector<float>>(hio, "float", {0.123, -0.12, 921.1});
    write_read_check<std::vector<double>>(hio, "double", {0.1, 0.01, 0.0001, 1.2e-7});
    write_read_check<std::vector<std::string>>(hio, "string", {"test", "this", "strings"});
}

TEST(HDF5IO, WriteAndReadArrays) {
    const std::string filename = "arrays.hdf5";
    hdf5IO hio(filename);
    write_read_check<std::array<char, 2>>(hio, "char", {'c', 'a'});
    write_read_check<std::array<signed char, 2>>(hio, "signed_char", {'u', 'a'});
    write_read_check<std::array<unsigned char, 1>>(hio, "unsigned_char", {'u'});
    write_read_check<std::array<short, 3>>(hio, "short", {-1234, 9183, 821});
    write_read_check<std::array<unsigned short, 3>>(hio, "unsigned_short", {1234, 87, 8123});
    write_read_check<std::array<int, 6>>(hio, "int", {1,-2,3,4,-5,6});
    write_read_check<std::array<unsigned int, 3>>(hio, "unsigned_int", {0, 291, 2});
    write_read_check<std::array<long, 4>>(hio, "long", {9, 1823, 948183, -918238});
    write_read_check<std::array<unsigned long, 4>>(hio, "unsigned_long", {9, 1, 3243, 12388213});
    write_read_check<std::array<long long, 4>>(hio, "long_long", {-921323, 83841, -123912, 21});
    write_read_check<std::array<unsigned long long, 2>>(hio, "unsigned_long_long", {18, 1923});
    write_read_check<std::array<float, 3>>(hio, "float", {0.123, -0.12, 921.1});
    write_read_check<std::array<double, 4>>(hio, "double", {0.1, 0.01, 0.0001, 1.2e-7});
    write_read_check<std::array<bool, 2>>(hio, "bool", {true, false});
    write_read_check<std::array<std::string, 3>>(hio, "string", {"test", "this", "strings"});
}

TEST(HDF5IO, WriteAndReadMyType) {
    const std::string filename = "mytype.hdf5";
    hdf5IO hio(filename);
    mytype_space::mytype mt, copy;
    mt.vg_.engine().jump();
    mt.ten_.setRandom();
    hio["mt"] << mt;
    hio["copy"] << copy;
    hio["mt"] >> copy;
    hio["copy_after_read"] << copy;
    ASSERT_EQ(mt, copy);
}

TEST(HDF5IO, WriteAndReadComplex) {
    using c_t = std::complex<double>;
    const std::string filename = "complex.hdf5";
    hdf5IO hio(filename);
    write_read_check<c_t>(hio, "complex", {-1.9123, 0.82312});
    std::vector<c_t> z_vec{{1.123, 2.234}, {0.0987, 9.9876}, {0, 0}};
    write_read_check<std::vector<c_t>>(hio, "complex_vec", z_vec);
    std::array<c_t, 3> z_arr;
    std::copy(z_vec.begin(), z_vec.end(), z_arr.begin());
    write_read_check<std::array<c_t, 3>>(hio, "complex_arr", z_arr);
}

TEST(HDF5IO, WriteAndReadEigen) {
    using namespace Eigen;
    const std::string filename = "eigen.hdf5";
    hdf5IO hio(filename);
    Vector2d col1{1.9123781238, 2.13289473}, col1_copy;
    MatrixXd randmat(17, 23), randmat_copy(17, 23);
    randmat.setRandom(17, 23);
    Matrix<double, 2, 2, Eigen::RowMajor> rowmajmat, rowmajmat_copy;
    Matrix<double, 2, 2> colmajmat;
    rowmajmat.setRandom();
    write_read_check(hio, "col1", col1, col1_copy);
    write_read_check(hio, "group/randmat", randmat, randmat_copy);
    write_read_check(hio, "rowmajmat", rowmajmat, rowmajmat_copy);
    hio["rowmajmat"] >> colmajmat;
    ASSERT_EQ(rowmajmat, colmajmat.transpose());
}

TEST(HDF5IO, WriteAndReadGrids) {
    using namespace simpleMC::utils;
    const std::string filename = "grids.hdf5";
    hdf5IO hio(filename);
    linear_grid lg(0.0, 2.0, 33), lg_copy;
    power_grid pg(13.2, -4.123, 1000, 4), pg_copy;
    index_grid ig(19), ig_copy;
    nd_grid<std::uint16_t, linear_grid, power_grid> ndg(lg, pg), ndg_copy;
    hio["lg"] << lg;
    hio["lg"] >> lg_copy;
    hio["pg"] << pg;
    hio["pg"] >> pg_copy;
    hio["ig"] << ig;
    hio["ig"] >> ig_copy;
    hio["ndg"] << ndg;
    hio["ndg_copy"] << ndg_copy;
    hio["ndg"] >> ndg_copy;
    hio["ndg_copy_after_reading"] << ndg_copy;
    ASSERT_EQ(lg.start(), lg_copy.start());
    ASSERT_EQ(lg.stop(), lg_copy.stop());
    ASSERT_EQ(lg.numpoints(), lg_copy.numpoints());
    ASSERT_EQ(pg.start(), pg_copy.start());
    ASSERT_EQ(pg.stop(), pg_copy.stop());
    ASSERT_EQ(pg.numpoints(), pg_copy.numpoints());
    ASSERT_EQ(pg.power(), pg_copy.power());
    ASSERT_EQ(ig.numpoints(), ig_copy.numpoints());
    ASSERT_EQ(ndg.get<0>().start(), ndg_copy.get<0>().start());
    ASSERT_EQ(ndg.get<0>().stop(), ndg_copy.get<0>().stop());
    ASSERT_EQ(ndg.get<0>().numpoints(), ndg_copy.get<0>().numpoints());
    ASSERT_EQ(ndg.get<1>().start(), ndg_copy.get<1>().start());
    ASSERT_EQ(ndg.get<1>().stop(), ndg_copy.get<1>().stop());
    ASSERT_EQ(ndg.get<1>().numpoints(), ndg_copy.get<1>().numpoints());
}

TEST(HDF5IO, ReadAndWriteWalkerAlias) {
    using namespace simpleMC::utils;
    walker_alias wa, wa_copy;
    std::vector<double> weights{1.0, 2.0, 3.0};
    wa.initialize(weights);
    const std::string filename = "walias.hdf5";
    hdf5IO hio(filename);
    hio["walias"] << wa;
    hio["walias"] >> wa_copy;
    ASSERT_EQ(wa.weights(), wa_copy.weights());
    ASSERT_EQ(wa.probabilities(),wa_copy.probabilities());
    ASSERT_EQ(wa.alias(), wa_copy.alias());
}
