/**
 * @file    json_test.cpp
 * @brief   Unit tests for Json IO support.
 * @author  Thomas Hahn
 */

#include <gtest/gtest.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_eigen.h>
#include <simpleMC/json/jsonIO_random.h>
#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/json/jsonIO_walker_alias.h>

using namespace simpleMC::json;

template <typename T>
void write_read_check(jsonIO& jio, const std::string& path, const T& t) {
    T copy;
    jio[path] << t;
    jio[path] >> copy;
    ASSERT_EQ(t, copy);
}

template <typename T>
void write_read_check(jsonIO& jio, const std::string& path, const T& t, T& copy) {
    jio[path] << t;
    jio[path] >> copy;
    ASSERT_EQ(t, copy);
}

namespace mytype_space {

using nlohmann::json;
using namespace Eigen;
using namespace simpleMC::utils;

bool compare_tensors(const TensorFixedSize<double, Sizes<2, 2>>& lhs,
                     const TensorFixedSize<double, Sizes<2, 2>>& rhs) {
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 2; i++) {
            if (lhs(i,j) != rhs(i,j))
                return false;
        }
    }
    return true;
}

struct mytype {
    mytype() : vg_(xoshiro256pp{}, uniform_real_distribution{}), ten_() {
        ten_.setZero();
    }

    friend bool operator==(const mytype& lhs, const mytype& rhs) {
        bool result = lhs.vg_.engine() == rhs.vg_.engine() &&
                lhs.vg_.distribution() == rhs.vg_.distribution() &&
                compare_tensors(lhs.ten_, rhs.ten_);
        return result;
    }

    friend bool operator!=(const mytype& lhs, const mytype& rhs) {
        return !(lhs == rhs);
    }

    variate_generator<xoshiro256pp, uniform_real_distribution> vg_;
    TensorFixedSize<double, Sizes<2, 2>> ten_;
};

void to_json(json& j, const mytype& f) {
    j["vg"] = f.vg_;
    j["tensor"] = f.ten_;
}

void from_json(const json& j, mytype& f) {
    j.at("vg").get_to(f.vg_);
    j.at("tensor").get_to(f.ten_);
}

} // namespace mytype_space

void test_conversion_const(const nlohmann::json& j) {
    std::cout << j << std::endl;
}

void test_conversion(nlohmann::json& j) {
    j["added"] = "this string";
}

TEST(JsonIO, LinkingAndCompilation) {
    using json = nlohmann::json;
    json j;
    int to_write = 10;
    int to_read;
    j["int"] = to_write;
    to_read = j["int"];
    ASSERT_EQ(to_write, to_read);
}

TEST(JsonIO, StreamOperators) {
    jsonIO jio;
    jio["int"] << 12341;
    jio["0"] << 100;
    jio["1"] << 1;
    int int0;
    jio["0"] >> int0;
    std::cout << std::setw(4) << jio.get() << std::endl;
    std::cout << int0 << std::endl;
}

TEST(JsonIO, WriteAndReadScalars) {
    const std::string filename = "scalar.json";
    jsonIO jio;
    write_read_check<char>(jio, "char", 'c');
    write_read_check<signed char>(jio, "signed_char", 'u');
    write_read_check<unsigned char>(jio, "unsigned_char", 'u');
    write_read_check<short>(jio, "short", -1234);
    write_read_check<unsigned short>(jio, "unsigned_short", 1234);
    write_read_check<int>(jio, "int", -123456);
    write_read_check<unsigned int>(jio, "unsigned_int", 123456);
    write_read_check<long>(jio, "long", -123456789);
    write_read_check<unsigned long>(jio, "unsigned_long", 123456789);
    write_read_check<long long>(jio, "long_long", -12345678912);
    write_read_check<unsigned long long>(jio, "unsigned_long_long", 12345678912);
    write_read_check<float>(jio, "float", -12.34567);
    write_read_check<double>(jio, "double", 12.34567);
    write_read_check<bool>(jio, "bool", true);
    write_read_check<std::string>(jio, "string", "test string");
    jio.dump_to_file(filename);
    jsonIO copy;
    copy.load_from_file(filename);
    ASSERT_EQ(jio.get(), copy.get());
}

TEST(JsonIO, WriteAndReadVectors) {
    const std::string filename = "vectors.json";
    jsonIO jio;
    write_read_check<std::vector<char>>(jio, "char", {'c', 'a'});
    write_read_check<std::vector<signed char>>(jio, "signed_char", {'u', 'a'});
    write_read_check<std::vector<unsigned char>>(jio, "unsigned_char", {'u'});
    write_read_check<std::vector<short>>(jio, "short", {-1234, 9183, 821});
    write_read_check<std::vector<unsigned short>>(jio, "unsigned_short", {1234, 87, 8123});
    write_read_check<std::vector<int>>(jio, "int", {1,-2,3,4,-5,6});
    write_read_check<std::vector<unsigned int>>(jio, "unsigned_int", {0,291,2});
    write_read_check<std::vector<long>>(jio, "long", {9, 1823, 948183, -918238});
    write_read_check<std::vector<unsigned long>>(jio, "unsigned_long", {9, 1, 3243, 12388213});
    write_read_check<std::vector<long long>>(jio, "long_long", {-921323, 838412, 1, -123921});
    write_read_check<std::vector<unsigned long long>>(jio, "unsigned_long_long", {18, 1923});
    write_read_check<std::vector<float>>(jio, "float", {0.123, -0.12, 921.1});
    write_read_check<std::vector<double>>(jio, "double", {0.1, 0.01, 0.0001, 1.2e-7});
    write_read_check<std::vector<std::string>>(jio, "string", {"test", "this", "strings"});
    jio.dump_to_file(filename);
    jsonIO copy;
    copy.load_from_file(filename);
    ASSERT_EQ(jio.get(), copy.get());
}

TEST(JsonIO, WriteAndReadArrays) {
    const std::string filename = "array.json";
    jsonIO jio;
    write_read_check<std::array<char, 2>>(jio, "char", {'c', 'a'});
    write_read_check<std::array<signed char, 2>>(jio, "signed_char", {'u', 'a'});
    write_read_check<std::array<unsigned char, 1>>(jio, "unsigned_char", {'u'});
    write_read_check<std::array<short, 3>>(jio, "short", {-1234, 9183, 821});
    write_read_check<std::array<unsigned short, 3>>(jio, "unsigned_short", {1234, 87, 8123});
    write_read_check<std::array<int, 6>>(jio, "int", {1,-2,3,4,-5,6});
    write_read_check<std::array<unsigned int, 3>>(jio, "unsigned_int", {0, 291, 2});
    write_read_check<std::array<long, 4>>(jio, "long", {9, 1823, 948183, -918238});
    write_read_check<std::array<unsigned long, 4>>(jio, "unsigned_long", {9, 1, 3243, 12388213});
    write_read_check<std::array<long long, 4>>(jio, "long_long", {-921323, 83841, -123912, 21});
    write_read_check<std::array<unsigned long long, 2>>(jio, "unsigned_long_long", {18, 1923});
    write_read_check<std::array<float, 3>>(jio, "float", {0.123, -0.12, 921.1});
    write_read_check<std::array<double, 4>>(jio, "double", {0.1, 0.01, 0.0001, 1.2e-7});
    write_read_check<std::array<bool, 2>>(jio, "bool", {true, false});
    write_read_check<std::array<std::string, 3>>(jio, "string", {"test", "this", "strings"});
    jio.dump_to_file(filename);
    jsonIO copy;
    copy.load_from_file(filename);
    ASSERT_EQ(jio.get(), copy.get());
}

TEST(JsonIO, WriteAndReadComplex) {
    using c_t = std::complex<double>;
    const std::string filename = "complex.json";
    jsonIO jio;
    write_read_check<c_t>(jio, "complex", {-1.9123, 0.82312});
    std::vector<c_t> z_vec{{1.123, 2.234}, {0.0987, 9.9876}, {0, 0}};
    write_read_check<std::vector<c_t>>(jio, "complex_vec", z_vec);
    std::array<c_t, 3> z_arr;
    std::copy(z_vec.begin(), z_vec.end(), z_arr.begin());
    write_read_check<std::array<c_t, 3>>(jio, "complex_arr", z_arr);
    jio.dump_to_file(filename);
}

TEST(JsonIO, WriteAndReadMyType) {
    const std::string filename = "mytype.json";
    jsonIO jio, jio_copy;
    mytype_space::mytype mt, mt_copy;
    mt.vg_.engine().jump();
    mt.ten_.setRandom();
    jio["mt"] << mt;
    jio["copy"] << mt_copy;
    jio.dump_to_file(filename);
    jio_copy.load_from_file(filename);
    jio_copy["mt"] >> mt_copy;
    jio_copy["copy_after_read"] << mt_copy;
    jio_copy.dump_to_file(filename);
    ASSERT_EQ(mt, mt_copy);
}

TEST(JsonIO, WriteAndReadEigen) {
    using namespace Eigen;
    const std::string filename = "eigen.json";
    jsonIO jio;
    Vector2d col1{1.9123781238, 2.13289473}, col1_copy;
    MatrixXd randmat(13, 3), randmat_copy(13, 3);
    Matrix<double, 2, 2, Eigen::RowMajor> rowmajmat, rowmajmat_copy;
    Matrix<double, 2, 2> colmajmat;
    randmat.setRandom(13, 3);
    rowmajmat.setRandom();
    write_read_check(jio, "col1", col1, col1_copy);
    write_read_check(jio, "group/randmat", randmat, randmat_copy);
    write_read_check(jio, "rowmajmat", rowmajmat, rowmajmat_copy);
    jio["rowmajmat"] >> colmajmat;
    ASSERT_EQ(rowmajmat, colmajmat.transpose());
    jio.dump_to_file(filename);
}

TEST(JsonIO, WriteAndReadGrids) {
    using namespace simpleMC::utils;
    const std::string filename = "grids.json";
    jsonIO jio;
    linear_grid lg(0.0, 2.0, 33), lg_copy;
    power_grid pg(13.2, -4.123, 1000, 4), pg_copy;
    index_grid ig(19), ig_copy;
    nd_grid<int, linear_grid, power_grid> ndg(lg, pg), ndg_copy;
    jio["lg"] << lg;
    jio["lg"] >> lg_copy;
    jio["pg"] << pg;
    jio["pg"] >> pg_copy;
    jio["ig"] << ig;
    jio["ig"] >> ig_copy;
    jio["ndg"] << ndg;
    jio["ndg_copy"] << ndg_copy;
    jio["ndg"] >> ndg_copy;
    jio["ndg_copy_after_reading"] << ndg_copy;
    jio.dump_to_file(filename);
    ASSERT_EQ(lg.start(), lg_copy.start());
    ASSERT_EQ(lg.stop(), lg_copy.stop());
    ASSERT_EQ(lg.numpoints(), lg_copy.numpoints());
    ASSERT_EQ(pg.start(), pg_copy.start());
    ASSERT_EQ(pg.stop(), pg_copy.stop());
    ASSERT_EQ(pg.numpoints(), pg_copy.numpoints());
    ASSERT_EQ(pg.power(), pg_copy.power());
    ASSERT_EQ(ig.numpoints(), ig_copy.numpoints());
    ASSERT_EQ(ndg.get<0>().start(), ndg_copy.get<0>().start());
    ASSERT_EQ(ndg.get<0>().stop(), ndg_copy.get<0>().stop());
    ASSERT_EQ(ndg.get<0>().numpoints(), ndg_copy.get<0>().numpoints());
    ASSERT_EQ(ndg.get<1>().start(), ndg_copy.get<1>().start());
    ASSERT_EQ(ndg.get<1>().stop(), ndg_copy.get<1>().stop());
    ASSERT_EQ(ndg.get<1>().numpoints(), ndg_copy.get<1>().numpoints());
}

TEST(JsonIO, ReadAndWriteToFiles) {
    using namespace Eigen;
    const std::string filename = "fileio.";
    jsonIO jio;
    MatrixXd mat(7, 11);
    mat.setRandom();
    jio["mat"] << mat;
    jio.dump_to_file(filename + "json");
    jio.dump_to_file(filename + "bson", std::ios_base::binary, jsonIO_mode::bson);
    jio.dump_to_file(filename + "ubjson", std::ios_base::binary, jsonIO_mode::ubjson);
    jio.dump_to_file(filename + "cbor", std::ios_base::binary, jsonIO_mode::cbor);
    jio.dump_to_file(filename + "msgpack", std::ios_base::binary, jsonIO_mode::msgpack);

    jsonIO jio_text;
    MatrixXd copy_text(7, 11);
    jio_text.load_from_file(filename + "json");
    jio_text["mat"] >> copy_text;
    ASSERT_EQ(mat, copy_text);

    jsonIO jio_bson;
    MatrixXd copy_bson(7, 11);
    jio_bson.load_from_file(filename + "bson", std::ios_base::binary, jsonIO_mode::bson);
    jio_bson["mat"] >> copy_bson;
    ASSERT_EQ(mat, copy_bson);

    jsonIO jio_ubjson;
    MatrixXd copy_ubjson(7, 11);
    jio_ubjson.load_from_file(filename + "ubjson", std::ios_base::binary, jsonIO_mode::ubjson);
    jio_ubjson["mat"] >> copy_ubjson;
    ASSERT_EQ(mat, copy_ubjson);

    jsonIO jio_cbor;
    MatrixXd copy_cbor(7, 11);
    jio_cbor.load_from_file(filename + "cbor", std::ios_base::binary, jsonIO_mode::cbor);
    jio_cbor["mat"] >> copy_cbor;
    ASSERT_EQ(mat, copy_cbor);

    jsonIO jio_msgpack;
    MatrixXd copy_msgpack(7, 11);
    jio_msgpack.load_from_file(filename + "msgpack", std::ios_base::binary, jsonIO_mode::msgpack);
    jio_msgpack["mat"] >> copy_msgpack;
    ASSERT_EQ(mat, copy_msgpack);
}

TEST(JsonIO, GetCurrentJsonObject) {
    jsonIO jio;
    auto& ref = jio.get();
    jio["int"] << 4;
    jio["double"] << 0.233e-16;
    jio["string"] << "test string";
    ASSERT_EQ(ref, jio.get());
    ASSERT_EQ(ref, jio[""].current_json());
    ASSERT_EQ(ref["int"], jio["int"].current_json());
}

TEST(JsonIO, ConversionToNlohmann) {
    jsonIO jio;
    jio["int"] << 10;
    jio["string"] << "hallo";
    jio["group/double"] << 1.12345;
    test_conversion_const(jio.get());
    test_conversion_const(jio);
    test_conversion_const(jio["group"]);
    test_conversion(jio);
    std::cout << jio.get() << std::endl;
    test_conversion(jio["group"]);
    std::cout << jio.get() << std::endl;
}

TEST(JsonIO, ReadAndWriteWalkerAlias) {
    using namespace simpleMC::utils;
    walker_alias wa, wa_copy;
    std::vector<double> weights{1.0, 2.0, 3.0};
    wa.initialize(weights);
    jsonIO jio;
    jio["walias"] << wa;
    jio["walias"] >> wa_copy;
    ASSERT_EQ(wa.weights(), wa_copy.weights());
    ASSERT_EQ(wa.probabilities(),wa_copy.probabilities());
    ASSERT_EQ(wa.alias(), wa_copy.alias());
}
