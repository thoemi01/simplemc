/**
 * @file    histogram_mc.h
 * @brief   MC classes for histogram test.
 * @author  Thomas Hahn
 */

#pragma once

#include <cmath>
#include <gtest/gtest.h>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/utils/cloneable.h>
#include <simpleMC/utils/grids.h>
#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/accs/mean.h>
#include <simpleMC/mc/mc_simulation.h>

using namespace simpleMC::mc;

/* Simulate Q(x) = exp(-alpha*x) */
struct configuration : public mc_configuration {
public:
    double weight(double tau_) const { return std::exp(-alpha*tau_); }
    double weight() const { return std::exp(-alpha*tau); }

    void to_json(nlohmann::json& j) const override {
        j["tau"] = tau;
        j["maxtau"] = maxtau;
        j["mintau"] = mintau;
        j["alpha"] = alpha;
    }

    void from_json(const nlohmann::json& j) override {
        j.at("tau").get_to(tau);
        j.at("maxtau").get_to(maxtau);
        j.at("mintau").get_to(mintau);
        j.at("alpha").get_to(alpha);
    }

    void read_input_json(const nlohmann::json& j) override {
        j.at("maxtau").get_to(maxtau);
        j.at("mintau").get_to(mintau);
        j.at("alpha").get_to(alpha);
    }

    void write_input_json(nlohmann::json& j) const override {
        j["maxtau"] = maxtau;
        j["mintau"] = mintau;
        j["alpha"] = alpha;
    }

public:
    double tau = 1;
    double maxtau = 5;
    double mintau = 0;
    double alpha = 1;
};

std::ostream& operator<<(std::ostream& os, const configuration& conf) {
    os << "#tau = " << conf.tau << "\n"
       << "#maxtau = " << conf.maxtau << "\n"
       << "#mintau = " << conf.mintau << "\n"
       << "#alpha = " << conf.alpha << "\n";
    return os;
}

class myupdate : public mc_update<configuration> {
public:
    using config_type = configuration;
    using gen_type = mc_traits::gen_type;

public:
    myupdate() :
        ntau_(0.0)
    {}

    myupdate* clone() const override {
        return new myupdate(*this);
    }

    double attempt(const config_type& config, gen_type& uni01) override {
        ntau_ = config.maxtau*uni01();
        return config.weight(ntau_)/config.weight();
    }

    void accept(config_type& config, gen_type& /*uni01*/) override {
        config.tau = ntau_;
    }

private:
    double ntau_;
};

class mymeasurement : public simpleMC::utils::cloneable<
        mc_measurement<configuration>, mymeasurement> {
public:
    using config_type = configuration;
    using mc_type = mc_simulation<config_type>;
    using base_type = mc_measurement<config_type>;
    using acc_type = simpleMC::accs::mean_acc<double>;
    using lg_type = simpleMC::utils::linear_grid;

public:
    mymeasurement(std::size_t bins, double min, double max) :
        hist_(bins),
        est_(bins),
        lg_(min, max, bins+1)
    {}

    void initialize(const mc_type& mc) override {
        auto start = mc.configuration().mintau;
        auto stop = mc.configuration().maxtau;
        lg_.set(start, stop, lg_.numpoints());
    }

    void measure(const config_type& config) override {
        auto idx = lg_.index(config.tau);
        hist_[idx] << 1.0;
        auto val = (lg_.value(idx) + lg_.value(idx+1))*0.5;
        est_[idx] << config.weight(val)/config.weight();
    }

    void write_accs(std::ostream& os) const {
        auto hist_mean = hist_.mean();
        auto est_mean = est_.mean();
        double norm =  std::exp(-lg_.start()) - std::exp(-lg_.stop());
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        os << "#steps = " << hist_.count() << "\n";
        fos << "#tau" << "histogram" << "est" << "exact"
            << "rel hist" << "rel est" << fos.endl();
        for (std::size_t i = 0; i < hist_.size(); i++) {
            double tau = 0.5*(lg_.value(i) + lg_.value(i+1));
            double exact = std::exp(-tau);
            double hist = hist_mean[i]/lg_.binsize(i)*norm;
            double est = est_mean[i]/lg_.binsize(i)*norm;
            double rel_hist = std::abs(hist - exact)/exact;
            double rel_est = std::abs(est - exact)/exact;
            fos << tau << hist << est << exact
                << rel_hist << rel_est << fos.endl();
        }
    }

    void check_accs(double eps) const {
        auto hist_mean = hist_.mean();
        auto est_mean = est_.mean();
        double norm =  std::exp(-lg_.start()) - std::exp(-lg_.stop());
        for (std::size_t i = 0; i < hist_.size(); i++) {
            double tau = 0.5*(lg_.value(i) + lg_.value(i+1));
            double exact = std::exp(-tau);
            double hist = hist_mean[i]/lg_.binsize(i)*norm;
            double est = est_mean[i]/lg_.binsize(i)*norm;
            double rel_hist = std::abs(hist - exact)/exact;
            double rel_est = std::abs(est - exact)/exact;
            ASSERT_LE(rel_hist, eps);
            ASSERT_LE(rel_est, eps);
        }
    }

#ifdef SIMPLEMC_HAS_MPI
    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto hist_red = hist_;
        auto est_red = est_;
        simpleMC::accs::reduce(hist_red, hist_, comm, /*all=*/true);
        simpleMC::accs::reduce(est_red, est_, comm, /*all=*/true);
    }
#endif

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["hist"] = hist_;
        j["est"] = est_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("hist").get_to(hist_);
        j.at("est").get_to(est_);
        j.at("lg").get_to(lg_);
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t bins;
        j.at("num_bins").get_to(bins);
        hist_.reset(bins);
        est_.reset(bins);
        lg_.set(lg_.start(), lg_.stop(), bins + 1);
    }

    void write_input_json(nlohmann::json& j) const override {
        j["num_bins"] = hist_.size();
    }

    bool compare(const mymeasurement& other) const {
        return hist_ == other.hist_ &&
               est_ == other.est_;
    }

private:
    acc_type hist_;
    acc_type est_;
    lg_type lg_;
};
