/**
 * @file    simple_histogram_mc.h
 * @brief   MC classes for simple histogram test.
 * @author  Thomas Hahn
 */

#pragma once

#include <cmath>
#include <gtest/gtest.h>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/utils/cloneable.h>
#include <simpleMC/utils/grids.h>
#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/accs/mean.h>
#include <simpleMC/mc/mc_simulation.h>

using namespace simpleMC::mc;

/* Simulate Q(x) = exp(-alpha*x) */
struct configuration : public mc_configuration {
public:
    double weight(double tau_) const { return std::exp(-alpha*tau_); }
    double weight() const { return std::exp(-alpha*tau); }

public:
    double tau = 1;
    double maxtau = 5;
    double mintau = 0;
    double alpha = 1;
};

class myupdate : public mc_update<configuration> {
public:
    using config_type = configuration;
    using gen_type = mc_traits::gen_type;

public:
    myupdate() :
        ntau_(0.0)
    {}

    myupdate* clone() const override {
        return new myupdate(*this);
    }

    double attempt(const config_type& config, gen_type& uni01) override {
        ntau_ = config.maxtau*uni01();
        return config.weight(ntau_)/config.weight();
    }

    void accept(config_type& config, gen_type& /*uni01*/) override {
        config.tau = ntau_;
    }

private:
    double ntau_;
};

class mymeasurement : public simpleMC::utils::cloneable<
        mc_measurement<configuration>, mymeasurement> {
public:
    using config_type = configuration;
    using mc_type = mc_simulation<config_type>;
    using base_type = mc_measurement<config_type>;
    using acc_type = simpleMC::accs::mean_acc<double>;
    using lg_type = simpleMC::utils::linear_grid;

public:
    mymeasurement(std::size_t bins, double min, double max) :
        hist_(bins),
        lg_(min, max, bins+1)
    {}

    void initialize(const mc_type& mc) override {
        auto start = mc.configuration().mintau;
        auto stop = mc.configuration().maxtau;
        lg_.set(start, stop, lg_.numpoints());
    }

    void measure(const config_type& config) override {
        auto idx = lg_.index(config.tau);
        hist_[idx] << 1.0;
    }

    void write_histogram(std::ostream& os) const {
        auto mean = hist_.mean();
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        os << "#steps = " << hist_.count() << "\n";
        fos << "#tau" << "histogram" << fos.endl();
        for (std::size_t i = 0; i < hist_.size(); i++) {
            fos << 0.5*(lg_.value(i) + lg_.value(i+1)) << mean[i] << fos.endl();
        }
    }

private:
    acc_type hist_;
    lg_type lg_;
};
