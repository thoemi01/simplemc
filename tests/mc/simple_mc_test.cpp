/**
 * @file    simple_mc_test.cpp
 * @brief   Unit tests for simpleMC.
 * @author  Thomas Hahn
 */

#include <fstream>
#include "simple_histogram_mc.h"

using namespace simpleMC;

TEST(SimpleMCSimulation, HistogramWithoutRunTest) {
    using namespace simpleMC::json;
    using simpleMC::utils::duration;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    int steps = 100000000;
    simpleMC::utils::timer timer;
    timer.start();
    for (int i = 0; i < steps; i++) {
        mc.do_step();
        mc.do_measurements();
    }
    timer.stop();
    std::cout << "Runtime: " << timer.since_start<duration::sec_d>() << std::endl;
    std::cout << mc.update_stats() << std::endl;
    const auto& me = mc.get_measurement<mymeasurement>("histogram");
    std::ofstream of("simple_mc_without_run.dat");
    me.write_histogram(of);
}

TEST(SimpleMCSimulation, HistogramWithRunTest) {
    using namespace simpleMC::json;
    using simpleMC::utils::duration;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    mc.set_run_vars(mc_simulation_stats::limit_runtime(),
                    100000000, 1, 10000000, mc_simulation_stats::limit_runtime(),
                    mc_simulation_stats::limit_steps());
    mc.run();
    std::cout << "Runtime: " << mc.simulation_stats().runtime() << std::endl;
    std::cout << mc.update_stats() << std::endl;
    const auto& me = mc.get_measurement<mymeasurement>("histogram");
    std::ofstream of("simple_mc_with_run.dat");
    me.write_histogram(of);
}
