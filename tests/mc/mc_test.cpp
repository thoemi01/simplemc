/**
 * @file    mc_test.cpp
 * @brief   Unit tests for simpleMC.
 * @author  Thomas Hahn
 */

#include <fstream>
#include "histogram_mc.h"

using namespace simpleMC::mc;

TEST(MCSimulation, WriteInputFile) {
    using namespace simpleMC::json;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    jsonIO jio;
    write_input_json(jio, mc);
    jio.dump_to_file("default_input.json", jsonIO_mode::text4);
}

TEST(MCSimulation, HistogramWithRunTest) {
    using namespace simpleMC::json;
    using simpleMC::utils::duration;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    mc.set_run_vars(mc_simulation_stats::limit_runtime(),
                    10000000, 1, 10000000, mc_simulation_stats::limit_runtime(),
                    mc_simulation_stats::limit_steps());
    mc.run();
    std::cout << "Runtime: " << mc.simulation_stats().runtime() << std::endl;
    std::cout << mc.update_stats() << std::endl;
    const auto& me = mc.get_measurement<mymeasurement>("histogram");
    std::ofstream of("mc_with_run.dat");
    me.write_accs(of);
}

TEST(MCSimulation, ReadInputAndRunSimulation) {
    using namespace simpleMC::json;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    jsonIO jio;
    jio.load_from_file("read_input_and_run_simulation_input.json");
    read_input_json(jio, mc);
    std::cout << "Simulation stats:\n" << mc.simulation_stats() << std::endl;
    std::cout << "Start warm up...\n" << std::endl;
    mc.warmup();
    std::cout << "Finished warm up...\n" << std::endl;
    std::cout << "Start simulation...\n" << std::endl;
    mc.run();
    std::cout << "Finished simulation...\n" << std::endl;
    std::cout << "Simulation stats:\n" << mc.simulation_stats() << std::endl;
    const auto& hist = mc.get_measurement<mymeasurement>("histogram");
    std::ofstream of("read_input_and_run_simulation_result.dat");
    hist.write_accs(of);
    hist.check_accs(0.05);
    jio << mc;
    jio.dump_to_file("read_input_and_run_simulation_result.json",
                     jsonIO_mode::text4);
}

TEST(MCSimulation, LoadCheckpoint) {
    using namespace simpleMC::json;
    jsonIO jio, jio_cp, jio_lo;
    mc_simulation<configuration> mc;
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    mc.set_run_vars(mc_simulation_stats::limit_runtime(),
                    2000000, 1, 2000000);
    mc.run();
    mc_simulation<configuration> mc_cp, mc_lo;
    mc_cp.add_update(myupdate(), 1.0, "change_tau");
    mc_cp.add_measurement(mymeasurement(50, 0, 5), "histogram");
    mc_cp.set_run_vars(mc_simulation_stats::limit_runtime(),
                       1000000, 1, 1000000);
    mc_cp.run();
    mc_lo.add_update(myupdate(), 1.0, "change_tau");
    mc_lo.add_measurement(mymeasurement(50, 0, 5), "histogram");
    default_load_checkpoint(mc_lo);
    mc_lo.set_run_vars(mc_simulation_stats::limit_runtime(),
                       1000000, 1, 1000000);
    mc_lo.run();
    const auto& hist = mc.get_measurement<mymeasurement>("histogram");
    const auto& hist_cp = mc_lo.get_measurement<mymeasurement>("histogram");
    ASSERT_TRUE(hist.compare(hist_cp));
}
