/**
 * @file    mpi_test.cpp
 * @brief   Unit tests for MPI support.
 * @author  Thomas Hahn
 */

#include <fstream>
#include "../gtest-mpi-listener.hpp"
#include "histogram_mc.h"
#include <simpleMC/utils/timer.h>
#include <simpleMC/mpi/mpi.h>

using namespace simpleMC::mpi;

TEST(MPIMCSimulation, ReadInputAndRunSimulation) {
    using namespace simpleMC::json;
    communicator world;
    int rank = world.rank();
    mc_simulation<configuration> mc(rank);
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    jsonIO jio;
    jio.load_from_file("read_input_and_run_simulation_input.json");
    read_input_json(jio, mc);
    mc.warmup();
    mc.run();
    auto mc_res = mc.mpi_collect_results(world);
    if (rank == 0) {
        const auto& hist_res = mc_res.get_measurement<mymeasurement>("histogram");
        std::ofstream of_res("mc_mpi.dat");
        hist_res.write_accs(of_res);
        hist_res.check_accs(0.05);
        const auto& hist = mc.get_measurement<mymeasurement>("histogram");
        std::ofstream of("mc_mpi_rank0.dat");
        hist.write_accs(of);
        hist.check_accs(0.05);
    }
}

TEST(MPIMCSimulation, LoadCheckpoint) {
    using namespace simpleMC::json;
    communicator world;
    int rank = world.rank();
    mc_simulation<configuration> mc(rank);
    mc.add_update(myupdate(), 1.0, "change_tau");
    mc.add_measurement(mymeasurement(50, 0, 5), "histogram");
    jsonIO jio;
    jio.load_from_file("read_input_and_run_simulation_input.json");
    read_input_json(jio, mc);
    default_load_checkpoint(mc);
    mc.run();
    auto mc_res = mc.mpi_collect_results(world);
    if (rank == 0) {
        const auto& hist_res = mc_res.get_measurement<mymeasurement>("histogram");
        std::ofstream of_res("mc_mpi_cp.dat");
        hist_res.write_accs(of_res);
        hist_res.check_accs(0.05);
        const auto& hist = mc.get_measurement<mymeasurement>("histogram");
        std::ofstream of("mc_mpi_rank0_cp.dat");
        hist.write_accs(of);
        hist.check_accs(0.05);
    }
}

int main(int argc, char** argv) {
    // Filter out Google Test arguments
    ::testing::InitGoogleTest(&argc, argv);

    // Initialize MPI
    //MPI_Init(&argc, &argv);
    environment env(argc, argv);

    // Add object that will finalize MPI on exit; Google Test owns this pointer
    ::testing::AddGlobalTestEnvironment(new GTestMPIListener::MPIEnvironment);

    // Get the event listener list.
    ::testing::TestEventListeners& listeners =
            ::testing::UnitTest::GetInstance()->listeners();

    // Remove default listener: the default printer and the default XML printer
    ::testing::TestEventListener *l =
            listeners.Release(listeners.default_result_printer());

    // Adds MPI listener; Google Test owns this pointer
    listeners.Append(new GTestMPIListener::MPIWrapperPrinter(l, MPI_COMM_WORLD));

    // Run tests, then clean up and exit. RUN_ALL_TESTS() returns 0 if all tests
    // pass and 1 if some test fails.
    int result = RUN_ALL_TESTS();

    return result;
}
