/**
 * @file    mpi_test.cpp
 * @brief   Unit tests for MPI support.
 * @author  Thomas Hahn
 */

#include <numeric>
#include "../gtest-mpi-listener.hpp"
#include <simpleMC/mpi/mpi.h>
#include <simpleMC/mpi/eigen.h>
#include <simpleMC/mpi/json.h>


using namespace simpleMC::mpi;

template <typename T>
void broadcast_and_check(communicator& comm, const T& t, int root) {
    T value;
    if (comm.rank() == root) {
        value = t;
    }
    broadcast(value, root, comm);
    ASSERT_EQ(value, t);
}

template <typename T>
void reduce_and_check(communicator& comm, const T& in, const T& expect,
                      MPI_Op op, int root, bool all = false) {
    T res;
    reduce(in, res, op, comm, all, root);
    if (comm.rank() == root || all) {
        ASSERT_EQ(res, expect);
    } else {
        ASSERT_NE(res, expect);
    }
}

template <typename T>
void send_and_receive(communicator& comm, const T& t, int source, int dest) {
    int tag = 0;
    T value;
    if (comm.rank() == source) {
        value = t;
        send(value, dest, tag, comm);
        ASSERT_EQ(value, t);
    } else if (comm.rank() == dest) {
        receive(value, source, tag, comm);
        ASSERT_EQ(value, t);
    }
}

template <typename T>
void gather_and_check(communicator& comm, const T& in, const std::vector<T>& expect,
                      int root, bool all = false) {
    std::vector<T> res;
    if (all) {
        all_gather(in, res, comm);
    } else {
        res = gather(in, root, comm);
    }
    if (comm.rank() == root || all) {
        ASSERT_EQ(res, expect);
    } else {
        ASSERT_NE(res, expect);
    }
}

template <typename T>
bool compare_eigen_type(const T& lhs, const T& rhs) {
    if (lhs.size() != rhs.size()) {
        std::cout << "wrong sizes: " << lhs.size() << " != "
                  << rhs.size() << std::endl;
        return false;
    }
    auto plhs = lhs.data();
    auto prhs = rhs.data();
    for (int i = 0; i < lhs.size(); i++) {
        if (plhs[i] != prhs[i]) {
            std::cout << "wrong elems: " << plhs[i] << " != "
                      << prhs[i] << std::endl;
            return false;
        }
    }
    return true;
}

TEST(SimpleMPI, PrintRanks) {
    communicator world;
    if (world.rank() == 0) {
        std::cout << "There are " << world.size() << " processes.\n";
    }
    world.barrier();
    std::cout << "Rank " << world.rank() << " says Hello World!" << "\n";
}

TEST(SimpleMPI, BroadcastSingleValues) {
    communicator world;
    int root = 0;
    broadcast_and_check<char>(world, 'c', root);
    broadcast_and_check<signed char>(world, 'u', root);
    broadcast_and_check<unsigned char>(world, 'u', root);
    broadcast_and_check<short>(world, -1234, root);
    broadcast_and_check<unsigned short>(world, 1234, root);
    broadcast_and_check<int>(world, -123456, root);
    broadcast_and_check<unsigned int>(world, 123456, root);
    broadcast_and_check<long>(world, -123456789, root);
    broadcast_and_check<unsigned long>(world, 123456789, root);
    broadcast_and_check<long long>(world, -12345678912, root);
    broadcast_and_check<unsigned long long>(world, 12345678912, root);
    broadcast_and_check<float>(world, -12.34567, root);
    broadcast_and_check<double>(world, 12.34567, root);
    broadcast_and_check<bool>(world, true, root);
    broadcast_and_check<std::complex<double>>(world, {2.3, -4.98}, root);
    broadcast_and_check(world, std::string("root string"), root);
}

TEST(SimpleMPI, BroadCastVectors) {
    communicator world;
    int root = 0;
    broadcast_and_check<std::vector<char>>(world, {'c', 'a'}, root);
    broadcast_and_check<std::vector<signed char>>(world, {'u', 'a'}, root);
    broadcast_and_check<std::vector<unsigned char>>(world, {'u'}, root);
    broadcast_and_check<std::vector<short>>(world, {-1234, 9183, 821}, root);
    broadcast_and_check<std::vector<unsigned short>>(world, {1234, 87, 8123}, root);
    broadcast_and_check<std::vector<int>>(world, {1,-2,3,4,-5,6}, root);
    broadcast_and_check<std::vector<unsigned int>>(world, {0,291,2}, root);
    broadcast_and_check<std::vector<long>>(world, {9, 1823, 948183, -918238}, root);
    broadcast_and_check<std::vector<unsigned long>>(world, {9, 1, 3243, 12388213}, root);
    broadcast_and_check<std::vector<long long>>(world, {-921323, 838412, 1, -123921}, root);
    broadcast_and_check<std::vector<unsigned long long>>(world, {18, 1923}, root);
    broadcast_and_check<std::vector<float>>(world, {0.123, -0.12, 921.1}, root);
    broadcast_and_check<std::vector<double>>(world, {0.1, 0.01, 0.0001, 1.2e-7}, root);
    broadcast_and_check<std::vector<std::string>>(world, {"test", "this", "strings"}, root);
}

TEST(SimpleMPI, BroadcastEigen) {
    using namespace Eigen;
    communicator world;
    int root = 0;
    Vector2d colvec{1.9123781238, 2.13289473};
    Matrix<double, 2, 2, Eigen::RowMajor> rowmajmat;
    rowmajmat << 1, 2, 3, 4;
    broadcast_and_check(world, colvec, root);
    broadcast_and_check(world, rowmajmat, root);
    Tensor<int, 3> tensor(4, 2, 5), bench_tensor(4, 2, 5);
    bench_tensor.setConstant(3);
    if (world.rank() == root)
        tensor = bench_tensor;
    broadcast(tensor, root, world);
    ASSERT_TRUE(compare_eigen_type(bench_tensor, tensor));
}

TEST(SimpleMPI, ReduceSingleValues) {
    communicator world;
    int root = 0;
    int rank = world.rank();
    int size = world.size();
    int fac = size - 1;
    reduce_and_check<int>(world, 5, 5*size, MPI_SUM, root);
    reduce_and_check<double>(world, 1.2, 1.2*size, MPI_SUM, root);
    reduce_and_check<double>(world, 2.3*rank, 2.3*fac, MPI_MAX, root);
    reduce_and_check<int>(world, 5, 5*size, MPI_SUM, root, true);
    reduce_and_check<double>(world, 1.2, 1.2*size, MPI_SUM, root, true);
    reduce_and_check<double>(world, 2.3*rank, 2.3*fac, MPI_MAX, root, true);
}

TEST(SimpleMPI, ReduceVectors) {
    communicator world;
    int root = 0;
    int rank = world.rank();
    int size = world.size();
    int fac = size - 1;
    reduce_and_check<std::vector<int>>(world, {3, 5}, {3*size, 5*size}, MPI_SUM, root);
    reduce_and_check<std::vector<double>>(world, {1.2, 2.3}, {1.2*size, 2.3*size}, MPI_SUM, root);
    reduce_and_check<std::vector<double>>(world, {1.2*rank, 2.3*rank}, {1.2*fac, 2.3*fac},
                                          MPI_MAX, root);
    reduce_and_check<std::vector<int>>(world, {3, 5}, {3*size, 5*size}, MPI_SUM, root, true);
    reduce_and_check<std::vector<double>>(world, {1.2, 2.3}, {1.2*size, 2.3*size}, MPI_SUM, root,
                                          true);
    reduce_and_check<std::vector<double>>(world, {1.2*rank, 2.3*rank}, {1.2*fac, 2.3*fac},
                                          MPI_MAX, root, true);
}

TEST(SimpleMPI, ReduceEigen) {
    using namespace Eigen;
    communicator world;
    int root = 0;
    int size = world.size();
    Vector2d colvec{1.2, 2.3}, exp_colvec{1.2*size, 2.3*size};
    reduce_and_check(world, colvec, exp_colvec, MPI_SUM, root);
    Tensor<int, 3> tensor(2, 2, 2), red_tensor(2, 2, 2), bench_tensor(2, 2, 2);
    int constant = 2;
    tensor.setConstant(constant);
    bench_tensor.setConstant(constant*size);
    reduce(tensor, red_tensor, MPI_SUM, world);
    ASSERT_TRUE(compare_eigen_type(bench_tensor, red_tensor));
}

TEST(SimpleMPI, SendAndReceiveSingleValues) {
    communicator world;
    int source = 1;
    int dest = 0;
    send_and_receive<char>(world, 'c', source, dest);
    send_and_receive<signed char>(world, 'u', source, dest);
    send_and_receive<unsigned char>(world, 'u', source, dest);
    send_and_receive<short>(world, -1234, source, dest);
    send_and_receive<unsigned short>(world, 1234, source, dest);
    send_and_receive<int>(world, -123456, source, dest);
    send_and_receive<unsigned int>(world, 123456, source, dest);
    send_and_receive<long>(world, -123456789, source, dest);
    send_and_receive<unsigned long>(world, 123456789, source, dest);
    send_and_receive<long long>(world, -12345678912, source, dest);
    send_and_receive<unsigned long long>(world, 12345678912, source, dest);
    send_and_receive<float>(world, -12.34567, source, dest);
    send_and_receive<double>(world, 12.34567, source, dest);
    send_and_receive<bool>(world, true, source, dest);
    send_and_receive<std::complex<double>>(world, {2.3, -4.98}, source, dest);
    send_and_receive(world, std::string("root string"), source, dest);
}

TEST(SimpleMPI, SendAndReduceVectors) {
    communicator world;
    int source = 0;
    int dest = 1;
    send_and_receive<std::vector<char>>(world, {'c', 'a'}, source, dest);
    send_and_receive<std::vector<signed char>>(world, {'u', 'a'}, source, dest);
    send_and_receive<std::vector<unsigned char>>(world, {'u'}, source, dest);
    send_and_receive<std::vector<short>>(world, {-1234, 9183, 821}, source, dest);
    send_and_receive<std::vector<unsigned short>>(world, {1234, 87, 8123}, source, dest);
    send_and_receive<std::vector<int>>(world, {1,-2,3,4,-5,6}, source, dest);
    send_and_receive<std::vector<unsigned int>>(world, {0,291,2}, source, dest);
    send_and_receive<std::vector<long>>(world, {9, 1823, 948183, -918238}, source, dest);
    send_and_receive<std::vector<unsigned long>>(world, {9, 1, 3243, 12388213}, source, dest);
    send_and_receive<std::vector<long long>>(world, {-921323, 838412, 1, -123921}, source, dest);
    send_and_receive<std::vector<unsigned long long>>(world, {18, 1923}, source, dest);
    send_and_receive<std::vector<float>>(world, {0.123, -0.12, 921.1}, source, dest);
    send_and_receive<std::vector<double>>(world, {0.1, 0.01, 0.0001, 1.2e-7}, source, dest);
    send_and_receive<std::vector<std::string>>(world, {"test", "this", "strings"}, source, dest);
}

TEST(SimpleMPI, SendAndReceiveEigen) {
    using namespace Eigen;
    communicator world;
    int source = 0;
    int dest = 1;
    int tag = 0;
    Vector2d colvec{1.2, 2.3};
    send_and_receive(world, colvec, source, dest);
    Tensor<int, 3> tensor(2, 2, 2), bench_tensor(2, 2, 2);
    int constant = 2;
    bench_tensor.setConstant(constant);
    if (world.rank() == source) {
        tensor.setConstant(constant);
        send(tensor, dest, tag, world);
        ASSERT_TRUE(compare_eigen_type(bench_tensor, tensor));
    } else if (world.rank() == dest) {
        receive(tensor, source, tag, world);
        ASSERT_TRUE(compare_eigen_type(bench_tensor, tensor));
    }
}

TEST(SimpleMPI, GatherSingleValues) {
    communicator world;
    int root = 0;
    int size = world.size();
    char char_val = 'a';
    int int_val = 192398;
    double double_val = 1.72381236e-8;
    std::string str_val = "test string";
    gather_and_check<char>(world, char_val, std::vector<char>(size, char_val), root);
    gather_and_check<int>(world, int_val, std::vector<int>(size, int_val), root);
    gather_and_check<double>(world, double_val, std::vector<double>(size, double_val),
                             root);
    gather_and_check<std::string>(world, str_val, std::vector<std::string>(size, str_val),
                                  root);
    gather_and_check<char>(world, char_val, std::vector<char>(size, char_val), root, true);
    gather_and_check<int>(world, int_val, std::vector<int>(size, int_val), root, true);
    gather_and_check<double>(world, double_val, std::vector<double>(size, double_val), root,
                             true);
    gather_and_check<std::string>(world, str_val, std::vector<std::string>(size, str_val),
                                  root, true);
}

TEST(SimpleMPI, GatherVectors) {
    communicator world;
    int size = world.size();
    std::vector<float> vec_float{1.123, 3.5, 4.6};
    std::vector<float> exp_float, res_float;
    for (int i = 0; i < size; i++) {
        exp_float.insert(exp_float.end(), vec_float.begin(), vec_float.end());
    }
    all_gather(vec_float, res_float, world);
    ASSERT_EQ(exp_float, res_float);
    std::vector<std::string> vec_str{"hallo", "du", "specht"};
    std::vector<std::string> exp_str, res_str;
    for (int i = 0; i < size; i++) {
        exp_str.insert(exp_str.end(), vec_str.begin(), vec_str.end());
    }
    all_gather(vec_str, res_str, world);
    ASSERT_EQ(exp_str, res_str);
}

TEST(SimpleMPI, GatherEigen) {
    using namespace Eigen;
    communicator world;
    int size = world.size();
    int rank = world.rank();
    Vector2d col{1.2, 2.3};
    std::vector<Vector2d> exp_col, res_col;
    for (int i = 0; i < size; i++) {
        exp_col.push_back(col);
    }
    all_gather(col, res_col, world);
    ASSERT_EQ(exp_col, res_col);
    Tensor<int, 3> tensor(2, 2, 2);
    tensor.setConstant(rank);
    std::vector<Tensor<int, 3>> exp_tensor, res_tensor;
    for (int i = 0; i < size; i++) {
        exp_tensor.push_back(tensor.constant(i));
    }
    all_gather(tensor, res_tensor, world);
    for (int i = 0; i < size; i++) {
        ASSERT_TRUE(compare_eigen_type(exp_tensor[i], res_tensor[i]));
    }
}

TEST(SimpleMPI, ScatterVectors) {
    communicator world;
    int size = world.size();
    int rank = world.rank();
    int root = 0;
    std::vector<int> scat_vec(3*size), scat_res;
    std::iota(scat_vec.begin(), scat_vec.end(), 0);
    scatter(scat_vec, scat_res, root, world);
    std::vector<int> expected{rank*3+0, rank*3+1, rank*3+2};
    ASSERT_EQ(scat_res, expected);
    scat_vec.pop_back();
    scatter(scat_vec, scat_res, root, world);
    if (rank < size - 1) {
        ASSERT_EQ(scat_res, expected);
    } else {
        expected[2] = 0;
        ASSERT_EQ(scat_res, expected);
    }
    std::vector<std::string> str_vec(3*size), str_res;
    for (std::size_t i = 0; i < str_vec.size(); i++) {
        str_vec[i] = std::to_string(i);
    }
    scatter(str_vec, str_res, root, world);
    std::vector<std::string> str_exp(str_vec.begin() + rank*3,
                                     str_vec.begin() + (rank + 1)*3);
    ASSERT_EQ(str_res, str_exp);
    str_vec.pop_back();
    scatter(str_vec, str_res, root, world);
    if (rank < size - 1) {
        ASSERT_EQ(str_res, str_exp);
    } else {
        str_exp[2] = "";
        ASSERT_EQ(str_res, str_exp);
    }
}

TEST(SimpleMPI, GatherAndScatterJsonIO) {
    using namespace simpleMC::json;
    const std::string filename = "gather_scatter.json";
    communicator world;
    int rank = world.rank();
    jsonIO jio;
    jio["int"] << rank;
    jio["string"] << std::to_string(rank);
    gather_write_jsonIO(jio, filename, jsonIO_mode::text, world, 1);
    jsonIO jio_copy;
    read_scatter_jsonIO(jio_copy, filename, jsonIO_mode::text, world, 1);
    ASSERT_EQ(jio.get(), jio_copy.get());
    const std::string filename_arr = "gather_scatter_arr.cbor";
    gather_write_jsonIO(jio, filename_arr, jsonIO_mode::cbor, world, 1, "");
    read_scatter_jsonIO(jio_copy, filename_arr, jsonIO_mode::cbor, world, 1, "");
    ASSERT_EQ(jio.get(), jio_copy.get());
}

int main(int argc, char** argv) {
    // Filter out Google Test arguments
    ::testing::InitGoogleTest(&argc, argv);

    // Initialize MPI
    //MPI_Init(&argc, &argv);
    environment env(argc, argv);

    // Add object that will finalize MPI on exit; Google Test owns this pointer
    ::testing::AddGlobalTestEnvironment(new GTestMPIListener::MPIEnvironment);

    // Get the event listener list.
    ::testing::TestEventListeners& listeners =
            ::testing::UnitTest::GetInstance()->listeners();

    // Remove default listener: the default printer and the default XML printer
    ::testing::TestEventListener *l =
            listeners.Release(listeners.default_result_printer());

    // Adds MPI listener; Google Test owns this pointer
    listeners.Append(new GTestMPIListener::MPIWrapperPrinter(l, MPI_COMM_WORLD));

    // Run tests, then clean up and exit. RUN_ALL_TESTS() returns 0 if all tests
    // pass and 1 if some test fails.
    int result = RUN_ALL_TESTS();

    return result;
}
