/**
 * @file    math_test.cpp
 * @brief   Unit tests for math utils.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <gtest/gtest.h>
#include <simpleMC/utils/math.h>

using namespace simpleMC::utils::math_functions;
using namespace simpleMC::utils;

TEST(MathUtils, InfNanArithmeticTypes) {
    ASSERT_FALSE(inf_nan(0.0));
    ASSERT_FALSE(inf_nan(-1.23898123));
    ASSERT_TRUE(inf_nan(1.0/0.0));
    ASSERT_TRUE(inf_nan(std::log(-1.0)));
}

TEST(MathUtils, InfNanComplexTypes) {
    ASSERT_FALSE(inf_nan(std::complex<double>{ 98185, -1382.123}));
    ASSERT_TRUE(inf_nan(std::complex<double>{ 1.0, 1.0/0.0 }));
}

TEST(MathUtils, InfNanEigenTypes) {
    vector<1> v1{ 1.0 };
    vector<2> v2{ 1.0, 2.0 };
    vector<3> v3{ 1.0, 2.0, 3.0 };
    ASSERT_FALSE(inf_nan(v2));
    ASSERT_FALSE(inf_nan(v3));
    v2[0] /= 0.0;
    v3[2] = std::log(-1.0);
    ASSERT_TRUE(inf_nan(v2));
    ASSERT_TRUE(inf_nan(v3));
}

TEST(MathUtils, AbsoluteDifferences) {
    std::complex<double> z1{ 1.0, 1.0 };
    std::complex<double> z2{ -1.0, 1.0 };
    std::complex<double> z3{ -1.0, -1.0 };
    vector<3> v1{ 1.0, 1.0, 1.0 };
    vector<3> v2{ -1.0, -1.0, -1.0 };
    ASSERT_DOUBLE_EQ(absolute_diff(-1.0, 1.2), 2.2);
    ASSERT_DOUBLE_EQ(absolute_diff(0.0, 0.0), 0.0);
    ASSERT_DOUBLE_EQ(absolute_diff(z1, z2), 2.0);
    ASSERT_DOUBLE_EQ(absolute_diff(z1, z3), 2*std::sqrt(2));
    ASSERT_DOUBLE_EQ(absolute_diff(v1, v2), 2*std::sqrt(3));
}

TEST(MathUtils, RelativeDifferences) {
    std::complex<double> z1{ 0.0, 0.0 };
    vector<2> v1{ 0.0, 0.0 };
    vector<2> v2{ 1e-5, 1e-5 };
    vector<2> v3{ 1e-5 + 1e-9, 1e-5 - 1e-9 };
    ASSERT_DOUBLE_EQ(relative_diff(0.0, 0.0), 0.0);
    ASSERT_DOUBLE_EQ(relative_diff(1e6, 1e6 + 5), 5e-6);
    ASSERT_DOUBLE_EQ(relative_diff(1e-3, 1e-3 + 1e-4), 1e-1);
    ASSERT_DOUBLE_EQ(relative_diff(z1, z1), 0.0);
    ASSERT_DOUBLE_EQ(relative_diff(v1, v1), 0.0);
    ASSERT_TRUE(relative_diff(v2, v3) < 0.0002);
}
