/**
 * @file    walias_test.cpp
 * @brief   Unit tests for walker alias algorithm.
 * @author  Thomas Hahn
 */

#include <gtest/gtest.h>
#include <simpleMC/utils/walker_alias.h>
#include <simpleMC/utils/random.h>

using namespace simpleMC::utils;

class AliasTest : public ::testing::Test {
public:
    using eng_type = xoshiro256pp;
    using dis_type = uniform_real_distribution;
    using rng_type = variate_generator<eng_type, dis_type>;

    AliasTest() :
        rng(eng_type(), dis_type(0.0, 1.0))
    {}

    rng_type rng;
};

TEST_F(AliasTest, SimpleTest) {
    int nsam = 1000000;
    std::vector<double> weights{0.7, 0, 0.2, 0.1};
    std::vector<int> samples1(weights.size(), 0), samples2(weights.size(), 0);
    walker_alias wa(weights);
    for (int i = 0; i < nsam; i ++) {
        samples1[wa.gen(rng())] += 1;
        samples2[wa.gen(rng(), rng())] += 1;
    }
    for (std::size_t i = 0; i < samples1.size(); i++) {
        ASSERT_NEAR(double(samples1[i])/nsam, weights[i], 0.01);
        ASSERT_NEAR(double(samples2[i])/nsam, weights[i], 0.01);
    }
}
