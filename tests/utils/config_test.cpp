/**
 * @file    config_test.cpp
 * @brief   Unit tests for config header.
 * @author  Thomas Hahn
 */

#include <iostream>
#include <gtest/gtest.h>
#include <simpleMC/config.h>

TEST(SimpleDMC, ConfigHeader) {
    std::cout << "VERSION: " << SIMPLEMC_VERSION << "\n";
    std::cout << "MPI enabled: ";
#ifdef SIMPLEMC_HAS_MPI
    std::cout << "true\n";
#else
    std::cout << "false\n";
#endif
    std::cout << "HDF5 enabled: ";
#ifdef SIMPLEMC_HAS_HDF5
    std::cout << "true\n";
#else
    std::cout << "false\n";
#endif
}
