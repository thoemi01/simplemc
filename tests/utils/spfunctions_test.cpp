/**
 * @file    poly_test.cpp
 * @brief   Unit tests for polynomial recurrence relations.
 * @author  Thomas Hahn
 */

#include <vector>
#include <iostream>
#include <gtest/gtest.h>
#include <simpleMC/utils/special_functions.h>
#include <simpleMC/utils/timer.h>

using namespace simpleMC::utils;

void check_map(const map_interval& ma, double a, double b, double c, double d) {
    ASSERT_EQ(c, ma.map(a));
    ASSERT_EQ(d, ma.map(b));
    ASSERT_EQ(a, ma.inv_map(c));
    ASSERT_EQ(b, ma.inv_map(d));
}

TEST(SpecialPolynomials, MapInterval) {
    double a = -1;
    double b = 1;
    double c = 0;
    double d = 80;
    map_interval ma(a, b, c, d);
    check_map(ma, a, b, c, d);
}

TEST(SpecialPolynomials, MapIntervalMinusInfinity) {
    double a = math_constants::minus_inf;
    double b = 0;
    double c = math_constants::minus_inf;
    double d = 100;
    map_interval ma(a, b, c, d);
    check_map(ma, a, b, c, d);
}

TEST(SpecialPolynomials, MapIntervalPlusInfinity) {
    double a = 5;
    double b = math_constants::inf;
    double c = -17;
    double d = math_constants::inf;
    map_interval ma(a, b, c, d);
    check_map(ma, a, b, c, d);
}

TEST(SpecialPolynomials, MapIntervalMinusPlusInfinity) {
    double a = math_constants::minus_inf;
    double b = math_constants::inf;
    double c = math_constants::minus_inf;
    double d = math_constants::inf;
    map_interval ma(a, b, c, d);
    check_map(ma, a, b, c, d);
}

TEST(SpecialPolynomials, LegendreRuntime) {
    double x = 0.75;
    std::vector<double> res(128);
    timer time;
    time.start();
    double p0 = 1;
    double p1 = x;
    res[0] = p0;
    res[1] = p1;
    for (unsigned int i = 2; i < 128; i++) {
        double tmp = legendre_next(i-1, x, p1, p0);
        p0 = p1;
        p1 = tmp;
        res[i] = p1;
    }
    time.stop();
    std::cout << "legendre_next: " << time.start_to_stop() << std::endl;
    time.start();
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = legendre_poly(i, x);
    }
    time.stop();
    std::cout << "legendre: " << time.start_to_stop() << std::endl;
    time.start();
    legendre leg(x);
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = leg.next();
    }
    time.stop();
    std::cout << "legendre_rec: " << time.start_to_stop() << std::endl;
}

TEST(SpecialPolynomials, LegendrePolynomials) {
    unsigned int niter = 10;
    double x = 0.75;
    double tol = 1e-9;
    std::vector<double> boost_l{1, 0.75, 0.34375, -0.0703125, -0.3500976562,
                                -0.4163818359, -0.2807769775, -0.0341835022,
                                0.1976093054, 0.3103318512};
    std::vector<double> boost_p{0, 1, 2.25, 2.71875, 1.7578125, -0.4321289062,
                                -2.822387695, -4.082229614, -3.335140228,
                                -0.7228714228};
    legendre rec(x);
    legendre rec_5(x, 5);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(boost_l[i], legendre_poly(i, x), tol);
        ASSERT_NEAR(boost_l[i], rec.next(), tol);
        ASSERT_NEAR(boost_p[i], legendre_poly_prime(i, x), tol);
        if (i >= 5) {
            ASSERT_NEAR(boost_l[i], rec_5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, ChebyshevPolynomials) {
    unsigned int niter = 10;
    double x = 0.25;
    double tol = 1e-10;
    std::vector<double> boost_t{1, 0.25, -0.875, -0.6875, 0.53125, 0.953125,
                                -0.0546875, -0.98046875, -0.435546875,
                                0.7626953125};
    std::vector<double> boost_u{1, 0.5, -0.75, -0.875, 0.3125, 1.03125,
                                0.203125, -0.9296875, -0.66796875, 0.595703125};
    std::vector<double> boost_tp{0, 1, 1, -2.25, -3.5, 1.5625, 6.1875, 1.421875,
                                 -7.4375, -6.01171875};
    chebyshev rec_t(x);
    chebyshev rec_u(x, 0, chebyshev_kind::second);
    chebyshev rec_t5(x, 5);
    chebyshev rec_u5(x, 5, chebyshev_kind::second);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(boost_t[i], chebyshev_poly_t(i, x), tol);
        ASSERT_NEAR(boost_u[i], chebyshev_poly_u(i, x), tol);
        ASSERT_NEAR(boost_tp[i], chebyshev_poly_t_prime(i, x), tol);
        ASSERT_NEAR(boost_t[i], rec_t.next(), tol);
        ASSERT_NEAR(boost_u[i], rec_u.next(), tol);
        if (i >= 5) {
            ASSERT_NEAR(boost_t[i], rec_t5.next(), tol);
            ASSERT_NEAR(boost_u[i], rec_u5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, LaguerrePolynomials) {
    unsigned int niter = 10;
    double x = 17;
    double tol = 1e-6;
    std::vector<double> boost_l{1, -16, 111.5, -435.3333333, 1004.708333,
                                -1259.266667, 422.0097222, 838.2230159,
                                -578.8142609, -745.0871252};
    laguerre rec(x);
    laguerre rec_5(x, 5);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(boost_l[i], laguerre_poly(i, x), tol);
        ASSERT_NEAR(boost_l[i], rec.next(), tol);
        if (i >= 5) {
            ASSERT_NEAR(boost_l[i], rec_5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, HermitePolynomials) {
    unsigned int niter = 10;
    double x = -1;
    double tol = 1e-10;
    std::vector<double> boost_h{1, -2, 2, 4, -20, 8, 184, -464, -1648, 10720};
    hermite rec(x);
    hermite rec_5(x, 5);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(boost_h[i], hermite_poly(i, x), tol);
        ASSERT_NEAR(boost_h[i], rec.next(), tol);
        if (i >= 5) {
            ASSERT_NEAR(boost_h[i], rec_5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, CosinePolynomials) {
    unsigned int niter = 10;
    double x = 2.345;
    double tol = 1e-10;
    cosine rec(x);
    cosine rec_5(x, 5);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(std::cos(i*x), rec.next(), tol);
        if (i >= 5) {
            ASSERT_NEAR(std::cos(i*x), rec_5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, CosinePolynomialsRuntime) {
    double x = 2.345;
    std::vector<double> res(128);
    timer time;
    time.start();
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = std::cos(i*x);
    }
    time.stop();
    std::cout << "std::cos: " << time.start_to_stop() << std::endl;
    time.start();
    cosine rec(x);
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = rec.next();
    }
    time.stop();
    std::cout << "cosine_rec: " << time.start_to_stop() << std::endl;
}

TEST(SpecialPolynomials, SinePolynomials) {
    unsigned int niter = 10;
    double x = 2.345;
    double tol = 1e-10;
    sine rec(x);
    sine rec_5(x, 5);
    for (unsigned int i = 0; i < niter; i++) {
        ASSERT_NEAR(std::sin(i*x), rec.next(), tol);
        if (i >= 5) {
            ASSERT_NEAR(std::sin(i*x), rec_5.next(), tol);
        }
    }
}

TEST(SpecialPolynomials, SinePolynomialsRuntime) {
    double x = 2.345;
    std::vector<double> res(128);
    timer time;
    time.start();
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = std::sin(i*x);
    }
    time.stop();
    std::cout << "std::sin: " << time.start_to_stop() << std::endl;
    time.start();
    sine rec(x);
    for (unsigned int i = 0; i < 128; i++) {
        res[i] = rec.next();
    }
    time.stop();
    std::cout << "sine_rec: " << time.start_to_stop() << std::endl;
}
