/**
 * @file    grids_test_helper.h
 * @brief   Helper functions for testing grids.
 * @author  Thomas Hahn
 */

#pragma once

#include <algorithm>
#include <functional>
#include <iterator>
#include <iostream>
#include <numeric>
#include <cmath>
#include <gtest/gtest.h>

template <typename G, typename C>
void assert_eq_grid_with_container(const G& grid, const C& cont) {
    for (std::size_t i = 0; i < grid.numpoints(); i++) {
        ASSERT_EQ(grid.value(i), cont[i]);
    }
}

template <typename G, typename C>
void assert_double_eq_grid_with_container(const G& grid, const C& cont) {
    for (std::size_t i = 0; i < grid.numpoints(); i++) {
        ASSERT_DOUBLE_EQ(grid.value(i), cont[i]);
    }
}

template <typename G, typename C>
void assert_double_eq_grid_with_container(const G& grid, const C& cont,
                                          std::size_t start, std::size_t stop) {
    for (std::size_t i = start; i < stop; i++) {
        ASSERT_DOUBLE_EQ(grid.value(i), cont[i]);
    }
}

template <typename G, typename C>
void assert_near_grid_with_container(const G& grid, const C& cont, double eps = 1e-12) {
    for (std::size_t i = 0; i < grid.numpoints(); i++) {
        ASSERT_NEAR(grid.value(i), cont[i], eps);
    }
}

template <typename G, typename C>
void assert_near_grid_with_container(const G& grid, const C& cont,
                                     std::size_t start, std::size_t stop,
                                     double eps = 1e-12) {
    for (std::size_t i = start; i < stop; i++) {
        ASSERT_NEAR(grid.value(i), cont[i], eps);
    }
}

template <typename T, typename C>
T get_index_in_container(T value, const C& cont, bool ascend) {
    auto it = cont.end();
    if (ascend) {
        it = std::lower_bound(cont.begin(), cont.end(), value, std::less<T>());
        if (*it != value && it != cont.begin())
            --it;
    } else {
        it = std::lower_bound(cont.begin(), cont.end(), value, std::greater<T>());
        if (*it != value && it != cont.begin())
            --it;
    }
    return std::distance(cont.begin(), it);
}

template <typename G>
void print_grid(const G& grid) {
    for (int i = 0; i < grid.numpoints(); i++) {
        std::cout << grid.value(i) << " ";
    }
    std::cout << "\n";
}

template <typename C>
void print_container(const C& cont) {
    for (auto el : cont) {
        std::cout << el << " ";
    }
    std::cout << "\n";
}

template <typename V>
V generate_linspace(double start, double stop, std::size_t numpoints) {
    V res(numpoints);
    double step = (stop - start)/(numpoints - 1);
    for (std::size_t i = 0; i < numpoints; i++) {
        res[i] = start + step*i;
    }
    return res;
}

template <typename V>
V generate_indexspace(std::size_t numpoints) {
    V res(numpoints);
    std::iota(res.begin(), res.end(), 0);
    return res;
}

template <typename V>
V generate_powerspace(double start, double stop, std::size_t numpoints,
                      std::size_t power = 2) {
    V res(numpoints);
    double step = (stop - start)/std::pow(numpoints - 1.0, power);
    for (std::size_t i = 0; i < numpoints; i++) {
        res[i] = start + step*std::pow(static_cast<double>(i), power);
    }
    return res;
}
