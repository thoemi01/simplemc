/**
 * @file    string_test.cpp
 * @brief   Unit tests for string utilities.
 * @author  Thomas Hahn
 */

#include <gtest/gtest.h>
#include <simpleMC/utils/string.h>

using namespace simpleMC::utils;

TEST(String, SplitEmptyAtChar) {
    std::string to_split = "";
    auto tokens_without_empty = split_at_char(to_split, '/');
    ASSERT_EQ(tokens_without_empty.size(), 0);
    auto tokens_with_empty = split_at_char(to_split, '/', true);
    ASSERT_EQ(tokens_with_empty.size(), 1);
}

TEST(String, SplitEmptyAtString) {
    std::string to_split = "";
    auto tokens_without_empty = split_at_string(to_split, "/");
    ASSERT_EQ(tokens_without_empty.size(), 0);
    auto tokens_with_empty = split_at_string(to_split, "/", true);
    ASSERT_EQ(tokens_with_empty.size(), 1);
}

TEST(String, SplitAtChar) {
    std::string to_split = "hallo_wie_geht_es_dir_";
    std::vector<std::string> exact_tokens{"hallo", "wie", "geht", "es", "dir"};
    auto tokens_without_empty = split_at_char(to_split, '_');
    ASSERT_EQ(tokens_without_empty, exact_tokens);
    auto tokens_with_empty = split_at_char(to_split, '_', true);
    exact_tokens.push_back("");
    ASSERT_EQ(tokens_with_empty, exact_tokens);
}

TEST(String, SplitAtString) {
    std::string to_split = "halloABCwieABCgehtABCesABCdirABC";
    std::vector<std::string> exact_tokens{"hallo", "wie", "geht", "es", "dir"};
    auto tokens_without_empty = split_at_string(to_split, "ABC");
    ASSERT_EQ(tokens_without_empty, exact_tokens);
    auto tokens_with_empty = split_at_string(to_split, "ABC", true);
    exact_tokens.push_back("");
    ASSERT_EQ(tokens_with_empty, exact_tokens);
}

TEST(String, ConcatenateTokens) {
    std::vector<std::string> tokens{"path", "to", "file"};
    std::string exact_result = "/path/to/file";
    auto concat_string = concat_tokens(tokens, "/", "/", "");
    ASSERT_EQ(concat_string, exact_result);
}

TEST(String, BaseOfPath) {
    std::string input = "/path/to/file";
    std::string exact_base = "/path/to";
    ASSERT_EQ(get_base(input, "/"), exact_base);
    input = "/";
    exact_base = "";
    ASSERT_EQ(get_base(input, "/"), exact_base);
    input = "";
    exact_base = "";
    ASSERT_EQ(get_base(input, "/"), exact_base);
    input = "path";
    exact_base = "";
    ASSERT_EQ(get_base(input, "/"), exact_base);
    input = "path";
    exact_base = "path";
    ASSERT_EQ(get_base(input, "/", true), exact_base);
    input = "/path/";
    exact_base = "/path";
    ASSERT_EQ(get_base(input, "/"), exact_base);
    input = "path/";
    exact_base = "path";
    ASSERT_EQ(get_base(input, "/"), exact_base);
}

TEST(String, LeafOfPath) {
    std::string input = "/path/to/file";
    std::string exact_leaf = "file";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "/";
    exact_leaf = "";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "";
    exact_leaf = "";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "path";
    exact_leaf = "path";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "path";
    exact_leaf = "";
    ASSERT_EQ(get_leaf(input, "/", false), exact_leaf);
    input = "/path/";
    exact_leaf = "";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "path/";
    exact_leaf = "";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
    input = "/path";
    exact_leaf = "path";
    ASSERT_EQ(get_leaf(input, "/"), exact_leaf);
}

TEST(String, ConversionToString) {
    double z{ -3.12345678912345 };
    std::string str3 = "-3.12";
    std::string str10 = "-3.123456789";
    ASSERT_EQ(to_string(z, 3), str3);
    ASSERT_EQ(to_string(z, 10), str10);
}
