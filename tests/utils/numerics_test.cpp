/**
 * @file    numerics_test.cpp
 * @brief   Unit tests for numerics utils.
 * @author  Thomas Hahn
 */

#include <vector>
#include <iostream>
#include <gtest/gtest.h>
#include <simpleMC/utils/numerics.h>
#include <simpleMC/utils/grids.h>
#include <simpleMC/utils/random.h>

using namespace simpleMC::utils;
using rng_type = variate_generator<xoshiro256pp, uniform_real_distribution>;

template <typename G, typename F>
std::vector<double> generate_points(const G& grid, F&& func) {
    std::vector<double> res(grid.numpoints());
    for (std::size_t i = 0; i < grid.numpoints(); i++) {
        res[i] = func(grid.value(i));
    }
    return res;
}

double linear(double x) { return 2.23 - 3.9*x; }

double quadratic(double x) { return x*x; }

double cubic(double x) { return 1.2*x*x*x + 0.23*x*x - 4.5*x + 2; }

double sine(double x) { return std::sin(x); }

double expo(double x) { return std::exp(-x); }

TEST(LinearInterpolation, LinearFunction) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, linear);
    linear_interpolation lin(lg, make_view(yvals));
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = lin.interpolate(x);
        double y_ex = linear(x);
        ASSERT_NEAR(y_int, y_ex, 1e-10);
    }
}

TEST(LinearInterpolation, QuadraticFunction) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, quadratic);
    linear_interpolation lin(lg, make_view(yvals));
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = lin.interpolate(x);
        double y_ex = quadratic(x);
        ASSERT_NEAR(y_int, y_ex, 1e-2);
    }
}

TEST(PolynomialInterpolation, QuadraticFunctionWithQuadraticInterpolation) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, quadratic);
    polynomial_interpolation quad(lg, make_view(yvals), 2);
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = quad.interpolate(x);
        double y_ex = quadratic(x);
        ASSERT_NEAR(y_int, y_ex, 1e-10);
    }
}


TEST(PolynomialInterpolation, CubicFunctionWithCubicInterpolation) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, cubic);
    polynomial_interpolation cub(lg, make_view(yvals), 3);
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = cub.interpolate(x);
        double y_ex = cubic(x);
        ASSERT_NEAR(y_int, y_ex, 1e-10);
    }
}

TEST(CubicSplineInterpolation, QuadraticFunction) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, quadratic);
    double yp0 = 2*(-2);
    double ypnm1 = 2*2;
    cbspline_interpolation spline(lg, make_view(yvals), yp0, ypnm1);
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = spline.interpolate(x);
        double y_ex = quadratic(x);
        ASSERT_NEAR(y_int, y_ex, 1e-1);
    }
}

TEST(CubicSplineInterpolation, CubicFunction) {
    linear_grid lg(-2, 2, 100);
    auto yvals = generate_points(lg, cubic);
    cbspline_interpolation spline(lg, make_view(yvals));
    rng_type rng(xoshiro256pp(), uniform_real_distribution(-2.0, 2.0));
    for (int i = 0; i < 50; i++) {
        double x = rng();
        double y_int = spline.interpolate(x);
        double y_ex = cubic(x);
        ASSERT_NEAR(y_int, y_ex, 1e-1);
    }
}

TEST(BasicQuadrature, SineFunction) {
    basic_quadrature bq([](double x) {
        return std::sin(x);
    }, 0, 3.1415);
    for (int i = 0; i < 15; i++) {
        bq.next();
    }
    ASSERT_NEAR(bq.current(), 2.0, 1e-5);
}

TEST(TrapezQuadrature, SineFunction) {
    double res = trapez_quad([](double x) { return std::sin(x); }, 0, 3.1415);
    ASSERT_NEAR(res, 2.0, 1e-5);
}

TEST(TrapezQuadrature, SineFunctionDiscreteData) {
    std::size_t np = 500;
    linear_grid lg(0, 3.1415, np);
    auto data = generate_points(lg, sine);
    double res = trapez_quad(lg, make_view(data));
    ASSERT_NEAR(res, 2.0, 1e-5);
}

TEST(SimpsonQuadrature, SineFunction) {
    double res = simpson_quad([](double x) { return std::sin(x); }, 0, 3.1415);
    ASSERT_NEAR(res, 2.0, 1e-5);
}

TEST(SimpsonQuadrature, SineFunctionDiscreteData) {
    std::size_t np = 100;
    linear_grid lg(0, 3.1415, np);
    auto data = generate_points(lg, sine);
    double res = simpson_quad(lg, make_view(data));
    ASSERT_NEAR(res, 2.0, 1e-5);
}

TEST(SimpsonQuadrature, ExpFunctionDiscreteData) {
    std::size_t np = 101;
    linear_grid lg(0, 2, np);
    auto data = generate_points(lg, expo);
    double res = simpson_quad(lg, make_view(data));
    ASSERT_NEAR(res, 1 - std::exp(-2), 1e-5);
}
