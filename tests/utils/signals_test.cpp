/**
 * @file    signals_test.cpp
 * @brief   Unit tests for signal handler.
 * @author  Thomas Hahn
 */

#include <thread>
#include <csignal>
#include <gtest/gtest.h>
#include <simpleMC/utils/signals.h>

using namespace simpleMC;

void my_handler(int /*signal*/) {
    signals::stop_listening();
    signals::start_listening();
}

TEST(SignalTest, SIGINTTest) {
    signals::start_listening();
    int i = 0;
    int end = 5;
    while (signals::empty()) {
        i++;
        if (i == end) std::raise(SIGINT);
    }
    ASSERT_EQ(i, end);
    signals::stop_listening();
    ASSERT_TRUE(signals::empty());
}

TEST(SignalTest, CustomHandler) {
    signals::start_listening(&my_handler);
    int i = 0;
    int end1 = 5, end2 = 10;
    while (signals::empty()) {
        i++;
        if (i == end1) std::raise(SIGTERM);
        if (i == end2) std::raise(SIGUSR1);
    }
    ASSERT_EQ(i, end2);
    ASSERT_EQ(signals::last(), SIGUSR1);
    signals::pop();
    ASSERT_TRUE(signals::empty());
    signals::stop_listening();
    ASSERT_TRUE(signals::empty());
}
