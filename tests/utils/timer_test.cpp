/**
 * @file    timer_test.cpp
 * @brief   Unit tests for timer.
 * @author  Thomas Hahn
 */

#include <thread>
#include <cmath>
#include <gtest/gtest.h>
#include <simpleMC/utils/timer.h>

using namespace simpleMC::utils;

void sleep_for_one_second() {
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

TEST(MeasureTime, AfterDefaultConstructor) {
    timer t;
    ASSERT_EQ(0, t.start_to_stop());
    ASSERT_EQ(0, t.start_to_interim());
    ASSERT_EQ(0, t.interim_to_stop());
}

TEST(MeasureTime, InDefaultDuration) {
    timer t;
    using rep_t = typename timer<>::duration_type::rep;
    t.start();
    sleep_for_one_second();
    t.stop();
    rep_t onesec = 1e+09; // in nanoseconds
    rep_t tolerance = 1e+06; // one millisecond
    ASSERT_LT(std::abs(onesec - t.start_to_stop()), tolerance);
}

TEST(MeasureTime, InSeconds) {
    timer t;
    using rep_t = typename timer<>::duration_type::rep;
    t.start();
    sleep_for_one_second();
    t.stop();
    rep_t onesec = 1; // in nanoseconds
    ASSERT_EQ(onesec, t.start_to_stop<std::chrono::seconds>());
}

TEST(MeasureTime, InDefaultDurationWithInterim) {
    timer t;
    using rep_t = typename timer<>::duration_type::rep;
    t.start();
    sleep_for_one_second();
    t.interim();
    sleep_for_one_second();
    t.stop();
    rep_t onesec = 1e+09; // in nanoseconds
    rep_t twosec = 2*onesec;
    rep_t tolerance = 1e+06; // one millisecond
    ASSERT_LT(std::abs(twosec - t.start_to_stop()), tolerance);
    ASSERT_LT(std::abs(onesec - t.interim_to_stop()), tolerance);
}
