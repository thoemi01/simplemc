/**
 * @file    ndgrid_test.cpp
 * @brief   Unit tests for n-dimensional grids.
 * @author  Thomas Hahn
 */

#include "grid_test_helper.h"
#include <simpleMC/utils/grids.h>

using namespace simpleMC::utils;

TEST(nd_grid, OneDimensionalGrid) {
    using lg_type = linear_grid;
    lg_type lg(0.0, 1.0, 11);
    nd_grid<std::size_t, lg_type> ndg(lg);
    ASSERT_EQ(ndg.dim(), 1);
    auto dim = nd_grid<std::size_t, lg_type>::dim();
    ASSERT_EQ(dim, 1);
    ASSERT_EQ(ndg.numpoints(), 11);
    ndg.get<0>().set(0.0, 100, 101);
    ASSERT_EQ(ndg.numpoints(), 101);
    auto startarr = std::array<double, 1>{0.0};
    ASSERT_EQ(ndg.start(), startarr);
    auto stoparr = std::array<double, 1>{100.0};
    ASSERT_EQ(ndg.stop(), stoparr);
    auto indexarr = std::array<std::size_t, 1>{0};
    auto valuearr = std::array<double, 1>{0.0};
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 100;
    valuearr[0] = 100.0;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 47;
    valuearr[0] = 47.0;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 12;
    valuearr[0] = 12.3;
    ASSERT_EQ(ndg.index(valuearr), indexarr);
    ASSERT_EQ(ndg.binsize(indexarr), 1.0);
}

TEST(nd_grid, TwoDimensionalGrid) {
    using lg_type = linear_grid;
    using pg_type = power_grid;
    lg_type lg(0.0, 1.0, 11);
    pg_type qg(0.5, 1.0, 5);
    nd_grid<std::size_t, lg_type, pg_type> ndg(lg, qg);
    ASSERT_EQ(ndg.dim(), 2);
    ASSERT_EQ(ndg.numpoints(), 55);
    ndg.get<0>().set(0.0, 100, 101);
    ASSERT_EQ(ndg.numpoints(), 505);
    auto startarr = std::array<double, 2>{0.0, 0.5};
    ASSERT_EQ(ndg.start(), startarr);
    auto stoparr = std::array<double, 2>{100.0, 1.0};
    ASSERT_EQ(ndg.stop(), stoparr);
    auto indexarr = std::array<std::size_t, 2>{0, 0};
    auto valuearr = std::array<double, 2>{0.0, 0.5};
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 100;
    indexarr[1] = 4;
    valuearr[0] = 100.0;
    valuearr[1] = 1.0;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 47;
    indexarr[1] = 0;
    valuearr[0] = 47.0;
    valuearr[1] = 0.5;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    valuearr[0] = 47.1;
    valuearr[1] = 0.50001;
    ASSERT_EQ(ndg.index(valuearr), indexarr);
}

TEST(nd_grid, ThreeDimensionalGrid) {
    using ig_type = index_grid;
    using lg_type = linear_grid;
    ig_type ig(10);
    lg_type lg(0.0, 1.0, 11);
    lg_type qg(0.0, 1.0, 5);
    nd_grid<long, ig_type, lg_type, lg_type> ndg(ig, lg, qg);
    ASSERT_EQ(ndg.dim(), 3);
    ASSERT_EQ(ndg.numpoints(), 550);
    ndg.get<1>().set(0.0, 100, 101);
    ASSERT_EQ(ndg.numpoints(), 5050);
    auto startarr = std::array<double, 3>{0.0, 0.0, 0.0};
    ASSERT_EQ(ndg.start(), startarr);
    auto stoparr = std::array<double, 3>{9.0, 100.0, 1.0};
    ASSERT_EQ(ndg.stop(), stoparr);
    auto indexarr = std::array<long, 3>{0, 0, 0};
    auto valuearr = std::array<double, 3>{0.0, 0.0, 0.0};
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 9;
    indexarr[1] = 100;
    indexarr[2] = 4;
    valuearr[0] = 9;
    valuearr[1] = 100.0;
    valuearr[2] = 1.0;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    indexarr[0] = 5;
    indexarr[1] = 23;
    indexarr[2] = 3;
    valuearr[0] = 5;
    valuearr[1] = 23.0;
    valuearr[2] = 0.75;
    ASSERT_EQ(ndg.value(indexarr), valuearr);
    valuearr[0] = 5;
    valuearr[1] = 23.9;
    valuearr[2] = 0.76;
    ASSERT_EQ(ndg.index(valuearr), indexarr);
    ASSERT_EQ(ndg.binsize(indexarr), 0.25);
    ndg.get<1>().set(0.0, 1, 11);
    ASSERT_EQ(ndg.binsize(indexarr), 0.025);
}
