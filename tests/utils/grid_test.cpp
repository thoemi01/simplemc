/**
 * @file    grids_test.cpp
 * @brief   Unit tests for different grids.
 * @author  Thomas Hahn
 */

#include <vector>
#include "grid_test_helper.h"
#include <simpleMC/utils/grids.h>

using namespace simpleMC::utils;

class mygrid {
public:
    explicit mygrid(const std::vector<double>& vec) :
        cont_(vec),
        numpoints_(cont_.size())
    {}

    double start() const {
        return cont_[0];
    }

    double stop() const {
        return cont_[numpoints_ - 1];
    }

    double numpoints() const {
        return numpoints_;
    }

    double value(int index) const {
        return cont_[index];
    }

    int index(double value) const {
        if (value == cont_[0])
            return 0;
        else if (value == cont_[numpoints_ - 1])
            return numpoints_ - 1;
        auto it = std::lower_bound(cont_.begin(), cont_.end(), value);
        return std::distance(cont_.begin(), it) - 1;
    }

    double binsize(int index) const {
        return value(index + 1) - value(index);
    }

private:
    std::vector<double> cont_;
    int numpoints_;
};

TEST(LinearGrid, PositiveDefiniteAscendingGrid) {
    linear_grid lingrid(0.0, 1.0, 11);
    auto linspace = generate_linspace<std::vector<double>>(0.0, 1.0, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(0), 0);
    ASSERT_EQ(lingrid.index(0.55), 5);
    ASSERT_EQ(lingrid.index(1), 10);
    ASSERT_DOUBLE_EQ(lingrid.binsize(0), 0.1);
}

TEST(LinearGrid, PositiveDefiniteDescendingGrid) {
    linear_grid lingrid(2.0, 1.0, 11);
    auto linspace = generate_linspace<std::vector<double>>(2.0, 1.0, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(2.0), 0);
    ASSERT_EQ(lingrid.index(1.55), 4);
    ASSERT_EQ(lingrid.index(1.0), 10);
    ASSERT_EQ(lingrid.binsize(0), 0.1);
}

TEST(LinearGrid, NegativeDefiniteAscendingGrid) {
    linear_grid lingrid(-1.0, 0.0, 11);
    auto linspace = generate_linspace<std::vector<double>>(-1.0, 0.0, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(-1.0), 0);
    ASSERT_EQ(lingrid.index(-0.55), 4);
    ASSERT_EQ(lingrid.index(0.0), 10);
    ASSERT_DOUBLE_EQ(lingrid.binsize(0), 0.1);
}

TEST(LinearGrid, NegativeDefiniteDescendingGrid) {
    linear_grid lingrid(-1.0, -2.0, 11);
    auto linspace = generate_linspace<std::vector<double>>(-1.0, -2.0, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(-1.0), 0);
    ASSERT_EQ(lingrid.index(-1.55), 5);
    ASSERT_EQ(lingrid.index(-2.0), 10);
    ASSERT_DOUBLE_EQ(lingrid.binsize(0), 0.1);
}

TEST(LinearGrid, CrossingZeroAscendingGrid) {
    linear_grid lingrid(-0.5, 0.5, 11);
    auto linspace = generate_linspace<std::vector<double>>(-0.5, 0.5, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(-0.5), 0);
    ASSERT_EQ(lingrid.index(0.05), 5);
    ASSERT_EQ(lingrid.index(0.5), 10);
    ASSERT_DOUBLE_EQ(lingrid.binsize(0), 0.1);
}

TEST(LinearGrid, CrossingZeroDescendingGrid) {
    linear_grid lingrid(0.5, -0.5, 11);
    auto linspace = generate_linspace<std::vector<double>>(0.5, -0.5, 11);
    assert_double_eq_grid_with_container(lingrid, linspace);
    ASSERT_EQ(lingrid.index(0.5), 0);
    ASSERT_EQ(lingrid.index(0.05), 4);
    ASSERT_EQ(lingrid.index(-0.5), 10);
    ASSERT_DOUBLE_EQ(lingrid.binsize(0), 0.1);
}

TEST(PowerGrid, PositiveDefiniteAscendingGrid) {
    power_grid quadgrid(0.0, 100.0, 11);
    power_grid cubegrid(0.0, 100.0, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(0.0, 100.0, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(0.0, 100.0, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(0), 0);
    ASSERT_EQ(quadgrid.index(55), 7);
    ASSERT_EQ(quadgrid.index(100), 10);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(0), 1.0);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(9), 19);
    ASSERT_EQ(cubegrid.index(0), 0);
    ASSERT_EQ(cubegrid.index(55), 8);
    ASSERT_EQ(cubegrid.index(99.99999), 9);
    ASSERT_DOUBLE_EQ(cubegrid.binsize(0), 0.1);
    ASSERT_DOUBLE_EQ(cubegrid.binsize(9), 27.1);
}

TEST(PowerGrid, PositiveDefiniteDescendingGrid) {
    power_grid quadgrid(100.0, 0.0, 11);
    power_grid cubegrid(100.0, 0.0, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(100.0, 0.0, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(100.0, 0.0, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(100.0), 0);
    ASSERT_EQ(quadgrid.index(85.0), 3);
    ASSERT_EQ(quadgrid.index(0.0), 10);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(0), 1);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(9), 19);
    ASSERT_EQ(cubegrid.index(100), 0);
    ASSERT_EQ(cubegrid.index(85.0), 5);
    ASSERT_EQ(cubegrid.index(0.0000001), 9);
    ASSERT_NEAR(cubegrid.binsize(0), 0.1, 1e-10);
    ASSERT_NEAR(cubegrid.binsize(9), 27.1, 1e-10);
}

TEST(PowerGrid, NegativeDefiniteAscendingGrid) {
    power_grid quadgrid(-100.0, 0.0, 11);
    power_grid cubegrid(-100.0, 0.0, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(-100.0, 0.0, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(-100.0, 0.0, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(-100.0), 0);
    ASSERT_EQ(quadgrid.index(-85.0), 3);
    ASSERT_EQ(quadgrid.index(0.0), 10);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(0), 1);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(9), 19);
    ASSERT_EQ(cubegrid.index(-100), 0);
    ASSERT_EQ(cubegrid.index(-85.0), 5);
    ASSERT_EQ(cubegrid.index(-0.0000001), 9);
    ASSERT_NEAR(cubegrid.binsize(0), 0.1, 1e-10);
    ASSERT_NEAR(cubegrid.binsize(9), 27.1, 1e-10);
}

TEST(PowerGrid, NegativeDefiniteDescendingGrid) {
    power_grid quadgrid(0.0, -100.0, 11);
    power_grid cubegrid(0.0, -100.0, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(0.0, -100.0, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(0.0, -100.0, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(0), 0);
    ASSERT_EQ(quadgrid.index(-55), 7);
    ASSERT_EQ(quadgrid.index(-100), 10);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(0), 1.0);
    ASSERT_DOUBLE_EQ(quadgrid.binsize(9), 19);
    ASSERT_EQ(cubegrid.index(0), 0);
    ASSERT_EQ(cubegrid.index(-55.0), 8);
    ASSERT_EQ(cubegrid.index(-99.9999999), 9);
    ASSERT_NEAR(cubegrid.binsize(0), 0.1, 1e-10);
    ASSERT_NEAR(cubegrid.binsize(9), 27.1, 1e-10);
}

TEST(PowerGrid, CrossingZeroAscendingGrid) {
    power_grid quadgrid(-0.5, 0.5, 11);
    power_grid cubegrid(-0.5, 0.5, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(-0.5, 0.5, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(-0.5, 0.5, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(-0.5), 0);
    ASSERT_EQ(quadgrid.index(0.05), 7);
    ASSERT_EQ(quadgrid.index(0.5), 10);
    ASSERT_NEAR(quadgrid.binsize(0), 0.01, 1e-10);
    ASSERT_EQ(cubegrid.index(-0.5), 0);
    ASSERT_EQ(cubegrid.index(0.05), 8);
    ASSERT_EQ(cubegrid.index(0.49999999), 9);
    ASSERT_NEAR(cubegrid.binsize(0), 0.001, 1e-10);
}

TEST(PowerGrid, CrossingZeroDescendingGrid) {
    power_grid quadgrid(0.5, -0.5, 11);
    power_grid cubegrid(0.5, -0.5, 11, 3);
    auto quadspace = generate_powerspace<std::vector<double>>(0.5, -0.5, 11);
    auto cubespace = generate_powerspace<std::vector<double>>(0.5, -0.5, 11, 3);
    assert_double_eq_grid_with_container(quadgrid, quadspace);
    assert_double_eq_grid_with_container(cubegrid, cubespace);
    ASSERT_EQ(quadgrid.index(0.5), 0);
    ASSERT_EQ(quadgrid.index(-0.05), 7);
    ASSERT_EQ(quadgrid.index(-0.5), 10);
    ASSERT_NEAR(quadgrid.binsize(0), 0.01, 1e-10);
    ASSERT_EQ(cubegrid.index(0.5), 0);
    ASSERT_EQ(cubegrid.index(-0.05), 8);
    ASSERT_EQ(cubegrid.index(-0.499999999), 9);
    ASSERT_NEAR(cubegrid.binsize(0), 0.001, 1e-10);
}

TEST(IndexGrid, With1Point) {
    index_grid indGrid(1);
    std::vector<std::size_t> indspace{0};
    assert_eq_grid_with_container(indGrid, indspace);
}

TEST(IndexGrid, With11Points) {
    index_grid indGrid(11);
    auto indspace = generate_indexspace<std::vector<std::size_t>>(11);
    assert_eq_grid_with_container(indGrid, indspace);
}

TEST(IndexGrid, IndexFromGridWith1Point) {
    index_grid indGrid(1);
    ASSERT_EQ(indGrid.index(0), 0);
}

TEST(IndexGrid, IndexFromGridWith100Point) {
    index_grid indGrid(100);
    ASSERT_EQ(indGrid.index(0), 0);
    ASSERT_EQ(indGrid.index(99), 99);
    ASSERT_EQ(indGrid.index(9), 9);
}

TEST(CustomGrid, PositiveDefiniteAscendingGrid) {
    auto custspace = std::vector<double>{0.5, 1.2, 2.5, 3.0, 5.0};
    custom_grid<double, std::size_t> custgrid(custspace, bisector());
    assert_eq_grid_with_container(custgrid, custspace);
    ASSERT_EQ(custgrid.index(0.5), 0);
    ASSERT_EQ(custgrid.index(5.0), 4);
    ASSERT_EQ(custgrid.index(2.7), 2);
    ASSERT_DOUBLE_EQ(custgrid.binsize(0), 0.7);
    ASSERT_DOUBLE_EQ(custgrid.binsize(2), 0.5);
}


TEST(CustomGrid, PositiveDefiniteDescendingGrid) {
    auto custspace = std::vector<double>{10.0, 8.2, 7.3, 0.3, 0.1};
    custom_grid<double, std::size_t> custgrid(custspace, bisector());
    assert_eq_grid_with_container(custgrid, custspace);
    ASSERT_EQ(custgrid.index(10.0), 0);
    ASSERT_EQ(custgrid.index(0.1), 4);
    ASSERT_EQ(custgrid.index(2.7), 2);
    ASSERT_DOUBLE_EQ(custgrid.binsize(0), 1.8);
    ASSERT_DOUBLE_EQ(custgrid.binsize(2), 7);
}

TEST(CustomGrid, NegativeDefiniteDescendingGrid) {
    auto custspace = std::vector<double>{-0.5, -1.2, -2.5, -3.0, -5.0};
    custom_grid<double, std::size_t> custgrid(custspace, bisector());
    assert_eq_grid_with_container(custgrid, custspace);
    ASSERT_EQ(custgrid.index(-0.5), 0);
    ASSERT_EQ(custgrid.index(-5.0), 4);
    ASSERT_EQ(custgrid.index(-2.7), 2);
    ASSERT_DOUBLE_EQ(custgrid.binsize(0), 0.7);
    ASSERT_DOUBLE_EQ(custgrid.binsize(2), 0.5);
}


TEST(CustomGrid, NegativeDefiniteAscendingGrid) {
    auto custspace = std::vector<double>{-10.0, -8.2, -7.3, -0.3, -0.1};
    custom_grid<double, std::size_t> custgrid(custspace, bisector());
    assert_eq_grid_with_container(custgrid, custspace);
    ASSERT_EQ(custgrid.index(-10.0), 0);
    ASSERT_EQ(custgrid.index(-0.1), 4);
    ASSERT_EQ(custgrid.index(-2.7), 2);
    ASSERT_DOUBLE_EQ(custgrid.binsize(0), 1.8);
    ASSERT_DOUBLE_EQ(custgrid.binsize(2), 7);
}

TEST(CustomGrid, WithLambdaAsSearcher) {
    auto custspace = std::vector<double>{0.5, 1.2, 2.5, 3.0, 5.0};
    auto searcher = [](const auto& cont, const auto& value) -> std::size_t {
        auto size = cont.size();
        if (value == cont[0])
            return 0;
        else if (value == cont[size - 1])
            return size - 1;
        auto it = std::lower_bound(cont.begin(), cont.end(), value);
        return std::distance(cont.begin(), it) - 1;
    };
    custom_grid<double, std::size_t, std::vector<double>, decltype(searcher)> custgrid(custspace, searcher);
    assert_eq_grid_with_container(custgrid, custspace);
    ASSERT_EQ(custgrid.index(0.5), 0);
    ASSERT_EQ(custgrid.index(5.0), 4);
    ASSERT_EQ(custgrid.index(2.7), 2);
    ASSERT_DOUBLE_EQ(custgrid.binsize(0), 0.7);
    ASSERT_DOUBLE_EQ(custgrid.binsize(2), 0.5);
}

TEST(GridInterface, ArbitraryGridFromGridInterface) {
    std::vector<double> vec{-2.0, -1.4, 2.3, 5.0, 6.0, 7.1, 7.2};
    mygrid mg(vec);
    ASSERT_EQ(mg.index(-2.0), 0);
    ASSERT_EQ(mg.index(7.2), 6);
    ASSERT_EQ(mg.index(-0.7), 1);
    ASSERT_EQ(mg.index(5.5), 3);
    ASSERT_DOUBLE_EQ(mg.binsize(2), 2.7);
}

TEST(SymmetricGrid, Linear) {
    symmetric_grid symgrid(0.0, 1.0, 11, 1);
    auto symspace = generate_linspace<std::vector<double>>(0.0, 1.0, 11);
    assert_near_grid_with_container(symgrid, symspace);
    ASSERT_EQ(symgrid.index(0), 0);
    ASSERT_EQ(symgrid.index(0.55), 5);
    ASSERT_EQ(symgrid.index(1), 10);
    ASSERT_DOUBLE_EQ(symgrid.binsize(0), 0.1);
}

TEST(SymmetricGrid, Quadratic) {
    double tol = 1e-12;
    symmetric_grid symgrid(0.0, 1.0, 11);
    auto symspace = generate_powerspace<std::vector<double>>(0.0, 0.5, 6);
    auto symspace2 = generate_powerspace<std::vector<double>>(1.0, 0.5, 6);
    auto it = symspace2.rbegin();
    it++;
    symspace.insert(symspace.end(), it, symspace2.rend());
    assert_near_grid_with_container(symgrid, symspace, 0, 5);
    assert_near_grid_with_container(symgrid, symspace, 5, 11);
    ASSERT_EQ(symgrid.index(0), 0);
    ASSERT_EQ(symgrid.index(0.4), 4);
    ASSERT_EQ(symgrid.index(0.55), 5);
    ASSERT_EQ(symgrid.index(0.85), 7);
    ASSERT_EQ(symgrid.index(1), 10);
    ASSERT_NEAR(symgrid.binsize(0), 0.02, tol);
    ASSERT_NEAR(symgrid.binsize(4), 0.18, tol);
    ASSERT_NEAR(symgrid.binsize(5), 0.18, tol);
    ASSERT_NEAR(symgrid.binsize(9), 0.02, tol);
}
