/**
 * @file    random_test.cpp
 * @brief   Unit tests for random utilities.
 * @author  Thomas Hahn
 */

#include <random>
#include <vector>
#include <sstream>
#include <gtest/gtest.h>
#include <simpleMC/utils/random.h>
#include <simpleMC/utils/grids.h>

using namespace simpleMC::utils;

class histogram {
public:

    explicit histogram(std::size_t nbins) :
        nsamples_(0),
        nbins_(nbins),
        lg_(0.0, 1.0, nbins + 1),
        hist_(nbins)
    {}

    void add(double value) {
        auto idx = lg_.index(value);
        hist_[idx] += 1.0;
        nsamples_ += 1;
    }

    bool check_bins(double tol) {
        double exact = static_cast<double>(nsamples_)/nbins_;
        double err = tol*exact;
        double lower = exact - err;
        double upper = exact + err;
        bool res = true;
        for (std::size_t i = 0; i < nbins_; i++) {
            if (hist_[i] < lower || hist_[i] > upper) {
                res = false;
            }
        }
        return res;
    }
private:
    unsigned long nsamples_;
    std::size_t nbins_;
    linear_grid lg_;
    std::vector<double> hist_;
};

TEST(RandomUtils, SaveAndRestoreUniformRealDistributionFromStreams) {
    uniform_real_distribution urd(-2.3, 5.7);
    std::stringstream ss;
    ss << urd;
    uniform_real_distribution urd_copy;
    uniform_real_distribution urd_default;
    ss >> urd_copy;
    ASSERT_EQ(urd, urd_copy);
    ASSERT_NE(urd_copy, urd_default);
}

TEST(RandomUtils, SaveAndRestoreSplitmix64FromStreams) {
    splitmix64 sm(0x1238123abcd);
    std::stringstream ss;
    ss << sm;
    splitmix64 sm_copy;
    splitmix64 sm_default;
    ss >> sm_copy;
    ASSERT_EQ(sm, sm_copy);
    ASSERT_NE(sm_copy, sm_default);
}

TEST(RandomUtils, SaveAndRestoreXoshiro256FromStreams) {
    xoshiro256p xo(0x1238123abcd);
    std::stringstream ss;
    ss << xo;
    xoshiro256p xo_copy;
    xoshiro256p xo_default;
    ss >> xo_copy;
    ASSERT_EQ(xo, xo_copy);
    ASSERT_NE(xo_copy, xo_default);
}

TEST(RandomUtils, Uniform01withSplitmix64) {
    splitmix64 sm;
    uniform_real_distribution urd;
    histogram hist(100);
    int n = 10000000;
    for (int i = 0; i < n; i++) {
        hist.add(urd(sm));
    }
    double tolerance = 0.01;
    ASSERT_TRUE(hist.check_bins(tolerance));
}

TEST(RandomUtils, Uniform01withXoshiro256) {
    xoshiro256ss xo;
    uniform_real_distribution urd;
    histogram hist(100);
    int n = 10000000;
    for (int i = 0; i < n; i++) {
        hist.add(urd(xo));
    }
    double tolerance = 0.05;
    ASSERT_TRUE(hist.check_bins(tolerance));
}

TEST(RandomUtils, VariateGeneratorWithXoshiro256ss) {
    using rng_t = variate_generator<xoshiro256ss, uniform_real_distribution>;
    std::seed_seq seq({9, 123, 8231738, 81, 8123, 1});
    xoshiro256ss eng1(seq);
    xoshiro256ss eng2 = eng1;
    xoshiro256ss eng3 = eng1;
    eng2.long_jump();
    eng3.long_jump();
    eng3.long_jump();
    uniform_real_distribution urd(0.0, 1.0);
    rng_t vg1(eng1, urd);
    rng_t vg2(eng2, urd);
    rng_t vg3(eng3, urd);
    std::size_t n = 100;
    for (std::size_t i = 0; i < n ; i++) {
        auto res1 = vg1();
        auto res2 = vg2();
        auto res3 = vg3();
        ASSERT_NE(res1, res2);
        ASSERT_NE(res1, res3);
        ASSERT_NE(res2, res3);
    }
}
