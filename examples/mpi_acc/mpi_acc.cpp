#include <iostream>
#include <iomanip>
#include <simpleMC/utils/timer.h>
#include <simpleMC/utils/grids.h>
#include <simpleMC/utils/random.h>
#include <simpleMC/accs/accs.h>
#include <simpleMC/mpi/mpi.h>

int main(int argc, char** argv) {
    using namespace simpleMC::utils;
    using namespace simpleMC::mpi;
    using namespace simpleMC::accs;

    // Initialize MPI environment
    environment env(argc, argv);
    communicator world;
    int root = 0;

    // Start time measurement
    timer clock;
    clock.start();

    // Create RNG and seed it
    xoshiro256ss xo;
    for (int i = 0; i < world.rank(); i++) {
        xo.long_jump();
    }
    uniform_real_distribution urd(0.0, 5.0);
    variate_generator<xoshiro256ss, uniform_real_distribution> vg(xo, urd);

    // Histogram
    std::size_t numbins = 20;
    mean_acc<double> histogram(numbins);
    linear_grid lg(0.0, 5.0, numbins + 1);

    // Fill histogram
    std::size_t nsamples = 100000000;
    for (std::size_t i = 0; i < nsamples; i++) {
        histogram[lg.index(vg())] << 1;
    }

    // Stop time measurement
    clock.stop();
    auto runtime = clock.start_to_stop<duration::millisec_d>();

    // Gather results from all ranks
    mean_acc<double> res;
    reduce(histogram, res, world, /*all=*/ false, /*root=*/ root);
    auto max_runtime = runtime;
    reduce(runtime, max_runtime, MPI_MAX, world, /*all=*/ false, /*root=*/ root);

    // Reduce time measurement from all ranks and print the largest
    if (world.rank() == 0) {
        res.convert_to_result();
        std::cout << "Runtime: " << max_runtime << "\n";
        std::cout << res << "\n";
    }
}
