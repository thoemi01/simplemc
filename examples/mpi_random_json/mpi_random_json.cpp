#include <iostream>
#include <iomanip>
#include <simpleMC/utils/timer.h>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/json/jsonIO_random.h>
#include <simpleMC/mpi/mpi.h>
#include <simpleMC/mpi/json.h>

int main(int argc, char** argv) {
    using namespace simpleMC::utils;
    using namespace simpleMC::json;
    using namespace simpleMC::mpi;

    // Initialize MPI environment
    environment env(argc, argv);
    communicator world;

    // Start time measurement
    timer clock;
    clock.start();

    // Create RNG and seed it
    xoshiro256ss xo;
    for (int i = 0; i < world.rank(); i++) {
        xo.long_jump();
    }
    uniform_real_distribution urd(0.0, 1.0);
    variate_generator<xoshiro256ss, uniform_real_distribution> vg(xo, urd);

    // Use RNG to create random samples
    std::size_t num = 5;
    std::vector<double> random_samples;
    random_samples.reserve(num);
    for (std::size_t i = 0; i < num; i++) {
        random_samples.emplace_back(vg());
    }

    // Write RNG to Json file
    const std::string base = std::to_string(world.rank());
    std::string jsonfile = "rng" + base + ".json";
    jsonIO jio;
    jio["rng"] << vg;
    jio["samples"] << random_samples;
    jio.dump_to_file(jsonfile);

    // Write single Json file for all processes
    const std::string allfile = "rng.cbor";
    gather_write_jsonIO(jio, allfile, jsonIO_mode::cbor, world, 0, "");

    // Read single Json file and scatter it to all processes
    jsonIO jio_copy;
    read_scatter_jsonIO(jio_copy, allfile, jsonIO_mode::cbor, world, 0, "");

    // Write again to file
    jsonfile += ".copy";
    jio_copy.dump_to_file(jsonfile);

    // Stop time measurement
    clock.stop();
    auto runtime = clock.start_to_stop<duration::millisec_d>();

    // Reduce time measurement from all ranks and print the largest
    auto max_runtime = runtime;
    reduce(runtime, max_runtime, MPI_MAX, world, /*all=*/ false, /*root=*/ 0);
    if (world.rank() == 0) {
        std::cout << "Runtime: " << max_runtime << "\n";
    }
}
