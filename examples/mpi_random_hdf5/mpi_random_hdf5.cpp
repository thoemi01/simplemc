#include <simpleMC/utils/timer.h>
#include <simpleMC/hdf5/hdf5IO.h>
#include <simpleMC/hdf5/hdf5IO_random.h>
#include <simpleMC/mpi/mpi.h>

int main(int argc, char** argv) {
    using namespace simpleMC::utils;
    using namespace simpleMC::hdf5;
    using namespace simpleMC::mpi;

    // Initialize MPI environment
    environment env(argc, argv);
    communicator world;

    // Start time measurement
    timer clock;
    clock.start();

    // Create RNG and seed it
    xoshiro256ss xo;
    for (int i = 0; i < world.rank(); i++) {
        xo.long_jump();
    }
    uniform_real_distribution urd(0.0, 1.0);
    variate_generator<xoshiro256ss, uniform_real_distribution> vg(xo, urd);

    // Use RNG to create random samples
    std::size_t num = 100;
    std::vector<double> random_samples;
    random_samples.reserve(num);
    for (std::size_t i = 0; i < num; i++) {
        random_samples.emplace_back(urd(xo));
    }

    // Write RNG to HDF5 file
    const std::string base = std::to_string(world.rank());
    const std::string h5file = "rng" + base + ".h5";
    hdf5IO hio(h5file, HighFive::File::ReadWrite | HighFive::File::Create |
               HighFive::File::Truncate, HighFive::FileDriver());
    hio["rng"] << xo;
    hio["samples"] << random_samples;

    // Stop time measurement
    clock.stop();
    auto runtime = clock.start_to_stop<duration::millisec_d>();

    // Reduce time measurement from all ranks and print the largest
    auto max_runtime = runtime;
    reduce(runtime, max_runtime, MPI_MAX, world, /*all=*/ false, /*root=*/ 0);
    if (world.rank() == 0) {
        std::cout << "Runtime: " << max_runtime << "\n";
    }
}
