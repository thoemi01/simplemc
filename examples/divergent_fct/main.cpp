#include <iostream>
#include <fstream>
#include <simpleMC/json/jsonIO.h>
#include <simpleMC/mpi/mpi.h>
#include <simpleMC/mpi/only_on_rank.h>
#include "updates.h"
#include "measurements.h"

void my_checkpoint(const simpleMC::mc::mc_simulation<config>& mc) {
    using namespace simpleMC::mc;
    using namespace simpleMC::mpi;
    using namespace simpleMC::utils;
    static std::size_t ncalls = 0;
    fixedwidth_ostream fos(std::cout, 20);
    mpi_ostream<decltype(fos)> mos(fos, mc.rank());
    mos << fos.endl()
        << "Checkpoint" << (++ncalls) << fos.endl()
        << "Runtime" << mc.simulation_stats().runtime() << fos.endl()
        << "Steps done" << mc.simulation_stats().steps_done << fos.endl()
        << fos.endl();
    default_checkpoint_callback(mc);
}

int main(int argc, char** argv) {
    using namespace simpleMC::mpi;
    using namespace simpleMC::mc;
    using namespace simpleMC::json;

    // Initialize MPI environment
    environment env(argc, argv);
    communicator world;
    int rank = world.rank();
    only_on_rank only_rank0(rank);

    // Initialize MC simulation
    mc_simulation<config> mc(rank);
    mc.set_checkpoint_callback(my_checkpoint);
    mc.add_update(change_tau(), 1.0, "change_tau");
    mc.add_measurement(histogram(50, 5), "histogram");
    mc.add_measurement(fourier_legendre(20, 10), "legendre");

    // check cmd line arguments
    if (argc > 1) {
        if (std::string(argv[1]) == "def") {
            only_rank0([&mc](){
                jsonIO jio;
                write_input_json(jio, mc);
                jio.dump_to_file("default_input.json", jsonIO_mode::text4);
            });
            environment::finalize();
            std::exit(0);
        }
    }

    // read input file
    jsonIO jio;
    jio.load_from_file("input.json");
    read_input_json(jio, mc);

    // load checkpint or perform warm up
    mpi_ostream mos(std::cout, rank);
    if (mc.simulation_stats().load_checkpoint) {
        mos << "Load checkpoint...\n";
        default_load_checkpoint(mc);
    } else {
        mos << "Start warm up...\n";
        mc.warmup();
        mos << "Finished warm up...\n";
    }

    // simulation
    mos << "Start simulation...\n";
    mc.run();
    mos << "Finished simulation...\n";

    // gather results
    auto mc_res = mc.mpi_collect_results(world);

    // write results
    only_rank0([](const mc_simulation<config>& mc_res){
        std::cout << mc_res.update_stats() << std::endl;
        std::cout << mc_res.simulation_stats() << std::endl;
        const auto& hist_res = mc_res.get_measurement<histogram>("histogram");
        std::ofstream of_res("results.dat");
        hist_res.write_accs(of_res, mc_res.configuration());
        const auto& leg_res = mc_res.get_measurement<fourier_legendre>("legendre");
        std::ofstream of_leg("legendre.dat");
        leg_res.write_accs(of_leg, mc_res.configuration(), 50);
        std::ofstream of_leg_coeff("legendre_coefficients.dat");
        leg_res.write_coeffs(of_leg_coeff, mc_res.configuration());
    }, mc_res);
}
