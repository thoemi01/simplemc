﻿#pragma once

#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/utils/special_functions.h>
#include <simpleMC/utils/cloneable.h>
#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/accs/mean.h>
#include <simpleMC/accs/jackknife.h>
#include <simpleMC/mc/mc_simulation.h>
#include "configuration.h"

struct histogram : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_measurement<config>, histogram> {
public:
    using mean_acc = simpleMC::accs::mean_acc<double>;
    using lg_type = simpleMC::utils::linear_grid;

public:
    histogram(std::size_t bins, double max) :
        hist_(bins),
        est_(bins),
        lg_(0, max, bins+1)
    {}

    void initialize(const mc_type& mc) override {
        auto stop = mc.configuration().maxtau;
        lg_.set(lg_.start(), stop, lg_.numpoints());
    }

    void measure(const config& con) override {
        auto idx = lg_.index(con.tau);
        auto val = (lg_.value(idx) + lg_.value(idx+1))*0.5;
        hist_[idx] << 1.0;
        //est_[idx] << std::sqrt(con.tau/val);
        est_[idx] << std::sqrt(con.tau/val)*std::exp(-val + con.tau);
    }

    void write_accs(std::ostream& os, const config& con) const {
        auto hist_mean = hist_.mean();
        auto est_mean = est_.mean();
        double norm_factor = norm(con.maxtau);
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        fos << "#tau" << "exact" << "histogram" << "est"
            << "rel hist" << "rel est" << fos.endl();
        for (std::size_t i = 0; i < hist_.size(); i++) {
            double tau = 0.5*(lg_.value(i) + lg_.value(i+1));
            double ex = exact(tau);
            double hist = hist_mean[i]/lg_.binsize(i)*norm_factor;
            double est = est_mean[i]/lg_.binsize(i)*norm_factor;
            double rel_hist = std::abs(hist - ex)/ex;
            double rel_est = std::abs(est - ex)/ex;
            fos << tau << ex << hist << est
                << rel_hist << rel_est << fos.endl();
        }
    }

    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto hist_red = hist_;
        auto est_red = est_;
        simpleMC::accs::reduce(hist_red, hist_, comm, /*all=*/true);
        simpleMC::accs::reduce(est_red, est_, comm, /*all=*/true);
    }

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["hist"] = hist_;
        j["est"] = est_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("hist").get_to(hist_);
        j.at("est").get_to(est_);
        j.at("lg").get_to(lg_);
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t bins;
        j.at("bins").get_to(bins);
        hist_.reset(bins);
        est_.reset(bins);
        lg_.set(lg_.start(), lg_.stop(), bins + 1);
    }

    void write_input_json(nlohmann::json& j) const override {
        j["bins"] = hist_.size();
    }

private:
    mean_acc hist_;
    mean_acc est_;
    lg_type lg_;
};

struct fourier_legendre : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_measurement<config>, fourier_legendre> {
public:
    using mean_acc = simpleMC::accs::mean_acc<double>;
    using mean_type = typename mean_acc::mean_type;
    using lg_type = simpleMC::utils::linear_grid;
    using map_int = simpleMC::utils::map_interval;
    using legendre = simpleMC::utils::legendre;

public:
    fourier_legendre(unsigned int order, std::size_t bins) :
        coeff_(order*bins),
        lg_(0, 5, bins+1),
        maxorder_(order),
        maps_(bins)
    {}

    void initialize(const mc_type& mc) override {
        auto maxtau = mc.configuration().maxtau;
        lg_.set(lg_.start(), maxtau, lg_.numpoints());
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void measure(const config& con) override {
        auto idx = lg_.index(con.tau);
        auto sva = coeff_.make_sva();
        legendre leg(maps_[idx].inv_map(con.tau));
        if (idx == 0) {
            for (std::size_t i = idx*maxorder_; i < (idx+1)*maxorder_; i++) {
                sva[i] << leg.next()*std::sqrt(con.tau);
            }
        } else {
            for (std::size_t i = idx*maxorder_; i < (idx+1)*maxorder_; i++) {
                sva[i] << leg.next()*leg.weight();
            }
        }
    }

    void write_accs(std::ostream& os, const config& con, std::size_t npoints) const {
        auto coeff_mean = coeff_.mean();
        double norm_factor = norm(con.maxtau);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm_factor/
                        (legendre::norm_factor(j)*alpha);
            }
        }
        lg_type lg(0.001, con.maxtau, npoints);
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        std::vector<unsigned int> ncoeffs(5, static_cast<unsigned int>(0.2*maxorder_));
        for (int i = 1; i < 5; i++) {
            ncoeffs[i] = ncoeffs[i-1] + static_cast<unsigned int>(0.2*maxorder_);
        }
        fos << "#tau" << "exact" << ncoeffs[0] << ncoeffs[1]
            << ncoeffs[2] << ncoeffs[3] << ncoeffs[4] << fos.endl();
        for (std::size_t i = 0; i < npoints; i++) {
            double tau = lg.value(i);
            double ex = exact(tau);
            fos << tau << ex
                << green(coeff_mean, tau, ncoeffs[0])
                << green(coeff_mean, tau, ncoeffs[1])
                << green(coeff_mean, tau, ncoeffs[2])
                << green(coeff_mean, tau, ncoeffs[3])
                << green(coeff_mean, tau, ncoeffs[4]) << fos.endl();
        }
    }

    void write_coeffs(std::ostream& os, const config& con) const {
        auto coeff_mean = coeff_.mean();
        double norm_factor = norm(con.maxtau);
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        fos << "#bin" << "order" << "G_i" << fos.endl();
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm_factor/
                        (legendre::norm_factor(j)*alpha);
                fos << i << j << coeff_mean[i*maxorder_ + j] << fos.endl();
            }
        }
    }

    double green(const mean_type& coeff, double tau, unsigned int order) const {
        double res = 0;
        auto idx = std::min(lg_.index(tau), maps_.size() -1);
        legendre leg(maps_[idx].inv_map(tau));
        if (idx == 0) {
            for (unsigned int i = idx*maxorder_; i < idx*maxorder_ + order; i++) {
                res += coeff[i]*leg.next()/std::sqrt(tau);
            }
        } else {
            for (unsigned int i = idx*maxorder_; i < idx*maxorder_ + order; i++) {
                res += coeff[i]*leg.next();
            }
        }
        return res;
    }

    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto coeff_red = coeff_;
        simpleMC::accs::reduce(coeff_red, coeff_, comm, /*all=*/true);
    }

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["coeff"] = coeff_;
        j["maxorder"] = maxorder_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("coeff").get_to(coeff_);
        j.at("maxorder").get_to(maxorder_);
        j.at("lg").get_to(lg_);
        maps_.resize(lg_.numpoints() - 1);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t numbins;
        j.at("bins").get_to(numbins);
        j.at("maxorder").get_to(maxorder_);
        coeff_.reset(maxorder_*numbins);
        lg_.set(lg_.start(), lg_.stop(), numbins + 1);
        maps_.resize(numbins);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void write_input_json(nlohmann::json& j) const override {
        j["bins"] = lg_.numpoints() - 1;
        j["maxorder"] = maxorder_;
    }

private:
    mean_acc coeff_;
    lg_type lg_;
    unsigned int maxorder_;
    std::vector<map_int> maps_;
};
