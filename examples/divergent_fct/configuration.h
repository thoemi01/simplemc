#pragma once

#include <cmath>
#include <nlohmann/json.hpp>
#include <simpleMC/utils/math.h>
#include <simpleMC/mc/mc_configuration.h>

struct config : public simpleMC::mc::mc_configuration {
public:
    void to_json(nlohmann::json& j) const override {
        j["tau"] = tau;
    }

    void from_json(const nlohmann::json& j) override {
        j.at("tau").get_to(tau);
    }

public:
    double tau = 1;
    double maxtau = 4;
};

/* f(x) = 1/sqrt(x) */
inline double exact(double tau) {
    //return 1.0/std::sqrt(tau);
    return std::exp(-tau)/std::sqrt(tau);
}

/* int f(x) = 2*sqrt(x) */
inline double norm(double /*maxtau*/) {
    //return 2.0*(std::sqrt(maxtau));
    using namespace simpleMC::utils::math_constants;
    return pi_sqrt*std::erf(2.0);
}
