#pragma once

#include <cmath>
#include <simpleMC/utils/cloneable.h>
#include <simpleMC/mc/mc_simulation.h>
#include "configuration.h"

struct change_tau : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_update<config>, change_tau> {
public:
    double attempt(const config& con, gen_type& uni01) override {
//        ntau = uni01();
//        ntau *= ntau*con.maxtau;
//        return 1.0;
        ntau = uni01()*con.maxtau;
        return std::sqrt(con.tau/ntau)*std::exp(-ntau + con.tau);
    }

    void accept(config& con, gen_type& /*uni01*/) override {
        con.tau = ntau;
    }

public:
    double ntau = 0.0;
};
