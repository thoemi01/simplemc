#pragma once

#include <cmath>
#include <nlohmann/json.hpp>
#include <simpleMC/mc/mc_configuration.h>

struct diagram : public simpleMC::mc::mc_configuration {
public:
    void to_json(nlohmann::json& j) const override {
        j["tau"] = tau;
        j["tau1"] = tau1;
        j["tau2"] = tau2;
        j["order"] = order;
        j["cur_beta"] = cur_beta;
        j["alpha"] = alpha;
        j["betas"] = betas;
        j["vertex"] = vertex;
        j["maxtau"] = maxtau;
    }

    void from_json(const nlohmann::json& j) override {
        j.at("tau").get_to(tau);
        j.at("tau1").get_to(tau1);
        j.at("tau2").get_to(tau2);
        j.at("order").get_to(order);
        j.at("cur_beta").get_to(cur_beta);
        j.at("alpha").get_to(alpha);
        j.at("betas").get_to(betas);
        j.at("vertex").get_to(vertex);
        j.at("maxtau").get_to(maxtau);
    }

    void read_input_json(const nlohmann::json& j) override {
        j.at("alpha").get_to(alpha);
        j.at("betas").get_to(betas);
        j.at("vertex").get_to(vertex);
        j.at("maxtau").get_to(maxtau);
        if (tau > maxtau) tau = maxtau*0.5;
    }

    void write_input_json(nlohmann::json& j) const override {
        j["alpha"] = alpha;
        j["betas"] = betas;
        j["vertex"] = vertex;
        j["maxtau"] = maxtau;
    }

public:
    double tau = 1;
    double tau1 = 0;
    double tau2 = 0;
    std::size_t order = 0;
    std::size_t cur_beta = 0;
    double alpha = 1;
    std::vector<double> betas = std::vector<double>{0.25, 0.75};
    double vertex = 0.5;
    double maxtau = 5;
};

inline double exact0(double tau, double alpha) {
    return std::exp(-alpha*tau);
}

inline double exact2(double tau, double alpha, double vertex,
                     const std::vector<double>& betas) {
    double res = 0.0;
    for (std::size_t i = 0; i < betas.size(); i++) {
        double ba = betas[i] - alpha;
        res += vertex*vertex*std::exp(-alpha*tau)/ba*
                (tau + (std::exp(-ba*tau) - 1)/ba);
    }
    return res;
}

inline double exact(double tau, double alpha, double vertex,
                    const std::vector<double>& betas) {
    return exact0(tau, alpha) + exact2(tau, alpha, vertex, betas);
}

inline double norm0(double maxtau, double alpha) {
    return (1.0 - std::exp(-alpha*maxtau))/alpha;
}

inline double norm2(double maxtau, double alpha, double vertex,
                    const std::vector<double>& betas) {
    double res = 0.0;
    for (std::size_t i = 0; i < betas.size(); i++) {
        double ba = betas[i] - alpha;
        res += ((1 - (alpha*maxtau + 1)*std::exp(-alpha*maxtau))/(alpha*alpha) +
                ((1 - std::exp(-betas[i]*maxtau))/betas[i] -
                 (1 - std::exp(-alpha*maxtau))/alpha)/ba)/ba;
    }
    res *= vertex*vertex;
    return res;
}
