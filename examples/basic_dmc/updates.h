#pragma once

#include <cmath>
#include <simpleMC/utils/cloneable.h>
#include <simpleMC/mc/mc_simulation.h>
#include "diagram.h"

struct change_tau : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_update<diagram>, change_tau> {
public:
    double attempt(const diagram& diag, gen_type& uni01) override {
        double right = (diag.order == 0 ? 0.0 : diag.tau2);
        ntau = right + uni01()*(diag.maxtau - right);
        return std::exp(-diag.alpha*(ntau - diag.tau));
    }

    void accept(diagram& diag, gen_type& /*uni01*/) override {
        diag.tau = ntau;
    }

public:
    double ntau = 0.0;
};

struct add_beta : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_update<diagram>, add_beta> {
public:
    void initialize(const mc_type& mc) override {
        auto rem_info = mc.update_stats().get_info("remove_beta");
        auto add_info = mc.update_stats().get_info("add_beta");
        double v2 = mc.configuration().vertex*mc.configuration().vertex;
        fix_fac = rem_info.weight*v2/add_info.weight;
    }

    double attempt(const diagram& diag, gen_type& uni01) override {
        if (diag.order == 2) return 0.0;
        ntau1 = uni01()*diag.tau;
        ntau2 = ntau1 + uni01()*(diag.tau - ntau1);
        beta = uni01()*diag.betas.size();
        double prop = diag.tau*(diag.tau - ntau1)*diag.betas.size();
        return fix_fac*std::exp(-(diag.betas[beta] - diag.alpha)*(ntau2 - ntau1))*
                prop;
    }

    void accept(diagram& diag, gen_type& /*uni01*/) override {
        diag.tau1 = ntau1;
        diag.tau2 = ntau2;
        diag.cur_beta = beta;
        diag.order += 2;
    }

public:
    double ntau1 = 0;
    double ntau2 = 0;
    std::size_t beta = 0;
    double fix_fac = 1.0;
};

struct remove_beta : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_update<diagram>, remove_beta> {
public:
    void initialize(const mc_type& mc) override {
        auto rem_info = mc.update_stats().get_info("remove_beta");
        auto add_info = mc.update_stats().get_info("add_beta");
        double v2 = mc.configuration().vertex*mc.configuration().vertex;
        fix_fac = add_info.weight/v2/rem_info.weight;
    }

    double attempt(const diagram& diag, gen_type& /*uni01*/) override {
        if (diag.order == 0) return 0.0;
        double len_prop = diag.tau2 - diag.tau1;
        double prop = diag.tau*(diag.tau - diag.tau1)*diag.betas.size();
        return fix_fac*std::exp(-(diag.alpha - diag.betas[diag.cur_beta])*len_prop)/
                prop;
    }

    void accept(diagram& diag, gen_type& /*uni01*/) override {
        diag.order = 0;
    }

public:
    double fix_fac = 1.0;
};
