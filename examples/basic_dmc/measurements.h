#pragma once

#include <simpleMC/utils/cloneable.h>
#include <simpleMC/utils/fixedwidth_ostream.h>
#include <simpleMC/utils/special_functions.h>
#include <simpleMC/json/jsonIO_grids.h>
#include <simpleMC/accs/mean.h>
#include <simpleMC/accs/batch.h>
#include <simpleMC/accs/jackknife.h>
#include <simpleMC/mc/mc_simulation.h>
#include "diagram.h"

struct histogram : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_measurement<diagram>, histogram> {
public:
    using mean_acc = simpleMC::accs::mean_acc<double>;
    using batch_acc = simpleMC::accs::batch_acc<double>;
    using lg_type = simpleMC::utils::linear_grid;

public:
    histogram(std::size_t bins, double max) :
        hist_(bins),
        est_(bins, batch_acc::default_num_batches, batch_acc::default_block_size),
        zeroth_(1, batch_acc::default_num_batches, batch_acc::default_block_size),
        lg_(0, max, bins+1)
    {}

    void initialize(const mc_type& mc) override {
        auto stop = mc.configuration().maxtau;
        lg_.set(lg_.start(), stop, lg_.numpoints());
    }

    void measure(const diagram& diag) override {
        auto idx = lg_.index(diag.tau);
        hist_[idx] << 1.0;
        auto val = (lg_.value(idx) + lg_.value(idx+1))*0.5;
        if (diag.order == 0) {
            est_[idx] << std::exp(-diag.alpha*(val - diag.tau));
            zeroth_ << 1;
        } else {
            double rat = val/diag.tau;
            double tmp = (rat - 1.0)*(diag.alpha*(diag.tau1 + diag.tau - diag.tau2) +
                diag.betas[diag.cur_beta]*(diag.tau2 - diag.tau1));
            est_[idx] << rat*rat*std::exp(-tmp);
            zeroth_ << 0;
        }
    }

    void write_accs(std::ostream& os, const diagram& diag) const {
        auto hist_mean = hist_.mean();
        auto est_mean = est_.mean();
        auto zeroth_mean = zeroth_.mean();
        double n0 = norm0(diag.maxtau, diag.alpha);
        double n2= norm2(diag.maxtau, diag.alpha, diag.vertex, diag.betas);
        double norm = n0 + n2;
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        os << "#norm0 = " << n0 << "\n"
           << "#norm2 = " << n2 << "\n"
           << "#norm = " << n0 + n2 << "\n"
           << "#norm with 0th-diagram = " << n0/zeroth_mean[0] << "\n"
           << "#steps = " << hist_.count() << "\n";
        fos << "#tau" << "histogram" << "est" << "exact"
            << "rel hist" << "rel est" << fos.endl();
        for (std::size_t i = 0; i < hist_.size(); i++) {
            double tau = 0.5*(lg_.value(i) + lg_.value(i+1));
            double ex = exact(tau, diag.alpha, diag.vertex, diag.betas);
            double hist = hist_mean[i]/lg_.binsize(i)*norm;
            double est = est_mean[i]/lg_.binsize(i)*norm;
            double rel_hist = std::abs(hist - ex)/ex;
            double rel_est = std::abs(est - ex)/ex;
            fos << tau << hist << est << ex
                << rel_hist << rel_est << fos.endl();
        }
    }

    void jackknife(const diagram& diag) const {
        using namespace simpleMC::accs;
        double n0 = norm0(diag.maxtau, diag.alpha);
        jackknife_data<double> est_jd(est_);
        jackknife_data<double> zeroth_jd(zeroth_);
        auto fn = [this, n0](double es, double ze) {
            return es*n0/ze/lg_.binsize(0);
        };
        jackknife_propagator<double> est_jp(est_jd.size(), est_jd.num_batches());
        auto est_res = est_jp.propagate(fn, est_jd, zeroth_jd);
        jackknife_to_file(est_res, "exact_estimator_jackknife.dat");
    }

    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto hist_red = hist_;
        auto est_red = est_;
        auto zeroth_red = zeroth_;
        simpleMC::accs::reduce(hist_red, hist_, comm, /*all=*/true);
        simpleMC::accs::reduce(est_red, est_, comm, /*all=*/true);
        simpleMC::accs::reduce(zeroth_red, zeroth_, comm, /*all=*/true);
    }

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["hist"] = hist_;
        j["est"] = est_;
        j["zeroth"] = zeroth_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("hist").get_to(hist_);
        j.at("est").get_to(est_);
        j.at("zeroth").get_to(zeroth_);
        j.at("lg").get_to(lg_);
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t bins, num_batches, block_size;
        j.at("num_bins").get_to(bins);
        j.at("num_batches").get_to(num_batches);
        j.at("block_size").get_to(block_size);
        hist_.reset(bins);
        est_.reset(bins, num_batches, block_size);
        zeroth_.reset(zeroth_.size(), num_batches, block_size);
        lg_.set(lg_.start(), lg_.stop(), bins + 1);
    }

    void write_input_json(nlohmann::json& j) const override {
        j["num_bins"] = hist_.size();
        j["num_batches"] = est_.num_batches();
        j["block_size"] = est_.block_size();
    }

private:
    mean_acc hist_;
    batch_acc est_;
    batch_acc zeroth_;
    lg_type lg_;
};

struct fourier_legendre : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_measurement<diagram>, fourier_legendre> {
public:
    using mean_acc = simpleMC::accs::mean_acc<double>;
    using mean_type = typename mean_acc::mean_type;
    using lg_type = simpleMC::utils::linear_grid;
    using map_int = simpleMC::utils::map_interval;
    using legendre = simpleMC::utils::legendre;

public:
    fourier_legendre(unsigned int order, std::size_t bins) :
        coeff_(order*bins),
        lg_(0, 5, bins+1),
        maxorder_(order),
        maps_(bins)
    {}

    void initialize(const mc_type& mc) override {
        auto maxtau = mc.configuration().maxtau;
        lg_.set(lg_.start(), maxtau, lg_.numpoints());
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void measure(const diagram& diag) override {
        auto idx = lg_.index(diag.tau);
        auto sva = coeff_.make_sva();
        legendre leg(maps_[idx].inv_map(diag.tau));
        for (std::size_t i = idx*maxorder_; i < (idx+1)*maxorder_; i++) {
            sva[i] << leg.next();
        }
    }

    void write_accs(std::ostream& os, const diagram& diag, std::size_t npoints) const {
        auto coeff_mean = coeff_.mean();
        double n0 = norm0(diag.maxtau, diag.alpha);
        double n2= norm2(diag.maxtau, diag.alpha, diag.vertex, diag.betas);
        double norm = n0 + n2;
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm/(legendre::norm_factor(j)*alpha);
            }
        }
        lg_type lg(0, diag.maxtau, npoints);
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        std::vector<unsigned int> ncoeffs(5, static_cast<unsigned int>(0.2*maxorder_));
        for (int i = 1; i < 5; i++) {
            ncoeffs[i] = ncoeffs[i-1] + static_cast<unsigned int>(0.2*maxorder_);
        }
        os << "#norm0 = " << n0 << "\n"
           << "#norm2 = " << n2 << "\n"
           << "#norm = " << n0 + n2 << "\n"
           << "#steps = " << coeff_.count() << "\n";
        fos << "#tau" << "exact" << ncoeffs[0] << ncoeffs[1]
            << ncoeffs[2] << ncoeffs[3] << ncoeffs[4] << fos.endl();
        for (std::size_t i = 0; i < npoints; i++) {
            double tau = lg.value(i);
            double ex = exact(tau, diag.alpha, diag.vertex, diag.betas);
            fos << tau << ex
                << green(coeff_mean, tau, ncoeffs[0])
                << green(coeff_mean, tau, ncoeffs[1])
                << green(coeff_mean, tau, ncoeffs[2])
                << green(coeff_mean, tau, ncoeffs[3])
                << green(coeff_mean, tau, ncoeffs[4]) << fos.endl();
        }
    }

    void write_coeffs(std::ostream& os, const diagram& diag) const {
        auto coeff_mean = coeff_.mean();
        double n0 = norm0(diag.maxtau, diag.alpha);
        double n2= norm2(diag.maxtau, diag.alpha, diag.vertex, diag.betas);
        double norm = n0 + n2;
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        fos << "#bin" << "order" << "G_i" << fos.endl();
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm/(legendre::norm_factor(j)*alpha);
                fos << i << j << coeff_mean[i*maxorder_ + j] << fos.endl();
            }
        }
    }

    double green(const mean_type& coeff, double tau, unsigned int order) const {
        double res = 0;
        auto idx = std::min(lg_.index(tau), maps_.size() -1);
        legendre leg(maps_[idx].inv_map(tau));
        for (unsigned int i = idx*maxorder_; i < idx*maxorder_ + order; i++) {
            res += coeff[i]*leg.next();
        }
        return res;
    }

    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto coeff_red = coeff_;
        simpleMC::accs::reduce(coeff_red, coeff_, comm, /*all=*/true);
    }

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["coeff"] = coeff_;
        j["maxorder"] = maxorder_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("coeff").get_to(coeff_);
        j.at("maxorder").get_to(maxorder_);
        j.at("lg").get_to(lg_);
        maps_.resize(lg_.numpoints() - 1);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t numbins;
        j.at("num_bins").get_to(numbins);
        j.at("maxorder").get_to(maxorder_);
        coeff_.reset(maxorder_*numbins);
        lg_.set(lg_.start(), lg_.stop(), numbins + 1);
        maps_.resize(numbins);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void write_input_json(nlohmann::json& j) const override {
        j["num_bins"] = lg_.numpoints() - 1;
        j["maxorder"] = maxorder_;
    }

private:
    mean_acc coeff_;
    lg_type lg_;
    unsigned int maxorder_;
    std::vector<map_int> maps_;
};

struct fourier_chebyshev : public simpleMC::utils::cloneable<
        simpleMC::mc::mc_measurement<diagram>, fourier_chebyshev> {
public:
    using mean_acc = simpleMC::accs::mean_acc<double>;
    using mean_type = typename mean_acc::mean_type;
    using lg_type = simpleMC::utils::linear_grid;
    using map_int = simpleMC::utils::map_interval;
    using chebyshev = simpleMC::utils::chebyshev;

public:
    fourier_chebyshev(unsigned int order, std::size_t bins) :
        coeff_(order*bins),
        lg_(0, 5, bins+1),
        maxorder_(order),
        maps_(bins)
    {}

    void initialize(const mc_type& mc) override {
        auto maxtau = mc.configuration().maxtau;
        lg_.set(lg_.start(), maxtau, lg_.numpoints());
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void measure(const diagram& diag) override {
        auto idx = lg_.index(diag.tau);
        auto tau_map = maps_[idx].inv_map(diag.tau);
        chebyshev cheb(tau_map, 0, simpleMC::utils::chebyshev_kind::first);
        if (!std::isinf(cheb.weight())) {
            auto sva = coeff_.make_sva();
            for (std::size_t i = idx*maxorder_; i < (idx+1)*maxorder_; i++) {
                sva[i] << cheb.next()*cheb.weight();
            }
        }
    }

    void write_accs(std::ostream& os, const diagram& diag, std::size_t npoints) const {
        auto coeff_mean = coeff_.mean();
        double n0 = norm0(diag.maxtau, diag.alpha);
        double n2= norm2(diag.maxtau, diag.alpha, diag.vertex, diag.betas);
        double norm = n0 + n2;
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm/(chebyshev::norm_factor(j)*alpha);
            }
        }
        lg_type lg(0, diag.maxtau, npoints);
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        std::vector<unsigned int> ncoeffs(5, static_cast<unsigned int>(0.2*maxorder_));
        for (int i = 1; i < 5; i++) {
            ncoeffs[i] = ncoeffs[i-1] + static_cast<unsigned int>(0.2*maxorder_);
        }
        os << "#norm0 = " << n0 << "\n"
           << "#norm2 = " << n2 << "\n"
           << "#norm = " << n0 + n2 << "\n"
           << "#steps = " << coeff_.count() << "\n";
        fos << "#tau" << "exact" << ncoeffs[0] << ncoeffs[1]
            << ncoeffs[2] << ncoeffs[3] << ncoeffs[4] << fos.endl();
        for (std::size_t i = 0; i < npoints; i++) {
            double tau = lg.value(i);
            double ex = exact(tau, diag.alpha, diag.vertex, diag.betas);
            fos << tau << ex
                << green(coeff_mean, tau, ncoeffs[0])
                << green(coeff_mean, tau, ncoeffs[1])
                << green(coeff_mean, tau, ncoeffs[2])
                << green(coeff_mean, tau, ncoeffs[3])
                << green(coeff_mean, tau, ncoeffs[4]) << fos.endl();
        }
    }

    void write_coeffs(std::ostream& os, const diagram& diag) const {
        auto coeff_mean = coeff_.mean();
        double n0 = norm0(diag.maxtau, diag.alpha);
        double n2= norm2(diag.maxtau, diag.alpha, diag.vertex, diag.betas);
        double norm = n0 + n2;
        simpleMC::utils::fixedwidth_ostream fos(os, 20);
        fos << "#bin" << "order" << "G_i" << fos.endl();
        for (std::size_t i = 0; i < maps_.size(); i++) {
            double alpha = maps_[i].alpha();
            for (unsigned int j = 0; j < maxorder_; j++) {
                coeff_mean[i*maxorder_ + j] *= norm/(chebyshev::norm_factor(j)*alpha);
                fos << i << j << coeff_mean[i*maxorder_ + j] << fos.endl();
            }
        }
    }

    double green(const mean_type& coeff, double tau, unsigned int order) const {
        double res = 0;
        auto idx = std::min(lg_.index(tau), maps_.size() -1);
        chebyshev cheb(maps_[idx].inv_map(tau), 0,
                       simpleMC::utils::chebyshev_kind::first);
        for (unsigned int i = idx*maxorder_; i < idx*maxorder_ + order; i++) {
            res += coeff[i]*cheb.next();
        }
        return res;
    }

    void mpi_collect(simpleMC::mpi::communicator comm) override {
        auto coeff_red = coeff_;
        simpleMC::accs::reduce(coeff_red, coeff_, comm, /*all=*/true);
    }

    void to_json(nlohmann::json& j) const override {
        mc_measurement::to_json(j);
        j["coeff"] = coeff_;
        j["maxorder"] = maxorder_;
        j["lg"] = lg_;
    }

    void from_json(const nlohmann::json& j) override {
        mc_measurement::from_json(j);
        j.at("coeff").get_to(coeff_);
        j.at("maxorder").get_to(maxorder_);
        j.at("lg").get_to(lg_);
        maps_.resize(lg_.numpoints() - 1);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void read_input_json(const nlohmann::json& j) override {
        std::size_t numbins;
        j.at("num_bins").get_to(numbins);
        j.at("maxorder").get_to(maxorder_);
        coeff_.reset(maxorder_*numbins);
        lg_.set(lg_.start(), lg_.stop(), numbins + 1);
        maps_.resize(numbins);
        for (std::size_t i = 0; i < maps_.size(); i++) {
            maps_[i].set(-1.0, 1.0, lg_.value(i), lg_.value(i+1));
        }
    }

    void write_input_json(nlohmann::json& j) const override {
        j["num_bins"] = lg_.numpoints() - 1;
        j["maxorder"] = maxorder_;
    }

private:
    mean_acc coeff_;
    lg_type lg_;
    unsigned int maxorder_;
    std::vector<map_int> maps_;
};
